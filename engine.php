<?php

require_once 'init.php';

class TApplication extends \Adianti\Core\AdiantiCoreApplication
{

    static public function run($debug = FALSE)
    {
        new TSession;
        if ($_REQUEST) {
            $class = isset($_REQUEST['class']) ? $_REQUEST['class'] : '';

            if (TSession::getValue('logged')) { // logged
                parent::run($debug);

                /*
                 $programs = TSession::getValue('paginas'); // programs with permission
                // $programs = array_merge($programs, array('Adianti\Base\TStandardSeek' => TRUE, 'LoginForm' => TRUE, 'AdiantiMultiSearchService' => TRUE, 'AdiantiUploaderService' => TRUE, 'EmptyPage' => TRUE)); // default programs

                if (isset($programs[$class])) {
                    parent::run($debug);
                } else {
                    new TMessage('error', _t('Permission denied'));
                }
                */
            } else if (($class == 'LoginForm') or ($class == 'SenhaForm')) {
                parent::run($debug);
            } else {
                new TMessage('error', _t('Permission denied'), new TAction(array('LoginForm', 'onLogout')));
            }
        }
    }

}

TApplication::run(TRUE);
