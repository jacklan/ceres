<?php

//ini_set('display_errors', 1);
//ini_set('display_startup_erros', 1);
//error_reporting(E_ALL);

Namespace Adianti\Widget\Menu;

use Adianti\Database\TCriteria;
use Adianti\Database\TFilter;
use Adianti\Database\TRepository;
use Adianti\Database\TTransaction;
use Adianti\Widget\Base\TElement;
use App\Model\Acesso_Servidor\Acesso_ServidorRecord;
use Lib\Funcoes\Util;
use SimpleXMLElement;

//use App\Model\vw_usuario;
//use App\Model\vw_usuarioperfil;
//use App\Model\Acesso_ServidorRecord;

/**
 * Menu Widget
 *
 * @version    2.0
 * @package    widget
 * @subpackage menu
 * @author     Pablo Dall'Oglio
 * @copyright  Copyright (c) 2006-2014 Adianti Solutions Ltd. (http://www.adianti.com.br)
 * @license    http://www.adianti.com.br/framework-license
 */
class TMenu extends TElement
{

    private $items;
    private $menu_class;
    private $item_class;
    private $menu_level;

    /**
     * Class Constructor
     * @param $xml SimpleXMLElement parsed from XML Menu
     */
    public function __construct($xml, $permission_callback = NULL, $menu_level = 1, $menu_class = 'dropdown-menu', $item_class = '')
    {
        parent::__construct('ul');
        $this->items = array();

        $this->{'class'} = $menu_class . " level-{$menu_level}";
        $this->menu_class = $menu_class;
        $this->menu_level = $menu_level;
        $this->item_class = $item_class;

        if ($xml instanceof SimpleXMLElement) {
            $this->parse($xml, $permission_callback);
        }
    }

    /**
     * Add a MenuItem
     * @param $menuitem A TMenuItem Object
     */
    public function addMenuItem(TMenuItem $menuitem)
    {
        $this->items[] = $menuitem;
    }

    /**
     * Return the menu items
     */
    public function getMenuItems()
    {
        return $this->items;
    }

    /**
     * Parse a XMLElement reading menu entries
     * @param $xml A SimpleXMLElement Object
     * @param $permission_callback check permission callback
     */
    public function parse($xml, $permission_callback = NULL)
    {
        $i = 0;
        foreach ($xml as $xmlElement) {
            $atts = $xmlElement->attributes();
            $label = (string)$atts['label'];
            $action = (string)$xmlElement->action;
            $icon = (string)$xmlElement->icon;
            $menu = NULL;
            $menuItem = new TMenuItem($label, $action, $icon);

            if ($xmlElement->menu) {
                $menu = new TMenu($xmlElement->menu->menuitem, $permission_callback, $this->menu_level + 1, $this->menu_class, $this->item_class);
                $menuItem->setMenu($menu);
            }

            // just child nodes have actions
            if ($action) {
                if (!empty($action) AND $permission_callback) {
                    // check permission
                    $parts = explode('#', $action);
                    $className = $parts[0];
                    if (call_user_func($permission_callback, $className)) {
                        $this->addMenuItem($menuItem);
                    }
                } else {
                    // menus without permission check
                    $this->addMenuItem($menuItem);
                }
            } // parent nodes are shown just when they have valid children (with permission)
            else if (isset($menu) AND count($menu->getMenuItems()) > 0) {
                $this->addMenuItem($menuItem);
            }

            $i++;
        }
    }

    static public function validaClasse($classe)
    {
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');
        // instancia um repositorio da Classe
        $repository = new TRepository('Vw_usuariopaginagrupo');

        // cria um criterio de selecao
        $criteria = new TCriteria;
        //filtra pelo campo arquivo
        $criteria->add(new TFilter('arquivo', '=', $classe));
        // carrega os objetos de acordo com o criterio
        $usuarios = $repository->load($criteria);
        if ($usuarios) {
            // percorre os objetos retornados
            return TRUE;
        } else {
            return FALSE;
        }
    }

    static public function usuarioAtivo($usuario_id)
    {
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');
        // instancia um repositorio da Classe
        $acesso = new Acesso_servidorRecord($usuario_id);
        $acesso->dataacesso = date('d/m/Y');
        $acesso->store();
        // finaliza a transacao
        TTransaction::close();

        return TRUE;
    }

    static public function montaMenuTop($verificaModulo)
    {
        $menutop = '';
        $menutop .= " <ul class='right-icons' id='step3'>";
        if ($verificaModulo) {
            $menutop .= " <li>
                <a href='index.php?modulo=PONTO&class=MarcacaoList' class='lock' title='Bater o Ponto'> <i class='fa fa-clock-o'></i>  </a>
            </li>";
        }
        $menutop .= "  <li>
            <a href='#' class='lock'> <i class='fa fa-info'></i>  </a>
            <ul class='dropdown'>
                <li ><a href='index.php?class=Sobre' >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Sobre</a></li>
            </ul>
        </li>

        <li>
            <a href='#' class='lock'> <i class='fa fa-book'></i>   </a>
            <ul class='dropdown'>
                <!--  <li ><a href='http://servicos.emater.rn.gov.br/wikiceres/doku.php' target='_blank' >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Wiki</a></li> -->
                <li ><a href='index.php?class=VideoList'  >&nbsp;&nbsp;Videoaulas&nbsp;&nbsp;</a></li>
            </ul>
        </li>";
        if ($verificaModulo) {
            $menutop .= " <li>
                <a href='#' class='lock'> <i class='fa fa-comments'></i> <div class='notify' style='visibility: hidden'></div>  </a>
                <ul class='dropdown' id='listAviso' style='left: -175%'>

                </ul>";
        }
        $menutop .= "<li>
            <a href='index.php?class=MeuPerfilList' class='lock'> <i class='fa fa-user'></i>   </a>
            <ul class='dropdown'>
                <li ><a href='index.php?class=MeuPerfilList' >&nbsp;&nbsp;&nbsp;&nbsp;Meu perfil</a></li>
            </ul>
        </li>
        <li>
            <a href='index.php?class=LoginForm&method=onLogout&static=1'class='lock'> <i class='fa fa-power-off'></i>  </a>
            <ul class='dropdown'>
                <li ><a href='index.php?class=LoginForm&method=onLogout&static=1' >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Sair</a></li>
            </ul>
        </li>

    </ul>";
        return $menutop;
    }

    static public function montaModulo($usuario_id)
    {

        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');

        // instancia um repositorio da Classe
        $repository = new TRepository('vw_usuarioperfil');

        // cria um criterio de selecao
        $criteria = new TCriteria;
        //filtra pelo campo usuario_id
        $criteria->add(new TFilter('usuario_id', '=', $usuario_id));
        $criteria->setProperty('order', 'modulo');

        // carrega os objetos de acordo com o criterio
        $usuarios = $repository->load($criteria);
        // var_dump($criteria->dump());
        // exit();

        $menu = "";
        $menu .= "<option>SELECIONE O MODULO</option>";

        $servidor_id = 0;

        if ($usuarios) {
            $_SESSION['MODULO_PONTO'] = false;
            // percorre os objetos retornados
            foreach ($usuarios as $usuario) {
                // adiciona os dados do perfil do usuario no menu
                $menu .= "<option ";
                if ($_SESSION['modulo'] == $usuario->modulo)
                    $menu .= ' selected ';
                $menu .= "value='?modulo=" . $usuario->modulo . "' data-image=\"app/images/modulos/icon_" . $usuario->modulo . ".png\" data-title=" . $usuario->modulo . ">" . $usuario->modulo . "</option>";

                $servidor_id = $usuario->servidor_id;

                if ($usuario->modulo == "PONTO") {
                    $_SESSION['MODULO_PONTO'] = true;
                }
            }

        }

        // finaliza a transacao
        TTransaction::close();

        /*
          //usuarioAtivo($usuario_id);
          // inicia transacao com o banco 'pg_ceres'
          TTransaction::open('pg_ceres');
          // instancia um repositorio da Classe
          $acesso = new Acesso_ServidorRecord($usuario_id);
          $acesso->id = $usuario_id;
          $acesso->servidor_id = $servidor_id;
          $acesso->dataacesso = date('d/m/Y');
          $acesso->store();
          // finaliza a transacao
          TTransaction::close();
         * 
         */

        return $menu;
    }

    /*
      static public function montaMenu($modulo, $usuario_id) {

      // inicia transacao com o banco 'pg_ceres'
      TTransaction::open('pg_ceres');

      // instancia um repositorio da Classe
      $repository = new TRepository('vw_usuariopagina');

      // cria um criterio de selecao
      $criteria = new TCriteria;
      //filtra pelo campo usuario_id
      $criteria->add(new TFilter('usuario_id', '=', $usuario_id));
      //filtra pelo campo modulo
      $criteria->add(new TFilter('modulo', '=', $modulo));

      // carrega os objetos de acordo com o criterio
      $usuarios = $repository->load($criteria);
      $menu='';
      if ($usuarios) {
      $menu = '<ul id="nav">';
      // percorre os objetos retornados
      foreach ($usuarios as $usuario) {
      // adiciona os dados do perfil do usuario no menu
      $menu .= "<li><a href=\"#\" OnClick=\"document.location='?class=".$usuario->arquivo."'\">".$usuario->pagina."</a></li>";
      }
      $menu .= '</ul>';
      }
      // finaliza a transacao
      TTransaction::close();

      return $menu;

      }
     */

    static public function montaMenu($modulo, $usuario_id)
    {
/*
        TTransaction::open('conn_cp');

        $repositoryCP = new TRepository('vw_grupomenuusuario');

        $criteriaCP = new TCriteria;

        $criteriaCP->add(new TFilter('usuario_id', '=', $usuario_id));
        $criteriaCP->add(new TFilter('modulo', '=', $modulo));

        $objects = $repositoryCP->load($criteriaCP);

        if ($objects) {

            foreach ($objects as $object) {


            }
        }
*/

        TTransaction::open('pg_ceres');

        $repositorygrupo = new TRepository('vw_grupomenuusuario');

        $criteria1 = new TCriteria;

        //filtra pelo campo usuario_id
        $criteria1->add(new TFilter('usuario_id', '=', $usuario_id));
        //filtra pelo campo modulo
        $criteria1->add(new TFilter('modulo', '=', $modulo));

        $criteria1->setProperty('order', 'grupo');

        $grupos = $repositorygrupo->load($criteria1);

        $menu = '';
        if ($grupos) {
            $id = '';
            $menu .= '<ul class="menu">';
            foreach ($grupos as $grupo) {
                $id = $grupo->id;
                // adiciona os dados do perfil do usuario no menu
                $menu .= "<li class=\"parent green\" ><a href=\"index.html\"><span class=\"menu-icon\"><i class='" . $grupo->icone . "'></i></span><span class=\"menu-text\">" . $grupo->grupo . "</span></a>";

                // instancia um repositorio da Classe
                $repository = new TRepository('vw_usuariopaginagrupo');

                // cria um criterio de selecao
                $criteria = new TCriteria;
                //filtra pelo campo usuario_id
                $criteria->add(new TFilter('usuario_id', '=', $usuario_id));
                //filtra pelo campo modulo
                $criteria->add(new TFilter('modulo', '=', $modulo));
                //filtra pelo campo
                $criteria->setProperty('order', 'grupo');
                $criteria->setProperty('order', 'pagina');

                // carrega os objetos de acordo com o criterio
                $usuarios = $repository->load($criteria);
                if ($usuarios) {
                    // percorre os objetos retornados
                    $menu .= '<ul class="child">';
                    foreach ($usuarios as $usuario) {
                        if ($usuario->grupo_id == $id) {
                            // adiciona os dados do perfil do usuario no menu
                            if ($usuario->arquivo == 'AtualizaForm') {
                                $menu .= "<li><a href=\"#\" OnClick=\"document.location='?class=" . $usuario->arquivo . "&method=onEdit&key=" . $_SESSION['servidor_id'] . "&fk=" . $_SESSION['servidor_id'] . "'\">" . $usuario->pagina . "</a></li>";
                            } else {
                                if ($usuario->arquivoleitura == 'SIM' && $usuario->novajanela == 'SIM') {
                                    $menu .= "<li><a target=\"_blank\" href=\"http://servicos.emater.rn.gov.br/novoceres/" . $usuario->arquivo . "\">" . $usuario->pagina . "</a></li>";
                                } else {
                                    if ($usuario->novajanela == 'NAO') {
                                        $menu .= "<li><a href=\"#\" OnClick=\"document.location='?class=" . $usuario->arquivo . "'\">" . $usuario->pagina . "</a></li>";
                                    } else {
                                        $menu .= "<li><a href=\"#\" OnClick=\"javascript:window.open('?class=" . $usuario->arquivo . "');\">" . $usuario->pagina . "</a></li>";
                                    }
                                }
                            }
                        }
                    }

                    $menu .= '</ul>';
                }

                $menu .= "</li>";
            }

            $menu .= '</ul>';
        }
        // finaliza a transacao
        TTransaction::close();

        return $menu;
    }

    static public function montaError()
    {

        return 'teste';
    }

    static public function montaMural()
    {
        $mural = "";

        $negado = '<div class="error-404 text-center">
            <i class="fa fa-frown-o"></i>
            <h1>Whooops!</h1>
            <h4>Você não tem permissão para acessar esta página</h4>
            
            <p>para continuar <a href="index.php">clique aqui</a></p>
          </div>';


        if (isset($_REQUEST['acesso']) == 'negado') {
            return $negado;
        } else {
            return $mural;
        }
    }

    static public function montaValidacao()
    {

        TTransaction::open('pg_ceres');

        // instancia um repositorio para aniversariantes
        $repository = new TRepository('vw_validacaoRecord');

        // cria um criterio de selecao, ordenado pelo id
        $criteria = new TCriteria;
        $criteria->setProperty('order', 'nome');
        // carrega os objetos de acordo com o criterio
        $results = $repository->load($criteria);

        $validar = '';

        if ($results) {

            $validar = '<table id="validacao">';
            $validar .= '<center><b><h3>CAPACITACOES PARA VALIDAR</h3></b></center>';
            $validar .= '<tr><th>Nome Servidor</th><th>Capacitacao</th1><th>Data de Conclusao</th><th>Situacao</th></tr>';
            // percorre os objetos retornados
            foreach ($results as $result) {

                $validar .= "<tr><td>" . $result->nome . '</td><td>' . $result->capacitacao . '</td><td>' . $result->datafim . '</td><td>' . $result->situacao . "</td></tr>";
            }
            $validar .= '</table>';
        }

        $validar = '';

        // finaliza a transacao
        TTransaction::close();

        return $validar;
    }

    static public function montaAvaliacao()
    {

        TTransaction::open('pg_ceres');

        // instancia um repositorio para aniversariantes
        $repository = new TRepository('vw_avaliacao_servidoresRecord');

        // cria um criterio de selecao, ordenado pelo id
        $criteria = new TCriteria;
        //filtra pelo campo avaliador_id
        $criteria->add(new TFilter('avaliador_id', '=', $_SESSION['servidor_id']));
        // carrega os objetos de acordo com o criterio
        $results = $repository->load($criteria);

        $avaliacao = '';

        if ($results) {

            $avaliacao = '<table cellpadding="0" cellspacing="0" border="0" class="display" id="example2">';
            $avaliacao .= "<thead>";
            $avaliacao .= '<center><b><h3>AVALIAÇÕES PENDENTES</h3></b></center>';
            $avaliacao .= '<tr><th>Avaliação</th><th>Servidor Avaliado</th><th>Lotação</th><th>Data de Conclusão da Avaliação</th><th>Responder</th></tr>';
            $avaliacao .= "</thead>";
            // percorre os objetos retornados
            foreach ($results as $result) {


                $avaliacao .= "<tr class=\"gradeC\"><td>" . $result->nomeavaliacao . '</td><td>' . $result->servidoravaliado . '</td><td>' . $result->lotacaoavaliado . '</td><td>' . $result->datafim . "</td><td><font color='ff0000'><a href='index.php?class=QuestionarioAvaliacaoServidorForm&avaliacao_id=" . $result->avaliacao_id . "&avaliacaoservidor_id=" . $result->avaliacaoservidor_id . "&ordem=1'>Responder Avaliação</a></font></td></tr>";
            }
            $avaliacao .= '</table>';
        }

        //$avaliacao = '';
        // finaliza a transacao
        TTransaction::close();

        return $avaliacao;
    }

    /**
     * Shows the widget at the screen
     */
    public function show()
    {
        if ($this->items) {
            foreach ($this->items as $item) {
                parent::add($item);
            }
        }
        parent::show();
    }

}
