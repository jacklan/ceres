<?php

Namespace Adianti\Wrapper;

use TPDFDesigner;

/**
 * FPDF Adapter that parses XML files from Adianti Framework
 *
 * @version    2.0
 * @package    wrapper
 * @author     Equipe GIN EMATER
 * @copyright  Copyright (c) 2006-2014 Adianti Solutions Ltd. (http://www.adianti.com.br)
 * @license    http://www.adianti.com.br/framework-license
 */
class TRPDFPadraoDesigner extends TPDFDesigner {

    private $empresa_id;
    private $empresa_nome;
    private $titulo;
    private $tipo_orient;

    public function __construct($orientation = 'P', $format = 'A4', $empresa_id = '', $empresanome = '', $nome_rel = '') {
        
        parent::__construct($orientation,  $format);

        $this->empresa_id = $empresa_id;
        $this->empresa_nome = $empresanome;
        $this->titulo = $nome_rel;
        $this->colunas = $colunas;
        $this->tipo_orient = $orientation;

        $this->SetTitle($nome_rel);
        $this->SetSubject($nome_rel);
    }


    //Page header
    public function Header() {

        //endereco da imagem,posicao X(horizontal),posicao Y(vertical), tamanho altura, tamanho largura
        $this->Image("app/images/logo_relatorio_" . $this->empresa_id . ".jpg", 8, 11, 26, 18);

        if( $this->tipo_orient == 'P'){
        
            $this->SetFont('Arial', 'B', 10);
        
        }else{
        
            $this->SetFont('Arial', 'B', 12);
        
        }
        
        $this->SetY("12");
        $this->SetX("25");
        $this->Cell(0, 5, utf8_decode("GOVERNO DO ESTADO DO RIO GRANDE DO NORTE"), 0, 1, 'C');

        $this->SetY("17");
        $this->SetX("25");
        $this->Cell(0, 5, utf8_decode($this->empresa_nome), 0, 1, 'C');

        $this->SetY("22");
        $this->SetX("25");
        $this->Cell(0, 5, utf8_decode($this->titulo), 0, 1, 'C');
        $this->Ln(3);

    }


    public function save($output) {
       
        if (!file_exists($output) OR is_writable($output)) {
            
            parent::Output($output);
            $this->unsetLocale();
        
        } else {
        
            throw new Exception(_t('Permission denied') . ': ' . $output);
        
        }
    }

    public function getLinha(){

        if( $this->tipo_orient  == 'P' ){
        
            $this->SetX("5");
            $this->Cell( 200, 0, '', 1, 0, 'L');  
        
        }else{

            $this->SetX("5");
            $this->Cell( 285, 0, '', 1, 0, 'L');   
        
        }
        
    }

    public function getTitle($titulo){

        $this->SetFont('Courier', 'B', 18);
        $this->SetX("10");
        $this->Cell(0, 10, strtoupper(utf8_decode($titulo)), 0, 1, 'C');
        $this->SetFont('Arial', '', 10);
    }

    /**
     * Back to the old locale
     * @author  Pablo Dall'Oglio
     */
    public function unsetLocale() {
        if (OS == 'WIN') {
            setlocale(LC_ALL, 'english');
        } else {
            setlocale(LC_ALL, 'pt_BR');
        }
    }

    //Page footer
    function Footer() {
        //Position at 1.5 cm from bottom
        $this->SetY(-15);
        //Arial italic 8
        $this->SetFont('Arial', 'I', 8);
        //Page number
        //data atual
        $data = date("d/m/Y H:i:s");
        $conteudo = "impresso em " . $data;
        $texto = "http://www.emater.rn.gov.br";
        //imprime uma linha... largura,altura, texto,borda,quebra de linha, alinhamento
        $this->getLinha();
        $this->SetX("5");

        //imprime uma celula... largura,altura, texto,borda,quebra de linha, alinhamento
        $this->Cell(0, 5, $texto, 0, 0, 'L');
        $this->Cell(0, 5, $conteudo, 0, 0, 'R');
        $this->Ln();
    }

 
}
