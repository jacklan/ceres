<?php

Namespace Adianti\Wrapper;

use FPDF;

/**
 * FPDF Adapter that parses XML files from Adianti Framework
 *
 * @version    2.0
 * @package    wrapper
 * @author     Equipe GIN EMATER
 * @copyright  Copyright (c) 2006-2014 Adianti Solutions Ltd. (http://www.adianti.com.br)
 * @license    http://www.adianti.com.br/framework-license
 */
class TRPDFPadrao extends FPDF {

    private $empresa_id;
    private $empresa_nome;
    private $titulo;
    private $colunas;
    private $rows;
    private $subGrupo;

    public function __construct($orientation = 'P', $format = 'A4', $empresa_id = '', $empresanome = '', $nome_rel = '', $colunas) {
       
        parent::__construct($orientation, 'mm', $format);

        $this->empresa_id = $empresa_id;
        $this->empresa_nome = $empresanome;
        $this->titulo = $nome_rel;
        $this->colunas = $colunas;

        $this->SetTitle($nome_rel);
        $this->SetSubject($nome_rel);
    }

//Page header
    public function Header() {

        //endereco da imagem,posicao X(horizontal),posicao Y(vertical), tamanho altura, tamanho largura
        $this->Image("app/images/logo_relatorio_" . $this->empresa_id . ".jpg", 8, 11, 26, 18);

        //Arial bold 15
        $this->SetFont('Arial', 'B', 12);
        $this->SetY("12");
        $this->SetX("25");
        $this->Cell(0, 5, utf8_decode("GOVERNO DO ESTADO DO RIO GRANDE DO NORTE"), 0, 1, 'C');

        $this->SetY("17");
        $this->SetX("25");
        $this->Cell(0, 5, utf8_decode($this->empresa_nome), 0, 1, 'C');

        $this->SetY("22");
        $this->SetX("25");
        $this->Cell(0, 5, utf8_decode($this->titulo), 0, 1, 'C');
        $this->Ln(3);

        
        $this->ColumnHeader();
      
    }

    public function ColumnHeader() {

        //define a fonte a ser usada
        $this->SetFont('Arial', 'B', 10);
        $tamanho = count($this->colunas);

        for ($i = 0; $i < $tamanho; $i++) {
            $this->SetX($this->colunas[$i][1]);
            $this->Cell(0, 5, utf8_decode($this->colunas[$i][0]), 0, (($i + 1) == $tamanho ? 1 : 0), 'L');
        }
        

        $this->ColumnDetail();

    }


    public function setRows($rows){
        $this->rows = $rows;
    }

    public function getSubGrupo($grupoo){
        
        $this->SetFont('Courier', 'B', 18);
        $this->SetX("10");
        $this->Cell(0, 10, strtoupper(utf8_decode('TESTE')), 0, 1, 'C');
        $this->SetFont('Arial', '', 10);
    }


    public function ColumnDetail() {
        
        $this->SetFont('Arial', '', 9);

        $this->SetX("20");
        $tamanho = count($this->colunas);
        $rowss = $this->rows;
        $gruop = $this->subGrupo;
        $gruopVar = '';

        if ($rowss) {

        
            // percorre os objetos retornados
            foreach ($rowss as $row) {
               
                $dados = $row->toArray(); 
               
                //$this->Cell(0, 0, strtoupper(utf8_decode( $dados["regional"] )), 0, 1, 'C');

                $this->Cell(0, 0, strtoupper(utf8_decode( $this->subGrupo )), 0, 1, 'C');


              /*  $dados = $row->toArray(); 

                    if ($gruopVar != array_keys( $dados, $gruop )) {

                    $gruopVar = array_keys( $dados, $gruop );

                    $this->SetFont('Courier', 'B', 18);
                    $this->SetX("10");
                    $this->Cell(0, 10, strtoupper(utf8_decode( array_keys( $dados, $gruop ) )), 0, 1, 'C');
                    $this->SetFont('Arial', '', 10);
                    
                    } */


                for ($i = 0; $i < $tamanho; $i++) {
                    $this->SetX($this->colunas[$i][1]);
                    //$valor = substr($dados[$this->colunas[$i][2]], 0, $dados[$this->colunas[$i][3]]) ;    // ERRRO
                    $valor = substr($dados[$this->colunas[$i][2]], 0, $this->colunas[$i][3]) ;
                    $this->Cell(0, 5, $valor, 0, (($i + 1) == $tamanho ? 1 : 0), 'L');
                }
            }
 

        }else{
                $this->SetFont('arial','',15);
                $this->SetX("15");
                $this->Cell(0,30,utf8_decode("Nao existem dados cadastrados para os  parametros desta consulta"),0,1,'J');
                $this->Ln();
        }



    }

    public function save($output) {
        if (!file_exists($output) OR is_writable($output)) {
            parent::Output($output);
            $this->unsetLocale();
        } else {
            throw new Exception(_t('Permission denied') . ': ' . $output);
        }
    }

    /**
     * Back to the old locale
     * @author  Pablo Dall'Oglio
     */
    public function unsetLocale() {
        if (OS == 'WIN') {
            setlocale(LC_ALL, 'english');
        } else {
            setlocale(LC_ALL, 'pt_BR');
        }
    }

//Page footer

    function Footer() {
        //Position at 1.5 cm from bottom
        $this->SetY(-15);
        //Arial italic 8
        $this->SetFont('Arial', 'I', 8);
        //Page number
        //data atual
        $data = date("d/m/Y H:i:s");
        $conteudo = "impresso em " . $data;
        $texto = "http://www.emater.rn.gov.br";
        //imprime uma linha... largura,altura, texto,borda,quebra de linha, alinhamento
        $this->Cell(0, 0, '', 1, 1, 'L');

        //imprime uma celula... largura,altura, texto,borda,quebra de linha, alinhamento
        $this->Cell(0, 5, $texto, 0, 0, 'L');
        $this->Cell(0, 5, $conteudo, 0, 0, 'R');
        $this->Ln();
    }

 
}
