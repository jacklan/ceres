<?php

if (isset($_GET['file'])) {

    $file = $_GET['file'];
    $info = pathinfo($file);
    $extension = $info['extension'];

    $content_type_list = [];
    $content_type_list['html'] = 'text/html';
    $content_type_list['pdf'] = 'application/pdf';
    $content_type_list['rtf'] = 'application/rtf';
    $content_type_list['csv'] = 'application/csv';
    $content_type_list['xls'] = 'application/xls';
    $content_type_list['txt'] = 'text/plain';
    $content_type_list['xml'] = 'application/xml';

    if (in_array($extension, ['html', 'pdf', 'rtf', 'csv', 'txt', 'xls', 'xml'])) {

        $basename = basename($file);

        // get the filesize
        $filesize = filesize($file);

        header("Pragma: public");
        header("Expires: 0"); // set expiration time
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Content-type: " . $content_type_list[$extension]);
        header("Content-Length: {$filesize}");
        header("Content-disposition: inline; filename=\"{$basename}\"");
        header("Content-Transfer-Encoding: binary");

        echo file_get_contents($file);

    }

}
