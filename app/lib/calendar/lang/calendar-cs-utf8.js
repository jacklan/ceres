/* 
	calendar-cs-win.js
	language: Czech
	encoding: windows-1250
	author: Lubos Jerabek (xnet@seznam.cz)
	        Jan Uhlir (espinosa@centrum.cz)
*/

// ** I18N
Calendar._DN  = new Array('Neděle','Ponděl�','Úterý','Středa','Čtvrtek','P�tek','Sobota','Neděle');
Calendar._SDN = new Array('Ne','Po','Út','St','Čt','P�','So','Ne');
Calendar._MN  = new Array('Leden','Únor','Březen','Duben','Květen','Červen','Červenec','Srpen','Z�ř�','Ř�jen','Listopad','Prosinec');
Calendar._SMN = new Array('Led','Úno','Bře','Dub','Kvě','Črv','Čvc','Srp','Z�ř','Ř�j','Lis','Pro');

// tooltips
Calendar._TT = {};
Calendar._TT["INFO"] = "O komponentě kalend�ř";
Calendar._TT["TOGGLE"] = "Změna prvn�ho dne v týdnu";
Calendar._TT["PREV_YEAR"] = "Předchoz� rok (přidrž pro menu)";
Calendar._TT["PREV_MONTH"] = "Předchoz� měs�c (přidrž pro menu)";
Calendar._TT["GO_TODAY"] = "Dnešn� datum";
Calendar._TT["NEXT_MONTH"] = "Dalš� měs�c (přidrž pro menu)";
Calendar._TT["NEXT_YEAR"] = "Dalš� rok (přidrž pro menu)";
Calendar._TT["SEL_DATE"] = "Vyber datum";
Calendar._TT["DRAG_TO_MOVE"] = "Chyť a t�hni, pro přesun";
Calendar._TT["PART_TODAY"] = " (dnes)";
Calendar._TT["MON_FIRST"] = "Ukaž jako prvn� Ponděl�";
//Calendar._TT["SUN_FIRST"] = "Ukaž jako prvn� Neděli";

Calendar._TT["ABOUT"] =
"DHTML Date/Time Selector\n" +
"(c) dynarch.com 2002-2005 / Author: Mihai Bazon\n" + // don't translate this this ;-)
"For latest version visit: http://www.dynarch.com/projects/calendar/\n" +
"Distributed under GNU LGPL.  See http://gnu.org/licenses/lgpl.html for details." +
"\n\n" +
"Výběr datumu:\n" +
"- Use the \xab, \xbb buttons to select year\n" +
"- Použijte tlač�tka " + String.fromCharCode(0x2039) + ", " + String.fromCharCode(0x203a) + " k výběru měs�ce\n" +
"- Podržte tlač�tko myši na jak�mkoliv z těch tlač�tek pro rychlejš� výběr.";

Calendar._TT["ABOUT_TIME"] = "\n\n" +
"Výběr času:\n" +
"- Klikněte na jakoukoliv z č�st� výběru času pro zvýšen�.\n" +
"- nebo Shift-click pro sn�žen�\n" +
"- nebo klikněte a t�hněte pro rychlejš� výběr.";

// the following is to inform that "%s" is to be the first day of week
// %s will be replaced with the day name.
Calendar._TT["DAY_FIRST"] = "Zobraz %s prvn�";

// This may be locale-dependent.  It specifies the week-end days, as an array
// of comma-separated numbers.  The numbers are from 0 to 6: 0 means Sunday, 1
// means Monday, etc.
Calendar._TT["WEEKEND"] = "0,6";

Calendar._TT["CLOSE"] = "Zavř�t";
Calendar._TT["TODAY"] = "Dnes";
Calendar._TT["TIME_PART"] = "(Shift-)Klikni nebo t�hni pro změnu hodnoty";

// date formats
Calendar._TT["DEF_DATE_FORMAT"] = "d.m.yy";
Calendar._TT["TT_DATE_FORMAT"] = "%a, %b %e";

Calendar._TT["WK"] = "wk";
Calendar._TT["TIME"] = "Čas:";
