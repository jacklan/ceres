
  function handleFileSelect(evt) {
    var files = evt.target.files;

                for (var i = 0, f; f = files[i]; i++) {

                  if (!f.type.match('image.*') ) {
                    continue;
                  }

                  var reader = new FileReader();

                  reader.onload = (function(theFile) {
                    return function(e) {
                      var span = document.querySelector('#list');
                      span.innerHTML = ['<img class="thum thumbb" src="', e.target.result,
                                        '" title="', escape(theFile.name), '"/>'].join('');

                      document.getElementById('list').insertBefore(span, null);
                    };
                  })(f);

                  reader.readAsDataURL(f);

            }

   
    document.getElementById("imgNone").remove();

  };

   document.getElementById('files').addEventListener('change', handleFileSelect, false);





