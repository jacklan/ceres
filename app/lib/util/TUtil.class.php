<?php

namespace Lib\Util;

class TUtil {
#funcao para formatar a data no grid

    static function formatar_data($valor) {
        return FormatDateTime($valor, 7);
    }

#funcao para exibir mensagem de interacao com usuario

    static function msgAlert($class, $method, $param, $msg) {

        // Change the value of the outputText field
        echo "<script language='javascript' type='text/javascript'>\n";

        echo "location.href='index.php?class=" . $class . "&method=" . $method . "&" . $param . "&msg=" . $msg . "';\n";

        echo "</script>";
    }

    /**
     *
     * @param int $time O tempo em segundos
     * @return string O tempo em forma textual
     */
#funcao converter segundos para anos, meses, dias
    static function time1text($time) {
        $response = array();
        $years = floor($time / (86400 * 365));
        $time = $time % (86400 * 365);
        $months = floor($time / (86400 * 30));
        $time = $time % (86400 * 30);
        $days = floor($time / 86400);
        $time = $time % 86400;

        if ($years > 0)
            $response[] = $years . ' ano' . ($years > 1 ? 's' : ' ');
        if ($months > 0)
            $response[] = $months . ' mes' . ($months > 1 ? 'es' : ' ');
        if ($days > 0)
            $response[] = $days . ' dia' . ($days > 1 ? 's' : ' ');
        return implode(', ', $response);
    }

    /**
     *
     * @param int $time O tempo em segundos
     * @return string O tempo em forma textual
     */
#funcao converter segundos para anos, meses, dias, horas, minutos e segundos
    static function time2text($time) {
        $response = array();
        $years = floor($time / (86400 * 365));
        $time = $time % (86400 * 365);
        $months = floor($time / (86400 * 30));
        $time = $time % (86400 * 30);
        $days = floor($time / 86400);
        $time = $time % 86400;
        $hours = floor($time / (3600));
        $time = $time % 3600;
        $minutes = floor($time / 60);
        $seconds = $time % 60;
        if ($years > 0)
            $response[] = $years . ' ano' . ($years > 1 ? 's' : ' ');
        if ($months > 0)
            $response[] = $months . ' mes' . ($months > 1 ? 'es' : ' ');
        if ($days > 0)
            $response[] = $days . ' dia' . ($days > 1 ? 's' : ' ');
        if ($hours > 0)
            $response[] = $hours . ' hora' . ($hours > 1 ? 's' : ' ');
        if ($minutes > 0)
            $response[] = $minutes . ' minuto' . ($minutes > 1 ? 's' : ' ');
        if ($seconds > 0)
            $response[] = $seconds . ' segundo' . ($seconds > 1 ? 's' : ' ');
        return implode(', ', $response);
    }

    static function diasDeDiferenca($dataInicial, $dataFinal) {
        if ($dataInicial && $dataFinal) {
            $vetorDataInicial = explode('/', $dataInicial);
            $timeInicial = mktime(0, 0, 0, $vetorDataInicial[1], $vetorDataInicial[0], $vetorDataInicial[2]);
            $vetorDataFinal = explode('/', $dataFinal);
            $timeFinal = mktime(0, 0, 0, $vetorDataFinal[1], $vetorDataFinal[0], $vetorDataFinal[2]);
// CALCULA A DIFERENÇA DE SEGUNDOS ENTRE AS DUAS DATAS:
            $diferenca = $timeFinal - $timeInicial;
            $dias = (int) floor($diferenca / (60 * 60 * 24));
        } else {
            $dias = 0;
        }
        return $dias;
    }

//calcular idade
    static function calcularIdade($data_nasc) {

        $ano_diff = date("Y") - substr($data_nasc, 0, 4);
        $mes_diff = date("m") - substr($data_nasc, 5, 2);
        $dia_diff = date("d") - substr($data_nasc, 8, 2);

        if (($mes_diff < 0) || ($mes_diff == 0 && $dia_diff < 0))
            $ano_diff-=$ano_diff;
        return $ano_diff;
    }

    static function formatar_hora($valor) {
        $hora = substr($valor, 0, 2) . ":" . substr($valor, 3, 2);
        //return strftime('%H:%M', $valor);
        return $hora;
        //return $valor;
    }

    static function formatar_hora2($valor) {
        $hora = substr($valor, 11, 2) . ":" . substr($valor, 14, 2);
        //return strftime('%H:%M', $valor);
        return $hora;
        //return $valor;
    }

    static function GravarDataPostgres($data) {
        $tmp = str_replace("'", "''", $data);
        if ($tmp == "") {
            return "NULL";
        } else {
            $dataemvetor = explode("/", $data);
            $dataatual = mktime(0, 0, 0, $dataemvetor[1], $dataemvetor[0], $dataemvetor[2]);
            if (strcasecmp(date("Y-m-d", $dataatual), "1969-12-31") == 0) {
                return "NULL";
            } else {
                return date("'Y-m-d'", $dataatual);
            }
#     return date("Y-m-d", $dataatual);
        }
    }

    static function FormatarDataPostgres($data) {
        $dataemvetor = explode("-", $data);
        $dataatual = mktime(0, 0, 0, $dataemvetor[1], $dataemvetor[0], $dataemvetor[2]);
        return date("d/m/Y", $dataatual);
    }

    static function FormatDateTime($ts, $namedformat) {
        $separador = "/";
        $formato = "dd/mm/yyyy";
        $DefDateFormat = str_replace("yyyy", "%Y", $formato);
        $DefDateFormat = str_replace("mm", "%m", $DefDateFormat);
        $DefDateFormat = str_replace("dd", "%d", $DefDateFormat);
        if (is_numeric($ts)) { // timestamp
            switch (strlen($ts)) {
                case 14:
                    $patt = '/(\d{4})(\d{2})(\d{2})(\d{2})(\d{2})(\d{2})/';
                    break;
                case 12:
                    $patt = '/(\d{2})(\d{2})(\d{2})(\d{2})(\d{2})(\d{2})/';
                    break;
                case 10:
                    $patt = '/(\d{2})(\d{2})(\d{2})(\d{2})(\d{2})/';
                    break;
                case 8:
                    $patt = '/(\d{4})(\d{2})(\d{2})/';
                    break;
                case 6:
                    $patt = '/(\d{2})(\d{2})(\d{2})/';
                    break;
                case 4:
                    $patt = '/(\d{2})(\d{2})/';
                    break;
                case 2:
                    $patt = '/(\d{2})/';
                    break;
                default:
                    return $ts;
            }
            if ((isset($patt)) && (preg_match($patt, $ts, $matches))) {
                $year = $matches[1];
                $month = @$matches[2];
                $day = @$matches[3];
                $hour = @$matches[4];
                $min = @$matches[5];
                $sec = @$matches[6];
            }
            if (($namedformat == 0) && (strlen($ts) < 10))
                $namedformat = 2;
        }
        elseif (is_string($ts)) {
            if (preg_match('/(\d{4})-(\d{2})-(\d{2}) (\d{2}):(\d{2}):(\d{2})/', $ts, $matches)) { // datetime
                $year = $matches[1];
                $month = $matches[2];
                $day = $matches[3];
                $hour = $matches[4];
                $min = $matches[5];
                $sec = $matches[6];
            } elseif (preg_match('/(\d{4})-(\d{2})-(\d{2})/', $ts, $matches)) { // date
                $year = $matches[1];
                $month = $matches[2];
                $day = $matches[3];
                if ($namedformat == 0)
                    $namedformat = 2;
            }
            elseif (preg_match('/(^|\s)(\d{2}):(\d{2}):(\d{2})/', $ts, $matches)) { // time
                $hour = $matches[2];
                $min = $matches[3];
                $sec = $matches[4];
                if (($namedformat == 0) || ($namedformat == 1))
                    $namedformat = 3;
                if ($namedformat == 2)
                    $namedformat = 4;
            }
            else {
                return $ts;
            }
        } else {
            return $ts;
        }
        if (!isset($year))
            $year = 0; // dummy value for times
        if (!isset($month))
            $month = 1;
        if (!isset($day))
            $day = 1;
        if (!isset($hour))
            $hour = 0;
        if (!isset($min))
            $min = 0;
        if (!isset($sec))
            $sec = 0;
        $uts = @mktime($hour, $min, $sec, $month, $day, $year);
        if ($uts < 0) { // failed to convert
            $year = substr_replace("0000", $year, -1 * strlen($year));
            $month = substr_replace("00", $month, -1 * strlen($month));
            $day = substr_replace("00", $day, -1 * strlen($day));
            $hour = substr_replace("00", $hour, -1 * strlen($hour));
            $min = substr_replace("00", $min, -1 * strlen($min));
            $sec = substr_replace("00", $sec, -1 * strlen($sec));
            $DefDateFormat = str_replace("yyyy", $year, DEFAULT_DATE_FORMAT);
            $DefDateFormat = str_replace("mm", $month, $DefDateFormat);
            $DefDateFormat = str_replace("dd", $day, $DefDateFormat);
            switch ($namedformat) {
                case 0:
                    return $DefDateFormat . " $hour:$min:$sec";
                    break;
                case 1://unsupported, return general date
                    return $DefDateFormat . " $hour:$min:$sec";
                    break;
                case 2:
                    return $DefDateFormat;
                    break;
                case 3:
                    if (intval($hour) == 0)
                        return "12:$min:$sec AM";
                    elseif (intval($hour) > 0 && intval($hour) < 12)
                        return "$hour:$min:$sec AM";
                    elseif (intval($hour) == 12)
                        return "$hour:$min:$sec PM";
                    elseif (intval($hour) > 12 && intval($hour) <= 23)
                        return (intval($hour) - 12) . ":$min:$sec PM";
                    else
                        return "$hour:$min:$sec";
                    break;
                case 4:
                    return "$hour:$min:$sec";
                    break;
                case 5:
                    return "$year" . $separador . "$month" . $separador . "$day";
                    break;
                case 6:
                    return "$month" . $separador . "$day" . $separador . "$year";
                    break;
                case 7:
                    return "$day" . $separador . "$month" . $separador . "$year";
                    break;
                case 8:
                    return "$year" . $separador . "$day" . $separador . "$month";
                    break;
            }
        } else {
            switch ($namedformat) {
                case 0:
                    return strftime($DefDateFormat . " %H:%M:%S", $uts);
                    break;
                case 1:
                    return strftime("%A, %B %d, %Y", $uts);
                    break;
                case 2:
                    return strftime($DefDateFormat, $uts);
                    break;
                case 3:
                    return strftime("%I:%M:%S %p", $uts);
                    break;
                case 4:
                    return strftime("%H:%M:%S", $uts);
                    break;
                case 5:
                    return strftime("%Y" . $separador . "%m" . $separador . "%d", $uts);
                    break;
                case 6:
                    return strftime("%m" . $separador . "%d" . $separador . "%Y", $uts);
                    break;
                case 7:
                    return strftime("%d" . $separador . "%m" . $separador . "%Y", $uts);
                    break;
                case 8:
                    return strftime("%Y" . $separador . "%d" . $separador . "%m", $uts);
                    break;
            }
        }
    }

    static function FormatDateTime_old($ts, $namedformat) {
        define(EW_DATE_SEPARATOR, "/", true);
        define(DEFAULT_DATE_FORMAT, "dd/mm/yyyy", true);

        $DefDateFormat = str_replace("yyyy", "%Y", DEFAULT_DATE_FORMAT);
        $DefDateFormat = str_replace("mm", "%m", $DefDateFormat);
        $DefDateFormat = str_replace("dd", "%d", $DefDateFormat);
        if (is_numeric($ts)) { // timestamp
            switch (strlen($ts)) {
                case 14:
                    $patt = '/(\d{4})(\d{2})(\d{2})(\d{2})(\d{2})(\d{2})/';
                    break;
                case 12:
                    $patt = '/(\d{2})(\d{2})(\d{2})(\d{2})(\d{2})(\d{2})/';
                    break;
                case 10:
                    $patt = '/(\d{2})(\d{2})(\d{2})(\d{2})(\d{2})/';
                    break;
                case 8:
                    $patt = '/(\d{4})(\d{2})(\d{2})/';
                    break;
                case 6:
                    $patt = '/(\d{2})(\d{2})(\d{2})/';
                    break;
                case 4:
                    $patt = '/(\d{2})(\d{2})/';
                    break;
                case 2:
                    $patt = '/(\d{2})/';
                    break;
                default:
                    return $ts;
            }
            if ((isset($patt)) && (preg_match($patt, $ts, $matches))) {
                $year = $matches[1];
                $month = @$matches[2];
                $day = @$matches[3];
                $hour = @$matches[4];
                $min = @$matches[5];
                $sec = @$matches[6];
            }
            if (($namedformat == 0) && (strlen($ts) < 10))
                $namedformat = 2;
        }
        elseif (is_string($ts)) {
            if (preg_match('/(\d{4})-(\d{2})-(\d{2}) (\d{2}):(\d{2}):(\d{2})/', $ts, $matches)) { // datetime
                $year = $matches[1];
                $month = $matches[2];
                $day = $matches[3];
                $hour = $matches[4];
                $min = $matches[5];
                $sec = $matches[6];
            } elseif (preg_match('/(\d{4})-(\d{2})-(\d{2})/', $ts, $matches)) { // date
                $year = $matches[1];
                $month = $matches[2];
                $day = $matches[3];
                if ($namedformat == 0)
                    $namedformat = 2;
            }
            elseif (preg_match('/(^|\s)(\d{2}):(\d{2}):(\d{2})/', $ts, $matches)) { // time
                $hour = $matches[2];
                $min = $matches[3];
                $sec = $matches[4];
                if (($namedformat == 0) || ($namedformat == 1))
                    $namedformat = 3;
                if ($namedformat == 2)
                    $namedformat = 4;
            }
            else {
                return $ts;
            }
        } else {
            return $ts;
        }
        if (!isset($year))
            $year = 0; // dummy value for times
        if (!isset($month))
            $month = 1;
        if (!isset($day))
            $day = 1;
        if (!isset($hour))
            $hour = 0;
        if (!isset($min))
            $min = 0;
        if (!isset($sec))
            $sec = 0;
        $uts = @mktime($hour, $min, $sec, $month, $day, $year);
        if ($uts < 0) { // failed to convert
            $year = substr_replace("0000", $year, -1 * strlen($year));
            $month = substr_replace("00", $month, -1 * strlen($month));
            $day = substr_replace("00", $day, -1 * strlen($day));
            $hour = substr_replace("00", $hour, -1 * strlen($hour));
            $min = substr_replace("00", $min, -1 * strlen($min));
            $sec = substr_replace("00", $sec, -1 * strlen($sec));
            $DefDateFormat = str_replace("yyyy", $year, DEFAULT_DATE_FORMAT);
            $DefDateFormat = str_replace("mm", $month, $DefDateFormat);
            $DefDateFormat = str_replace("dd", $day, $DefDateFormat);
            switch ($namedformat) {
                case 0:
                    return $DefDateFormat . " $hour:$min:$sec";
                    break;
                case 1://unsupported, return general date
                    return $DefDateFormat . " $hour:$min:$sec";
                    break;
                case 2:
                    return $DefDateFormat;
                    break;
                case 3:
                    if (intval($hour) == 0)
                        return "12:$min:$sec AM";
                    elseif (intval($hour) > 0 && intval($hour) < 12)
                        return "$hour:$min:$sec AM";
                    elseif (intval($hour) == 12)
                        return "$hour:$min:$sec PM";
                    elseif (intval($hour) > 12 && intval($hour) <= 23)
                        return (intval($hour) - 12) . ":$min:$sec PM";
                    else
                        return "$hour:$min:$sec";
                    break;
                case 4:
                    return "$hour:$min:$sec";
                    break;
                case 5:
                    return "$year" . EW_DATE_SEPARATOR . "$month" . EW_DATE_SEPARATOR . "$day";
                    break;
                case 6:
                    return "$month" . EW_DATE_SEPARATOR . "$day" . EW_DATE_SEPARATOR . "$year";
                    break;
                case 7:
                    return "$day" . EW_DATE_SEPARATOR . "$month" . EW_DATE_SEPARATOR . "$year";
                    break;
            }
        } else {
            switch ($namedformat) {
                case 0:
                    return strftime($DefDateFormat . " %H:%M:%S", $uts);
                    break;
                case 1:
                    return strftime("%A, %B %d, %Y", $uts);
                    break;
                case 2:
                    return strftime($DefDateFormat, $uts);
                    break;
                case 3:
                    return strftime("%I:%M:%S %p", $uts);
                    break;
                case 4:
                    return strftime("%H:%M:%S", $uts);
                    break;
                case 5:
                    return strftime("%Y" . EW_DATE_SEPARATOR . "%m" . EW_DATE_SEPARATOR . "%d", $uts);
                    break;
                case 6:
                    return strftime("%m" . EW_DATE_SEPARATOR . "%d" . EW_DATE_SEPARATOR . "%Y", $uts);
                    break;
                case 7:
                    return strftime("%d" . EW_DATE_SEPARATOR . "%m" . EW_DATE_SEPARATOR . "%Y", $uts);
                    break;
            }
        }
    }

    static function dataExtenso($data) {
// leitura das datas
        $dia = date($data, 'd');
        $mes = date($data, 'm');
        $ano = date($data, 'Y');
        $semana = date($data, 'w');


// configuração mes

        switch ($mes) {

            case 1: $mes = "JANEIRO";
                break;
            case 2: $mes = "FEVEREIRO";
                break;
            case 3: $mes = "MARÇO";
                break;
            case 4: $mes = "ABRIL";
                break;
            case 5: $mes = "MAIO";
                break;
            case 6: $mes = "JUNHO";
                break;
            case 7: $mes = "JULHO";
                break;
            case 8: $mes = "AGOSTO";
                break;
            case 9: $mes = "SETEMBRO";
                break;
            case 10: $mes = "OUTUBRO";
                break;
            case 11: $mes = "NOVEMBRO";
                break;
            case 12: $mes = "DEZEMBRO";
                break;
        }


// configuração semana

        switch ($semana) {

            case 0: $semana = "DOMINGO";
                break;
            case 1: $semana = "SEGUNDA FEIRA";
                break;
            case 2: $semana = "TERÇA-FEIRA";
                break;
            case 3: $semana = "QUARTA-FEIRA";
                break;
            case 4: $semana = "QUINTA-FEIRA";
                break;
            case 5: $semana = "SEXTA-FEIRA";
                break;
            case 6: $semana = "S�?BADO";
                break;
        }

        return "$dia DE $mes DE $ano";
    }

    static function retornaMes($param) {
        switch ($param) {


            case 1: $mes = "Janeiro";
                break;
            case 2: $mes = "Fevereiro";
                break;
            case 3: $mes = "Março";
                break;
            case 4: $mes = "Abril";
                break;
            case 5: $mes = "Maio";
                break;
            case 6: $mes = "Junho";
                break;
            case 7: $mes = "Julho";
                break;
            case 8: $mes = "Agosto";
                break;
            case 9: $mes = "Setembro";
                break;
            case 10: $mes = "Outubro";
                break;
            case 11: $mes = "Novembro";
                break;
            case 12: $mes = "Dezembro";
                break;
        }
        return $mes;
    }

    static function defineRangerMes($mes1, $ano) {
        switch ($mes1) {
            case 0: $mes = " start='01/01/" . $ano . "' end='31/12/" . $ano . "' label='Meses' ";
                break;
            case 1: $mes = " start='01/01/" . $ano . "' end='31/01/" . $ano . "' label='Jan' ";
                break;
            case 2: $mes = " start='01/02/" . $ano . "' end='28/02/" . $ano . "' label='Fev' ";
                break;
            case 3: $mes = " start='01/03/" . $ano . "' end='31/03/" . $ano . "' label='Mar' ";
                break;
            case 4: $mes = " start='01/04/" . $ano . "' end='30/04/" . $ano . "' label='Abr' ";
                break;
            case 5: $mes = " start='01/05/" . $ano . "' end='31/05/" . $ano . "' label='Mai' ";
                break;
            case 6: $mes = " start='01/06/" . $ano . "' end='30/06/" . $ano . "' label='Jun' ";
                break;
            case 7: $mes = " start='01/07/" . $ano . "' end='31/07/" . $ano . "' label='Jul' ";
                break;
            case 8: $mes = " start='01/08/" . $ano . "' end='31/08/" . $ano . "' label='Ago' ";
                break;
            case 9: $mes = " start='01/09/" . $ano . "' end='30/09/" . $ano . "' label='Set' ";
                break;
            case 10: $mes = " start='01/10/" . $ano . "' end='31/10/" . $ano . "' label='Out' ";
                break;
            case 11: $mes = " start='01/11/" . $ano . "' end='30/11/" . $ano . "' label='Nov' ";
                break;
            case 12: $mes = " start='01/12/" . $ano . "' end='31/12/" . $ano . "' label='Dez' ";
                break;
        }
        return $mes;
    }

    static function validaCPF($cpf) {

        $status = false;

        if (!is_numeric($cpf)) {
            $status = false;
        } else {

            /* aqui ele verifica se todos os números digitados são iguais, caso sejam, faz o mesmo que na condição anterior */

            if (($cpf == '11111111111') || ($cpf == '22222222222') ||
                    ($cpf == '33333333333') || ($cpf == '44444444444') ||
                    ($cpf == '55555555555') || ($cpf == '66666666666') ||
                    ($cpf == '77777777777') || ($cpf == '88888888888') ||
                    ($cpf == '99999999999') || ($cpf == '00000000000')) {
                $status = false;
            } else {

                /* se todos os testes anteriores retonaram true, então será iniciada a verificação dos números */
                /* primeiro o script vai pegar o numero do dígito verificador */

                $dv_informado = substr($cpf, 9, 2);
                for ($i = 0; $i <= 8; $i++) {
                    $digito[$i] = substr($cpf, $i, 1);
                }

                /* Agora será calculado o valor do décimo dígito de verificação */

                $posicao = 10;
                $soma = 0;
                for ($i = 0; $i <= 8; $i++) {
                    $soma = $soma + $digito[$i] * $posicao;
                    $posicao = $posicao - 1;
                }
                $digito[9] = $soma % 11;
                if ($digito[9] < 2) {
                    $digito[9] = 0;
                } else {
                    $digito[9] = 11 - $digito[9];
                }

                /* Agora será calculado o valor do décimo primeiro dígito de verificação */

                $posicao = 11;
                $soma = 0;

                for ($i = 0; $i <= 9; $i++) {
                    $soma = $soma + $digito[$i] * $posicao;
                    $posicao = $posicao - 1;
                }
                $digito[10] = $soma % 11;
                if ($digito[10] < 2) {
                    $digito[10] = 0;
                } else {
                    $digito[10] = 11 - $digito[10];
                }

                /* Nessa parte do script será verificado se o dígito verificador é igual ao informado pelo usuário */

                $dv = $digito[9] * 10 + $digito[10];
                if ($dv != $dv_informado) {
                    $status = false;
                } else
                    $status = true;
            }
        }
        return $status;
    }

    static function validaCNPJ($cnpj) {
        if (strlen($cnpj) <> 14)
            return false;
        $soma1 = ($cnpj[0] * 5) +
                ($cnpj[1] * 4) +
                ($cnpj[2] * 3) +
                ($cnpj[3] * 2) +
                ($cnpj[4] * 9) +
                ($cnpj[5] * 8) +
                ($cnpj[6] * 7) +
                ($cnpj[7] * 6) +
                ($cnpj[8] * 5) +
                ($cnpj[9] * 4) +
                ($cnpj[10] * 3) +
                ($cnpj[11] * 2);
        $resto = $soma1 % 11;
        $digito1 = $resto < 2 ? 0 : 11 - $resto;

        $soma2 = ($cnpj[0] * 6) +
                ($cnpj[1] * 5) +
                ($cnpj[2] * 4) +
                ($cnpj[3] * 3) +
                ($cnpj[4] * 2) +
                ($cnpj[5] * 9) +
                ($cnpj[6] * 8) +
                ($cnpj[7] * 7) +
                ($cnpj[8] * 6) +
                ($cnpj[9] * 5) +
                ($cnpj[10] * 4) +
                ($cnpj[11] * 3) +
                ($cnpj[12] * 2);
        $resto = $soma2 % 11;
        $digito2 = $resto < 2 ? 0 : 11 - $resto;
        return (($cnpj[12] == $digito1) && ($cnpj[13] == $digito2));
    }

    static function valorPorExtenso($valor = 0) {
        $singular = array("centavo", "real", "mil", "milhão", "bilhão", "trilhão", "quatrilhão");
        $plural = array("centavos", "reais", "mil", "milhões", "bilhões", "trilhões",
            "quatrilhões");

        $c = array("", "cem", "duzentos", "trezentos", "quatrocentos",
            "quinhentos", "seiscentos", "setecentos", "oitocentos", "novecentos");
        $d = array("", "dez", "vinte", "trinta", "quarenta", "cinquenta",
            "sessenta", "setenta", "oitenta", "noventa");
        $d10 = array("dez", "onze", "doze", "treze", "quatorze", "quinze",
            "dezesseis", "dezesete", "dezoito", "dezenove");
        $u = array("", "um", "dois", "três", "quatro", "cinco", "seis",
            "sete", "oito", "nove");

        $z = 0;

        $valor = number_format($valor, 2, ".", ".");
        $inteiro = explode(".", $valor);
        for ($i = 0; $i < count($inteiro); $i++)
            for ($ii = strlen($inteiro[$i]); $ii < 3; $ii++)
                $inteiro[$i] = "0" . $inteiro[$i];

        // $fim identifica onde que deve se dar junção de centenas por "e" ou por "," ;)
        $fim = count($inteiro) - ($inteiro[count($inteiro) - 1] > 0 ? 1 : 2);
        for ($i = 0; $i < count($inteiro); $i++) {
            $valor = $inteiro[$i];
            $rc = (($valor > 100) && ($valor < 200)) ? "cento" : $c[$valor[0]];
            $rd = ($valor[1] < 2) ? "" : $d[$valor[1]];
            $ru = ($valor > 0) ? (($valor[1] == 1) ? $d10[$valor[2]] : $u[$valor[2]]) : "";

            $r = $rc . (($rc && ($rd || $ru)) ? " e " : "") . $rd . (($rd &&
                    $ru) ? " e " : "") . $ru;
            $t = count($inteiro) - 1 - $i;
            $r .= $r ? " " . ($valor > 1 ? $plural[$t] : $singular[$t]) : "";
            if ($valor == "000")
                $z++;
            elseif ($z > 0)
                $z--;
            if (($t == 1) && ($z > 0) && ($inteiro[0] > 0))
                $r .= (($z > 1) ? " de " : "") . $plural[$t];
            if ($r)
                $rt = $rt . ((($i > 0) && ($i <= $fim) &&
                        ($inteiro[0] > 0) && ($z < 1)) ? ( ($i < $fim) ? ", " : " e ") : " ") . $r;
        }

        return($rt ? $rt : "zero");
    }

    static function diasPorExtenso($valor = 0) {
        $singular = array("centavo", "dias", "mil", "milhão", "bilhão", "trilhão", "quatrilhão");
        $plural = array("centavos", "dias", "mil", "milhões", "bilhões", "trilhões",
            "quatrilhões");

        $c = array("", "cem", "duzentos", "trezentos", "quatrocentos",
            "quinhentos", "seiscentos", "setecentos", "oitocentos", "novecentos");
        $d = array("", "dez", "vinte", "trinta", "quarenta", "cinquenta",
            "sessenta", "setenta", "oitenta", "noventa");
        $d10 = array("dez", "onze", "doze", "treze", "quatorze", "quinze",
            "dezesseis", "dezesete", "dezoito", "dezenove");
        $u = array("", "um", "dois", "três", "quatro", "cinco", "seis",
            "sete", "oito", "nove");

        $z = 0;

        $valor = number_format($valor, 2, ".", ".");
        $inteiro = explode(".", $valor);
        for ($i = 0; $i < count($inteiro); $i++)
            for ($ii = strlen($inteiro[$i]); $ii < 3; $ii++)
                $inteiro[$i] = "0" . $inteiro[$i];

        // $fim identifica onde que deve se dar junção de centenas por "e" ou por "," ;)
        $fim = count($inteiro) - ($inteiro[count($inteiro) - 1] > 0 ? 1 : 2);
        for ($i = 0; $i < count($inteiro); $i++) {
            $valor = $inteiro[$i];
            $rc = (($valor > 100) && ($valor < 200)) ? "cento" : $c[$valor[0]];
            $rd = ($valor[1] < 2) ? "" : $d[$valor[1]];
            $ru = ($valor > 0) ? (($valor[1] == 1) ? $d10[$valor[2]] : $u[$valor[2]]) : "";

            $r = $rc . (($rc && ($rd || $ru)) ? " e " : "") . $rd . (($rd &&
                    $ru) ? " e " : "") . $ru;
            $t = count($inteiro) - 1 - $i;
            $r .= $r ? " " . ($valor > 1 ? $plural[$t] : $singular[$t]) : "";
            if ($valor == "000")
                $z++;
            elseif ($z > 0)
                $z--;
            if (($t == 1) && ($z > 0) && ($inteiro[0] > 0))
                $r .= (($z > 1) ? " de " : "") . $plural[$t];
            if ($r)
                $rt = $rt . ((($i > 0) && ($i <= $fim) &&
                        ($inteiro[0] > 0) && ($z < 1)) ? ( ($i < $fim) ? ", " : " e ") : " ") . $r;
        }

        return($rt ? $rt : "zero");
    }

    static function pegaMac() {

        exec("ipconfig /all", $output);
        foreach ($output as $line) {
            if (preg_match("/(.*)Endereço físico(.*)/", $line)) {
                $mac = $line;
                $mac = str_replace("Endereço físico . . . . . . . . . . :", "", $mac);
            }
        }
        return $mac;
    }

    static function retornaAnocomZero() {
        $itens = array();
        //$itens['0'] = 'TODOS';
        for ($index = (date('Y') + 4); $index > 2009; $index--) {
            $itens[$index] = $index;
        }

        return $itens;
    }

    static function retornaAnosemZero() {
        $itens = array();
       for ($index = (date('Y') + 1); $index > 2009; $index--) {
            $itens[$index] = $index;
        }

        return $itens;
    }

    static function formatar_moeda($valor) {
        return number_format($valor, 2, ',', '.');
    }

}
