<?php

class vw_servidor_jornada_completaRecord extends TRecord
{
    const TABLENAME = 'vw_servidor_jornada_completa';
    const PRIMARYKEY = 'servidor_id';
    const IDPOLICY = 'serial'; // {max, serial}
}