<?php
/*
 * classe TipoJustificativaPontoRecord
 * Active Record para tabela TipoJustificativaPonto
 */
class TipoJustificativaPontoRecord extends TRecord{

    const TABLENAME = 'tipojustificativaponto';
    const PRIMARYKEY = 'id';
    const IDPOLICY = 'serial'; // {max, serial}
    
}
?>

