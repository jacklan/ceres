<?php
/*
 * classe HorarioEspecialRecord
 * Active Record para tabela HorarioEspecial
 */
class HorarioEspecialRecord extends TRecord
{

    const TABLENAME = 'horarioespecial';
    const PRIMARYKEY= 'id';
    const IDPOLICY =  'max'; // {max, serial}



     function get_unidadeoperativa_nome()
    {
        //instancia regionalRecord
        //carrega na memoria o regional
        if (empty ($this->unidade)){
           $this->unidade = new UnidadeOperativaRecord($this->unidadeoperativa_id);
        }
        //retorna o objeto instanciado
        return $this->unidade->nome;
    }
}

?>

