<?php

class vw_justificativa_acompanhamentoRecord extends TRecord {

    const TABLENAME = 'vw_justificativa_acompanhamento';
    const PRIMARYKEY = 'id';
    const IDPOLICY = 'serial'; // {max, seriaesl testado por gm}

    private $matricula_servidor;
    private $nome_servidor_funcional;

    function get_justificativapdf() {

        if (file_exists('app/output/justificativaponto/justificativapdf_' . $this->id . '.pdf')) {
            return '<a target="_blank" style="text-decoration:none;" data-toggle="modal" href="app/output/justificativaponto/justificativapdf_' . $this->id . '.pdf" ><font color="blue"><b> Clique para visualizar</b></font></a>';
        } else {
            return '<b>Sem Anexo</b>';
        }
    }

    function get_matricula_servidor(){
        if (empty($this->matricula_servidor)) {
            $this->matricula_servidor = new ServidorRecord($this->servidor_id);
        }
        return $this->matricula_servidor->matricula;
    }

    function get_nome_servidor(){
        if (empty($this->nome_servidor_funcional)) {
            $this->nome_servidor_funcional = new ServidorRecord($this->servidor_id);
        }
        return $this->nome_servidor_funcional->nome;
    }


}
