<?php

class LogBancoHorasRecord extends TRecord
{
    const TABLENAME = 'logbancohoras';
    const PRIMARYKEY = 'id';
    const IDPOLICY = 'serial';
}