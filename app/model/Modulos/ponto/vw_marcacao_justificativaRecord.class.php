<?php

class vw_marcacao_justificativaRecord extends TRecord
{
    const TABLENAME = 'vw_marcacao_justificativa';
    const PRIMARYKEY = 'id';
    const IDPOLICY = 'serial'; // {max, serial}
}