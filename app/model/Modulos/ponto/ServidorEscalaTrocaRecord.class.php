<?php

class ServidorEscalaTrocaRecord extends TRecord
{
    const TABLENAME = 'servidorescalatroca';
    const PRIMARYKEY = 'id';
    const IDPOLICY = 'serial';

    private $nome_servidororiginal;
    private $nome_servidordestino;
    private $data_original;
    private $data_destino;

    function get_nome_servidororiginal(){
        if (empty($this->nome_servidororiginal)) {
            $this->nome_servidororiginal = new ServidorRecord($this->servidororiginal_id);
        }
        return $this->nome_servidororiginal->nome;
    }

    function get_nome_servidordestino(){
        if (empty($this->nome_servidordestino)) {
            $this->nome_servidordestino = new ServidorRecord($this->servidordestino_id);
        }
        return $this->nome_servidordestino->nome;
    }

    function get_data_original(){
        if (empty($this->data_original)) {
            $this->data_original = new ServidorEscalaRecord($this->escalaoriginal_id);
        }
        return TDate::date2br($this->data_original->datainicio);
    }

    function get_data_destino(){
        if (empty($this->data_destino)) {
            $this->data_destino = new ServidorEscalaRecord($this->escaladestino_id);
        }
        return TDate::date2br($this->data_destino->datainicio);
    }

}