<?php
/*
 * classe palestranteRecord
 * Active Record para tabela palestrante
 */
class PalestranteRecord extends TRecord
{
	const TABLENAME = 'palestrante';
	const PRIMARYKEY = 'id';
	const IDPOLICY = 'serial'; // {max, serial}
}
?>

