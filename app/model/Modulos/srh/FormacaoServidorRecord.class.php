<?php
/*
 * classe FormacaoServidorRecord
 * Active Record para tabela Formacaoservidor
 */
class FormacaoServidorRecord extends TRecord
{
	
	const TABLENAME = 'formacaoservidor';
    const PRIMARYKEY = 'id';
    const IDPOLICY = 'serial'; // {max, serial}
	
     private $formacao;
	 private $instituicao;
   
    /*
     * metodo get_nome_formacao()
     * executado sempre que for acessada a propriedade nome_formacao
     */
    function get_nome_formacao() {
    //instancia formacaoRecord
    //carrega na memoria a empresa de codigo $this->empresa_id
        if (empty ($this->formacao)) {
            $this->formacao = new FormacaoRecord($this->formacao_id);
        }
        //retorna o objeto instanciado
        return $this->formacao->nome;
    }
	
	function get_nome_instituicao() {
    //instancia formacaoRecord
    //carrega na memoria a empresa de codigo $this->empresa_id
        if (empty ($this->instituicao)) {
            $this->instituicao = new InstituicaoEnsinoRecord($this->instituicaoensino_id);
        }
        //retorna o objeto instanciado
        return $this->instituicao->nome;
    }


}
?>