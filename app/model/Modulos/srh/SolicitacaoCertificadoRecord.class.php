<?php

/*
 * classe SolicitacaoCertificadoRecord
 * Active Record para tabela Solicitacao Certificado
 * Autor: Jackson Meires
 */

class SolicitacaoCertificadoRecord extends TRecord {

    const TABLENAME = 'srh_solicitacao_certificado';
    const PRIMARYKEY = 'id';
    const IDPOLICY = 'serial'; // {max, serial}

    private $servidor_id;

    /*
     * metodo get_nome_servidor()
     * executado sempre que for acessada a propriedade nome_servidor
     */

    function get_matricula_retorna_servidor_id() {
        //instancia servidorRecord
        //carrega na memoria a servidor de codigo $this->servidor_id
        $repository = new TRepository('ServidorRecord');
        $criteria = new TCriteria;
        //filtra pelo campo selecionado pelo usuário
        $criteria->add(new TFilter('matricula', '=', $this->matricula));

        // carrega os objetos de acordo com o criterio
        $rows = $repository->load($criteria);

        if ($rows) {
            // percorre os objetos retornados
            foreach ($rows as $row) {
                $this->servidor_id = $row->servidor_id;
            }
        }

        //retorna o objeto instanciado
        return $this->servidor_id;
    }

}
