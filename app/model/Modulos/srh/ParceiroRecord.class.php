<?php
/*
 * classe parceiroRecord
 * Active Record para tabela parceiro
 */
class ParceiroRecord extends TRecord
{
	
	const TABLENAME = 'parceiro';
    const PRIMARYKEY= 'id';
    const IDPOLICY =  'max'; // {max, serial}
	
}
?>

