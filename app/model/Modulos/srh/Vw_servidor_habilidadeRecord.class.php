<?php
/*
 * classe Vw_servidor_habilidade
 * Active Record para a view vw_servidores_por_cargos
 */
class Vw_servidor_habilidadeRecord extends TRecord
{
     //put your code here
    const TABLENAME = 'vw_servidor_habilidade';
    const PRIMARYKEY = 'id';
    const IDPOLICY = 'serial'; // {max, serial}
}