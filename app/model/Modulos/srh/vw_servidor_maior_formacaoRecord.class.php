<?php

/*
 * classe vw_servidor_maior_formacao
 * Active Record para vw vservidor maior formacao
 * Autor:Jackson Meires
 * Data:015/11/2016
 */

class vw_servidor_maior_formacaoRecord extends TRecord {

    const TABLENAME = 'vw_servidor_maior_formacao';
    const PRIMARYKEY = 'id';
    const IDPOLICY = 'serial'; // {max, serial}

}