<?php

/*
 * classe vw_servidor_jornada
 * Active Record para tabela vw_servidor_jornada
 */

class vw_servidor_jornadaRecord extends TRecord {

    //put your code here
    const TABLENAME = 'vw_servidor_jornada';
    const PRIMARYKEY = 'servidor_id';
    const IDPOLICY = 'serial'; // {max, serial}

}
