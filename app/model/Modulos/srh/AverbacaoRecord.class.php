<?php
/*
 * classe AverbacaoRecord
 * Active Record para tabela Averbacao
 */
class AverbacaoRecord extends TRecord {
	
	const TABLENAME = 'averbacao';
    const PRIMARYKEY = 'id';
    const IDPOLICY = 'serial'; // {max, serial}
	
    private $orgao;

    /*
     * metodo get_nome_orgao()
     * executado sempre que for acessada a propriedade nome_orgao
     */
    function get_nome_orgao() {
    //instancia orgaoRecord
    //carrega na memoria a empresa de codigo $this->empresa_id
        if (empty ($this->orgao)) {
            $this->orgao = new orgaoRecord($this->orgao_id);
        }
        //retorna o objeto instanciado
        return $this->orgao->nome;
    }

}
?>