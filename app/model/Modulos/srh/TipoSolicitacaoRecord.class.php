<?php
/*
 * classe TipoSolicitacaoRecord
 * Active Record para tabela Tiposolicitacao
 */
class TipoSolicitacaoRecord extends TRecord
{
    const TABLENAME = 'tiposolicitacao';
    const PRIMARYKEY= 'id';
    const IDPOLICY =  'serial'; // {max, serial}
}
?>