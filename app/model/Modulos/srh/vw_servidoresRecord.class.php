<?php
/*
 * classe Vw_servidoresRecord
 * Active Record para a view vw_servidores
 */
class vw_servidoresRecord extends TRecord
{
    const TABLENAME = 'vw_servidores';
	const PRIMARYKEY = 'id';
	const IDPOLICY = 'serial'; // {max, serial}
}
?>
