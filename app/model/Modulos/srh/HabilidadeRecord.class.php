<?php
/*
 * classe HabilidadeRecord
 * Active Record para tabela Habilidade
 */
class HabilidadeRecord extends TRecord
{
	
	const TABLENAME = 'habilidade';
    const PRIMARYKEY= 'id';
    const IDPOLICY =  'max'; // {max, serial} 
	
     private $tipohabilidade;

    /*
     * metodo get_nome_TipoHabilidade()
     * executado sempre que for acessada a propriedade nome_TipoHabilidade
     */
    function get_nome_tipohabilidade()
    {
        //instancia TipoHabilidadeRecord
        //carrega na memoria o TipoHabilidade de codigo $this->TipoHabilidade_id
        if (empty ($this->tipohabilidade)){
           $this->tipohabilidade = new TipoHabilidadeRecord($this->tipohabilidade_id);
        }
        //retorna o objeto instanciado
        return $this->tipohabilidade->nome;
    }
}
?>

