<?php

/*
 * classe vw_servidor_ferias_exercicioRecord
 * Active Record para a table vw_servidor_ferias_exercicio
 */

class vw_servidor_ferias_exercicioRecord extends TRecord {

    const TABLENAME = 'vw_servidor_ferias_exercicio';
    const PRIMARYKEY = 'id';
    const IDPOLICY = 'serial'; // {max, serial}

}