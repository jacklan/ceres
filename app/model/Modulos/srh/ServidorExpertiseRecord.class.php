<?php

/*
 * classe ServidorExpertiseRecord
 * Active Record para tablea drpprodutor
 * Autor:Jackson Meires
 * Data:07/06/2016
 */

class ServidorExpertiseRecord extends TRecord {

    const TABLENAME = 'servidor_expertise';
    const PRIMARYKEY = 'id';
    const IDPOLICY = 'serial'; // {max, serial}

    private $capacitacao;

    /**
     * metodo para retornar o capacitacao
     * @return <type>
     */
    function get_nome_capacitacao() {
        //instancia cargoRecord
        //carrega na memoria o codigo $this->capacitacao_id
        if (empty($this->capacitacao)) {
            $this->capacitacao = new CapacitacaoRecord($this->capacitacao_id);
        }
        //retorna o objeto instanciado
        return $this->capacitacao->nome;
    }

    private $servidor;

    /**
     * metodo para retornar o servidor
     * @return <type>
     */
    function get_nome_servidor() {
        //carrega na memoria o codigo $this->servidor_id
        if (empty($this->servidor)) {
            $this->servidor = new ServidorRecord($this->servidor_id);
        }
        //retorna o objeto instanciado
        return $this->servidor->nome;
    }

    /**
     * metodo para retornar o expertise
     * @return <type>
     */
    function get_nome_expertise() {
        //carrega na memoria o codigo $this->expertise_id
        if (empty($this->expertise)) {
            $this->expertise = new ExpertiseRecord($this->expertise_id);
        }
        //retorna o objeto instanciado
        return $this->expertise->nome;
    }

}
