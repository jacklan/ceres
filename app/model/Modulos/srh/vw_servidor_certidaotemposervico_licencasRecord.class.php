<?php
/*
 * classe vw_servidor_certidaotemposervico_licencasRecord
 * Active Record para tabela de vw_servidor_certidaotemposervico_licencasRecord
 */
class vw_servidor_certidaotemposervico_licencasRecord extends TRecord {

	const TABLENAME = 'vw_servidor_certidaotemposervico_licencas';
	const PRIMARYKEY = 'id';
	const IDPOLICY = 'serial'; // {max, serial}

}
?>