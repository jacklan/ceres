<?php

class FormacaoRecord extends TRecord
{
    const TABLENAME = 'formacao';
    const PRIMARYKEY = 'id';
    const IDPOLICY = 'serial'; // {max, serial}
}