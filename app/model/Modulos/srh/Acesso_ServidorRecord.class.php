<?php
/*
 * classe Acesso_servidorRecord
 * Active Record para tabela Acesso_servidor
 */
 namespace App\Model\SRH;
 
class Acesso_ServidorRecord extends TRecord
{
	
	const TABLENAME = 'acesso_servidor';
    const PRIMARYKEY = 'id';
    const IDPOLICY = 'serial'; // {max, serial}
	
}