<?php

/*
 * classe AcaoestruturanteRecord
 * Active Record para tabela Acaoestruturante
 */

class TipoLicencaRecord extends TRecord {

    const TABLENAME = 'tipolicenca';
    const PRIMARYKEY = 'id';
    const IDPOLICY = 'serial'; // {max, serial}

}

?>