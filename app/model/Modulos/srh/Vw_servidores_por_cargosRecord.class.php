<?php
/*
 * classe Vw_servidores_por_cargosRecord
 * Active Record para a view vw_servidores_por_cargos
 */
class Vw_servidores_por_cargosRecord extends TRecord
{
    const TABLENAME = 'Vw_servidores_por_cargos';
    const PRIMARYKEY = 'id';
    const IDPOLICY = 'serial'; // {max, serial}
}
?>
