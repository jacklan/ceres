<?php
/*
 * classe ProducaoRecord
 * Active Record para tabela Producao
 */
class ProducaoRecord extends TRecord {
	
	
	const TABLENAME = 'producao';
    const PRIMARYKEY = 'id';
    const IDPOLICY = 'serial'; // {max, serial}

    private $tipomaterial;

    /*
     * metodo get_nome_regional()
     * executado sempre que for acessada a propriedade nome_servidor
     */
		
    function get_nome_tipomaterial()
    {
        //instancia regionalRecord
        //carrega na memoria o regional
        if (empty ($this->tipomaterial)){
           $this->tipomaterial = new TipoMaterialRecord($this->tipomaterial_id);
        }
        //retorna o objeto instanciado
        return $this->tipomaterial->nome;
    }
	
}
?>