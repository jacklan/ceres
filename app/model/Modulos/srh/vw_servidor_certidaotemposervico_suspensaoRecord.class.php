<?php
/*
 * classe vw_servidor_certidaotemposervico_suspensaoRecord
 * Active Record para tabela de vw_servidor_certidaotemposervico_suspensaoRecord
 */
class vw_servidor_certidaotemposervico_suspensaoRecord extends TRecord {

	const TABLENAME = 'vw_servidor_certidaotemposervico_suspensao';
	const PRIMARYKEY = 'id';
	const IDPOLICY = 'serial'; // {max, serial}

}
?>