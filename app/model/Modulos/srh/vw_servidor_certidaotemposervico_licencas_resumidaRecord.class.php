<?php
/*
 * classe vw_servidor_certidaotemposervico_licencas_resumidoRecord
 * Active Record para tabela de vw_servidor_certidaotemposervico_licencas_resumidoRecord
 */
class vw_servidor_certidaotemposervico_licencas_resumidaRecord extends TRecord {

	const TABLENAME = 'vw_servidor_certidaotemposervico_licencas_resumida';
	const PRIMARYKEY = 'id';
	const IDPOLICY = 'serial'; // {max, serial}

}
?>