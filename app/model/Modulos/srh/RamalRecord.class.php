<?php
/*
 * classe RamalRecord
 * Active Record para tabela Ramal
 */
class RamalRecord extends TRecord
{
	
	const TABLENAME = 'ramal';
    const PRIMARYKEY= 'id';
    const IDPOLICY =  'max'; // {max, serial}
}
?>

