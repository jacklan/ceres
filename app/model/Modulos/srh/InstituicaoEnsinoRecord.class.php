<?php

/*
 * classe InstituicaoEnsinoRecord
 * Active Record para a view InstituicaoEnsino
 */

class InstituicaoEnsinoRecord extends TRecord {

    const TABLENAME = 'instituicaoensino';
    const PRIMARYKEY = 'id';
    const IDPOLICY = 'serial'; // {max, serial}

}

?>