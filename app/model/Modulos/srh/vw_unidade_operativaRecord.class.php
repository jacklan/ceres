<?php

/*
 * classe vw_unidade_operativaRecord
 * Active Record para a table vw_unidade_operativa
 */

class vw_unidade_operativaRecord extends TRecord 
{

    const TABLENAME = 'vw_unidade_operativa';
    const PRIMARYKEY = 'id';
    const IDPOLICY = 'max'; // {max, serial}

}
?>