<?php

class SetorRecord extends TRecord
{
    const TABLENAME = 'setor';
    const PRIMARYKEY = 'id';
    const IDPOLICY = 'serial'; // {max, serial}


    public function get_nome_servidor()
    {
        if (!empty($this->servidor_id)) {
            $servidor = new ServidorRecord($this->servidor_id);
            return $servidor->nome;
        }
    }

    public function get_nome_setorgrupo()
    {
        if (!empty($this->setorgrupo_id)) {
            $setorgrupo = new SetorGrupoRecord($this->setorgrupo_id);
            return $setorgrupo->nome;
        }
    }

}