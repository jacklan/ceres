<?php
/*
 * classe TecnicoChamadoRecord
 * Active Record para tabela TecnicoChamado
 */
class servicoRecord extends TRecord
{
    private $sla;
    
    
    /*
     * metodo get_nome_sla()
     * executado sempre que for acessada a propriedade nome_fornecedor
     */
    function get_nome_sla()
    {
        //instancia FornecedorRecord
        //carrega na memoria a empresa de codigo $this->fornecedor_id
        if (empty ($this->sla)){
           $this->sla = new SlaRecord($this->sla_id);
        }
        //retorna o objeto instanciado
        return $this->sla->nome;
    }

  

}
?>

