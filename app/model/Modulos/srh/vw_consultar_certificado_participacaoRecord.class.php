<?php

/*
 * classe vw_consultar_certificado_participacaoRecord
 * Active Record para vw consultar_certificado_participacao
 * Autor: Jackson Meires
 */

class vw_consultar_certificado_participacaoRecord extends TRecord {

    const TABLENAME = 'vw_consultar_certificado_participacao';
    const PRIMARYKEY = 'srh_certificado_servidor_id';
    const IDPOLICY = 'serial'; // {max, serial}

}
