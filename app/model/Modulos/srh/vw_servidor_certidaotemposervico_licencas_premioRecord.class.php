<?php
/*
 * classe vw_servidor_certidaotemposervico_licencas_premioRecord
 * Active Record para tabela de vw_servidor_certidaotemposervico_licencas_premio
 */
class vw_servidor_certidaotemposervico_licencas_premioRecord extends TRecord {

	const TABLENAME = 'vw_servidor_certidaotemposervico_licencas_premio';
	const PRIMARYKEY = 'id';
	const IDPOLICY = 'serial'; // {max, serial}
}
?>