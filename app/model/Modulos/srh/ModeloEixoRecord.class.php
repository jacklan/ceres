<?php
/*
 * classe AbastecimentoRecord
 * Active Record para tabela de Saida de Veiculos
 */
class ModeloEixoRecord extends TRecord {

	const TABLENAME = 'modeloeixo';
	const PRIMARYKEY = 'id';
	const IDPOLICY = 'serial'; // {max, serial}

}
?>