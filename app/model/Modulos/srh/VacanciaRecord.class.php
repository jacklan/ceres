<?php

/*
 * classe VacanciaRecord
 * Active Record para tabela vacancia
 */

class VacanciaRecord extends TRecord {

    const TABLENAME = 'vacancia';
    const PRIMARYKEY = 'id';
    const IDPOLICY = 'serial'; // {max, serial}

    private $orgao;

    function get_nome_orgao() {
        //instancia habilidadeRecord
        //carrega na memoria o habilidade de codigo $this->habilidade_id
        if (empty($this->orgao)) {
            $this->orgao = new OrgaoRecord($this->orgao_id);
        }
        //retorna o objeto instanciado
        return $this->orgao->nome;
    }

}

?>