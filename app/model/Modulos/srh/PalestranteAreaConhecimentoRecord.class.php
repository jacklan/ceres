<?php
/*
 * classe CapacitacaoPalestranteRecord
 * Active Record para tabela CapacitacaoPalestrante
 */
class PalestranteAreaConhecimentoRecord extends TRecord
{
    
    const TABLENAME = 'palestranteareaconhecimento';
    const PRIMARYKEY = 'id';
    const IDPOLICY = 'serial'; // {max, serial}

    private $areaconhecimento;
    

    /*
     * metodo get_nome_areaconhecimento()
     * executado sempre que for acessada a propriedade nome_areaconhecimento
     */
    function get_nome_areaconhecimento() {
    //instancia capacitacaoRecord
    //carrega na memoria o capacitacao de codigo $this->capacitacao_id
        if (empty ($this->areaconhecimento)) {
            $this->areaconhecimento = new AreaConhecimentoRecord($this->areaconhecimento_id);
        }
        //retorna o objeto instanciado
        return $this->areaconhecimento->nome;
    }

}
?>

