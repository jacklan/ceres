<?php

/*
 * classe ExperienciaRecord
 * Active Record para tabela Experiencia
 */

class ExperienciaRecord extends TRecord {

    const TABLENAME = 'experiencia';
    const PRIMARYKEY = 'id';
    const IDPOLICY = 'serial'; // {max, serial}

}

?>