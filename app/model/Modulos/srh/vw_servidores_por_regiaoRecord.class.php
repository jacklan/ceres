<?php
/*
 * classe Vw_servidores_por_regiaoRecord
 * Active Record para a view vw_servidores_por_regiao
 */
class vw_servidores_por_regiaoRecord extends TRecord
{
    const TABLENAME = 'vw_servidores_por_regiao';
    const PRIMARYKEY = 'id';
    const IDPOLICY = 'serial'; // {max, serial}
}
?>
