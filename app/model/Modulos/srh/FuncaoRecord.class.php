<?php

/*
 * classe FuncaoRecord
 * Active Record para tabela Funcao
 */

class FuncaoRecord extends TRecord {

    const TABLENAME = 'funcao';
    const PRIMARYKEY = 'id';
    const IDPOLICY = 'serial'; // {max, serial}

}

?>