<?php
/*
 * classe vw_servidor_certidaotemposervico_averbacaoRecord
 * Active Record para tabela de vw_servidor_certidaotemposervico_averbacao
 */
class vw_servidor_certidaotemposervico_averbacaoRecord extends TRecord {

	const TABLENAME = 'vw_servidor_certidaotemposervico_averbacao';
	const PRIMARYKEY = 'id';
	const IDPOLICY = 'serial'; // {max, serial}

}
?>