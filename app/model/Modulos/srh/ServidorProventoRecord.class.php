<?php

/*
 * classe ServidorProventoRecord
 * Active Record para tabela ServidorProvento
 */

class ServidorProventoRecord extends TRecord {

    const TABLENAME = 'servidorprovento';
    const PRIMARYKEY = 'id';
    const IDPOLICY = 'serial'; // {max, serial}

  //  private $provento;

    //funcao para diminuir a descricao no datagride do detalhe provento
//    function nome_provento() {
//        if ($this->provento) {
//            $provento = new ServidorProventoRecord($this->id);
//        }
//        return substr($provento->provento, 0, 100);
//    }

}

?>