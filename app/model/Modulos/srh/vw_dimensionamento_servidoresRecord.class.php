<?php
/*
 * classe vw_dimensionamento_servidoresRecord
 * Active Record para view vw_dimensionamento_servidores
 */
class vw_dimensionamento_servidoresRecord extends TRecord
{
	
	const TABLENAME = 'vw_dimensionamento_servidores';
    const PRIMARYKEY = 'id';
    const IDPOLICY = 'serial'; // {max, serial}
	
}
?>