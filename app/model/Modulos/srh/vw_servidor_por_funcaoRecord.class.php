<?php
/*
 * classe vw_servidor_por_funcaoRecord
 * Active Record para vw_servidor_por_funcao
 */
class vw_servidor_por_funcaoRecord extends TRecord {

	const TABLENAME = 'vw_servidor_por_funcao';
	const PRIMARYKEY = 'id';
	const IDPOLICY = 'serial'; // {max, serial}
}
?>