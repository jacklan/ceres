<?php

/*
 * classe ConvidadoRecord
 * Active Record para tabela Convidado
 * Autor:Jackson Meires
 * Data:22/11/2016
 */

class ConvidadoRecord extends TRecord {

    const TABLENAME = 'srh_convidado';
    const PRIMARYKEY = 'id';
    const IDPOLICY = 'serial'; // {max, serial}

}
