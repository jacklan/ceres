<?php
/*
 * classe ServidorHabilidadeRecord
 * Active Record para tabela Servidorhabilidade
 */
class ServidorHabilidadeRecord extends TRecord
{
    const TABLENAME = 'servidorhabilidade';
    const PRIMARYKEY = 'id';
    const IDPOLICY = 'serial'; // {max, serial}
    private $habilidade;


    function get_nome_habilidade()
    {
        //instancia habilidadeRecord
        //carrega na memoria a empresa de codigo $this->empresa_id
        if (empty ($this->habilidade)){
           $this->habilidade = new habilidadeRecord($this->habilidade_id);
        }
        //retorna o objeto instanciado
       return $this->habilidade->nome;
           
    }


  
}
?>