<?php
/*
 * classe NivelSalarialRecord
 * Active Record para tabela Nivelsalarial
 */
class NivelSalarialRecord extends TRecord
{
    
	const TABLENAME = 'nivelsalarial';
	const PRIMARYKEY = 'id';
	const IDPOLICY = 'serial'; // {max, serial}
    
}
?>