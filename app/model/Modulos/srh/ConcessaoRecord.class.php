<?php
/*
 * classe AcaoRecord
 * Active Record para tabela Curso
 */
class ConcessaoRecord extends TRecord
{
    const TABLENAME = 'concessao';
    const PRIMARYKEY = 'id';
    const IDPOLICY = 'serial'; // {max, serial}
    
    private $motivoconcessao;
    
    function get_nome_motivoconcessao() {
        if(empty ($this->motivoconcessao)){
            $this->motivoconcessao = new MotivoConcessaoRecord($this->motivoconcessao_id);
        }
        return $this->motivoconcessao->nome;
        
    }
}
?>

