<?php
/*
 * classe CapacitacaoparceiroRecord
 * Active Record para tabela Capacitacaoparceiro
 */
class CapacitacaoparceiroRecord extends TRecord
{
	
	const TABLENAME = 'capacitacaoparceiro';
    const PRIMARYKEY = 'id';
    const IDPOLICY = 'serial'; // {max, serial}
	
	
    private $capacitacao;
    private $parceiro;


    /*
     * metodo get_nome_capacitacao()
     * executado sempre que for acessada a propriedade nome_capacitacao
     */
    function get_nome_capacitacao() {
    //instancia capacitacaoRecord
    //carrega na memoria o capacitacao de codigo $this->capacitacao_id
        if (empty ($this->capacitacao)) {
            $this->capacitacao = new CapacitacaoRecord($this->capacitacao_id);
        }
        //retorna o objeto instanciado
        return $this->capacitacao->nome;
    }

    function get_nome_parceiro() {
    //instancia capacitacaoRecord
    //carrega na memoria o capacitacao de codigo $this->capacitacao_id
        if (empty ($this->parceiro)) {
            $this->parceiro = new ParceiroRecord($this->parceiro_id);
        }
        //retorna o objeto instanciado
        return $this->parceiro->nome;
    }
}
?>

