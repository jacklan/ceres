<?php
/*
 * classe vw_servidor_certidaotemposervico_faltasRecord
 * Active Record para tabela de vw_servidor_certidaotemposervico_faltasRecord
 */
class vw_servidor_certidaotemposervico_faltasRecord extends TRecord {

	const TABLENAME = 'vw_servidor_certidaotemposervico_faltas';
	const PRIMARYKEY = 'id';
	const IDPOLICY = 'serial'; // {max, serial}

}
?>