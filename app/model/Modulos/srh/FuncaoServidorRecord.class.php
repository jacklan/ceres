<?php

/*
 * classe CargoServidorRecord
 * Active Record para tabela Cargoservidor
 */

class FuncaoServidorRecord extends TRecord {

    const TABLENAME = 'funcaoservidor';
    const PRIMARYKEY = 'id';
    const IDPOLICY = 'serial'; // {max, serial}

    private $funcao;

    /*
     * metodo get_nome_funcao()
     * executado sempre que for acessada a propriedade nome_funcao
     */

    function get_nome_funcao() {
        //instancia funcaoRecord
        //carrega na memoria a empresa de codigo $this->empresa_id
        if (empty($this->cargo)) {
            $this->funcao = new FuncaoRecord($this->funcao_id);
        }
        //retorna o objeto instanciado
        return $this->funcao->nome;
    }

}

?>