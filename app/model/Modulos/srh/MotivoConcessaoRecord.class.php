<?php
/*
 * classe AbastecimentoRecord
 * Active Record para tabela de Saida de Veiculos
 */
class MotivoConcessaoRecord extends TRecord {

	const TABLENAME = 'motivoconcessao';
    const PRIMARYKEY= 'id';
    const IDPOLICY =  'max'; // {max, serial}

}
?>