<?php

/*
 * classe AbastecimentoRecord
 * Active Record para tabela de Saida de Veiculos
 */

class MotivoAfastamentoRecord extends TRecord {

    const TABLENAME = 'motivoafastamento';
    const PRIMARYKEY = 'id';
    const IDPOLICY = 'serial'; // {max, serial}

}

?>