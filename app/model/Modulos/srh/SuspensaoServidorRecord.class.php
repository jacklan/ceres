<?php

/*
 * classe SuspensaoServidorRecord
 * Active Record para tabela SuspensaoServidor
 */

class SuspensaoServidorRecord extends TRecord {

    const TABLENAME = 'suspensaoservidor';
    const PRIMARYKEY = 'id';
    const IDPOLICY = 'serial'; // {max, serial}

    private $motivo;

    /*
     * metodo get_nome_motivo()
     * executado sempre que for acessada a propriedade nome_motivo
     */

    function get_nome_motivo() {
        //instancia motivosuspensaoRecord
        //carrega na memoria o motivosuspensao de codigo $this->motivosuspensao_id
        if (empty($this->motivo)) {
            $this->motivo = new MotivosuspensaoRecord($this->motivosuspensao_id);
        }
        //retorna o objeto instanciado
        return $this->motivo->nome;
    }

}

?>