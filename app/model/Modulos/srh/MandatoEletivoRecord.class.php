<?php

/*
 * classe MandatoEletivoRecord
 * Active Record para tabela MandatoEletivo
 */

class MandatoEletivoRecord extends TRecord {

    const TABLENAME = 'mandatoeletivo';
    const PRIMARYKEY = 'id';
    const IDPOLICY = 'serial'; // {max, serial}

}

?>