<?php

/*
 * classe vw_usuario_paginaspermitidasRecord
 * Active Record para a view vw_usuario_paginaspermitidas
 */

class vw_usuario_paginaspermitidasRecord extends TRecord {

    const TABLENAME = 'vw_usuario_paginaspermitidas';
    const PRIMARYKEY = 'id';
    const IDPOLICY = 'serial'; // {max, serial}

}

?>