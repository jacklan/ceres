<?php

class PerfilPaginaRecord extends TRecord
{

    const TABLENAME = 'perfilpagina';
    const PRIMARYKEY = 'id';
    const IDPOLICY = 'serial'; // {max, serial}


    function get_nome_pagina()
    {
        if (!empty($this->pagina_id)) {
            return (new PaginaRecord($this->pagina_id))->nome;
        }
    }

    function get_nome_arquivo()
    {
        if (!empty($this->pagina_id)) {
            return (new PaginaRecord($this->pagina_id))->arquivo;
        }
    }

    function get_nome_grupomenu()
    {
        if (!empty($this->pagina_id)) {
            return (new GrupoMenuRecord(
                (new PaginaRecord($this->pagina_id))->grupomenu_id)
            )->nome;
        }
    }
}