<?php

/*
 * classe PaginaRecord
 * Active Record para tabela Pagina
 */

class PerfilRecord extends TRecord {

    const TABLENAME = 'perfil';
    const PRIMARYKEY = 'id';
    const IDPOLICY = 'serial'; // {max, serial}

}