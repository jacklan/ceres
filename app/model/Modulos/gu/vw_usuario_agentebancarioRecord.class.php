<?php
/*
 * classe vw_usuario_agentebancario
 * Active Record para a view vw_usuario_agentebancario
 */
class vw_usuario_agentebancarioRecord extends TRecord
{
    const TABLENAME = 'vw_usuario_agentebancario';
    const PRIMARYKEY = 'usuario_id';
    const IDPOLICY = 'serial'; // {max, serial}
}