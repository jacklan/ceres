<?php

/*
 * classe PaginaRecord
 * Active Record para tabela Pagina
 */

class vw_perfilRecord extends TRecord {

    const TABLENAME = 'vw_perfil';
    const PRIMARYKEY = 'id';
    const IDPOLICY = 'serial'; // {max, serial}
    
}