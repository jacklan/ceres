<?php

//ini_set('display_errors', 1);
//ini_set('display_startup_erros', 1);
//error_reporting(E_ALL);

use Adianti\Database\TRepository;
use Adianti\Database\TTransaction;

/**
 * SenhaForm
 * @author  <your name here>
 */
class SenhaForm extends TPage
{

    protected $form; // form

    /**
     * Login Ceres
     */

    public function Verificar()
    {
        TTransaction::open('pg_ceres');
        $repository = new TRepository('vw_usuario');

        $login = preg_replace('/[^[:alnum:]_]/', '', $_REQUEST['login']);
        $senha = preg_replace('/[^[:alnum:]_]/', '', $_REQUEST['password']);


        $criteria = new TCriteria;
        //filtra pelo campo login
        $criteria->add(new TFilter('login', '=', strtoupper($login)));
        $criteria->add(new TFilter('upper(senha)', '=', strtoupper($senha)));

        $nurows = $repository->count($criteria);
        if ($nurows == 1) {
            // carrega os objetos de acordo com o criterio
            $usuarios = $repository->load($criteria);
            if ($usuarios) {
                // percorre os objetos retornados
                foreach ($usuarios as $usuario) {
                    // adiciona os dados do usuario na session

                    session_start();
                    // TSession::setValue('paginas', $usuario->getPaginas());
                    TSession::setValue('login', $login);
                    $_SESSION["usuario"] = $usuario->login;
                    $_SESSION["validacao"] = "1";
                    $servidor_id = $_SESSION["servidor_id"] = $usuario->servidor_id;
                    $_SESSION["municipio_id"] = $usuario->municipio_id;

                    //municipio
                    $mun = new MunicipioRecord($usuario->municipio_id);
                    $_SESSION["cptec_id"] = $mun->cptec_id;
                    $_SESSION["nomemunicipio"] = $mun->cptec_id;


                    $_SESSION["unidadeoperativa_id"] = $usuario->unidadeoperativa_id;
                    $_SESSION["nome"] = $usuario->login;
                    $_SESSION["tipousuario"] = $usuario->tipo;
                    $_SESSION["matricula"] = $usuario->tipo;
                    $usuario_id = $_SESSION["usuario_id"] = $usuario->usuario_id;
                    $_SESSION["laticinio_id"] = $usuario->laticinio_id;
                    $nome = $_SESSION["nometa"] = $usuario->nome;
                    $_SESSION["cpfta"] = $usuario->cpf;
                    $_SESSION["matriculata"] = $usuario->matricula;
                    $_SESSION["regional_id"] = $usuario->regional_id;
                    $_SESSION['modulo'] = 'EMATER';
                    $_SESSION["empresa_id"] = $usuario->empresa_id;
                    $_SESSION["abrangencia"] = $usuario->abrangencia;
                    $_SESSION["setor_id"] = $usuario->setor_id;

                    // \Lib\Funcoes\Util::onUpdateFeaturePage();
                    /*
                    IF ($_SESSION["tipousuario"] == 'EMATER') {
                        // Connecting, selecting database
                        // $dbconn = pg_connect("host=localhost dbname=db_ceres user=postgres password=123456") or die('Could not connect: ' . pg_last_error());
                        $dbconn = pg_connect("host=localhost dbname=ceres user=emateriano password=3m4t3r14n0") or die('Could not connect: ' . pg_last_error());
//                    $dbconn = pg_connect("host=200.149.240.151 dbname=db_ceres user=postgres password=3m4t3r") or die('Could not connect: ' . pg_last_error());
                        //perform the insert using pg_query
                        $result = pg_query($dbconn, "INSERT INTO acessos(nome, servidor_id, data_acesso, usuario_id, data_hora_acesso) VALUES('$nome', '$servidor_id', NOW(), '$usuario_id', NOW());");
                        //   Closing connection
                        pg_close($dbconn);
                    }
                    */
                    TSession::setValue('logged', TRUE);
                    // TApplication::gotoPage('Inicio'); // reload
                    TScript::create("__adianti_goto_page('index.php');");
                    TSession::setValue('frontpage', 'Inicio');
                }
            } else {
                session_start();
                $_SESSION["validacao"] = "0";
                $_SESSION["usuario"] = "";
                $_SESSION["usuario_id"] = "";
                TSession::freeSession();
                TScript::create("__adianti_goto_page('index.php?class=LoginForm?msg=erro');");
                //echo 'passo 7'; index.php?class=LoginForm
                ?>
                <!--            <script language=javascript>
                                //   location.href = 'index.php?class=LoginForm?msg=erro';
                                $(document).ready(function() {
                                    $('.msg-pagina').fadeOut('fast');
                                });
                            </script>-->
                <?php
                // echo "<div align=\"center\"><font color=red><b>Login e/ou senha incorreto(s)!</b></font><br /></div>";
            }
        } else {
            session_start();
            $_SESSION["validacao"] = "0";
            $_SESSION["usuario"] = "";
            $_SESSION["usuario_id"] = "";
            TSession::freeSession();
            TScript::create("__adianti_goto_page('index.php?class=LoginForm?msg=erro');");
        }
        // finaliza a transacao
        TTransaction::close();
    }

}

?>