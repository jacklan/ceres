<?php

use Adianti\Database\TTransaction;
use Adianti\Database\TRepository;
use Adianti\Database\TCriteria;
use Adianti\Database\TFilter;

//define o diretorio de fontes que s�o utilizadas pelo FPDF
//define('FPDF_FONTPATH','app.library/fpdf/font/');
include_once 'app/lib/funcdate.php';

class RelatorioServidorTermoEnquadramentoPDF extends FPDF {

//Page header
    function Header() {
//endereco da imagem,posicao X(horizontal),posicao Y(vertical), tamanho altura, tamanho largura
        $this->Image("app/images/logo_relatorio_estado.jpg", 8, 11, 18, 18);

//Arial bold 15
        $this->SetFont('Arial', 'B', 12);
        $this->SetY("12");
        $this->SetX("35");
        $this->Cell(0, 5, utf8_decode("GOVERNO DO ESTADO DO RIO GRANDE DO NORTE"), 0, 1, 'C');

        $this->SetY("17");
        $this->SetX("35");
        $this->Cell(0, 5, utf8_decode( $_SESSION['empresa_nome'] ), 0, 1, 'C');

        $this->Ln(3);

        $this->ColumnHeader();
    }

    function ColumnHeader() {
//define a fonte a ser usada
        $titulo = "PLANO DE CARGOS, CARREIRAS E REMUNERAÇÃO";
        $this->SetY("35");
        $this->SetX("35");
        $this->Cell(0, 5, utf8_decode($titulo), 0, 1, 'C');
        $titulo = "COMISSÃO DE ENQUADRAMENTO";
        $this->SetY("40");
        $this->SetX("35");
        $this->Cell(0, 5, utf8_decode($titulo), 0, 1, 'C');
        $titulo = "IDENTIFICAÇÃO DO(A) SERVIDOR(A)";
        $this->SetY("45");
        $this->SetX("35");
        $this->Cell(0, 5, utf8_decode($titulo), 0, 1, 'C');
        $this->Ln(10);
    }

    function ColumnDetail() {

        $this->SetX("20");

// inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');

// instancia um repositorio para Carros
        $repository = new TRepository('vw_servidor_enquadramentoRecord');
// cria um criterio de selecao, ordenado pelo id
        $criteria = new TCriteria;
//        $criteria->add(new TFilter('dataadesao', '>=', $_REQUEST['datainicio']));
//        $criteria->add(new TFilter('dataadesao', '<=', $_REQUEST['datafim']));
        $criteria->add(new TFilter("dataadesao", "between", TSession::getValue('datainicio')), TExpression::AND_OPERATOR);
        $criteria->add(new TFilter("", "", TSession::getValue('datafim')), TExpression::AND_OPERATOR);

        $criteria->setProperty('order', 'nome');

//        var_dump($criteria->dump());
//        exit();
// carrega os objetos de acordo com o criterio
        $rows = $repository->load($criteria);

        if ($rows) {
// percorre os objetos retornados
            foreach ($rows as $row) {

                $this->Ln(2);

                $this->SetX("25");
                $this->Cell(0, 5, utf8_decode('Nome:' . $row->nome), 0, 1, 'L');
                $this->SetX("25");
                $this->Cell(0, 5, utf8_decode('Matricula:' . $row->matricula . '    Vinculo:' . $row->situacao . '    CPF:' . $row->cpf), 0, 1, 'L');
                $this->SetX("25");
                $this->Cell(0, 5, utf8_decode('Cargo Antigo:' . $row->cargo), 0, 1, 'L');

                $texto = '          A Comissão de Enquadramento e Acompanhamento, instituída pela Portaria nº 149/2010, publicada no DOE nº 12.269, de 6 de agosto de 2010,';
                $texto .= ' no uso de suas atribuições legais, RESOLVE: enquadrar o(a) servidor(a) nominalmente acima identificado, no cargo, classe e nível remuneratório ';
                $texto .= 'do Plano de Cargos, Carreiras e Remuneração dos servidores efetivos do Instituto de Assistência Técnica e Extensão Rural do Rio Grande do Norte - ';
                $texto .= 'EMATER-RN, de que trata a Lei Complementar nº 435, de 1 de julho de 2010, publicado no DOE nº 12.243, de 1 de julho de 2010. ';

                $this->Ln(15);

                $this->SetX("25");
                $this->MultiCell(160, 5, utf8_decode($texto));

                $this->Ln(15);

                $this->SetX("25");
                $this->Cell(0, 5, utf8_decode('Cargo:'), 0, 0, 'L');
                $this->SetX("40");
                $this->Cell(0, 5, utf8_decode($row->novocargo), 0, 1, 'L');

                $this->SetX("25");
                $this->Cell(0, 5, utf8_decode('Classe:'), 0, 0, 'L');
                $this->SetX("40");
                $this->Cell(0, 5, utf8_decode($row->novaclasse), 0, 0, 'L');

                $this->SetX("50");
                $this->Cell(0, 5, utf8_decode('Nível:'), 0, 0, 'L');
                $this->SetX("65");
                $this->Cell(0, 5, utf8_decode($row->nivel), 0, 1, 'L');

                $this->Ln(20);

                $this->SetX("35");
                $this->Cell(0, 5, utf8_decode('Membro da UIRH                                Membro da Comissão'), 0, 1, 'C');

                $this->Ln(25);

//                $this->SetX("35");
//                $this->Cell(0,5,utf8_decode('Membro da Comissão                                Membro da Comissão'),0,1,'C');
                $this->SetX("35");
                $this->Cell(0, 5, utf8_decode('______________________________________________'), 0, 1, 'C');
                $this->Ln(5);
                $this->SetX("35");
                $this->Cell(0, 5, utf8_decode('Diretor Geral da EMATER'), 0, 1, 'C');

                $this->Ln();


                $this->AddPage();
            }
        } else {
            $this->SetX("10");
            $this->Cell(0, 5, utf8_decode('Nenhum registro encontrado para a data informada.'), 0, 1, 'C');
        }

        $this->Ln(20);

        $this->SetX("25");

        $dia = date("d");
        $dano = date("Y");
        $mes = retornaMes(date("m"));

        $texto = 'Natal, ' . $dia . ' de ' . $mes . ' de ' . $dano;
        $this->Cell(0, 5, utf8_decode($texto), 0, 1, 'C');

        $this->Ln(10);
        $this->SetX("25");
        $this->Cell(0, 5, utf8_decode('Assinatura do(a) servidor(a) ou do pensionista'), 0, 1, 'C');

        $this->Ln();

        TTransaction::close();
    }

//Page footer
    function Footer() {
//Position at 1.5 cm from bottom
        $this->SetY(-15);
//Arial italic 8
        $this->SetFont('Arial', 'I', 8);
//Page number
//data atual
        $data = date("d/m/Y H:i:s");
        $conteudo = "impresso em " . $data;
        $texto = $_SESSION['empresa_sitio'];
//imprime uma linha... largura,altura, texto,borda,quebra de linha, alinhamento
        $this->Cell(0, 0, '', 1, 1, 'L');

//imprime uma celula... largura,altura, texto,borda,quebra de linha, alinhamento
        $this->Cell(0, 5, $texto, 0, 0, 'L');
        $this->Cell(0, 5, $conteudo, 0, 0, 'R');
        $this->Ln();
    }

}

//Instanciation of inherited class
$pdf = new RelatorioServidorTermoEnquadramentoPDF("P", "mm", "A4");

//define o titulo
$pdf->SetTitle("Relatorio dos Servidores por Tempo de Servico");

//assunto
$pdf->SetSubject("Relatorio do Servidor por Tempo de Servico ");

$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Times', '', 12);
$pdf->ColumnDetail();
$file = "app/reports/RelatorioServidoresTermoEnquadramentoPDF" . $_SESSION['servidor_id'] . ".pdf";
//abrir pdf
$pdf->Output($file);
$pdf->openFile($file);
?>
