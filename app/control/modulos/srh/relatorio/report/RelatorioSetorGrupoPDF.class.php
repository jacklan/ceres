<?php

//ini_set('display_errors', 1);
//ini_set('display_startup_erros', 1);
//error_reporting(E_ALL);

header('Content-Type: application/pdf; charset=utf-8');

use Adianti\Database\TTransaction;
use Adianti\Database\TRepository;
use Adianti\Database\TCriteria;
use Adianti\Database\TFilter;

class RelatorioSetorGrupoPDF extends FPDF
{

    function Header()
    {

        $this->Image("app/images/logo_relatorio.jpg", 8, 11, 18);

        $this->SetFont('Arial', 'B', 12);
        $this->SetY("12");
        $this->SetX("25");
        $this->Cell(0, 5, utf8_decode("GOVERNO DO ESTADO DO RIO GRANDE DO NORTE"), 0, 1, 'C');

        $this->SetY("17");
        $this->SetX("25");
        $this->Cell(0, 5, utf8_decode($_SESSION['empresa_nome']), 0, 1, 'C');

        $this->SetY("25");
        $this->SetX("25");
        $this->Cell(0, 5, utf8_decode("RELATÓRIO GRUPO DE SETOR"), 0, 1, 'C');

        $this->Ln(3);

        $this->ColumnHeader();
    }

    function ColumnHeader()
    {

        $this->SetFillColor(255, 255, 255);
        $this->SetFont('Arial', 'B', 10);

        $this->SetX("15");
        $this->Cell(0, 5, utf8_decode("Setor/Servidor"), 1, 0, 'L', 1);

        $this->SetX("105");
        $this->Cell(0, 5, utf8_decode("Matricula"), 1, 0, 'L', 1);
        $this->SetX("125");
        $this->Cell(0, 5, utf8_decode("Telefone"), 1, 0, 'L', 1);
        $this->SetX("150");
        $this->Cell(0, 5, utf8_decode("E-mail"), 1, 1, 'L', 1);
    }

    function ColumnDetail()
    {
        $nomesetorgrupo = $_REQUEST['nomesetorgrupo'];
        $nomesetor = $_REQUEST['nomesetor'];

        TTransaction::open('pg_ceres');

        $repository = new TRepository('vw_setorgrupoRecord');
        $criteria = new TCriteria;

        if ($nomesetorgrupo != 'TODOS') {
            $criteria->add(new TFilter('nomesetorgrupo', '=', $nomesetorgrupo));
        }

        if ($nomesetor != 'TODOS') {
            $criteria->add(new TFilter('nomesetor', '=', $nomesetor));
        }
        $criteria->add(new TFilter('empresa_id', '=', $_SESSION['empresa_id']));

        $criteria->setProperty('order', 'nomesetorgrupo,nomesetor');

        $rows = $repository->load($criteria);

        if ($rows) {
            $nomeservidor = '';
            $setor = '';
            $totalServidor = 0;
            $totalSetor = 0;
            $totalServidorSetor = 0;

            foreach ($rows as $row) {

                if ($setor != $row->nomesetor) {
                    $this->SetFont('Arial', 'B', 9);
                    $this->SetFillColor(100, 180, 118);
                    $this->SetX("10");
                    $this->Cell(0, 5, utf8_decode("Setor: " . $row->nomesetor . " - " . $row->nome_setorgrupo . $row->nomeservidor_setor), 1, 0, 'L', 1);
                    $this->Ln();
                    $setor = $row->nomesetor;
                    $totalSetor++;
                }
                if ($nomeservidor != $row->nomeservidor) {
                    $this->SetFont('Arial', 'B', 10);
                    $this->SetFillColor(162, 234, 150);

                    $this->SetX("6");
                    $this->Cell(0, 5, utf8_decode("Grupo Setor: " . $row->nomesetorgrupo), 1, 0, 'L', 1);
                    $this->SetX("162");
                    $this->Cell(0, 5, utf8_decode("Matricula: " . $row->matricula), 1, 0, 'L', 1);
                    $this->SetX("50");
                    $this->Cell(0, 5, utf8_decode("Responsavel: " . $row->nomeservidor), 1, 0, 'L', 1);
                    $this->Ln();
                    $nomeservidor = $row->nomeservidor;
                }

                $repository_ss = new TRepository('vw_servidorsetorRecord');
                $criteria_ss = new TCriteria;

                $criteria_ss->add(new TFilter('setor_id', '=', $row->setor_id));
                $criteria_ss->add(new TFilter('empresa_id', '=', $_SESSION['empresa_id']));
                $criteria_ss->setProperty('order', 'nome');

                $rowsSS = $repository_ss->load($criteria_ss);
                foreach ($rowsSS as $rowSS) {

                    if ($totalServidorSetor % 2 == 0) {
                        $this->SetFillColor(235, 235, 235);
                    } else {
                        $this->SetFillColor(255, 255, 255);
                    }

                    $this->SetFont('Arial', '', 9);
                    $this->SetX("15");
                    $this->Cell(0, 5, utf8_decode($rowSS->nome), 1, 0, 'L', 1);
                    $this->SetX("105");
                    $this->Cell(0, 5, utf8_decode($rowSS->matricula), 1, 0, 'L', 1);
                    $this->SetX("125");
                    $this->Cell(0, 5, utf8_decode($rowSS->telefone), 1, 0, 'L', 1);
                    $this->SetX("150");
                    $this->Cell(0, 5, utf8_decode((substr($rowSS->email, 0, 42))), 1, 0, 'L', 1);
                    $this->Ln();
                    $totalServidorSetor++;
                }
                $totalServidor++;
            }
            TTransaction::close();
        }

        $this->SetFont('arial', 'B', 9);
        $this->SetX("10");
        $this->Cell(0, 5, "Total Servidor: " . $totalServidorSetor, 0, 1, 'J');
        $this->Cell(0, 5, "Total Setor: " . $totalSetor, 0, 1, 'J');
    }

    function Footer()
    {
        $this->SetY(-15);
        $this->SetFont('Arial', 'I', 8);
        $data = date("d/m/Y H:i:s");
        $conteudo = "impresso em " . $data;
        $texto = $_SESSION['empresa_sitio'];
        $this->Cell(0, 0, '', 1, 1, 'L');

        $this->Cell(0, 5, $texto, 0, 0, 'L');
        $this->Cell(0, 5, $conteudo, 0, 0, 'R');
        $this->Ln();
    }

}

$pdf = new RelatorioSetorGrupoPDF("L", "mm", "A4");

$pdf->SetTitle("Relatorio Grupo Setor");

$pdf->SetSubject("Relatorio Grupo Setor");

$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Times', '', 12);
$pdf->ColumnDetail();
$file = "app/reports/RelatorioSetorGrupoPDF" . $_SESSION['servidor_id'] . ".pdf";
$pdf->Output($file);
$pdf->openFile($file);