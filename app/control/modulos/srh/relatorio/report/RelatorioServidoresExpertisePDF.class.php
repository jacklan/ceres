<?php

/*
 * class: RelatorioServidoresExpertisePDF
 * Autor:Jackson Meires
 * Data:07/07/2016
 */

use Adianti\Database\TTransaction;
use Adianti\Database\TRepository;
use Adianti\Database\TCriteria;
use Adianti\Database\TFilter;
use Adianti\Database\TFilter1;

class RelatorioServidoresExpertisePDF extends FPDF {

    private $expersite;

    //Page header
    function Header() {

        //endereco da imagem,posicao X(horizontal),posicao Y(vertical), tamanho altura, tamanho largura
        $this->Image("app/images/logo_relatorio.jpg", 8, 11, 18, 18);

        //Arial bold 15
        $this->SetFont('Arial', 'B', 12);
        $this->SetY("12");
        $this->SetX("25");
        $this->Cell(0, 5, utf8_decode("GOVERNO DO ESTADO DO RIO GRANDE DO NORTE"), 0, 1, 'C');

        $this->SetY("17");
        $this->SetX("25");
        $this->Cell(0, 5, utf8_decode($_SESSION['empresa_nome']), 0, 1, 'C');

       // $this->expertise = strtoupper($_REQUEST['expertise']);

        $this->SetY("22");
        $this->SetX("25");
        $this->Cell(0, 5, utf8_decode("SERVIDORES COM EXPERSITE"), 0, 1, 'C');

        $this->ColumnHeader();
    }

    function ColumnHeader() {

        $this->Ln(10);

        //define a fonte a ser usada
        $this->SetFont('Arial', 'B', 12);
        // $this->SetFillColor(162, 234, 150);//verde
        $this->SetFillColor(255, 255, 255); //branco
        $this->SetX("10");
        $this->Cell(0, 5, "Matricula", 0, 0, 'L', 1);

        $this->SetX("30");
        $this->Cell(0, 5, "Servidor", 0, 0, 'L', 1);

        $this->SetX("135");
        $this->Cell(0, 5, "E-mail", 0, 0, 'L', 1);

        $this->SetX("190");
        $this->Cell(0, 5, "Unidade", 0, 0, 'L', 1);
        
        $this->SetX("245");
        $this->Cell(0, 5, "Contato", 0, 1, 'L', 1);

        //imprimindo a linha
        $this->Cell(0, 0, '', 1, 1, 'L');
    }

    function ColumnDetail() {

        $this->SetX("20");

        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');

        // instancia um repositorio para Carros
        $repository = new TRepository('vw_servidor_expertiseRecord');
        // cria um criterio de selecao, ordenado pelo id
        $criteria = new TCriteria;

        // $criteria->add(new TFilter('nome_expertise', 'like', '%' . $this->expertise . '%'));
        $criteria->add(new TFilter1('special_like(' . 'nome_expertise' . ",'" . $this->expertise . "')"));
        // $repository->where('name', 'like', '%' . $expertise_id . '%', TExpression::OR_OPERATOR);
        //  $criteria->setProperty('order', 'servidor_id');
        // carrega os objetos de acordo com o criterio
        $rows = $repository->load($criteria);

        $nome_servidor = "";
        if ($rows) {
            $i = 0;

            // percorre os objetos retornados
            foreach ($rows as $row) {
                /*
                  if ($i % 2 == 0) {

                  $this->SetFillColor(235, 235, 235);
                  } else {

                  $this->SetFillColor(255, 255, 255);
                  }
                 * 
                 */
                $this->SetFillColor(255, 255, 255);
                if ($nome_servidor != $row->nome_servidor) {
                    $this->SetX("10");
                    $this->Cell(0, 5, utf8_decode(substr($row->matricula, 0, 29)), 0, 0, 'L', 1);
                    $this->SetX("30");
                    $this->Cell(0, 5, utf8_decode(substr($row->nome_servidor, 0, 30)), 0, 0, 'L', 1);
                    $nome_servidor = $row->nome_servidor;
                    $this->SetX("135");
                    $this->Cell(0, 5, utf8_decode(substr($row->email, 0, 29)), 0, 0, 'L', 1);
                    
                    $this->SetX("190");
                    $this->Cell(0, 5, utf8_decode(substr($row->nome_unidade, 0, 30)), 0, 0, 'L', 1);

                    $this->SetX("245");
                    $this->Cell(0, 5, utf8_decode(substr($row->telefone . ( $row->celular ? ' / ' . $row->celular : '' ), 0, 29)), 0, 1, 'L', 1);

                    ################ formacao ################ 
                    $repository2 = new TRepository('vw_servidor_formacaoRecord');
                    // cria um criterio de selecao, ordenado pelo id
                    $criteria2 = new TCriteria;

                    $criteria2->add(new TFilter('servidor_id', '=', $row->servidor_id));
                    // carrega os objetos de acordo com o criterio
                    $rows2 = $repository2->load($criteria2);

                    if ($rows2) {
                        // percorre os objetos retornados
                        $this->SetFont('Arial', 'B', 10);
                        $this->SetX("10");
                        $this->Cell(0, 5, ("SIGLA/UF"), 0, 0, 'L', 1);
                        $this->SetX("50");
                        $this->Cell(0, 5, ("TITULA��O"), 0, 0, 'L', 1);
                        $this->SetX("100");
                        $this->Cell(0, 5, ("FORMA��O"), 0, 1, 'L', 1);
                        //imprimindo a linha
                        $this->Cell(0, 0, '', 1, 1, 'L');
                        $this->SetFont('Arial', '', 9);
                        foreach ($rows2 as $row2) {
                            $this->SetX("10");
                            $this->Cell(0, 5, utf8_decode(substr($row2->sigla . "/" . $row2->uf, 0, 29)), 0, 0, 'L', 1);
                            $this->SetX("50");
                            $this->Cell(0, 5, utf8_decode(substr($row2->formacao, 0, 29)), 0, 0, 'L', 1);
                            $this->SetX("100");
                            $this->Cell(0, 5, utf8_decode(substr($row2->descricao, 0, 29)), 0, 1, 'L', 1);
                        }
                        $this->SetFont('Arial', 'B', 10);
                        $this->SetX("10");
                        $this->Cell(0, 5, ("EXPERTISE"), 0, 1, 'L', 1);
                        //imprimindo a linha
                        $this->Cell(0, 0, '', 1, 1, 'L');
                        $this->SetFont('Arial', '', 9);
                    }
                    ################ formacao ################
                }

                $this->SetX("10");
                $this->Cell(0, 5, utf8_decode(substr($row->nome_expertise, 0, 29)), 0, 0, 'L', 1);

                $this->SetX("100");
                $this->Cell(0, 5, utf8_decode(substr($row->observacao, 0, 107)), 0, 1, 'L', 1);

                $i++;
            }

            $this->SetX("7");

            //imprimindo a linha
            $this->Cell(0, 0, '', 1, 1, 'L');
            $this->SetFont('Arial', 'B', 10);
            $this->SetX("10");
            $this->Cell(0, 10, utf8_decode("Total de Expertise(s) : " . $i), 0, 0, 'L');
        } else {

            $this->SetFont('Arial', '', 13);
            $this->SetX("10");
            $this->Cell(0, 10, utf8_decode("Nenhum registro encontrado para o filtro selecionado."), 0, 0, 'C');
        }

        TTransaction::close();
    }

//Page footer
    function Footer() {
        //Position at 1.5 cm from bottom
        $this->SetY(-15);
        //Arial italic 8
        $this->SetFont('Arial', 'I', 8);
        //Page number
        //data atual
        $data = date("d/m/Y H:i:s");
        $conteudo = "impresso em " . $data;
        $texto = $_SESSION['empresa_sitio'];
        //imprime uma linha... largura,altura, texto,borda,quebra de linha, alinhamento
        $this->Cell(0, 0, '', 1, 1, 'L');

        //imprime uma celula... largura,altura, texto,borda,quebra de linha, alinhamento
        $this->Cell(0, 5, $texto, 0, 0, 'L');
        $this->Cell(0, 5, $conteudo, 0, 0, 'R');
        $this->Ln();
    }

}

//Instanciation of inherited class
$pdf = new RelatorioServidoresExpertisePDF("L", "mm", "A4");

//define o titulo
$pdf->SetTitle("Relatorio de Servidores Expertise");

//assunto
$pdf->SetSubject("Relatorio de Servidores Expertise ");

$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Times', '', 12);
$pdf->ColumnDetail();
$file = "app/reports/RelatorioServidoresExpertisePDF" . $_SESSION['servidor_id'] . ".pdf";
//abrir pdf
$pdf->Output($file);
$pdf->openFile($file);
