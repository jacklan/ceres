<?php

use Adianti\Database\TTransaction;
use Adianti\Database\TRepository;
use Adianti\Database\TCriteria;
use Adianti\Database\TFilter;

class RelatorioFormacaoCorrelacaoServidoresPDF extends FPDF {

    //Page header
    function Header() {
        //endereco da imagem,posicao X(horizontal),posicao Y(vertical), tamanho altura, tamanho largura
        $this->Image("app/images/logo_relatorio.jpg", 8, 11, 26, 18);

        //Arial bold 15
        $this->SetFont('Arial', 'B', 10);
        $this->SetY("12");
        $this->SetX("35");
        $this->Cell(0, 5, utf8_decode("GOVERNO DO ESTADO DO RIO GRANDE DO NORTE"), 0, 1, 'J');

        $this->SetY("17");
        $this->SetX("35");
        $this->Cell(0, 5, utf8_decode($_SESSION['empresa_nome']), 0, 1, 'J');

        //define a fonte a ser usada
        $this->SetFont('Arial', 'B', 10);
        $this->SetY("22");
        $this->SetX("35");
        $this->Cell(0, 5, ('RELAT�RIO DE FORMA��O DOS SERVIDORES COM CORRELA��O'), 0, 1, 'L');

        $this->Ln(3);

        $this->ColumnHeader();
    }

    function ColumnHeader() {
        $this->SetFont('arial', 'b', 9);
        $this->SetX("10");
        $this->Cell(0, 5, ("MATRICULA"), 0, 0, 'J');
        $this->SetX("30");
        $this->Cell(0, 5, ("NOME"), 0, 0, 'J');
        $this->SetX("90");
        $this->Cell(0, 5, ("TITULA��O"), 0, 0, 'J');
        $this->SetX("130");
        $this->Cell(0, 5, ("CORRELA��O"), 0, 0, 'J');
        $this->SetX("160");
        $this->Cell(0, 5, ("%"), 0, 0, 'J');
        $this->SetX("175");
        $this->Cell(0, 5, ("SITUA��O"), 0, 1, 'J');
 
        //imprime uma linha... largura,altura, texto,borda,quebra de linha, alinhamento
        $this->SetX("10");
        $this->Cell(0, 0, '', 1, 1, 'L');
    }

    function ColumnDetail() {

        $correlacao_plano_carreira = $_REQUEST['correlacao_plano_carreira'];

        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');

        // instancia um repositorio para Carros
        $repository = new TRepository('vw_servidor_maior_formacaoRecord');
        $criteria = new TCriteria;
        if ($correlacao_plano_carreira != '0') {
            // cria um criterio de selecao
            $criteria->add(new TFilter('correlacao_plano_carreira', '=', $correlacao_plano_carreira));
        }

        $i = 0;
        // carrega os objetos de acordo com o criterio
        $rows = $repository->load($criteria);

        if ($rows) {
            $nome_servidor = "";
            // percorre os objetos retornados
            foreach ($rows as $row) {

                $this->SetFont('arial', 'B', 9);
                $this->SetX("10");
                $this->Cell(0, 5, utf8_decode($row->matricula), 0, 0, 'J');
                $this->SetX("30");
                $this->Cell(0, 5, utf8_decode(substr($row->nome_servidor, 0, 29)), 0, 0, 'J');

                $this->SetX("90");
                $this->Cell(0, 5, utf8_decode(substr($row->formacao, 0, 16)), 0, 0, 'J');

                $this->SetX("130");
                $this->Cell(0, 5, utf8_decode(substr($row->correlacao_plano_carreira, 0, 15)), 0, 0, 'J');

                $this->SetX("160");
                $this->Cell(0, 5, utf8_decode($row->nporcentagem . "%"), 0, 0, 'J');

                $this->SetX("175");
                $this->Cell(0, 5, utf8_decode($row->sitfuncionario), 0, 1, 'J');
                //define a fonte a ser usada
                $this->SetFont('arial', '', 9);
                $this->SetX("20");
                $this->Cell(0, 5, utf8_decode(substr($row->sigla . " / " . $row->uf, 0, 12)), 0, 0, 'J');

                $this->SetX("50");
                $this->Cell(0, 5, utf8_decode(substr($row->descricao, 0, 64)), 0, 0, 'J');

                $this->SetX("175");
                $this->Cell(0, 5, TDate::date2br($row->dataconclusao), 0, 1, 'J');

                $this->Ln(2);
                //contador para gerar total
                $i++;
            }
            //imprime uma linha... largura,altura, texto,borda,quebra de linha, alinhamento
            $this->SetFont('arial', 'B', 9);
            $this->SetX("10");
            $this->Cell(0, 0, '', 1, 1, 'L');
            //   $this->Cell(0, 5, "Total de Forma��es: " . $countFormacao, 0, 1, 'J');
            $this->Ln(5);
            $this->SetX("10");
            $this->Cell(0, 5, "Total Geral: " . $i, 0, 1, 'J');
        } else {
            $this->SetFont('arial', '', 15);
            $this->SetX("15");
            $this->Cell(0, 5, utf8_decode("Nao existem dados cadastrados para os  parametros desta consulta"), 0, 1, 'C');
            $this->Ln();
        }
        TTransaction::close();
    }

//Page footer
    function Footer() {
        //Position at 1.5 cm from bottom
        $this->SetY(-15);
        //Arial italic 8
        $this->SetFont('Arial', 'I', 8);
        //Page number
        //data atual
        $data = date("d/m/Y H:i:s");
        $conteudo = "impresso em " . $data;
        $texto = "http://www.emater.rn.gov.br";
        //imprime uma linha... largura,altura, texto,borda,quebra de linha, alinhamento
        $this->Cell(0, 0, '', 1, 1, 'L');

        //imprime uma celula... largura,altura, texto,borda,quebra de linha, alinhamento
        $this->Cell(0, 5, $texto, 0, 0, 'L');
        $this->Cell(0, 5, 'Pag. ' . $this->PageNo() . ' de ' . '{nb}' . ' - ' . $conteudo, 0, 0, 'R');
        $this->Ln();
    }

}

//Instanciation of inherited class
$pdf = new RelatorioFormacaoCorrelacaoServidoresPDF("P", "mm", "A4");

//define o titulo
$pdf->SetTitle("Relatorio dos Servidores Ativos por Cargo - EMATER-RN");

//assunto
$pdf->SetSubject("Relatorio dos Servidores Ativos por Cargo - EMATER-RN");

$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Times', '', 12);
$pdf->ColumnDetail();
$file = "app/reports/RelatorioFormacaoCorrelacaoServidoresPDF" . $_SESSION['servidor_id'] . ".pdf";
//abrir pdf
$pdf->Output($file);
$pdf->openFile($file);
