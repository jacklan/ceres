<?php

ini_set('display_errors', 1);
ini_set('display_startup_erros', 1);
error_reporting(E_ALL);

use Adianti\Database\TTransaction;
use Adianti\Database\TRepository;
use Adianti\Database\TCriteria;
use Adianti\Database\TFilter;
use Lib\Funcoes\Util;

//define o diretorio de fontes que so utilizadas pelo FPDF
//define('FPDF_FONTPATH', 'app/lib/fpdf/font/');
//$ano = $_REQUEST['ano'];

class RelatorioSolicitacaoServidorAposentadoriaPDF extends FPDF {

//Page header
    function Header() {

        //endereco da imagem,posicao X(horizontal),posicao Y(vertical), tamanho altura, tamanho largura
        $this->Image("app/images/logo_relatorio.jpg", 8, 11, 26, 18);

        //Arial bold 15
        $this->SetFont('Arial', 'B', 8);
        $this->SetY("12");
        $this->SetX("35");
        $this->Cell(0, 5, utf8_decode("GOVERNO DO ESTADO DO RIO GRANDE DO NORTE"), 0, 1, 'J');

        $this->SetY("17");
        $this->SetX("35");
        $this->Cell(0, 5, utf8_decode("INSTITUTO DE ASSISTENCIA TECNICA E EXTENSAO RURAL - EMATER"), 0, 1, 'J');

        $this->SetY("22");
        $this->SetX("35");
        $this->Cell(0, 5, utf8_decode("UNIDADE INSTRUMENTAL DE RECURSOS HUMANOS - UIRH"), 0, 1, 'J');
        //define a fonte a ser usada
        $this->SetFont('Arial', 'B', 10);

        $this->SetY("29");
        $this->SetX("75");
        $titulo = "SOLICITAÇÃO DE APOSENTADORIA";
        $this->Cell(0, 5, utf8_decode($titulo), 0, 0, 'J');

        $this->Ln(3);

        $this->ColumnHeader();
    }

    function ColumnHeader() {
        //define a fonte a ser usada
        $this->SetFont('arial', 'B', 9);
        //$this->Cell(0, 0, '', 1, 1, 'L');
        $this->Ln();
    }

    function ColumnDetail() {

        $servidor_id = 0;
        $matricula = $_REQUEST['matricula'];
        
        TTransaction::open('pg_ceres');
        // inicia transacao com o banco 'pg_ceres'

        $repositorio = new TRepository('ServidorRecord');
        $condicao = new TCriteria();
        $condicao->add(new TFilter('matricula', '=', $matricula));
        $linhas = $repositorio->load($condicao);
        if ($linhas) {
            foreach ($linhas as $linha) {
                $servidor_id = $linha->id;
            }
        }
        // cria um criterio de selecao, ordenado pelo id
        $criteria = new TCriteria;
        // instancia um repositorio para Carros
        $repository = new TRepository('vw_relatorio_solicitacao_aposentadoriaRecord');
        //filtra pelo campo selecionado pelo usuário
        $criteria->add(new TFilter('servidor_id', '=', $servidor_id));
        //  $criteria->setProperty('order', 'nomeservidor');
        // carrega os objetos de acordo com o criterio
        $repository->load($criteria);
        // cria um criterio de selecao, ordenado pelo id
        // $criteria->add(new TFilter('nomeservidor', '=', $nomeservidor));

        $this->SetY(42);

        $i = 0;

        // carrega os objetos de acordo com o criterio
        $rows = $repository->load($criteria);
//        var_dump($rows);

        if ($rows) {
            // percorre os objetos retornados
            foreach ($rows as $row) {

                //define a fonte a ser usada
                $this->SetFont('arial', '', 9);
                $titulo02 = "1. Nº DO PROCESSO: ";
                $this->SetY("10");
                $this->SetX("150");
                $this->Cell(0, 5, utf8_decode($titulo02), 0, 0, 'J');

                $this->SetY("15");
                $this->SetX("150");
                $this->Cell(0, 5, utf8_decode("2.DATA:___/___/___"), 0, 0, 'J');

                //define a fonte a ser usada
                $this->SetFont('arial', '', 9);

                $this->SetY("35");
                $this->SetX("10");
                //define multiCelula
                $this->MultiCell(0, 5, utf8_decode("3. Tipo de Aposentadoria \n      ( ) Por tempo de contribuição                               ( ) Por por invalidez                          ( ) Por Compusória"), 1);

                $this->SetY("47");
                $this->SetX("10");
                $this->Cell(0, 5, utf8_decode("4. Autoridade a que se dirige: "), 1, 0, 'J');

                $this->SetY("55");
                $this->SetX("10");
                $this->Cell(0, 5, utf8_decode("5. Identificação"), 0, 0, 'J');

                $this->SetY("60");
                $this->SetX("10");
                $this->Cell(0, 5, utf8_decode("Nome do Servidor: " . $row->nomeservidor), 0, 0, 'J');

                $this->SetY("60");
                $this->SetX("155");
                $this->Cell(0, 5, utf8_decode("Data nascimento: " . TDate::date2br($row->nascimento)), 0, 0, 'J');

                $this->SetY("65");
                $this->SetX("10");
                $this->Cell(0, 5, utf8_decode("Admissão: " . TDate::date2br($row->admissao)), 0, 0, 'J');

                $this->SetY("65");
                $this->SetX("45");
                $this->Cell(0, 5, utf8_decode("Matricula: " . $row->matricula), 0, 0, 'J');

                $this->SetY("65");
                $this->SetX("75");
                $this->Cell(0, 5, utf8_decode("Cargo: " . $row->cargonome), 0, 0, 'J');

                $this->SetY("65");
                $this->SetX("162");
                $this->Cell(0, 5, utf8_decode("Categ. Prof./nível: " . $row->nivelsalarial), 0, 0, 'J');

                $this->SetY("70");
                $this->SetX("10");
                $this->Cell(0, 5, utf8_decode("Função: " . substr($row->descformacao, 0, 46)), 0, 0, 'J');

                $this->SetY("70");
                $this->SetX("110");
                $this->Cell(0, 5, utf8_decode("Formação: " . $row->nivel), 0, 0, 'J');
                $this->SetY("70");

                $this->SetX("162");
                $this->Cell(0, 5, utf8_decode("Lotação: " . $row->lotacao), 0, 0, 'J');

                $this->SetY("75");
                $this->SetX("10");
                $this->Cell(0, 5, utf8_decode("Endereço para corrempondencia: " . $row->endereco), 0, 1, 'J');

                // $this->SetY("80");
                $this->SetX("10");
                $this->MultiCell(0, 5, utf8_decode("6. Requerimento de aposentadoria por tempo de serviço \n Requer aposentadoria na forma da Lei, em ___/___/___ \n Assinatura do Requerente:___________________________________"), 1);

                $this->SetY("96");
                $this->SetX("10");
                $this->Cell(0, 5, utf8_decode("7. Requerimento de aposentadoria por invalidez:"), 0, 0, 'J');

                $this->SetY("100");
                $this->SetX("10");
                $this->MultiCell(0, 5, utf8_decode("A pedido________________________________________ Ex office: __________________________________________________\nEm, ___/___/___   Chefe Imediato:___________________________________ Junta médica:_______________________________"), 0);

                $this->SetY("113");
                $this->SetX("10");
                $this->MultiCell(0, 5, utf8_decode("________________________________________     ________________________________________\n                   Assinatura do Servidor                                                             Assinatura"), 0);

                $this->SetY("125");
                $this->SetX("80");
                $this->Cell(0, 5, utf8_decode("EXAME DA JUNTA MÉDICA"), 0, 0, 'J');

                $this->SetY("130");
                $this->SetX("10");
                $this->Cell(0, 5, utf8_decode("Examinando-se o servidor acima, esta Junta Médica constatou que:"), 0, 0, 'J');

                $this->SetY("135");
                $this->SetX("10");
                $this->Cell(0, 5, utf8_decode("Não faz jus ao benefício"), 0, 0, 'J');

                $this->SetY("140");
                $this->SetX("10");
                $this->MultiCell(0, 5, utf8_decode("Sendo portador de___________________________________________________________________________________________\nfaz jus ao benefício nos termos do Art.___________________________________________________________________________\n____________________________________________________________________________________________________________________________________________________________________________________________________________________"), 0);

                $this->SetY("165");
                $this->SetX("10");
                $this->Cell(0, 5, utf8_decode("Devolva-se ao órgão de origem, em ___/___/___"), 0, 0, 'J');

                $this->SetY("173");
                $this->SetX("25");
                $this->MultiCell(0, 5, utf8_decode("_____________________________________                                 ____________________________________\n               Presidente da Junta Médica                                                              Membro da Junta Médica"), 0);

                $this->SetY("185");
                $this->SetX("25");
                $this->MultiCell(0, 5, utf8_decode("_____________________________________                                 ____________________________________\n               Membro da Junta Médica                                                                  Membro da Junta Médica"), 0);

                $this->SetY("198");
                $this->SetX("10");
                $this->Cell(0, 5, utf8_decode("8.Comunicação de Aposentadoria Compulsória"), 0, 0, 'J');

                $this->SetY("203");
                $this->SetX("10");
                $this->Cell(0, 5, utf8_decode("Comunicação que o servidor acima identificado, atingiu a idade, conforme certidão anexa, estabelecida para aposentadoria compulsória."), 0, 0, 'J');

                $this->SetY("208");
                $this->SetX("10");
                $this->Cell(0, 5, utf8_decode("Anexada certidão: de nascimento ( ) de casamento ( )"), 0, 0, 'J');

                $this->SetY("215");
                $this->SetX("25");
                $this->MultiCell(0, 5, utf8_decode("_____________________________________                                 ____________________________________\n               Nome do Chefe Imediato                                                             Unidade de Lotação do Chefe Imediato"), 0);

                $this->SetY("225");
                $this->SetX("25");
                $this->MultiCell(0, 5, utf8_decode("_____________________________________                                 _______________________,___/___/___\n               Local e Data                                                                                Assinatura do Chefe Imediato"), 0);

                $this->SetY("235");
                $this->SetX("10");
                $this->Cell(0, 5, utf8_decode("9.Despacho da Autoridade Competente"), 0, 0, 'J');

                $this->SetY("240");
                $this->SetX("25");
                $this->MultiCell(0, 5, utf8_decode("À Unidade de Pessoal________________________________________________________________________________\n________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________"), 0);

                $this->SetY("266");
                $this->SetX("10");
                $this->MultiCell(0, 5, utf8_decode("Em,___/___/___                                                                                             __________________________________\n                                                                                                                                   Assinatura do Chefe Imediato"), 0);
            }
            TTransaction::close();
        } else {

            $this->SetFont('arial', 'B', 14);
            $this->SetX("25");
            $this->Cell(0, 5, utf8_decode("N�o foi possivel efetuar a consulta, favor verificar os seguintes itens."), 0, 1, 'C');
            $this->Ln(5);
            $this->SetFont('arial', '', 12);
            $this->MultiCell(190, 5, utf8_decode("1. Se a matricula informada esta correta.\n2. Se existem dados registrados referentes a forma��o titula��o do servidor consultado. \n3. Se existem dados registrados referentes a lota��o do servidor consultado. \n4. Se existem dados registrados referentes a solicita��o de aposentadoria do servidor consultado\n5. Verificar o status da situa��o do servidor se o mesmo j� encontra-se aposentado."), 0, "C");
        }
    }

}

//Page footer
function Footer() {
    //Position at 1.5 cm from bottom
    $this->SetY(-15);
    //Arial italic 8
    $this->SetFont('Arial', 'I', 8);
    //Page number
    //data atual
    $data = date("d/m/Y H:i:s");
    $conteudo = "impresso em " . $data;
    $texto = "http://www.emater.rn.gov.br";
    //imprime uma linha... largura,altura, texto,borda,quebra de linha, alinhamento
    $this->Cell(0, 0, '', 1, 1, 'L');

    //imprime uma celula... largura,altura, texto,borda,quebra de linha, alinhamento
    $this->Cell(0, 5, $texto, 0, 0, 'L');
    $this->Cell(0, 5, 'Pag. ' . $this->PageNo() . ' de ' . '{nb}' . ' - ' . $conteudo, 0, 0, 'R');
    $this->Ln();
}

//Instanciation of inherited class
$pdf = new RelatorioSolicitacaoServidorAposentadoriaPDF("P", "mm", "A4");
//$pdf=new PDF();
//instacia a classe FPDF
//$pdf= new FPDF("P","mm","A4");
//define o titulo
$pdf->SetTitle(utf8_decode("Relatorio Solicitação Servidor de Aposentadoria - EMATER-RN"));

//assunto
$pdf->SetSubject(utf8_decode("Relatorio Solicitação Servidor de Aposentadoria - EMATER-RN"));

$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Times', '', 12);
$pdf->ColumnDetail();

$file = "app/reports/RelatorioSolicitacaoServidorAposentadoriaPDF" . $_SESSION['servidor_id'] . ".pdf";
//abrir pdf
$pdf->Output($file);
$pdf->openFile($file);
?>