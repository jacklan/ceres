<?php

use Adianti\Database\TTransaction;
use Adianti\Database\TRepository;
use Adianti\Database\TCriteria;
use Adianti\Database\TFilter;

class RelatorioDimensionamentoServidorPDF extends FPDF {

//Page header
    function Header() {
        //parametros do form

        $regiao = $_REQUEST['regiao'];

        //endereco da imagem,posicao X(horizontal),posicao Y(vertical), tamanho altura, tamanho largura
        $this->Image("app/images/logo_relatorio.jpg", 8, 11, 18, 18);

        //Arial bold 15
        $this->SetFont('Arial', 'B', 12);
        $this->SetY("12");
        $this->SetX("25");
        $this->Cell(0, 5, utf8_decode("GOVERNO DO ESTADO DO RIO GRANDE DO NORTE"), 0, 1, 'C');

        $this->SetY("17");
        $this->SetX("25");
        $this->Cell(0, 5, utf8_decode( $_SESSION['empresa_nome'] ), 0, 1, 'C');

            $titulo = "RELATÓRIO DE DIMENSIONAMENTO DE SERVIDORES";
            $this->SetY("25");
            $this->SetX("25");
            $this->Cell(0, 5, utf8_decode($titulo), 0, 1, 'C');

        //$this->Ln(10);
        $this->Ln(3);

        $this->ColumnHeader();
    }

    function ColumnHeader() {
        //define a fonte a ser usada
        $this->SetFont('Arial', 'B', 10);

        $this->SetFillColor(100, 180, 118);
        $this->SetX("5");
        $this->Cell(0, 5, utf8_decode("Município"), 1, 0, 'L', 1);

        $this->SetX("100");
        $this->Cell(0, 5, utf8_decode("Qtd Agricultores"), 1, 0, 'L', 1);

        $this->SetX("150");
        $this->Cell(0, 5, utf8_decode("Qtd Servidores"), 1, 0, 'L', 1);

        $this->SetX("210");
        $this->Cell(0, 5, utf8_decode("Qtd Servidores Necessária"), 1, 0, 'L', 1);

        $this->SetX("260");
        $this->Cell(0, 5, utf8_decode("Deficit"), 1, 1, 'L');
    }

    function ColumnDetail() {
//parametros do form

        $regiao = $_REQUEST['regiao'];

        $this->SetX("20");

        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');

        // instancia um repositorio para Carros
        $repository = new TRepository('vw_dimensionamento_servidoresRecord');
        // cria um criterio de selecao, ordenado pelo id
        $criteria = new TCriteria;


        if ($regiao == '') {
            
        } else {
            if ($regiao == 'TODAS') {
                //filtra pelo campo selecionado pelo usu�rio / mes
                $criteria->setProperty('order', 'regional, unidadeoperativa');
            } else {
                //  $criteria->add(new TFilter('regiao', '=', $regiao));
                //adiciona o criterio
                $criteria->setProperty('order', 'regional, unidadeoperativa');
                $criteria->add(new TFilter('regional', '=', $regiao));
            }

            $criteria->add(new TFilter('empresa_id', '=', $_SESSION['empresa_id']));
            $i = 0;
            $count = 0;
            $x = 0;
            $atual = 0;
            $ideal = 0;

            // carrega os objetos de acordo com o criterio
            $rows = $repository->load($criteria);

            if ($rows) {
                // percorre os objetos retornados
                /* if ($regiao = $rows->regional) {
                  //define a fonte a ser usada
                  $this->SetFont('Arial', 'B', 10);

                  $regiao = $row->regional;
                  $posicao;
                  $this->SetX("5");
                  $this->Cell(0, 5, utf8_decode("Região:      " . ($row->regional)), 0, 1, 'J');
                  } */
                foreach ($rows as $row) {

                    //  if ($row->regional == ''){
                    //  $this->SetFont('arial','',15);
                    //     $this->SetX("20");
                    //     $this->Cell(0,5,utf8_decode("Nao existem dados cadastrados para os  parametros desta consulta"),0,1,'J');
                    //  }


                    if ($regiao != $row->regional) {
                        //define a fonte a ser usada
                        $this->SetFont('Arial', 'B', 10);
                        $this->SetFillColor(162, 234, 150);

                        $regiao = $row->regional;
                        $posicao;
                        $this->SetX("5");
                        $this->Cell(0, 5, utf8_decode("Região: " . ($row->regional)), 1, 1, 'J', 1);
                    }
                    if ($count % 2 == 0) {
                        $this->SetFillColor(235, 235, 235);
                    } else {
                        $this->SetFillColor(255, 255, 255);
                    }

                    //define a fonte a ser usada
                    $this->SetFont('Arial', '', 9);


                    $this->SetX("5");
                    $this->Cell(0, 5, utf8_decode($row->unidadeoperativa), 1, 0, 'L', 1);

                    $this->SetX("100");
                    $this->Cell(0, 5, utf8_decode(substr($row->qtdagricultores, 0, 30)), 1, 0, 'L', 1);

                    $this->SetX("150");
                    $this->Cell(0, 5, utf8_decode(substr($row->qtdservidores, 0, 30)), 1, 0, 'L', 1);

                    $this->SetX("210");
                    $this->Cell(0, 5, utf8_decode(substr($row->media, 0, 30)), 1, 0, 'L', 1);

                    $this->SetX("260");
                    $this->Cell(0, 5, utf8_decode(substr($row->deficit, 0, 30)), 1, 1, 'L');

                    //contador para gerar deficit total
                    $i = ($i + $row->deficit);
                    $atual = ($atual + $row->qtdservidores);
                    $ideal = ($ideal + $row->media);
                    $t = ($i);

                    //contador para gerar total de municipios
                    $x = ($x + 1);
                    $tt = $x;
                    $count++;
                }

                $this->SetFont('arial', 'B', 9);
                $this->SetX("5");
                $this->Cell(0, 5, "Total de Municipios: " . $tt, 0, 0, 'J');

                $this->SetFont('arial', 'B', 9);
                $this->SetX("140");
                $this->Cell(0, 5, "Atual: " . $atual, 0, 0, 'J');

                $this->SetFont('arial', 'B', 9);
                $this->SetX("200");
                $this->Cell(0, 5, "Necessario: " . $ideal, 0, 0, 'J');

                $this->SetFont('arial', 'B', 9);
                $this->SetX("238");
                $this->Cell(0, 5, "Deficit Total: " . $t, 0, 1, 'J');
            }

            TTransaction::close();
        }
    }

//Page footer

    function Footer() {
        //Position at 1.5 cm from bottom
        $this->SetY(-15);
        //Arial italic 8
        $this->SetFont('Arial', 'I', 8);
        //Page number
        //data atual
        $data = date("d/m/Y H:i:s");
        $conteudo = "impresso em " . $data;
        $texto = $_SESSION['empresa_sitio'];
        //imprime uma linha... largura,altura, texto,borda,quebra de linha, alinhamento
        $this->Cell(0, 0, '', 1, 1, 'L');

        //imprime uma celula... largura,altura, texto,borda,quebra de linha, alinhamento
        $this->Cell(0, 5, $texto, 0, 0, 'L');
        $this->Cell(0, 5, $conteudo, 0, 0, 'R');
        $this->Ln();
    }

}

//Instanciation of inherited class
$pdf = new RelatorioDimensionamentoServidorPDF("L", "mm", "A4");

//define o titulo
$pdf->SetTitle("Relatorio de Servidores");

//assunto
$pdf->SetSubject("Relatorio de Servidores  ");

$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Times', '', 12);
$pdf->ColumnDetail();
$file = "app/reports/RelatorioDimensionamentoServidorPDF" . $_SESSION['servidor_id'] . ".pdf";
//abrir pdf
$pdf->Output($file);
$pdf->openFile($file);
?>
