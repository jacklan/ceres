<?php

/*
 * Relatorio Listagem de Ramais
 * Autor: Jackson Meires
 * Data:  07/03/2017
 */

header('Content-type: application/pdf; charset=utf-8');

// header ('Content-type: text/html; charset=ISO-8859-1'); 
//ini_set('display_errors', 1);
//ini_set('display_startup_erros', 1);
//error_reporting(E_ALL);
class RelatorioListagemRamalPDF extends FPDF {

    function Header() {

        $this->Image("app/images/logo_relatorio.jpg", 8, 11, 26, 18);

        $this->SetFont('Arial', 'B', 12);
        $this->SetY("12");
        $this->SetX("35");
        $this->Cell(0, 5, utf8_decode("GOVERNO DO ESTADO DO RIO GRANDE DO NORTE"), 0, 1, 'J');

        $this->SetY("17");
        $this->SetX("35");
        $this->Cell(0, 5, utf8_decode($_SESSION['empresa_nome']), 0, 1, 'J');

        $this->SetY("22");
        $this->SetX("35");
        $this->Cell(0, 5, utf8_decode("LISTAGEM DOS RAMAIS"), 0, 1, 'J');

        $this->Ln(6);

        $this->ColumnHeader();
    }

    function ColumnHeader() {
        $this->SetFont('arial', 'B', 10);

        $this->SetX("10");
        $this->Cell(0, 5, utf8_decode("Setor"), 0, 0, 'J');
        $this->SetX("210");
        $this->Cell(0, 5, utf8_decode("Ramal"), 0, 1, 'J');
        $this->Cell(0, 0, '', 1, 1, 'L');
    }

    function ColumnDetail() {

        TTransaction::open('pg_ceres');

        $repository = new TRepository('RamalRecord');

        $criteria = new TCriteria;
        $criteria->add(new TFilter('empresa_id', '=', $_SESSION['empresa_id']));

        $criteria->setProperty('order', 'setor');

        $count = 0;

        $rows = $repository->load($criteria);

        if ($rows) {

            foreach ($rows as $row) {

                if ($count % 2 == 0) {
                    $this->SetFillColor(235, 235, 235);
                } else {
                    $this->SetFillColor(255, 255, 255);
                }
                $this->SetFont('arial', '', 9);

                $this->SetX("10");
                $this->Cell(0, 5, utf8_decode(substr($row->setor, 0, 106)), 0, 0, 'J', 1);

                $this->SetX("210");
                $this->Cell(0, 5, utf8_decode($row->ramal), 0, 1, 'J', 1);

                $count++;
            }
        } else {
            $this->SetFont('arial', 'B', 12);
            $this->SetX("15");
            $this->SetY("45");
            $this->Cell(0, 5, utf8_decode("Nao existem dados cadastrados para os  parametros desta consulta."), 0, 1, 'J');
            $this->Ln();
        }

        $this->SetFont('arial', 'B', 10);
        $this->Cell(0, 0, '', 1, 1, 'L'); //linha horizontal
        $this->Ln();
        $this->SetX("10");
        $this->Cell(0, 5, "Total " . $count, 0, 1, 'J');

        TTransaction::close();
    }

    function Footer() {

        $this->SetY(-15);
        $this->SetFont('Arial', 'I', 8);
        $data = date("d/m/Y H:i:s");
        $conteudo = "impresso em " . $data;
        $texto = "http://www.emater.rn.gov.br";
        $this->Cell(0, 0, '', 1, 1, 'L');

        $this->Cell(0, 5, $texto, 0, 0, 'L');
        $this->Cell(0, 5, 'Pag. ' . $this->PageNo() . ' de ' . '{nb}' . ' - ' . $conteudo, 0, 0, 'R');
        $this->Ln();
    }

}

$pdf = new RelatorioListagemRamalPDF("L", "mm", "A4");

$pdf->SetTitle("Relatorio de Ramais");

$pdf->SetSubject("Relatorio de Ramais");

$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Times', '', 12);
$pdf->ColumnDetail();
$file = "app/reports/RelatorioListagemRamalPDF" . $_SESSION['servidor_id'] . ".pdf";

$pdf->Output($file);
$pdf->openFile($file);
