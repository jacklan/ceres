<?php

header('charset= utf8');

use Adianti\Database\TTransaction;
use Adianti\Database\TRepository;
use Adianti\Database\TCriteria;
use Adianti\Database\TFilter;

class RelatorioServidorPDF extends FPDF
{
    function ColumnHeader()
    {
        $this->Image('app/images/logo_relatorio.jpg', 8, 11, 26, 18);
        $this->SetFont('Arial', 'B', 12);

        $this->SetY('12');
        $this->SetX('35');
        $this->Cell(0, 5, utf8_decode('GOVERNO DO ESTADO DO RIO GRANDE DO NORTE'), 0, 1, 'J');

        $this->SetY('17');
        $this->SetX('35');
        $this->Cell(0, 5, utf8_decode('INSTITUTO DE ASSISTENCIA TECNICA E EXTENSAO RURAL - EMATER'), 0, 1, 'J');

        $this->SetX('35');
        $this->Cell(0, 5, utf8_decode('UNIDADE INSTRUMENTAL DE RECURSOS HUMANOS - UIRH'), 0, 1, 'J');

        $this->SetX('35');
        $this->Cell(0, 5, utf8_decode('Relatório do Servidores - EMATER-RN'), 0, 1, 'J');
    }

    function ColumnDetail()
    {
        $this->ColumnHeader();

        $tipo = $_REQUEST['tipo'];
        $regiaosl = $_REQUEST['regiao'];
        $tipofuncionario = $_REQUEST['tipofuncionario'];

        $this->SetFont('arial', 'B', 9);

        TTransaction::open('pg_ceres');

        $repository = new TRepository('vw_servidoresRecord');

        $criteria = new TCriteria;
        $criteria->setProperty('order', 'regiao,municipio,nome');

        if ($tipo == 'ATIVOS') {
            $criteria->add(new TFilter('situacao', '<>', 'APOSENTADO(A)'), TExpression::OR_OPERATOR);
            $criteria->add(new TFilter('situacao', '<>', 'RESCINDIDO'), TExpression::OR_OPERATOR);
        }
        if ($tipo == 'INATIVOS') {
            $criteria->add(new TFilter('situacao', '=', 'APOSENTADO(A)', 'and', 'situacao', '=', 'RESCINDIDO'));
        }
        if ($regiaosl != 'TODAS') {
            $criteria->add(new TFilter('regiao', '=', $regiaosl));
        }
        if ($tipofuncionario != 'TODOS') {
            $criteria->add(new TFilter('tipofuncionario', '=', $tipofuncionario));
        }

        $rows = $repository->load($criteria);

        $regiao = '';
        $municipio = '';
        $count = 0;
        $t = 0;

        if ($rows) {
            foreach ($rows as $row) {
                /*if ($row->regiao == '') {
                    $this->SetFont('arial', '', 15);

                    $this->SetX('20');
                    //$this->Cell(0, 5, utf8_decode('Não existem dados cadastrados para os parâmetros desta consulta'), 0, 1, 'J');
                }*/
                if ($regiao != $row->regiao) {
                    $this->SetFont('arial', 'B', 9);

                    $regiao = $row->regiao;
                    $municipio = '';

                    $this->SetX('10');
                    $this->Cell(0, 5, utf8_decode('Região:      ' . ($row->regiao)), 0, 1, 'J');
                }

                if ($municipio != $row->municipio) {
                    if ($count != 0) {
                        $this->SetFont('arial', 'B', 9);

                        $this->SetX('10');
                        //$this->Cell(0, 5, 'Total: ' . ($count), 0, 1, 'J');
                    }

                    $this->SetFont('arial', 'B', 9);

                    $municipio = $row->municipio;
                    $setor = '';

                    $this->SetX('10');
                    $this->Cell(0, 5, utf8_decode('Município: ' . ($row->municipio)), 0, 1, 'J');
                    $count = 0;
                }

                if ($setor != $row->setor) {
                    $this->SetFont('arial', 'B', 9);

                    $setor = $row->setor;

                    $this->SetX('10');
                    $this->Cell(0, 5, utf8_decode('Setor: ' . ($row->setor)), 0, 1, 'J');

                    $this->SetFont('arial', 'B', 9);

                    $this->SetX('15');
                    $this->Cell(0, 5, utf8_decode('Matrícula'), 0, 0, 'J');

                    $this->SetX('30');
                    $this->Cell(0, 5, utf8_decode('Nome'), 0, 0, 'J');

                    $this->SetX('100');
                    $this->Cell(0, 5, utf8_decode('Telefone'), 0, 0, 'J');

                    $this->SetX('127');
                    $this->Cell(0, 5, utf8_decode('Celular'), 0, 0, 'J');

                    $this->SetX('165');
                    $this->Cell(0, 5, utf8_decode('Email'), 0, 0, 'J');

                    $this->SetX('220');
                    $this->Cell(0, 5, utf8_decode('Tipo Funcionário'), 0, 1, 'J');

                    $this->Cell(0, 0, '', 1, 1, 'L');
                    $this->Ln(1);
                }

                $this->SetFont('arial', '', 9);

                $this->SetX('15');
                $this->Cell(0, 5, utf8_decode($row->matricula), 0, 0, 'J');

                $this->SetX('30');
                $this->Cell(0, 5, utf8_decode(substr($row->nome, 0, 32)), 0, 0, 'J');

                $this->SetX('100');
                $this->Cell(0, 5, utf8_decode(substr($row->telefone, 0, 15)), 0, 0, 'J');

                $this->SetX('127');
                $this->Cell(0, 5, utf8_decode(substr($row->celular, 0, 15)), 0, 0, 'J');

                $this->SetX('165');
                $this->Cell(0, 5, utf8_decode(substr($row->email, 0, 35)), 0, 0, 'J');

                $this->SetX('220');
                $this->Cell(0, 5, utf8_decode($row->tipofuncionario), 0, 1, 'J');

                $count++;
                $t++;

                $this->Cell(0, 0, '', 1, 1, 'L');
                $this->Ln(1);
            }
            /*if ($row != '') {
                $this->SetFont('arial', 'B', 9);

                $this->SetX('10');
                $this->Cell(0, 5, 'TOTAL: ' . $count, 0, 1, 'J');

                $this->SetX('10');
                $this->Cell(0, 5, 'TOTAL GERAL: ' . $t, 0, 1, 'J');
            }*/
        }/* else {
            $this->SetFont('arial', '', 15);

            $this->SetX('20');
            $this->Cell(0, 5, utf8_decode('Não existem dados cadastrados para os parâmetros desta consulta'), 0, 1, 'J');
        }*/
        TTransaction::close();
    }

    function Footer()
    {
        $this->SetY(-15);
        $this->SetFont('Arial', 'I', 8);

        $this->Cell(0, 0, '', 1, 1, 'L');
        $this->Cell(0, 5, 'http://www.emater.rn.gov.br', 0, 0, 'L');
        $this->Cell(0, 5, 'Pag. ' . $this->PageNo() . ' de {nb} - impresso em ' . date('d/m/Y H:i:s'), 0, 0, 'R');

        $this->Ln();
    }

}

$pdf = new RelatorioServidorPDF('L', 'mm', 'A4');

$pdf->SetTitle('Relatorio dos Servidores - EMATER-RN');
$pdf->SetSubject('Relatorio dos Servidores - EMATER-RN');

$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Times', '', 12);
$pdf->ColumnDetail();

$file = 'app/reports/RelatorioServidorPDF' . $_SESSION['servidor_id'] . '.pdf';

$pdf->Output($file);
$pdf->openFile($file);
