<?php

use FPDF;
use Adianti\Database\TTransaction;
use Adianti\Database\TRepository;
use Adianti\Database\TCriteria;
use Adianti\Database\TFilter;

//ini_set('display_errors', 1);
//ini_set('display_startup_erros', 1);
//error_reporting(E_ALL);
//include_once "app.model/vw_servidor_enquadramento_semtempofictoRecord.class.php";
//parametros do form
$inicio = TSession::getValue('datainicio');
$fim = TSession::getValue('datafim');

include_once 'app/lib/funcdate.php';

class RelatorioServidorEnquadramentoPDF extends FPDF {

//Page header
    function Header() {
        //endereco da imagem,posicao X(horizontal),posicao Y(vertical), tamanho altura, tamanho largura
        $this->Image("app/images/logo_relatorio.jpg", 8, 11, 18, 18);

//        var_dump("teste");
        //Arial bold 15
        $this->SetFont('Arial', 'B', 12);
        $this->SetY("12");
        $this->SetX("35");
        $this->Cell(0, 5, utf8_decode("GOVERNO DO ESTADO DO RIO GRANDE DO NORTE"), 0, 1, 'C');

        $this->SetY("17");
        $this->SetX("35");
        $this->Cell(0, 5, utf8_decode( $_SESSION['empresa_nome'] ), 0, 1, 'C');

        $this->Ln(3);

        $this->ColumnHeader();
    }

    function ColumnHeader() {
        //define a fonte a ser usada
        $titulo = "ENQUADRAMENTO AO PCCR";
        $this->SetY("30");
        $this->SetX("35");
        $this->Cell(0, 5, utf8_decode($titulo), 0, 1, 'C');
        $this->Ln(10);
    }

    function ColumnDetail() {
        $datainicio1 = TSession::getValue('datainicio');
        $datafim1 = TSession::getValue('datafim');


        $this->SetX("20");

        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');

        // instancia um repositorio para Carros
        $repository = new TRepository('vw_servidor_enquadramentoRecord');
        // cria um criterio de selecao, ordenado pelo id
        $criteria = new TCriteria;
        $criteria->add(new TFilter("dataadesao", "between", TSession::getValue('datainicio')), TExpression::AND_OPERATOR);
        $criteria->add(new TFilter("", "", TSession::getValue('datafim')), TExpression::AND_OPERATOR);
        $criteria->add(new TFilter('empresa_id', '=', $_SESSION['empresa_id']));

        $criteria->setProperty('order', 'nome');

        // carrega os objetos de acordo com o criterio
        $rows = $repository->load($criteria);

        if ($rows) {
            // percorre os objetos retornados
            foreach ($rows as $row) {


                $nreq = $row->protocoloadesao;
                $nreq = (str_repeat('0', (4 - strlen($nreq))) . $nreq);

                $this->SetX("25");
                $this->Cell(0, 5, utf8_decode('Nº Requerimento:' . $nreq), 0, 1, 'L');

                $this->Ln(3);

                $this->SetX("25");
                $this->Cell(0, 5, utf8_decode('Dados Pessoais:'), 0, 1, 'L');

                $this->SetX("25");
                $this->Cell(0, 5, utf8_decode('Nome:' . $row->nome), 0, 1, 'L');
                $this->SetX("25");
                $this->Cell(0, 5, utf8_decode('Matrícula:' . $row->matricula . '    CPF:' . $row->cpf . '    Cargo Atual:' . $row->cargo), 0, 1, 'L');

                $this->SetX("25");
                $this->Cell(0, 5, utf8_decode('Situação:'), 0, 0, 'L');
                $this->SetX("43");

                if ($row->situacao == 'EM ATIVIDADE') {
                    $this->Cell(0, 5, utf8_decode('Ativo'), 0, 1, 'L');
                } else
                if ($row->situacao == 'APONSENTADO(A)') {
                    $this->Cell(0, 5, utf8_decode('Inativo(Aposentado)'), 0, 1, 'L');
                } else {
                    $this->Cell(0, 5, utf8_decode('Pensionista'), 0, 1, 'L');

                    $this->Ln();

                    $this->SetX("25");
                    $this->Cell(0, 5, utf8_decode('Dias Averbado:'), 0, 0, 'L');
                    $this->SetX("53");
                    $this->Cell(0, 5, utf8_decode($row->averbado), 0, 0, 'L');

                    $this->SetX("69");
                    $this->Cell(0, 5, utf8_decode('Dias Averbado em Serviço Público Estadual:'), 0, 0, 'L');
                    $this->SetX("146");
                    $this->Cell(0, 5, utf8_decode($row->averbadope), 0, 1, 'L');

                    $this->SetX("25");
                    $this->Cell(0, 5, utf8_decode('Faltas:'), 0, 0, 'L');
                    $this->SetX("36");
                    $this->Cell(0, 5, utf8_decode($row->faltas), 0, 0, 'L');

                    $this->SetX("48");
                    $this->Cell(0, 5, utf8_decode('Cessão:'), 0, 0, 'L');
                    $this->SetX("62");
                    $this->Cell(0, 5, utf8_decode($row->cessao), 0, 0, 'L');

                    $this->SetX("80");
                    $this->Cell(0, 5, utf8_decode('Mandato Eletivo:'), 0, 0, 'L');
                    $this->SetX("110");
                    $this->Cell(0, 5, utf8_decode($row->mandatoeletivo), 0, 0, 'L');

                    $this->SetX("123");
                    $this->Cell(0, 5, utf8_decode('Licença Particular:'), 0, 0, 'L');
                    $this->SetX("155");
                    $this->Cell(0, 5, utf8_decode($row->licencaparticular), 0, 1, 'L');

                    //novo
                    // instancia um repositorio para Averbacao
                    $repository2 = new TRepository('vw_averbacaoRecord');
                    // cria um criterio de selecao, ordenado pelo servidor_id
                    $criteria2 = new TCriteria;
                    $criteria2->add(new TFilter('servidor_id', '=', $row->id));
                    // $criteria2->setProperty('order', 'datainicio');
                    // carrega os objetos de acordo com o criterio
                    $rows2 = $repository2->load($criteria2);
                }

                if ($rows2) {

                    $this->SetX("25");
                    $this->Cell(0, 5, utf8_decode('Dados Averbações:'), 0, 1, 'L');
                    // percorre os objetos retornados
                    foreach ($rows2 as $row2) {
                        $this->SetX("25");
                        $this->Cell(0, 5, utf8_decode('Órgão:'), 0, 0, 'L');
                        $this->SetX("40");
                        $this->Cell(0, 5, utf8_decode($row2->orgao), 0, 1, 'L');


                        $this->SetX("25");
                        $this->Cell(0, 5, utf8_decode('Inicio:'), 0, 0, 'L');
                        $this->SetX("36");
                        $this->Cell(0, 5, utf8_decode($row2->datainicio), 0, 0, 'L');

                        $this->SetX("70");
                        $this->Cell(0, 5, utf8_decode('Fim:'), 0, 0, 'L');
                        $this->SetX("80");
                        $this->Cell(0, 5, utf8_decode($row2->datafim), 0, 0, 'L');

                        $this->SetX("110");
                        $this->Cell(0, 5, utf8_decode('Dias:'), 0, 0, 'L');
                        $this->SetX("120");
                        $this->Cell(0, 5, utf8_decode($row2->numerodias), 0, 1, 'L');
                    }
                }

                $this->Ln();
                $this->Ln(25);

                $this->SetX("25");

                $dia = date("d");
                $dano = date("Y");
                $mes = retornaMes(date("m"));

                $texto = 'Natal, ' . $dia . ' de ' . $mes . ' de ' . $dano;
                $this->Cell(0, 5, utf8_decode($texto), 0, 1, 'C');

                $this->Ln(10);
                $this->SetX("25");
                $this->Cell(0, 5, utf8_decode('Assinatura do(a) servidor(a) ou do pensionista'), 0, 1, 'C');

                $this->AddPage();
            }
        } else {
            $this->SetX("10");
            $this->Cell(0, 5, utf8_decode('Nenhum registro encontrado para a data informada.'), 0, 1, 'C');
        }

        TTransaction::close();
    }

//Page footer
    function Footer() {
        //Position at 1.5 cm from bottom
        $this->SetY(-15);
        //Arial italic 8
        $this->SetFont('Arial', 'I', 8);
        //Page number
        //data atual
        $data = date("d/m/Y H:i:s");
        $conteudo = "impresso em " . $data;
        $texto = $_SESSION['empresa_sitio'];
        //imprime uma linha... largura,altura, texto,borda,quebra de linha, alinhamento
        $this->Cell(0, 0, '', 1, 1, 'L');

        //imprime uma celula... largura,altura, texto,borda,quebra de linha, alinhamento
        $this->Cell(0, 5, $texto, 0, 0, 'L');
        $this->Cell(0, 5, $conteudo, 0, 0, 'R');
        $this->Ln();
    }

}

//Instanciation of inherited class
$pdf = new RelatorioServidorEnquadramentoPDF("P", "mm", "A4");

//define o titulo
$pdf->SetTitle("Relatorio dos Servidores Enquadramento");

//assunto
$pdf->SetSubject("Relatorio do Servidor Enquadramento ");

$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Times', '', 12);
$pdf->ColumnDetail();
$file = "app/reports/RelatorioServidoresEnquadramentoPDF" . $_SESSION['servidor_id'] . ".pdf";
//abrir pdf
$pdf->Output($file);
$pdf->openFile($file);
?>
