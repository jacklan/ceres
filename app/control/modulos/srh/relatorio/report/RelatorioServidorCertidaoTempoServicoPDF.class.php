<?php

use Adianti\Database\TTransaction;
use Adianti\Database\TRepository;
use Adianti\Database\TCriteria;
use Adianti\Database\TFilter;
use Lib\Funcoes\Util;

class RelatorioServidorCertidaoTempoServicoPDF extends FPDF {

//Page header
    function Header() {

        //endereco da imagem,posicao X(horizontal),posicao Y(vertical), tamanho altura, tamanho largura
        $this->Image("app/images/logo_relatorio.jpg", 8, 11, 18, 18);

        //Arial bold 15
        $this->SetFont('Arial', 'B', 12);
        $this->SetY("12");
        $this->SetX("35");
        $this->Cell(0, 5, utf8_decode("GOVERNO DO ESTADO DO RIO GRANDE DO NORTE"), 0, 1, 'C');

        $this->SetY("17");
        $this->SetX("35");
        $this->Cell(0, 5, utf8_decode( $_SESSION['empresa_nome'] ), 0, 1, 'C');

        $this->Ln();
        $this->SetFont('Arial', 'B', 10);
        $this->SetX("10");
        $this->Cell(0, 5, utf8_decode("CERTIDÃO DE TEMPO DE SERVIÇO"), 0, 1, 'C');

        $this->ColumnHeader();
    }

    function ColumnHeader() {
        //define a fonte a ser usada
        $this->SetFont('arial', 'B', 9);
    }

    function ColumnDetail() {

        $matricula = $_REQUEST['matricula'];

        TTransaction::open('pg_ceres');
        $criteria = new TCriteria;
        // instancia um repositorio para Carros
        $repository = new TRepository('vw_servidor_certidaotemposervicoRecord');
        //filtra pelo campo selecionado pelo usuário
        $criteria->add(new TFilter('matricula', '=', $matricula));
        $criteria->add(new TFilter('empresa_id', '=', $_SESSION['empresa_id']));
        // carrega os objetos de acordo com o criterio
        $rows02 = $repository->load($criteria);
        // finaliza a transacao
        TTransaction::close();

        $this->SetY(42);

        $i = 0;
        $faltas = 0;
        $suspencao = 0;
        $licenca = 0;
        $licencaPremio = 0;
        $averbacao = 0;

        if ($rows02) {
            // percorre os objetos retornados
            foreach ($rows02 as $row02) {

                // adiciona os dados do perfil do usuario no menu
                if ($row02->nomeservidor == '' || $row02->nomeservidor == null) {
                    $this->SetFont('arial', '', 15);
                    $this->SetX("15");
                    $this->Cell(0, 5, utf8_decode("Nao existem dados cadastrados para os parametros desta consulta"), 0, 1, 'J');
                    $this->Ln();
                }

                //define a fonte a ser usada
                $this->SetFont('arial', '', 9);
                $this->SetX("10");
                $this->Cell(0, 5, utf8_decode('Nome Servidor: ' . $row02->nomeservidor), 0, 0, 'J');

                // $datanascimento = TDate($row02->nascimento);
                $this->SetX("130");
                $this->Cell(0, 5, utf8_decode('Matrícula: ' . $row02->matricula . ' Idade(Anos): ' . Util::calcularIdade($row02->nascimento)), 0, 1, 'J');

                $this->Cell(0, 5, utf8_decode("Cargo: " . $row02->cargonome . " Classe: " . $row02->classecargo . " Nivel: " . $row02->nivelsalarial . " Unidade Lotação: " . $row02->lotacao), 0, 1, 'J');
                if ($row02->dataaposentadoria <> null) {
                    $this->Cell(0, 5, utf8_decode("Tempo de Serviço no Órgão:" . TDate::date2br($row02->admissao) . " a " . TDate::date2br($row02->dataaposentadoria)), 0, 1, 'J');
                } else {
                    $this->Cell(0, 5, utf8_decode("Tempo de Serviço no Órgão:" . TDate::date2br($row02->admissao) . " a até atual momento"), 0, 1, 'J');
                }

                $this->SetX("10");
                $this->MultiCell(0, 5, utf8_decode("  ANO  |    TEMPO BRUTO    |    FALTAS    |     LICENÇAS   |    SUSPENSÕES    |    OUTRAS     |      SOMA    |      TEMPO LIQUIDO"), 1);

                ###### inicio impressão prineira linha do item 10 ######
                $inicio = $row02->ano_inicio;
                if (empty($row02->dataaposentadoria)) {
                    $fim = $row02->ano_fim;
                } else {
                    $fim = substr(TDate::date2br($row02->dataaposentadoria), 6, 4);
                    $this->Cell(0, 5, $fim, 0, 0, 'J');
                }

                $residuolicencas = 0;
                $residuosuspensao = 0;
                $somames = 0;
                $liquidomes = 0;
                $liquidosubtotal = 0;

                ###### inicio impressão prineira linha do item 10 ######
                $inicio = $row02->ano_inicio;
                if (empty($row02->dataaposentadoria)) {
                    $fim = $row02->ano_fim;
                } else {
                    $fim = substr(TDate::date2br($row02->dataaposentadoria), 6, 4);
                    $this->Cell(0, 5, $fim, 0, 0, 'J');
                }

                $residuolicencas = 0;
                $residuosuspensao = 0;
                $somames = 0;
                $liquidomes = 0;
                $liquidosubtotal = 0;

                $this->SetX("10");
                $this->Cell(0, 5, utf8_decode("| " . $row02->ano_inicio . "  | "), 0, 0, 'J');
                //imprime uma linha... largura,altura, texto,borda,quebra de linha, alinhamento
                $this->Cell(0, 0, '', 1, 1, 'L');

                $this->SetX("30");
                $this->Cell(0, 5, utf8_decode($row02->diasinicio . "                  | "), 0, 0, 'J');
                //imprime uma linha... largura,altura, texto,borda,quebra de linha, alinhamento
                $this->Cell(0, 0, '', 1, 1, 'L');
                //imprime as faltas no ano
                $faltas_d = $this->faltas($servidor_id, $inicio);
                $this->SetX("60");
                if ($faltas_d > 0) {
                    $this->Cell(0, 5, $faltas_d, 0, 0, 'J');
                    $faltas += $faltas_d;
                    $somames += $faltas_d;
                } else {
                    $this->Cell(0, 5, '-            | ', 0, 0, 'J');
                }

                //imprimir licencas no ano
                $nlicenca = $this->licencas($servidor_id, $inicio);
                $residuolicencas += $nlicenca;
                $this->SetX("80");
                if ($residuolicencas > 0) {
                    //existe licencas
                    //verifica se é maior do que os dias ano
                    if ($residuolicencas > $row02->diasinicio) {
                        $residuolicencas -= $row02->diasinicio;
                        $this->Cell(0, 5, "| " . $row02->diasinicio . " | ", 0, 0, 'J');
                        //imprime uma linha... largura,altura, texto,borda,quebra de linha, alinhamento
                        $this->Cell(0, 0, '', 1, 1, 'L');
                        $licenca += $row02->diasinicio;
                        $somames += $row02->diasinicio;
                    } else {
                        //coloca o restante para o ano
                        $this->Cell(0, 5, "| " . $residuolicencas . " | ", 0, 0, 'J');
                        //imprime uma linha... largura,altura, texto,borda,quebra de linha, alinhamento
                        $this->Cell(0, 0, '', 1, 1, 'L');
                        $licenca += $residuolicencas;
                        $somames += $residuolicencas;
                        $residuolicencas = 0;
                    }
                } else {
                    $this->Cell(0, 5, '-                | ', 0, 0, 'J');
                    //imprime uma linha... largura,altura, texto,borda,quebra de linha, alinhamento
                    $this->Cell(0, 0, '', 1, 1, 'L');
                }

                //imprimir suspensao
                $nsuspensao = $this->suspensao($servidor_id, $inicio);
                $residuosuspensao += $nsuspensao;
                $this->SetX("110");

                if ($residuosuspensao > 0) {
                    //existe licencas
                    //verifica se é maior do que os dias ano
                    if ($residuosuspensao > $row02->diasinicio) {
                        $residuosuspensao -= $row02->diasinicio;
                        $this->Cell(0, 5, $row02->diasinicio . "      ", 0, 0, 'J');
                        //imprime uma linha... largura,altura, texto,borda,quebra de linha, alinhamento
                        $this->Cell(0, 0, '', 1, 1, 'L');
                        $suspencao += $row02->diasinicio;
                        $somames += $row02->diasinicio;
                    } else {
                        //coloca o restante para o ano
                        $this->Cell(0, 5, $residuosuspensao, 0, 0, 'J');
                        $suspencao += $residuosuspensao;
                        $somames += $residuosuspensao;
                        $residuosuspensao = 0;
                    }
                } else {
                    $this->Cell(0, 5, '-                | ', 0, 0, 'J');
                    //imprime uma linha... largura,altura, texto,borda,quebra de linha, alinhamento
                    $this->Cell(0, 0, '', 1, 1, 'L');
                }
                ###### fim impressão prineira linha do item 10 ######
                //   $this->Ln();
                ###### inicio impressão intervalo do item 10 ######
                $liquidomes = ($row02->diasinicio - $somames);
                $liquidosubtotal = $liquidomes;
                $this->SetX("147");
                $this->Cell(0, 5, "|         " . $somames . "          |", 0, 0, 'J');
                //imprime uma linha... largura,altura, texto,borda,quebra de linha, alinhamento
                $this->Cell(0, 0, '', 1, 1, 'L');

                $this->SetX("190");
                $this->Cell(0, 5, $liquidomes . "   |", 0, 1, 'J');
                //imprime uma linha... largura,altura, texto,borda,quebra de linha, alinhamento
                $this->Cell(0, 0, '', 1, 1, 'L');

                $inicio++;
                $somadias = ($faltas + $licenca + $suspencao);
                $servidor_id = $row02->servidor_id;
                while ($inicio < $fim) {
                    $somames = 0;
                    $liquidomes = 0;
                    $diasano = 0;
                    //verificar se ano e bissexto
                    if (( (($inicio % 4) == 0 && ($inicio % 100) != 0) || ($inicio % 400) == 0)) {
                        $diasano = 366;
                    } else {
                        $diasano = 365;
                    }

                    $this->SetX("10");
                    $this->Cell(0, 5, utf8_decode("| " . $inicio . "  | "), 0, 0, 'J');
                    //imprime uma linha... largura,altura, texto,borda,quebra de linha, alinhamento
                    $this->Cell(0, 0, '', 1, 1, 'L');

                    $this->SetX("30");
                    $this->Cell(0, 5, utf8_decode($diasano . "                  | "), 0, 0, 'J');
                    //imprime uma linha... largura,altura, texto,borda,quebra de linha, alinhamento
                    $this->Cell(0, 0, '', 1, 1, 'L');

                    //imprime as faltas no ano
                    $faltas_d = $this->faltas($servidor_id, $inicio);
                    $this->SetX("60");
                    if ($faltas_d > 0) {
                        $this->Cell(0, 5, $faltas_d, 0, 0, 'J');
                        //imprime uma linha... largura,altura, texto,borda,quebra de linha, alinhamento
                        $this->Cell(0, 0, '', 1, 1, 'L');
                        $faltas += $faltas_d;
                        $somames += $faltas_d;
                    } else {
                        $this->Cell(0, 5, '-            | ', 0, 0, 'J');
                        //imprime uma linha... largura,altura, texto,borda,quebra de linha, alinhamento
                        $this->Cell(0, 0, '', 1, 1, 'L');
                    }

                    //imprimir licencas no ano
                    $nlicenca = $this->licencas($servidor_id, $inicio);
                    $residuolicencas += $nlicenca;
                    $this->SetX("80");
                    if ($residuolicencas > 0) {
                        //existe licencas
                        //verifica se é maior do que os dias ano
                        if ($residuolicencas > $diasano) {
                            $residuolicencas -= $diasano;
                            $this->Cell(0, 5, $diasano . '           | ', 0, 0, 'J');
                            //imprime uma linha... largura,altura, texto,borda,quebra de linha, alinhamento
                            $this->Cell(0, 0, '', 1, 1, 'L');
                            $licenca += $diasano;
                            $somames += $diasano;
                        } else {
                            //coloca o restante para o ano
                            $this->Cell(0, 5, $residuolicencas . '           | ', 0, 0, 'J');
                            //imprime uma linha... largura,altura, texto,borda,quebra de linha, alinhamento
                            $this->Cell(0, 0, '', 1, 1, 'L');
                            $licenca += $residuolicencas;
                            $somames += $residuolicencas;
                            $residuolicencas = 0;
                        }
                    } else {
                        $this->Cell(0, 5, '-                | ', 0, 0, 'J');
                        //imprime uma linha... largura,altura, texto,borda,quebra de linha, alinhamento
                        $this->Cell(0, 0, '', 1, 1, 'L');
                    }

                    //imprimir suspensao
                    $nsuspensao = $this->suspensao($servidor_id, $inicio);
                    $residuosuspensao += $nsuspensao;
                    $this->SetX("110");

                    if ($residuosuspensao > 0) {
                        //existe licencas
                        //verifica se é maior do que os dias ano
                        if ($residuosuspensao > $diasano) {
                            $residuosuspensao -= $diasano;
                            $this->Cell(0, 5, $diasano, 0, 0, 'J');
                            $suspencao += $diasano;
                            $somames += $diasano;
                        } else {
                            //coloca o restante para o ano
                            $this->Cell(0, 5, $residuosuspensao, 0, 0, 'J');
                            $suspencao += $residuosuspensao;
                            $somames += $residuosuspensao;
                            $residuosuspensao = 0;
                        }
                    } else {
                        $this->Cell(0, 5, '-                | ', 0, 0, 'J');
                        //imprime uma linha... largura,altura, texto,borda,quebra de linha, alinhamento
                        $this->Cell(0, 0, '', 1, 1, 'L');
                    }

                    $liquidomes = ($diasano - $somames);
                    $this->SetX("147");
                    if ($somames == 0) {
                        $this->Cell(0, 5, "|         " . $somames . "          |", 0, 0, 'J');
                        //imprime uma linha... largura,altura, texto,borda,quebra de linha, alinhamento
                        $this->Cell(0, 0, '', 1, 1, 'L');
                    } else {
                        $this->Cell(0, 5, "|         " . $somames . "      |", 0, 0, 'J');
                        //imprime uma linha... largura,altura, texto,borda,quebra de linha, alinhamento
                        $this->Cell(0, 0, '', 1, 1, 'L');
                    }

                    $this->SetX("190");
                    if ($liquidomes == 0) {
                        $this->Cell(0, 5, $liquidomes . "       |", 0, 1, 'J');
                        //imprime uma linha... largura,altura, texto,borda,quebra de linha, alinhamento
                        $this->Cell(0, 0, '', 1, 1, 'L');
                    } else {
                        $this->Cell(0, 5, $liquidomes . "   |", 0, 1, 'J');
                        //imprime uma linha... largura,altura, texto,borda,quebra de linha, alinhamento
                        $this->Cell(0, 0, '', 1, 1, 'L');
                    }

                    $somadias = ($faltas + $licenca + $suspencao);
                    $liquidosubtotal += $liquidomes;
                    $inicio++;
                }
                ###### fim impressão intervalo do item 10 ######
                ###### inicio impressão ultima linha do item 10 ######
                $this->SetX("10");
                //   $this->Cell(0, 5, utf8_decode("| " . $fim . "  |"), 0, 0, 'J');
                //imprime uma linha... largura,altura, texto,borda,quebra de linha, alinhamento
                $this->Cell(0, 0, '', 1, 1, 'L');

                $this->SetX("30");
                //verifica data da solicitacao da aposentadoria
                if ($row->tiposolicitacao == 'APOSENTADORIA' && $row->situacao == 'CONCLUIDA' || $row->situacao == 'EM TRAMITACAO') {
                    //calcula numero de dias da data da solicitacao aposentadoria com a datafim(data atual) vw_sertidaotempode servico
                    $dadafimsolicitacao = Util::diasDeDiferenca(TDate::date2br($row->database), TDate::date2br($row02->datafim));
                    $diasfim = $row02->diasfim - $dadafimsolicitacao;
                } else {
                    $diasfim = $row02->diasfim;
                }

                $this->Cell(0, 5, utf8_decode($diasfim . "                  |"), 0, 0, 'J');
                //imprime uma linha... largura,altura, texto,borda,quebra de linha, alinhamento
                $this->Cell(0, 0, '', 1, 1, 'L');

                $somames = 0;
                $liquidomes = 0;
//                        $liquidosubtotal = 0;
                //imprime as faltas no ano
                $faltas_d = $this->faltas($servidor_id, $inicio);
                $this->SetX("60");
                if ($faltas_d > 0) {
                    $this->Cell(0, 5, $faltas_d . "       |", 0, 0, 'J');
                    //imprime uma linha... largura,altura, texto,borda,quebra de linha, alinhamento
                    $this->Cell(0, 0, '', 1, 1, 'L');
                    $faltas += $faltas_d;
                    $somames += $faltas_d;
                } else {
                    $this->Cell(0, 5, '-            | ', 0, 0, 'J');
                    //imprime uma linha... largura,altura, texto,borda,quebra de linha, alinhamento
                    $this->Cell(0, 0, '', 1, 1, 'L');
                }

                //imprimir licencas no ano
                $nlicenca = $this->licencas($servidor_id, $inicio);
                $residuolicencas += $nlicenca;
                $this->SetX("80");
                if ($residuolicencas > 0) {
                    //existe licencas
                    //verifica se é maior do que os dias ano
                    if ($residuolicencas > $diasfim) {
                        $residuolicencas -= $diasfim;
                        $this->Cell(0, 5, $diasfim . "               |", 0, 0, 'J');
                        //imprime uma linha... largura,altura, texto,borda,quebra de linha, alinhamento
                        $this->Cell(0, 0, '', 1, 1, 'L');
                        $licenca += $diasfim;
                        $somames += $diasfim;
                    } else {
                        //coloca o restante para o ano
                        $this->Cell(0, 5, $residuolicencas . "           |", 0, 0, 'J');
                        //imprime uma linha... largura,altura, texto,borda,quebra de linha, alinhamento
                        $this->Cell(0, 0, '', 1, 1, 'L');
                        $residuolicencas -= $diasfim;
                        $licenca += $diasfim;
                        $somames += $diasfim;
                        $residuolicencas = 0;
                    }
                } else {
                    $this->Cell(0, 5, '-                | ', 0, 0, 'J');
                    //imprime uma linha... largura,altura, texto,borda,quebra de linha, alinhamento
                    $this->Cell(0, 0, '', 1, 1, 'L');
                }
                //imprimir suspensao
                $nsuspensao = $this->suspensao($servidor_id, $inicio);
                $residuosuspensao += $nsuspensao;
                $this->SetX("110");

                if ($residuosuspensao > 0) {
                    //existe licencas
                    //verifica se é maior do que os dias ano
                    if ($residuosuspensao > $diasfim) {
                        $residuosuspensao -= $diasfim;
                        $this->Cell(0, 5, $diasfim . "       |", 0, 0, 'J');
                        //imprime uma linha... largura,altura, texto,borda,quebra de linha, alinhamento
                        $this->Cell(0, 0, '', 1, 1, 'L');
                        $suspencao += $diasfim;
                        $somames += $diasfim;
                    } else {
                        //coloca o restante para o ano
                        $this->Cell(0, 5, $residuosuspensao . "     |", 0, 0, 'J');
                        //imprime uma linha... largura,altura, texto,borda,quebra de linha, alinhamento
                        $this->Cell(0, 0, '', 1, 1, 'L');
                        $suspencao += $residuosuspensao;
                        $somames += $residuosuspensao;
                        $residuosuspensao = 0;
                    }
                } else {
                    $this->Cell(0, 5, '-                | ', 0, 0, 'J');
                    //imprime uma linha... largura,altura, texto,borda,quebra de linha, alinhamento
                    $this->Cell(0, 0, '', 1, 1, 'L');
                }
                //verificar se valor por zero atribuir dias fim para dias fim não ficar negativo
                if ($somames == 0) {
                    $liquidomes = $diasfim;
                } else {
                    $liquidomes = ($somames - $diasfim);
                }

                $this->SetX("147");
                if ($somames == 0) {
                    $this->Cell(0, 5, "|         " . $somames . "          |", 0, 0, 'J');
                    //imprime uma linha... largura,altura, texto,borda,quebra de linha, alinhamento
                    $this->Cell(0, 0, '', 1, 1, 'L');
                } else {
                    $this->Cell(0, 5, "|         " . $somames . "      |", 0, 0, 'J');
                    //imprime uma linha... largura,altura, texto,borda,quebra de linha, alinhamento
                    $this->Cell(0, 0, '', 1, 1, 'L');
                }

                $this->SetX("190");
                if ($liquidomes == 0) {
                    $this->Cell(0, 5, $liquidomes . "       |", 0, 1, 'J');
                    //imprime uma linha... largura,altura, texto,borda,quebra de linha, alinhamento
                    $this->Cell(0, 0, '', 1, 1, 'L');
                } else {
                    $this->Cell(0, 5, $liquidomes . "   |", 0, 1, 'J');
                    //imprime uma linha... largura,altura, texto,borda,quebra de linha, alinhamento
                    $this->Cell(0, 0, '', 1, 1, 'L');
                }
                ###### fim impressão ultima linha do item 10 ######
            }// fim for
            ###### fim impressão intervalo do item 10 ######
            ###### inicio impressão ultima linha do item 10 ######
            $this->Ln(-5);
            $this->SetX("10");
            $this->Cell(0, 5, utf8_decode("| " . $fim . "  |"), 0, 0, 'J');
            
            $this->Ln(5);
            $this->SetX("10");
            //converter dias em os anos meses e dias
            $this->Cell(0, 5, utf8_decode("Tempo Trabalho no(a) " . $_SESSION['empresa_sigla'] . " conta com: " . Util::time1text($liquidosubtotal)), 0, 1, 'J');

            $this->SetX("10");
            //converter dias em os anos meses e dias
            $this->Cell(0, 5, utf8_decode("Averbação conta com: " . Util::time1text($averbacao)), 0, 1, 'J');

            $total = $liquidosubtotal + $licencaPremio + $averbacao;
            $this->SetFont('arial', 'B', 9);
            $this->SetX("128");
            $this->Cell(0, 5, "TEMPO TOTAL APURADO: " . $total . $t, 0, 1, 'J');
            $this->Ln();

            $this->Cell(0, 5, utf8_decode("CERTIFICO, face ao apurado, que o interessado conta nesta data, com o tempo total líquido de serviço de:"), 0, 1, 'J');
            $this->Cell(0, 5, utf8_decode($total . " " . diasPorExtenso($total) . " ou seja, " . time1text($total)), 0, 1, 'J');
            $this->Cell(0, 0, '', 1, 1, 'L');
            $this->Cell(0, 5, utf8_decode("Observações: Conclusão da Certidão no Verso da Folha, devidamente assinada pelos responsáveis. "), 0, 1, 'J');
        }else {

            $this->SetFont('arial', 'B', 14);
            $this->SetX("15");
            $this->Cell(0, 5, utf8_decode("Não foi possivel efetuar a consulta, favor verificar os seguintes itens."), 0, 1, 'C');
            $this->Ln(5);
            $this->SetFont('arial', '', 12);
            $this->MultiCell(190, 5, utf8_decode("1. Se a matricula informada esta correta.\n2. Se existem dados registrados referentes a formação titulação do servidor consultado. \n3. Se existem dados registrados referentes a lotação do servidor consultado. \n4. Se existem dados registrados referentes a solicitação de aposentadoria do servidor consultado\n5. Verificar o status da situação do servidor se o mesmo já encontra-se aposentado."), 0, "C");

        }

        TTransaction::close();
    }

    function faltas($servidor, $ano) {
        //imprimir faltas
        TTransaction::open('pg_ceres');
        // cria um criterio de selecao, ordenado pelo id
        $criteria_falta = new TCriteria;
        // instancia um repositorio para Carros
        $repository_falta = new TRepository('vw_servidor_certidaotemposervico_faltasRecord');
        //filtra pelo campo selecionado pelo usuário
        $criteria_falta->add(new TFilter('servidor_id', '=', $servidor));
        $criteria_falta->add(new TFilter('ano', '<=', $ano));
        $criteria_falta->add(new TFilter('anofim', '>=', $ano));

        // carrega os objetos de acordo com o criterio
        $rows_falta = $repository_falta->load($criteria_falta);
        // finaliza a transacao
        TTransaction::close();

        $nfalta = 0;
        if ($rows_falta) {
            foreach ($rows_falta as $row_falta) {
                if ($ano == $row_falta->ano) {
                    $nfalta += $row_falta->diasinicio;
                } else {
                    if ($ano == $row_falta->anofim) {
                        $nfalta += $row_falta->diasfim;
                    }
                }
            }
        }
        return $nfalta;
    }

    function licencas($servidor, $ano) {
        //imprimir licencas
        // cria um criterio de selecao, ordenado pelo id
        TTransaction::open('pg_ceres');
        $criteria_licencas = new TCriteria;
        // instancia um repositorio para Carros
        $repository_licencas = new TRepository('vw_servidor_certidaotemposervico_licencasRecord');
        //filtra pelo campo selecionado pelo usuário
        $criteria_licencas->add(new TFilter('servidor_id', '=', $servidor));
        $criteria_licencas->add(new TFilter('ano', '<=', $ano));
        $criteria_licencas->add(new TFilter('anofim', '>=', $ano));

        // carrega os objetos de acordo com o criterio
        $rows_licencas = $repository_licencas->load($criteria_licencas);
        // finaliza a transacao
        TTransaction::close();

        $nlicenca = 0;


        if ($rows_licencas) {
            foreach ($rows_licencas as $row_licenca) {
                if ($row_licenca->tipolicenca_id == 1 || $row_licenca->tipolicenca_id == 8 || $row_licenca->tipolicenca_id == 9) {

                    if ($ano == $row_licenca->ano) {
                        $nlicenca += $row_licenca->diasinicio;
                    } else {
                        if ($ano == $row_licenca->anofim) {
                            $nlicenca += $row_licenca->diasfim;
                        } else {
                            $nlicenca += 365;
                            //verificar se ano e bissexto
                            if (( (($ano % 4) == 0 && ($ano % 100) != 0) || ($ano % 400) == 0)) {
                                $nlicenca += 1;
                            }
                        }
                    }
                }
            }
        }
        return $nlicenca;
    }

    function suspensao($servidor, $ano) {
        //imprimir suspensao
        // cria um criterio de selecao, ordenado pelo id
        TTransaction::open('pg_ceres');
        $criteria_suspensao = new TCriteria;
        // instancia um repositorio para Carros
        $repository_suspensao = new TRepository('vw_servidor_certidaotemposervico_suspensaoRecord');
        //filtra pelo campo selecionado pelo usuário
        $criteria_suspensao->add(new TFilter('servidor_id', '=', $servidor));
        $criteria_suspensao->add(new TFilter('ano', '<=', $ano));
        $criteria_suspensao->add(new TFilter('anofim', '>=', $ano));

        // carrega os objetos de acordo com o criterio
        $rows_suspensao = $repository_suspensao->load($criteria_suspensao);
        // finaliza a transacao
        TTransaction::close();

        $nsuspensao = 0;
        if ($rows_suspensao) {
            foreach ($rows_suspensao as $row_suspensao) {
//                $nsuspensao += $row_suspensao->dias;
                if ($ano == $row_suspensao->ano) {
                    $nsuspensao += $row_suspensao->diasinicio;
                } else {
                    if ($ano == $row_suspensao->anofim) {
                        $nsuspensao += $row_suspensao->diasfim;
                    }
                }
            }
        }
        //var_dump($nsuspensao);
        return $nsuspensao;
    }

//Page footer
    function Footer() {
        //Position at 1.5 cm from bottom
        $this->SetY(-15);
        //Arial italic 8
        $this->SetFont('Arial', 'I', 8);
        //Page number
        //data atual
        $data = date("d/m/Y H:i:s");
        $conteudo = "impresso em " . $data;
        $texto = $_SESSION['empresa_sitio'];
        //imprime uma linha... largura,altura, texto,borda,quebra de linha, alinhamento
        $this->Cell(0, 0, '', 1, 1, 'L');

        //imprime uma celula... largura,altura, texto,borda,quebra de linha, alinhamento
        $this->Cell(0, 5, $texto, 0, 0, 'L');
        $this->Cell(0, 5, 'Pag. ' . $this->PageNo() . ' de ' . '{nb}' . ' - ' . $conteudo, 0, 0, 'R');
        $this->Ln();
    }

}

//Instanciation of inherited class
$pdf = new RelatorioServidorCertidaoTempoServicoPDF("P", "mm", "A4");

//define o titulo
$pdf->SetTitle("Certidao Tempo de Servico");

//assunto
$pdf->SetSubject("Certidao Tempo de Servico");

$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Times', '', 12);
$pdf->ColumnDetail();
$file = "app/reports/RelatorioServidorCertidaoTempoServicoPDF" . $_SESSION['servidor_id'] . ".pdf";
//abrir pdf
$pdf->Output($file);
$pdf->openFile($file);
?>