<?php

header('charset=utf-8');

class RelatorioListaServidoresExcel extends TPage
{

    private $planilha;

    public function __construct()
    {
        parent::__construct();

        $tipo = $_REQUEST['tipo'];
        $regional = $_REQUEST['regiao'];
        $vinculo = $_REQUEST['vinculo'];

        $arquivo = 'Relatorio de Listagem de Servidores ' . $_SESSION['servidor_id'] . ' .xls';

        $this->planilha = '';
        $this->planilha .= '<table border="1">';
        // $this->planilha .= "<tr><th colspan=6 align='left'><img src='app/images/logo_relatorio.jpg' style='width: 80px;height: 60px;'/></th></tr>";
        $this->planilha .= '<tr><th colspan="11">GOVERNO DO ESTADO DO RIO GRANDE DO NORTE</th></tr>';
        $this->planilha .= '<tr><th colspan="11">' . utf8_decode($_SESSION['empresa_nome']) . '</th></tr>';
        $this->planilha .= '<tr><th colspan="11">SRH</th></tr>';
        $this->planilha .= '<tr><th colspan="11">' . utf8_decode('RELATÓRIO DE LISTAGEM DE SERVIDORES') . '</th></tr>';
        $this->planilha .= '<tr><th colspan="11">' . utf8_decode('Tipo: ' . $tipo . ' / Região: ' . $regional . ' / Tipo de Funcionário: ' . $vinculo) . '</th></tr>';

        $this->ColumnHeader();
        $this->ColumnDetail();
        $this->Footer();

        $this->planilha .= '</table>';
        // echo $this->planilha;
        file_put_contents('app/output/' . $arquivo, $this->planilha);
        TPage::openFile('app/output/' . $arquivo);
    }

    function ColumnHeader()
    {
        $this->planilha .= '<tr>';
        $this->planilha .= '<td align="center" bgcolor="#7CCD7C"><b>' . utf8_decode('Nome do Servidor') . '</b></td>';
        $this->planilha .= '<td align="center" bgcolor="#7CCD7C"><b>' . utf8_decode('Matrícula') . '</b></td>';
        $this->planilha .= '<td align="center" bgcolor="#7CCD7C"><b>' . utf8_decode('CPF') . '</b></td>';
        $this->planilha .= '<td align="center" bgcolor="#7CCD7C"><b>' . utf8_decode('Nome do Cargo') . '</b></td>';
        $this->planilha .= '<td align="center" bgcolor="#7CCD7C"><b>' . utf8_decode('Situação') . '</b></td>';
        $this->planilha .= '<td align="center" bgcolor="#7CCD7C"><b>' . utf8_decode('Vinculo') . '</b></td>';
        $this->planilha .= '<td align="center" bgcolor="#7CCD7C"><b>' . utf8_decode('Nível Salarial') . '</b></td>';
        $this->planilha .= '<td align="center" bgcolor="#7CCD7C"><b>' . utf8_decode('Nome da Formação') . '</b></td>';
        $this->planilha .= '<td align="center" bgcolor="#7CCD7C"><b>' . utf8_decode('Titulação') . '</b></td>';
        $this->planilha .= '<td align="center" bgcolor="#7CCD7C"><b>' . utf8_decode('Lotação') . '</b></td>';
        $this->planilha .= '<td align="center" bgcolor="#7CCD7C"><b>' . utf8_decode('Nome Regional') . '</b></td>';
        $this->planilha .= '</tr>';
    }

    function ColumnDetail()
    {
        $tipo = $_REQUEST['tipo'];
        $regional = $_REQUEST['regiao'];
        $vinculo = $_REQUEST['vinculo'];
        $situacao = $_REQUEST['situacao'];
        $formacao = $_REQUEST['formacao'];

        TTransaction::open('pg_ceres');//vw_servidores_relatorio
        $repository = new Adianti\Database\TRepository('vw_servidores_relatorioRecord');
        $criteria = new TCriteria;

        $criteria1 = new TCriteria;

        if ($tipo != 'TODOS') {
            if ($tipo == 'ATIVOS') {
                $criteria->add(new TFilter('situacao', '<>', 'APOSENTADO(A)'));
                $criteria->add(new TFilter('situacao', '<>', 'RESCINDIDO'));
            } else if ($tipo == 'INATIVOS') {
                $criteria1->add(new TFilter('situacao', '=', 'APOSENTADO(A)'), TExpression::OR_OPERATOR);
                $criteria1->add(new TFilter('situacao', '=', 'RESCINDIDO'), TExpression::OR_OPERATOR);
            } else if ($situacao != 'TODAS') {
                $criteria1->add(new TFilter('situacao', '=', $situacao));
            }
        }
        if ($situacao != 'TODAS') {
            $criteria->add(new TFilter('situacao', '=', $situacao));
        }

        if ($formacao != 'TODAS') {
            $criteria->add(new TFilter('formacaorequisito', '=', $formacao));
        }
        if ($regional != 'TODAS') {
            $criteria->add(new TFilter('regional', '=', $regional));
        }
        if ($vinculo != 'TODAS') {
            $criteria->add(new TFilter('vinculo', '=', $vinculo));
        }
        $criteria->add(new TFilter('empresa_id', '=', $_SESSION['empresa_id']));

        $criteria->setProperty('order', 'lotacao, nome, vinculo');

       // var_dump($criteria->dump());

        $rows = $repository->load($criteria);

        $i = 0;
        if ($rows) {
            foreach ($rows as $row) {
                $this->planilha .= '<tr>';
                $this->planilha .= '<td align="center"><b>' . utf8_decode($row->nome) . '</b></td>';
                $this->planilha .= '<td align="center"><b>' . utf8_decode($row->matricula) . '</b></td>';
                $this->planilha .= '<td align="center"><b>' . utf8_decode($row->cpf) . '</b></td>';
                $this->planilha .= '<td align="center"><b>' . utf8_decode($row->cargonovo) . '</b></td>';
                $this->planilha .= '<td align="center"><b>' . utf8_decode($row->situacao) . '</b></td>';
                $this->planilha .= '<td align="center"><b>' . utf8_decode($row->vinculo) . '</b></td>';
                $this->planilha .= '<td align="center"><b>' . utf8_decode($row->nivelsalarial) . '</b></td>';
                $this->planilha .= '<td align="center"><b>' . utf8_decode($row->formacaorequisito) . '</b></td>';
                $this->planilha .= '<td align="center"><b>' . utf8_decode($row->formacao) . '</b></td>';
                $this->planilha .= '<td align="center"><b>' . utf8_decode($row->lotacao) . '</b></td>';
                $this->planilha .= '<td align="center"><b>' . utf8_decode($row->regional) . '</b></td>';
                $this->planilha .= '</tr>';
                $i++;
            }
            $this->planilha .= '<tr><td align="left" colspan="11"><b>Total de Servidores: ' . $i . '</b></td></tr>';
        } else {
            $this->planilha .= '<tr><td align="center" colspan="11"><b>Nenhum servidor encontrado para este filtro!</b></td></tr>';
        }

        TTransaction::close();
    }

    function Footer()
    {
        $this->planilha .= '<tr><td colspan="11" align="left" border="0">http://www.emater.rn.gov.br impresso em ' . date('d/m/Y H:i:s') . '</td></tr>';
    }

}
