<?php

use Adianti\Database\TTransaction;
use Adianti\Database\TRepository;
use Adianti\Database\TCriteria;
use Adianti\Database\TFilter;
use Lib\Funcoes\Util;

ini_set('display_errors', 1);
ini_set('display_startup_erros', 1);
error_reporting(E_ALL);

header('Content-Type: text/html; charset=utf-8');

class CertificadoCursoLotePDF extends PDF_HTML {

    private $opcao_imagem_certificado;
    private $certificado_id;

    function Header() {

        $this->opcao_imagem_certificado = !empty($_REQUEST['opcao_imagem_certificado']) ? $_REQUEST['opcao_imagem_certificado'] : 'off';
        $this->certificado_id = $_REQUEST['key'];

        TTransaction::open('pg_ceres');

        if ($this->certificado_id) {

            $repository = new TRepository('vw_consultar_certificado_participacaoRecord');

            $criteria = new TCriteria;
            $criteria->add(new TFilter('srh_certificado_id', '=', $this->certificado_id));

            $rows = $repository->load($criteria);

            if ($rows) {

                foreach ($rows as $row) {

                    $repository2 = new TRepository('SolicitacaoCertificadoRecord');
                    $criteria2 = new TCriteria;

                    $criteria2->add(new TFilter('srh_certificado_servidor_id', '=', $row->srh_certificado_servidor_id));

                    if($repository2->count($criteria2) == 0) {

                        $objectSC = new SolicitacaoCertificadoRecord();

                        $objectSC->srh_certificado_servidor_id = $row->srh_certificado_servidor_id;
                        $objectSC->dataemissao = date('d/m/Y');
                        $objectSC->codigoverificacao = Util::gerarCodigoAleatorio(10, true, true);
                        $objectSC->usuarioalteracao = $_SESSION['usuario'];
                        $objectSC->dataalteracao = date("d/m/Y H:i:s");

                        $objectSC->store();

                    }

                }

            }

        }

        TTransaction::close();

    }

    function pageDescricao() {

        TTransaction::open('pg_ceres');

        $certificado = new CErtificadoRecord($this->certificado_id);

        $repository = new TRepository('vw_consultar_certificado_participacaoRecord');

        $criteria = new TCriteria;
        $criteria->add(new TFilter('srh_certificado_id', '=', $this->certificado_id));

        $rows = $repository->load($criteria);

        $i = 0;

        foreach ($rows as $row) {

            $content_html = '';

            if($i != 0){

                $this->AddPage();

            }

            $i++;

            if ($this->opcao_imagem_certificado == 'off') {
                if ($certificado->empresa_id != 1) {
                    $this->Image("app/images/certificado/certificado_" . strtolower($certificado->logo_empresa) . "_em_branco_frente.jpg", 1, 1, 297, 210);
                } else {
                    $this->Image("app/images/certificado/certificado_emater_em_branco_frente.jpg", 1, 1, 297, 210);
                }
            }

            //define a fonte a ser usada
            $this->SetFont('arial', '', 16);

            $content_html .= utf8_decode($certificado->descricao);
            $conteudo = utf8_decode($certificado->conteudo);

            $this->setXY(10, 70);
            $this->Multicell(190, 8, str_replace('{participante}', utf8_decode($row->nome_participante), $content_html), 'J');

            if ($certificado->empresa_id == 1) {
                $this->Image("app/images/assinaturas_certificados/assinatura_catia_diretora_emater.jpg", 20, 135, 65, 25);
                $this->setXY(16, 148);
                $this->Multicell(70, 7, utf8_decode(" __________________ \n Diretora da Emater-RN "), 'C');
            }

            if ($certificado->assinatura_coordenador == "LEILA MESQUITA") {

                $this->Image("app/images/assinaturas_certificados/assinatura_leila_mesquita.jpg", 122, 135, 65, 25);
                $this->setXY(115, 148);
                $this->Multicell(140, 7, utf8_decode("        __________________ \nLeila Daniele F.da Silva Mesquita "), 'C');
            } else if ($certificado->assinatura_coordenador == "ADRIANA AMERICO DE SOUZA") {

                $this->Image("app/images/assinaturas_certificados/assinatura_adriana_americo.jpg", 130, 135, 65, 25);
                $this->setXY(125, 148);
                $this->Multicell(140, 7, utf8_decode("    ____________________ \n   Adriana Americo de Souza "), 'C');
            } else if ($certificado->assinatura_coordenador == "ELTON DANTAS DE OLIVEIRA") {

                $this->Image("app/images/assinaturas_certificados/assinatura_elton_oliveira.jpg", 130, 135, 65, 25);
                $this->setXY(125, 148);
                $this->Multicell(140, 7, utf8_decode("    ____________________ \n   Elton Dantas de Oliveira "), 'C');

            } else if ($certificado->assinatura_coordenador == "ARIAMELIA BANDEIRA CRUZ FEITOSA") {

                $this->Image("app/images/assinaturas_certificados/assinatura_ariamelia_bandeira_cruz_feitosa.jpg", 130, 137, 65, 25);
                $this->setXY(125, 148);
                $this->Multicell(140, 7, utf8_decode("    ____________________ \nAriamelia Bandeira Cruz Feitosa "), 'C');
            }

            //--------------------------
            $repository2 = new TRepository('SolicitacaoCertificadoRecord');

            $criteria2 = new TCriteria;
            $criteria2->add(new TFilter('srh_certificado_servidor_id', '=', $row->srh_certificado_servidor_id));

            $rows2 = $repository2->load($criteria2);

            $this->SetFont('arial', 'B', 8);
            $this->setXY(75, 170);
            $this->Multicell(70, 4, utf8_decode(" Código de verificação: " . $rows2[0]->codigoverificacao . "\n Data de emissão: ") . TDate::date2br($rows2[0]->dataemissao), '1', 'C');

            $this->Ln(5);
            $this->setXY(30, 180);
            $this->Multicell(160, 4, utf8_decode("Para verificar a autenticidade deste documento acesse http://ceres.rn.gov.br/certificado, informando o código de verificação."), 0, 'C');

            //--------------------------

            $this->AddPage();

            if ($this->opcao_imagem_certificado != 'on') {
                if ($certificado->empresa_id != 1) {
                    $this->Image("app/images/certificado/certificado_" . strtolower($certificado->logo_empresa) . "_em_branco_verso.jpg", 1, 1, 297, 210);
                } else {
                    $this->Image("app/images/certificado/certificado_emater_em_branco_verso.jpg", 1, 1, 297, 210);
                }
            }

            $this->setXY(15, 45);
            $this->Multicell(0, 5, $this->WriteHtmlCell(200, $conteudo), 'J');

        }

        TTransaction::close();

    }

    //Page footer
    function Footer()
    {

    }

}

$pdf = new CertificadoCursoLotePDF('L', 'mm', 'A4');

$pdf->SetTitle( "Certificados em Lote" );
$pdf->SetSubject( "Certificados em Lote" );

$pdf->AddPage();
$pdf->SetFont( 'Times', '', 12 );

$pdf->pageDescricao();

$file = "app/reports/CertificadoCursoLotePDF" . $_SESSION['servidor_id'] . ".pdf";

$pdf->Output($file);
$pdf->openFile($file);
