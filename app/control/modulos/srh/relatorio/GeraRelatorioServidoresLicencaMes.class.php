<?php

//ini_set('display_errors', 1);
//ini_set('display_startup_erros', 1);
//error_reporting(E_ALL);

/*
 * classe GeraRelatorioServidoresEmFeriasMes
 * Cadastro de GeraRelatorioServidoresEmFeriasMes: Contem o filtro para seleção do relatório dos servidores em ferias por mes
 */
include_once 'app/lib/funcdate.php';

class GeraRelatorioServidoresLicencaMes extends TPage {

    private $form;     // formulario de cadastro

    /*
     * metodo construtor
     * Cria a pagina, o formulario e a listagem
     */

    public function __construct() {
        parent::__construct();

        // instancia um formulario
        $this->form = new TForm('form_busca_GeraRelatorioServidoresEmFeriasMes');

        // instancia uma tabela
        $panel = new TPanelForm(700, 100);
        $panel->setColuna(250);

        // adiciona a tabela ao formulario
        $this->form->add($panel);

        // cria um r�tulo para o t�tulo
        $titulo = new TLabel('Relat&oacute;rio de Licen&ccedil;a dos Servidores');
        $titulo->setFontFace('Arial');
        $titulo->setFontColor('red');
        $titulo->setFontSize(12);

        // adiciona o campo Titulo
        $panel->put($titulo, $panel->getColuna(), $panel->getLinha());

        // cria os campos do formulario
        $tipolicenca_id = new TCombo('tipolicenca_id');
        $ano = new TCombo('ano');
        $mes = new TCombo('mes');

        //CRIA A RELACAO DA TABELA ESTRANGEIRA PARA MODELO
        $items = array();
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');
        // instancia objeto da Classe Record
        $repository = new TRepository('TipoLicencaRecord');
        $criteria = new TCriteria;

        $criteria->setProperty('order', 'nome');
        // carrega todos os objetos
        $items['0'] = 'TODOS';
        $collection = $repository->load($criteria);
        //adiciona os objetos no combo
        foreach ($collection as $object) {
            $items[$object->id] = $object->nome;
        }

        // adiciona as opcoes na combo
        $tipolicenca_id->addItems($items);
        $tipolicenca_id->setValue('TODOS');
        $tipolicenca_id->setSize(40);

        // finaliza a transacao
        TTransaction::close();

        $ano->addItems(retornaAnocomZero());

        $items2 = array();
        $items2['0'] = 'TODOS';
        $items2['01'] = 'Janeiro';
        $items2['02'] = 'Fevereiro';
        $items2['03'] = 'Marco';
        $items2['04'] = 'Abril';
        $items2['05'] = 'Maio';
        $items2['06'] = 'Junho';
        $items2['07'] = 'Julho';
        $items2['08'] = 'Agosto';
        $items2['09'] = 'Setembro';
        $items2['10'] = 'Outubro';
        $items2['11'] = 'Novembro';
        $items2['12'] = 'Dezembro';
        $mes->addItems($items2);

        $ano->setValue('2015');
        $ano->setSize(40);

        $mes->setValue('01');
        $mes->setSize(40);

        // cria um botao de acao (cadastrar)
        $new_button = new TButton('gerarelatorio');

        // define a acao do botao cadastrar
        $new_button->setAction(new TAction(array($this, 'onGenerate')), 'Gerar Relat&oacute;rio');

        $panel->setLinha(50);

        // adiciona campo opcao tipo licenca
        $panel->putCampo($tipolicenca_id, 'Tipo Licen&ccedil;a', 0, 1);
        // adiciona campo opcao Ano
        $panel->putCampo($ano, 'Ano', 0, 1);

        // adiciona campo opcao Mes
        $panel->putCampo($mes, 'M&ecirc;s', 0, 1);
        $panel->putCampo(NULL, null, 0, 1);

        //adiciona botao gerar grafico
        $panel->putCampo($new_button, null, -50, 1);

        //$panel->put($new_button, $panel->getColuna()+270,$panel->getLinha());
        // define quais sao os campos do formulario
        $this->form->setFields(array($new_button, $tipolicenca_id, $ano, $mes));

        // monta a paina atraves de uma tabela
        $panel = new TPanelForm(700, 500);
        $panel->put($this->form, 0, 0);


        // adiciona a tabela a pagina
        parent::add($panel);
    }

    function onGenerate() {

        new RelatorioServidoresLicencaMesPDF();
    }

}

?>