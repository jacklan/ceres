<?php

class GeraRelatorioServidorList extends TPage {

    private $form;

    public function __construct() {
        parent::__construct();

        $this->form = new TForm('gera_relatorio_servidor_list');

        $panel = new TPanelForm(700, 100);
        $panel->setColuna(150);

        $this->form->add($panel);

        $titulo = new TLabel('Gerar Relatório de Servidores');
        $titulo->setFontFace('Arial');
        $titulo->setFontColor('red');
        $titulo->setFontSize(18);

        $panel->put($titulo, $panel->getColuna(), $panel->getLinha());

        $tipo = new TCombo('tipo');
        $regiaosl = new TCombo('regiao');
        $tipofuncionario = new TCombo('tipofuncionario');

        $items = [];
        $items['ATIVOS'] = 'ATIVOS';
        $items['INATIVOS'] = 'INATIVOS';
        $items['TODOS'] = 'TODOS';

        $tipo->addItems($items);
        $tipo->setValue('ATIVOS');
        $tipo->setSize(100);

        $items2 = [];
        $items2['ASSU'] = 'ASSU';
        $items2['CAICO'] = 'CAICO';
        $items2['CURRAIS NOVOS'] = 'CURRAIS NOVOS';
        $items2['JOAO CAMARA'] = 'JOAO CAMARA';
        $items2['MOSSORO'] = 'MOSSORO';
        $items2['NATAL'] = 'NATAL';
        $items2['PAU DOS FERROS'] = 'PAU DOS FERROS';
        $items2['SANTA CRUZ'] = 'SANTA CRUZ';
        $items2['SAO JOSE DO MIPIBU'] = 'SAO JOSE DO MIPIBU';
        $items2['SAO PAULO DO POTENGI'] = 'SAO PAULO DO POTENGI';
        $items2['UMARIZAL'] = 'UMARIZAL';
        $items2['CENTERN'] = 'CENTERN';
        $items2['TODAS'] = 'TODAS';

        $regiaosl->addItems($items2);
        $regiaosl->setValue('TODAS');
        $regiaosl->setSize(150);

        $items3 = [];
        $items3['ESTAGIARIO'] = 'ESTAGIARIO';
        $items3['FUNCIONARIO'] = 'FUNCIONARIO';
        $items3['BOLSISTA'] = 'BOLSISTA';
        $items3['COMISSIONADO'] = 'COMISSIONADO';
        $items3['TODOS'] = 'TODOS';

        $tipofuncionario->addItems($items3);
        $tipofuncionario->setValue('TODOS');
        $tipofuncionario->setSize(150);

        $new_button = new TButton('gerarelatorio');
        $new_button2 = new TButton('gerarelatorio2');
        $new_button->{'style'} = 'width: 200px; float: right;';
        $new_button2->{'style'} = 'width: 200px; float: right;';

        $new_button->setAction(new TAction([$this, 'onGeneratePDF']), 'Gerar Relatório PDF');
        $new_button2->setAction(new TAction([$this, 'onGenerateEXCEL']), 'Gerar Relatório EXCEL');

        $tipo->setSize(20);
        $regiaosl->setSize(20);
        $tipofuncionario->setSize(20);

        $panel->putCampo($tipo, 'Tipo', 0, 0);
        $panel->putCampo($regiaosl, 'Regiao', 0, 1);
        $panel->putCampo($tipofuncionario, 'Vinculo', 0, 1);
        $panel->putCampo($new_button, null, 0, 1);
        $panel->putCampo($new_button2, null, 0, 1);

        $this->form->setFields([$new_button, $new_button2]);

        $panel = new TPanelForm(700, 500);
        $panel->put($this->form, 0, 0);

        parent::add($panel);
    }

    function onGeneratePDF() {
        $this->form->setData($this->form->getData());
        new RelatorioServidorPDF();
    }

    function onGenerateEXCEL() {
        $this->form->setData($this->form->getData());
        new RelatorioListaServidoresExcel();
    }

}
