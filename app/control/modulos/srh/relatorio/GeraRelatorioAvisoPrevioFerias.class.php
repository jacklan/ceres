<?php

/*
 * classe GeraRelatorioAvisoPrevioFerias
 * Autor: Jackson Meires
 * Data:08/12/2016
 */
class GeraRelatorioAvisoPrevioFerias extends TPage {

    private $form;     // formulario de cadastro

    /*
     * metodo construtor
     * Cria a pagina, o formulario e a listagem
     */

    public function __construct() {
        parent::__construct();
        
        $this->form = new TQuickForm('form');
        $this->form->setFormTitle('<font color="red" size="3px" face="Arial"><b>Gerar Relat&oacute;rio Aviso pr&eacute;vio de f&eacute;rias</b></font>');
       // $this->form->style = 'width: 80%';
        
        // cria os campos do formulario
        $matricula = new TEntry('matricula');
        $datainicio = new TDate('datainicio');
        $datafinal = new TDate('datafinal');
        $datavolta = new TDate('datavolta');
        $ano = new TEntry('ano');
       
        $this->form->addQuickField('Matricula: ',  $matricula, 50);
        $this->form->addQuickField('Data inicio: ',  $datainicio, 50);
        $this->form->addQuickField('Data final: ',  $datafinal, 50);
        $this->form->addQuickField('Data volta: ',  $datavolta, 50);
        $this->form->addQuickField('Ano: ',  $ano, 40);
        
        $this->form->addQuickAction('Gerar Relatório', new TAction(array($this, 'onGenerate')), 'fa:file-pdf-o');
        
        // monta a paina atraves de uma tabela
        $tvbox = new Adianti\Widget\Container\TVBox();
        $tvbox->add($this->form);

        // adiciona a tabela a pagina
        parent::add($tvbox);
    }
    

    function onGenerate() {

        new RelatorioAvisoPrevioFeriasPDF();
        $this->form->setData($this->form->getData());
    }
}

?>

