<?php

/*
 * classe GeraRelatorioSolicitacaoList
 * Cadastro de GeraRelatorioSolicitacao: Contem a listagem e o formulario de busca
 */

//include_once 'app.pdf/RelatorioServidorEnquadramentoPDF.php';

class GeraRelatorioServidorEnquadramentoList extends TPage {

    private $form;     // formulario de cadastro

    /*
     * metodo construtor
     * Cria a pagina, o formulario e a listagem
     */

    public function __construct() {
        parent::__construct();

        // instancia um formulario
        $this->form = new TForm('form_busca_GeraRelatorioServidorEnquadramento');

        // instancia uma tabela
        $panel = new TPanelForm(700, 100);
        $panel->setColuna(150);

        // adiciona a tabela ao formulario
        $this->form->add($panel);

        // cria um r�tulo para o t�tulo
        $titulo = new TLabel('Relatorio de Servidores - Enquadramento');
        $titulo->setFontFace('Arial');
        $titulo->setFontColor('red');
        $titulo->setFontSize(12);

        // adiciona o campo Titulo
        $panel->put($titulo, $panel->getColuna(), $panel->getLinha());


        // cria os campos do formulario
        $datainicio = new TDate('datainicio');
        $datafim = new TDate('datafim');

        $datainicio->setSize(10);
        $datafim->setSize(10);

        // cria um botao de acao (cadastrar)
        $new_button = new TButton('gerarelatorio');
        $new_button2 = new TButton('gerarelatorio2');

        // define a acao do botao cadastrar
        $new_button->setAction(new TAction(array($this, 'onGenerate')), 'Gerar Extrato');
        $new_button2->setAction(new TAction(array($this, 'onGenerate2')), 'Gerar Termo');

        $panel->setLinha(50);

        // adiciona campo datainicio
        $panel->putCampo($datainicio, 'In&iacute;cio', 0, 0);

        // adiciona campo datafim
        $panel->putCampo($datafim, 'Fim', 0, 1);

        //adiciona botao gerar grafico
        $panel->putCampo($new_button, null, 50, 1);
        $panel->putCampo($new_button2, null, -50, 0);
        //$panel->put($new_button, $panel->getColuna()+270,$panel->getLinha());
        // define quais sao os campos do formulario
        $this->form->setFields(array($new_button, $new_button2, $datainicio, $datafim));

        // monta a paina atraves de uma tabela
        $panel = new TPanelForm(700, 500);
        $panel->put($this->form, 0, 0);


        // adiciona a tabela a pagina
        parent::add($panel);
    }

    /*
     * metodo onReload()
     * Carrega a DataGrid com os objetos do banco de dados
     */

    function MostrarRelatorio() {
        // ob_end_clean();
        //include_once '../app.pdf/RelatorioServidorEnquadramentoPDF.php';
        //$obj = new RelatorioServid();
        $this->loaded = true;
    }

    function onReload() {
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');

        // instancia um repositorio para Carros
        //repository = new TRepository('GeraRelatorioSolicitacao');
        //obtem os dados do formulario de busca
        $campo = $this->form->getFieldData('opcao');
        $dados = $this->form->getFieldData('situacao');

        // cria um criterio de selecao, ordenado pelo id
        $criteria = new TCriteria;
        $criteria->setProperty('order', $campo);


        // finaliza a transacao
        TTransaction::close();
        $this->loaded = true;
    }

    function onGenerate() {
        new TSession ();

        $dados = $this->form->getData();
        $this->form->setData($dados);

        TSession::setValue('datainicio', $dados->datainicio);
        TSession::setValue('datafim', $dados->datafim);

        new RelatorioServidorEnquadramentoPDF();
    }

    function onGenerate2() {

        $dados = $this->form->getData();
        $this->form->setData($dados);

        TSession::setValue('datainicio', $dados->datainicio);
        TSession::setValue('datafim', $dados->datafim);

        new RelatorioServidorTermoEnquadramentoPDF();
    }

    /*
     * metodo show()
     * Exibe a pagina
     */

    function show() {
        // carrega os dados no datagrid
        $this->onReload();
        //chama o metodo show da super classe
        parent::show();
    }

}

?>