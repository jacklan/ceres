<?php
//ini_set('display_errors', 1);
//ini_set('display_startup_erros', 1);
//error_reporting(E_ALL);

/*
* classe GerarRelatorioServidoresFeriasExercicio
* Relatorio de GerarRelatorioServidoresFeriasExercicio: Contem o filtro para seleção do relatório dos servidores em ferias por mes
* Autor:Jackson Meires
* Data:14/08/2017
*/

use Lib\Funcoes\Util;

class GerarRelatorioServidoresFeriasExercicio extends TPage
{
    private $form;     // formulario de cadastro

    /*
     * metodo construtor
     * Cria a pagina, o formulario e a listagem
     */

    public function __construct()
    {
        parent::__construct();

        // instancia um formulario
        $this->form = new TQuickForm;
        $this->form->setFormTitle('<font color="red" size="3" face="Arial"><b>Relatório Exercicio de Férias dos Servidores</b></font>');

        // cria os campos do formulario
        $regional_id = new TCombo('regional_id');
        $vinculo = new TCombo('vinculo');

        $regional_id->setSize(40);

        TTransaction::open('pg_ceres');
        $repository = new TRepository('RegionalRecord');

        $criteria = new \Adianti\Database\TCriteria();
        $criteria->add(new TFilter('empresa_id', '=', $_SESSION['empresa_id']));
        $criteria->setProperty('order', 'nome');

        $collection = $repository->load($criteria);
        $items['0'] = 'TODAS';
        foreach ($collection as $object) {
            $items[$object->id] = $object->nome;
        }

        $regional_id->addItems($items);
        $regional_id->setValue('0');

        TTransaction::close();

        TTransaction::open('pg_ceres');
        $repository = new TRepository('vw_vinculo_servidorRecord');
        $criteria = new TCriteria;
        $objVinculoServidor = $repository->load($criteria);

        foreach ($objVinculoServidor as $object) {
            $items3[$object->vinculo] = $object->vinculo;
        }

        TTransaction::close();
        $items3['TODAS'] = 'TODAS';

        $vinculo->addItems($items3);
        $vinculo->setValue('EFETIVO');
        $vinculo->setSize(40);

        // cria um rotulo para o titulo
        $titulo = new TLabel('<div style="width: 200px;"> <b>* Campo obrigatorio</b></div>');
        $titulo->setFontFace('Arial');
        $titulo->setFontColor('red');
        $titulo->setFontSize(10);

        //campos obrigarorios
        $vinculo->addValidation('Vinculo', new TRequiredValidator); // required field
        $regional_id->addValidation('Regional', new TRequiredValidator); // required field

        // define os campos
        $this->form->addQuickField("Vínculo <font color=red><b>*</b></font>", $vinculo, 30);
        $this->form->addQuickField("Regional <font color=red><b>*</b></font>", $regional_id, 30);
        $this->form->addQuickField(null, $titulo, 50);

        // cria um botao de acao
        $this->form->addQuickAction('Gerar', new TAction(array($this, 'onGenerate')), 'fa:file-pdf-o red');

        // adiciona a tabela a pagina
        parent::add($this->form);
    }

    /*
     * metodo onGenerate()
     * Carrega a pagina do relatorio
     */

    function onGenerate()
    {

        try {
            $dados = $this->form->getData();

            // form validation
            $this->form->validate();

            $this->form->setData($dados);
            new RelatorioServidoresFeriasExercicioPDF();
        } catch (Exception $ex) {

        }
    }

}
