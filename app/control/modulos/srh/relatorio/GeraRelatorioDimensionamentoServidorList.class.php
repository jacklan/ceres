<?php

/*
 * classe DimensionamentoServidorReport
 * Gerador de Relatorio Dimensionamento: Contem o gerador do relatorio.
 */
include_once 'app/lib/funcdate.php';

class GeraRelatorioDimensionamentoServidorList extends TPage {

    private $form;     // formulario de cadastro

    /*
     * metodo construtor
     * Cria a pagina, o formulario e a listagem
     */

    public function __construct() {

        parent::__construct();

        // instancia um formulario
        $this->form = new TForm('form_busca_DimensionamentoServidor');

        // instancia uma tabela
        $panel = new TPanelForm(700, 100);
        $panel->setColuna(150);

        // adiciona a tabela ao formulario
        $this->form->add($panel);

        // cria um r�tulo para o t�tulo
        $titulo = new TLabel('Relat&oacute;rio de Dimensionamento de Servidores');
        $titulo->setFontFace('Arial');
        $titulo->setFontColor('red');
        $titulo->setFontSize(12);

        // adiciona o campo Titulo
        $panel->put($titulo, $panel->getColuna(), $panel->getLinha());

        $regiaosl = new TCombo('regiao');

        //cria a colecao da tabela estrangeira
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');
        // instancia objeto da Classe Record
        $repository = new TRepository('vw_regional_servidorRecord');
        // cria um criterio de selecao, ordenado pelo id
        $criteria = new TCriteria;
        // carrega os objetos de acordo com o criterio
        $cadastros = $repository->load($criteria);
        //adiciona os objetos no combo

        foreach ($cadastros as $object) {

            $items2[$object->regional] = $object->regional;
        }

        TTransaction::close();

        $items2['TODAS'] = 'TODAS';

        //cria os campos com opcoes de grafico
        $regiaosl->addItems($items2);
        //coloca o valor padrão
        $regiaosl->setValue('TODAS');
        $regiaosl->setSize(40);

        // adiciona campo opcao
        $panel->putCampo($regiaosl, 'Regional', 0, 1);

        // cria um botao de acao (cadastrar)
        $new_button = new TButton('gerarelatorio');

        $new_button->setAction(new TAction(array($this, 'onGenerate')), 'Gerar Relat&oacute;rio');

        //adiciona botao gerar grafico
        $panel->putCampo($new_button, null, 0, 1);

        // define quais sao os campos do formulario
        $this->form->setFields(array($new_button));

        // monta a paina atraves de uma tabela
        $panel = new TPanelForm(700, 500);
        $panel->put($this->form, 0, 0);

        // adiciona a tabela a pagina
        parent::add($panel);
    }

    function onGenerate() {
        new RelatorioDimensionamentoServidorPDF();
    }

}

?>