<?php

/*
 * classe GeraRelatorioFormacaoCorrelacao
 * Autor: Jackson Meires
 * Data: 15/07/2016
 */

class GeraRelatorioFormacaoCorrelacao extends TPage {

    private $form;     // formulario de cadastro

    /*
     * metodo construtor
     * Cria a pagina, o formulario e a listagem
     */

    public function __construct() {

        parent::__construct();

        // instancia um formulario
        $this->form = new TForm('form_busca_GeraRelatorioConselhodeClasseServidor');
        // instancia uma tabela
        $panel = new TPanelForm(700, 100);
        $panel->setColuna(150);

        // adiciona a tabela ao formulario
        $this->form->add($panel);

        // cria um r�tulo para o t�tulo
        $titulo = new TLabel('Relat&oacute;rio das Forma&ccedil;&otilde;es dos Servidores com Correla&ccedil;&atilde;o');
        $titulo->setFontFace('Arial');
        $titulo->setFontColor('red');
        $titulo->setFontSize(12);

        $correlacao_plano_carreira = new TCombo('correlacao_plano_carreira');

        // adiciona o campo Titulo
        $panel->put($titulo, $panel->getColuna(), $panel->getLinha());

        TTransaction::close();
        $items2 = array();
        //Cria um vetor com as opcoes da combo
        $items2['0'] = 'TODOS';
        $items2['DIRETA'] = 'DIRETA';
        $items2['INDIRETA'] = 'INDIRETA';
        $items2['NAO EXISTE'] = 'N&Atilde;O EXISTE';

        // adiciona as opcoes na combo situacao
        $correlacao_plano_carreira->addItems($items2);

        //coloca o valor padrão
        $correlacao_plano_carreira->setValue('TODOS');
        $correlacao_plano_carreira->setSize(40);

        // adiciona campo opcao
        // adiciona campo opcao tipo funcionario
        $panel->putCampo($correlacao_plano_carreira, 'Escolha a op&ccedil;&atilde;o', 0, 1);

        // cria um botao de acao (cadastrar)
        $new_button = new TButton('gerarelatorio');

        //$new_button->setAction(new TAction(array('RelatorioSetorGrupoPDF', '')), 'Gerar Relat&oacute;rio');
        $new_button->setAction(new TAction(array($this, 'onGenerate')), 'Gerar Relat&oacute;rio');

        //adiciona botao gerar grafico
        $panel->putCampo($new_button, null, 0, 1);
        // define quais sao os campos do formulario
        $this->form->setFields(array($new_button, $correlacao_plano_carreira));

        // monta a paina atraves de uma tabela
        $panel = new TPanelForm(700, 500);
        $panel->put($this->form, 0, 0);

        // adiciona a tabela a pagina
        parent::add($panel);
    }

    function onGenerate() {
        new RelatorioFormacaoCorrelacaoServidoresPDF();
    }

}
