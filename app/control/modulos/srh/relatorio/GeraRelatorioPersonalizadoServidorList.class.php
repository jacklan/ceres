<?php

/*
 * classe GeraRelatorioSolicitacaoList
 * Cadastro de GeraRelatorioSolicitacao: Contem a listagem e o formulario de busca
 */

class GeraRelatorioPersonalizadoServidorList extends TPage {

    private $form;     // formulario de cadastro

    /*
     * metodo construtor
     * Cria a pagina, o formulario e a listagem
     */

    public function __construct() {
        parent::__construct();

        // instancia um formulario
        $this->form = new TForm('form_busca_GeraRelatorioPersonalizadoServidor');

        // instancia uma tabela
        $panel = new TPanelForm(700, 100);

        // adiciona a tabela ao formulario
        $this->form->add($panel);

        // cria um rï¿½tulo para o tï¿½tulo
        $titulo = new TLabel('Gerar Relat&oacute;rio Personalizado de Servidores');
        $titulo->setFontFace('Arial');
        $titulo->setFontColor('red');
        $titulo->setFontSize(18);

        // adiciona o campo Titulo
        $panel->put($titulo, $panel->getColuna(), $panel->getLinha());

        // cria os campos do formulario
        $tipo = new TCombo('tipo');
        $regiao = new TCombo('regiao');
        $vinculo = new TCombo('vinculo');
        $situacao = new TCombo('situacao');
        $ordem = new TCombo('ordem');
        $formacao = new TCombo('formacao');
        $redistribuido = new TCheckButton('redistribuido');
        $relotado = new TCheckButton('relotado');
        $quadrosuplementar = new TCheckButton('quadrosuplementar');

        $items5 = array();
        $items5['1'] = 'CESS&Atilde;O';
        $items5['2'] = 'TITULA&Ccedil;&Atilde;O';
        $items5['3'] = 'QUADRO SUPLEMENTAR';
        $items5['4'] = 'REDISTRIBUIDO';
        $items5['5'] = 'RELOTADO';
        $items5['6'] = 'FUN&Ccedil;&Atilde;O';
        $items5['7'] = 'CARGO / NIVEL REMUNERAT&Oacute;RIO';
        $items5['8'] = 'CONSELHO DE CLASSE';
        $items5['9'] = 'LOTA&Ccedil;&Atilde;O / DATA ADMISS&Atilde;O';
        $items5['10'] = 'EMAIL / PISPASEP / DATA ADMISS&Atilde;O';

        $grupoopcoes = new TRadioGroup('opcao');
        $grupoopcoes->addItems($items5);

        //Cria um vetor com as opcoes da combo situacao
        $items = array();
        $items['ATIVOS'] = 'ATIVOS';
        $items['INATIVOS'] = 'INATIVOS';
        $items['TODOS'] = 'TODOS';

        // adiciona as opcoes na combo
        $tipo->addItems($items);
        //coloca o valor padrao no combo
        $tipo->setValue('ATIVOS');
        $tipo->setSize(40);

//Cria um vetor com as opcoes da combo situacao
        $items5 = array();
        $items5['1'] = 'CAMPO ESPECIFICO';
        $items5['2'] = 'NOME';
        $items5['3'] = 'MATRICULA';

        // adiciona as opcoes na combo
        $ordem->addItems($items5);
        //coloca o valor padrao no combo
        $ordem->setValue('1');
        $ordem->setSize(40);

        //cria a colecao da tabela estrangeira
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');
        // instancia objeto da Classe Record
        $repository = new TRepository('vw_regional_servidorRecord');
        // cria um criterio de selecao, ordenado pelo id
        $criteria = new TCriteria;
        // carrega os objetos de acordo com o criterio
        $cadastros = $repository->load($criteria);
        //adiciona os objetos no combo

        foreach ($cadastros as $object) {
            $items2[$object->regional] = $object->regional;
        }
        TTransaction::close();
        $items2['TODAS'] = 'TODAS';

        //cria os campos com opcoes de grafico
        $regiao->addItems($items2);
        //coloca o valor padrÃ£o
        $regiao->setValue('TODAS');
        $regiao->setSize(40);

        //cria a colecao da tabela estrangeira
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');
        // instancia objeto da Classe Record
        $repository = new TRepository('vw_vinculo_servidorRecord');
        // cria um criterio de selecao, ordenado pelo id
        $criteria = new TCriteria;
        // carrega os objetos de acordo com o criterio
        $cadastros = $repository->load($criteria);
        //adiciona os objetos no combo

        foreach ($cadastros as $object) {
            $items3[$object->vinculo] = $object->vinculo;
        }
        TTransaction::close();
        $items3['TODAS'] = 'TODAS';

        //cria os campos com opcoes de grafico
        $vinculo->addItems($items3);
        //coloca o valor padrÃ£o
        $vinculo->setValue('TODAS');
        $vinculo->setSize(40);

        //cria a colecao da tabela estrangeira
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');
        // instancia objeto da Classe Record
        $repository = new TRepository('vw_situacao_servidorRecord');
        // cria um criterio de selecao, ordenado pelo id
        $criteria = new TCriteria;
        // carrega os objetos de acordo com o criterio
        $cadastros = $repository->load($criteria);
        //adiciona os objetos no combo

        foreach ($cadastros as $object) {
            if ($object->situacao == 'CEDIDO(A)') {
                $items4[$object->situacao] = 'CEDIDO(A)';
            } else {
                if ($object->situacao == 'A DISPOSICAO') {
                    $items4[$object->situacao] = 'A DISPOSICAO';
                } else {
                    $items4[$object->situacao] = $object->situacao;
                }
            }
        }
        TTransaction::close();
        $items4['TODAS'] = 'TODAS';

        //cria os campos com opcoes de grafico
        $situacao->addItems($items4);
        //coloca o valor padrÃ£o
        $situacao->setValue('TODAS');
        $situacao->setSize(40);

        //cria a colecao da tabela estrangeira
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');
        // instancia objeto da Classe Record
        $repository = new TRepository('RequisitoCargoRecord');
        // cria um criterio de selecao, ordenado pelo id
        $criteria = new TCriteria;
        $criteria->setProperty('order', 'nome');
        // carrega os objetos de acordo com o criterio
        $cadastros = $repository->load($criteria);
        //adiciona os objetos no combo

        if ($cadastros) {
            $items8['TODAS'] = 'TODAS';
        }
        foreach ($cadastros as $object) {

            $items8[$object->nome] = $object->nome;
        }

        TTransaction::close();

        //cria os campos com opcoes de grafico
        $formacao->addItems($items8);
        //coloca o valor padrÃ£o
        $formacao->setValue('TODAS');
        $formacao->setSize(40);

        // adiciona campo opcao
        $panel->putCampo($tipo, 'Tipo', 0, 0);

        // adiciona campo opcao
        $panel->putCampo($regiao, 'Regional', 0, 1);

        // adiciona campo opcao tipo funcionario
        $panel->putCampo($vinculo, 'V&iacute;nculo', 0, 1);

        // adiciona campo opcao tipo funcionario
        $panel->putCampo($situacao, 'Situa&ccedil;&atilde;o', 0, 1);

        // adiciona campo opcao formacao
        $panel->putCampo($formacao, 'Forma&ccedil;&atilde;o', 0, 1);

        // adiciona campo opcao tipo funcionario
        $panel->putCampo($ordem, 'Ordenar por', 0, 1);

        //adiciona o campo para a opÃ§Ã£o de cadastro do nome
        $panel->putCampo($grupoopcoes, 'Escolha uma op&ccedil;&atilde;o: ', 0, 1);

        // cria um botao de acao (cadastrar)
        $new_button = new \Adianti\Widget\Form\TButton('gerarelatorio');
        $new_button->setImage('fa:file-pdf-o red');
        $new_button2 = new \Adianti\Widget\Form\TButton('gerarelatorio2');
        $new_button2->setImage('fa:file-excel-o green');

        $new_button->setAction(new TAction(array($this, 'onGenerate')), 'Gerar PDF');

        $new_button2->setAction(new TAction([$this, 'onGenerateEXCEL']), 'Gerar EXCEL');

        //adiciona botao gerar grafico
        $panel->putCampo($new_button, null, 0, 1);
        $panel->putCampo($new_button2, null, 0, 1);
        //$panel->put($new_button, $panel->getColuna()+270,$panel->getLinha());
        // define quais sao os campos do formulario
        $this->form->setFields(array($new_button2, $new_button));

        // monta a paina atraves de uma tabela
        $panel = new TPanelForm(700, 500);
        $panel->put($this->form, 0, 0);


        // adiciona a tabela a pagina
        parent::add($panel);
    }

    function onGenerate() {

        $this->form->setData($this->form->getData());
        new RelatorioPersonalizadoServidorPDF();
    }

    function onGenerateEXCEL() {
        $this->form->setData($this->form->getData());
        new RelatorioListaServidoresExcel();
    }

}
