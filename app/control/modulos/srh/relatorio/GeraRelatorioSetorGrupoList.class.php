<?php
ini_set('display_errors', 1);
ini_set('display_startup_erros', 1);
error_reporting(E_ALL);
class GeraRelatorioSetorGrupoList extends TPage
{

    private $form;     // formulario de cadastro

    /*
     * metodo construtor
     * Cria a pagina, o formulario e a listagem
     */
    public function __construct()
    {

        parent::__construct();

        $this->form = new TQuickForm('form_busca_Projeto');
        $this->form->style = 'width: 40%';
        $this->form->setFormTitle('<font color="red" size="3px" face="Arial"><b>Relat&oacute;rio Setor Grupo de Servidores</b></font>');

        $setorgrupo = new TCombo('nomesetorgrupo');
        $nomesetor = new TCombo('nomesetor');

        $itemsSG = [];
        TTransaction::open('pg_ceres');
        $repository = new TRepository('vw_setorgrupoRecord');
        $criteria = new TCriteria;

        $criteria->setProperty('order', 'nomesetorgrupo');
        $criteria->add(new TFilter('empresa_id', '=', $_SESSION['empresa_id']));

        $objects = $repository->load($criteria);

        $itemsSG['TODOS'] = 'TODOS';
        foreach ($objects as $object) {
            $itemsSG[$object->nomesetorgrupo] = $object->nomesetorgrupo;
        }
        TTransaction::close();

        $setorgrupo->addItems($itemsSG);
        $setorgrupo->setValue('TODOS');
        $setorgrupo->setSize(40);

        $itemsNS = [];
        TTransaction::open('pg_ceres');
        $repository = new TRepository('vw_setorgrupoRecord');
        $criteria = new TCriteria;

        $criteria->setProperty('order', 'nomesetor');
        $criteria->add(new TFilter('empresa_id', '=', $_SESSION['empresa_id']));

        $objects = $repository->load($criteria);

        $itemsNS['TODOS'] = 'TODOS';
        foreach ($objects as $object) {
            $itemsNS[$object->nomesetor] = $object->nomesetor;
        }
        TTransaction::close();

        $nomesetor->addItems($itemsNS);
        $nomesetor->setValue('TODOS');
        $nomesetor->setSize(40);

        $setorgrupo->addValidation('Setor Grupo', new TRequiredValidator); // required field
        $nomesetor->addValidation('Nome Setor', new TRequiredValidator); // required field

        $this->form->addQuickField('Setor Grupo ', $setorgrupo, 130);
        $this->form->addQuickField('Setor ', $nomesetor, 130);

        $this->form->addQuickAction('Gerar', new TAction(array($this, 'onGenerate')), 'fa:file-pdf-o red lg');

        $panel = new TVBox();
        $panel->add($this->form);
        $panel->style = 'width:100%;';

        parent::add($panel);

    }

    function onGenerate()
    {
        try {
            $this->form->validate();
            $this->form->setData($this->form->getData());

            new RelatorioSetorGrupoPDF();

        } catch (Exception $e) {
            new \Adianti\Widget\Dialog\TMessage('infor', $e);
        }

    }

}