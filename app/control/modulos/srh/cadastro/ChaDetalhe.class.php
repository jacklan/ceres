<?php

//ini_set('display_errors', 1);
//ini_set('display_startup_erros', 1);
//error_reporting(E_ALL);

/*
 * classe ChaDetalhe
 * Cadastro de Cha: Contem a listagem e o formulario de busca
 */
use Adianti\Database\TFilter1;
use Adianti\Widget\Datagrid\TDatagridTables;

class ChaDetalhe extends TPage 
{

    private $form;     // formulario de cadastro
    private $datagrid; // listagem

    /*
     * metodo construtor
     * Cria a pagina, o formulario e a listagem
     */
    public function __construct()
	{
		
        parent::__construct();

		// instancia um formulario
		$this->form = new TQuickForm;
        $this->form->class = 'form_Cha';
        $this->form->setFormTitle('<font color="red" size="3" face="Arial"><b>Detalhe do CHA</b></font>');
		
		// cria um rotulo para o titulo
        $titulo = new TLabel('<div style="position:floatval; width: 200px;"> <b>* Campo obrigatorio</b></div>');
        $titulo->setFontFace('Arial');
        $titulo->setFontColor('red');
        $titulo->setFontSize(10);
		
        // cria os campos do formulario
        $codigo = new THidden('id');
        $nome = new TEntry('nome');
        $tipohabilidade_id = new TCombo('tipohabilidade_id');
        $usuarioalteracao = new THidden('usuarioalteracao');
        $dataalteracao = new THidden('dataalteracao');

        //cria a colecao da tabela estrangeira habilidade_id
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');
		
        // instancia objeto da Classe Record
        $repository = new TRepository('TipoHabilidadeRecord');
		
        // carrega todos os objetos
        $collection = $repository->load(new TCriteria);
		
        //adiciona os objetos no combo
        foreach( $collection as $object )
		{
			
            $items[$object->id] = $object->nome;
			
        }

        // adiciona as opcoes na combo
        $tipohabilidade_id->addItems($items);

        // finaliza a transacao
        TTransaction::close();

        //cria a colecao da tabela estrangeira
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');
        // instancia objeto da Classe Record
        $repository = new TRepository('ChaRecord');
        // cria um criterio de selecao, ordenado pelo id
        $criteria = new TCriteria;
        $criteria->setProperty('order', 'cargo_id');
        // carrega os objetos de acordo com o criterio
        $cadastros = $repository->load($criteria);

        // finaliza a transacao
        TTransaction::close();

        //cria a colecao da tabela estrangeira do Cabecalho
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');
		
        // instancia objeto da Classe Record
        $cadastro = new CargoRecord( filter_input ( INPUT_GET, 'fk' ) );
		
        // cria um criterio de selecao, ordenado pelo id do servidor
        //adiciona os objetos na tela com as informacoes do servidor
        if( $cadastro ) 
		{
			
            $nomecargo = new TLabel( $cadastro->nome );
			
        }
		
        // finaliza a transacao
        TTransaction::close();

        //coloca o valor do relacionamento
        $id = new THidden('cargo_id');
        $id->setValue( filter_input ( INPUT_GET, 'fk' ) );

        //Campos obrigatórios
        $nome->setProperty('required', 'required');
		$tipohabilidade_id->setProperty('required', 'required');
       
        // define os campos
		$this->form->addQuickField(null, $id, 1);
		$this->form->addQuickField(null, $codigo, 10);
		$this->form->addQuickField(null, $usuarioalteracao, 10);
        $this->form->addQuickField(null, $dataalteracao, 10);
		$this->form->addQuickField('Cargo', $nomecargo, 40);
		$this->form->addQuickField('Tipo Habilidade <font color=red><b>*</b></font>', $tipohabilidade_id, 40);
		$this->form->addQuickField('Nome <font color=red><b>*</b></font>', $nome, 40);
		$this->form->addQuickField(null, $titulo, 50);
        
		 // create an action (Experiencia)
        $action = new TAction(array( $this, 'onSave'));
        $action->setParameter('fk', '' . filter_input(INPUT_GET, 'fk') . '');
			
		// cria um botao de acao
		$this->form->addQuickAction('Salvar', $action, 'ico_save.png');
		$this->form->addQuickAction('Voltar', new TAction(array('CargoList', 'onReload')), 'ico_datagrid.gif');
        
        // instancia objeto DataGrid
        $this->datagrid = new TDataGridTables;

        // instancia as colunas da DataGrid
        $dgnome = new TDataGridColumn('nome', 'Nome', 'left', 1200);
        $dgtipohabilidade = new TDataGridColumn('nome_tipohabilidade', 'Tipo', 'left', 200);

        // adiciona as colunas a DataGrid
        $this->datagrid->addColumn($dgnome);
        $this->datagrid->addColumn($dgtipohabilidade);

        // instancia duas acoes da DataGrid
        $action1 = new TDataGridAction(array($this, 'onEdit'));
        $action1->setLabel('Editar');
        $action1->setImage('ico_edit.png');
        $action1->setField('id');
		$action1->setFk('cargo_id');

        $action2 = new TDataGridAction(array($this, 'onDelete'));
        $action2->setLabel('Deletar');
        $action2->setImage('ico_delete.png');
        $action2->setField('id');
		$action2->setFk('cargo_id');
		
        // adiciona as acoes a DataGrid
        $this->datagrid->addAction($action1);
        $this->datagrid->addAction($action2);

        // cria o modelo da DataGrid, montando sua estrutura
        $this->datagrid->createModel();

        // monta a paina atraves de uma tabela
        $panel = new TPanelForm(700, 500);
        $panel->put($this->form, 0, 0);
        $panel->put($this->datagrid, 150, 165);


        // adiciona a tabela a pagina
        parent::add($panel);
    }

    /*
     * metodo onReload()
     * Carrega a DataGrid com os objetos do banco de dados
     */

    function onReload() {
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');

        // instancia um repositorio da Classe
        $repository = new TRepository('ChaRecord');

        // cria um criterio de selecao
        $criteria = new TCriteria;

        //filtra pelo campo selecionado pelo usuário
        $criteria->add(new TFilter('cargo_id', '=', filter_input ( INPUT_GET, 'fk' )));
		
        $criteria->setProperty('order', 'nome');

        // carrega os objetos de acordo com o criterio
        $cadastros = $repository->load($criteria);
		
        $this->datagrid->clear();
		
        if ($cadastros) {
            // percorre os objetos retornados
            foreach ($cadastros as $cadastro) {
                // adiciona o objeto na DataGrid
                $this->datagrid->addItem($cadastro);
            }
        }

        // finaliza a transacao
        TTransaction::close();
        $this->loaded = true;
		
    }

    /*
     * metodo onDelete()
     * Executada quando o usuario clicar no botao excluir da datagrid
     * Pergunta ao usuario se deseja realmente excluir um registro
     */
    function onDelete($param) {
        // obtem o parametro $key
        $key = $param['key'];

        // define duas acoes
        $action1 = new TAction(array($this, 'Delete'));

        // define os parametros de cada acao
        $action1->setParameter('key', $key);

        //encaminha a Chave estrangeira
        $action1->setParameter('fk', filter_input ( INPUT_GET, 'fk' ));

        // exibe um dialogo ao usuario
        new TQuestion('Deseja realmente excluir o registro ?', $action1);
    }

    /*
     * metodo Delete()
     * Exclui um registro
     */

    function Delete($param) {
		
        // obtem o parametro $key
        $key = $param['key'];
		
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');

        // instanicia objeto Record
        $cadastro = new ChaRecord($key);

        try {
            // deleta objeto do banco de dados
            $cadastro->delete();
			
			// exibe um dialogo ao usuario
            new TMessage("info", "Registro deletado com sucesso!");

            // finaliza a transacao
            TTransaction::close();
        } catch (Exception $e) { // em caso de exceção
		
            // exibe a mensagem gerada pela exceção
            new TMessage('error', $e->getMessage());
            
            // desfaz todas alterações no banco de dados
            TTransaction::rollback();
			
        }

        // re-carrega a datagrid
        $this->onReload();
		 
    }

    /*
     * metodo show()
     * Exibe a pagina
     */

    function show() {
        // carrega os dados no datagrid
        $this->onReload();
        //Chama o metodo show da super classe
        parent::show();
    }

    /*
     * metodo onSave()
     * Executada quando o usuario clicar no botao salvar do formulario
     */

    function onSave() {
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');
        // obtem os dados no formulario em um objeto Record da Classe
        $cadastro = $this->form->getData('ChaRecord');

		//lanca o default
        $cadastro->usuarioalteracao = $_SESSION['usuario'];
        $cadastro->dataalteracao = date("d/m/Y H:i:s");
		
        //antes de armazenar verifica se algum campo eh requerido e nao foi informado
        $dados = $cadastro->toArray();

        $msg = '';
        $icone = 'info';
        
        if (empty($dados['tipohabilidade_id'])) {
            $msg .= 'O Tipo Habilidade deve ser informado.<br/>';
        }

        if (empty($dados['nome'])) {
            $msg .= 'O Nome deve ser informado.<br/>';
        }

        try {

            if ($msg == '') {
                // armazena o objeto
                $cadastro->store();
                $msg = 'Dados armazenados com sucesso';

                // finaliza a transacao
                TTransaction::close();
            } else {
				
               $icone = 'error';
             
			   $this->form->setData($cadastro);   // fill the form with the active record data
			   
            }

            if ($icone == 'error') {
               // msgAlert('ChaDetalhe', 'onReload', 'fk='.$dados['cargo_id'], 'error');
                // exibe mensagem de erro
              new TMessage($icone, $msg);
			  $this->form->setData($cadastro);   // fill the form with the active record data
            } else {
				
				$param = array();
				$param ['fk'] = $dados['cargo_id'];
				
                //chama o formulario com o grid
                // exibe um dialogo ao usuario
                new TMessage("info", "Registro salvo com sucesso!");
				TApplication::gotoPage('ChaDetalhe','onReload', $param); // reload
				
            }
        } catch (Exception $e) { // em caso de exc
		
            // exibe a mensagem gerada pela exceção
            new TMessage('error', $e->getMessage());
			
            // desfaz todas alterações no banco de dados
            TTransaction::rollback();
			
			$this->form->setData($cadastro);   // fill the form with the active record data
			
        }
    }

    /*
     * metodo onEdit()
     * Edita os dados de um registro
     */
    function onEdit($param) {
		
		try 
		{
			
            if (isset($param['key'])) 
			{
				
                // get the parameter $key
                $key = $param['key'];

                TTransaction::open('pg_ceres');   // open a transaction with database 'samples'
				
                $object = new ChaRecord($key);        // instantiates object City
				
                $this->form->setData($object);   // fill the form with the active record data
				
                TTransaction::close();           // close the transaction
				
            } else 
			{
				
                $this->form->clear();
				
            }
			
        } catch (Exception $e) 
		{

			// in case of exception
            // shows the exception error message
            new TMessage('error', '<b>Error</b> ' . $e->getMessage());
            // undo all pending operations
            TTransaction::rollback();
			
        }	
      
    }

}

?>