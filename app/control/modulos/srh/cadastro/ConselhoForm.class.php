<?php
/*
 * classe ConselhoForm
 * Cadastro de Conselho: Contem o formularo
 */

class ConselhoForm extends TPage
{
    private $form;     // formulario de cadastro
    
    /*
     * metodo construtor
     * Cria a pagina do formulario
     */
    public function __construct(){
        parent::__construct();


        // instancia um formulario
        $this->form = new TQuickForm;
        $this->form->class = 'form_empresa';
        $this->form->setFormTitle('<font color="red" size="3" face="Arial"><b>Formul&aacute;rio de Conselho de Classe</b></font>');
        
        // cria um r�tulo para o t�tulo
        $titulo = new TLabel('Cadastro de Conselho de Classe');
        $titulo->setFontFace('Arial');
        $titulo->setFontColor('red');
        $titulo->setFontSize(16);

        // cria os campos do formulario
        $codigo     = new THidden('id');
        $codigo->setEditable(false);
        $nome  = new TEntry('nome');
        $sigla  = new TEntry('sigla');
        $uf = new TCombo('uf');

        $usuarioalteracao = new THidden('usuarioalteracao');
        $dataalteracao = new THidden('dataalteracao');


        // campo obrigatorio
        $campo = new TLabel('<div style="position:floatval; width: 200px;"> <b>* Campo obrigatorio</b></div>');
        $campo->setFontFace('Arial');
        $campo->setFontColor('red');
        $campo->setFontSize(10);

        // cria um vetor com as opcoes da combo estado
        $items= array();
        $items['AC'] = 'AC';
        $items['AL'] = 'AL';
        $items['AM'] = 'AM';
        $items['AP'] = 'AP';
        $items['BA'] = 'BA';
        $items['CE'] = 'CE';
        $items['DF'] = 'DF';
        $items['ES'] = 'ES';
        $items['GO'] = 'GO';
        $items['MA'] = 'MA';
        $items['MG'] = 'MG';
        $items['MS'] = 'MS';
        $items['MT'] = 'MT';
        $items['PA'] = 'PA';
        $items['PB'] = 'PB';
        $items['PE'] = 'PE';
        $items['PI'] = 'PI';
        $items['PR'] = 'PR';
        $items['RJ'] = 'RJ';
        $items['RN'] = 'RN';
        $items['RO'] = 'RO';
        $items['RS'] = 'RS';
        $items['SC'] = 'SC';
        $items['SE'] = 'SE';
        $items['SP'] = 'SP';
        $items['TO'] = 'TO';


        // adiciona as opcoes na combo
        $uf->addItems($items);
        //coloca o valor padrao no combo
        $uf->setValue('RN');

        $this->form->addQuickField(null, $codigo, 10);
        $this->form->addQuickField(null, $usuarioalteracao, 10);
        $this->form->addQuickField(null, $dataalteracao, 10);
        $this->form->addQuickField('Nome <font color=red><b>*</b></font>', $nome, 50);
        $this->form->addQuickField('Sigla <font color=red><b>*</b></font>', $sigla, 50);
        $this->form->addQuickField('Estado <font color=red><b>*</b></font>', $uf, 50);
        
        $this->form->addQuickField(null, $campo, 50);

        //Campos obrigatórios
        $nome->setProperty('required', 'required');
        $sigla->setProperty('required', 'required');


        // cria um botao de acao
        $this->form->addQuickAction('Salvar', new TAction(array($this, 'onSave')), 'ico_save.png');
        $this->form->addQuickAction('Voltar', new TAction(array('ConselhoList', 'onReload')), 'ico_datagrid.gif');


        // adiciona a tabela a pagina
        parent::add($this->form);
    }
    
   
    /*
     * metodo onSave()
     * Executada quando o usuario clicar no botao salvar do formulario
     */
    function onSave()
    {
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');
        // obtem os dados no formulario em um objeto BancoRecord
        $cadastro = $this->form->getData('ConselhoRecord');

        //lanca o default
        $cadastro->usuarioalteracao = $_SESSION['usuario'];
        $cadastro->dataalteracao = date("d/m/Y H:i:s");

        //antes de armazenar verifica se algum campo eh requerido e nao foi informado
        $dados = $cadastro->toArray();

        $msg = '';
        $icone = 'info';

        if (empty ($dados['nome'])){
           $msg .= 'O Nome deve ser informado.<br/>';
        }

        if (empty ($dados['sigla'])){
           $msg .= 'A sigla deve ser informada.<br/>';
        }

        if (empty ($dados['uf'])){
           $msg .= 'O Estado deve ser informado.<br/>';
        }
        try{

            if ($msg == ''){
              // armazena o objeto
              $cadastro->store();
              $msg = 'Dados armazenados com sucesso';

              // finaliza a transacao
              TTransaction::close();

            }else{
                $icone = 'error';
            }
            
            if ($icone == 'error'){
                // exibe mensagem de erro
                new TMessage($icone, $msg);
				$this->form->setData( $cadastro );   // fill the form with the active record data
            }else{
                // exibe um dialogo ao usuario
                new TMessage( "info", $msg );
				
				TApplication::gotoPage( 'ConselhoList', 'onReload' ); // reload
            }
        }
        catch (Exception $e) // em caso de exce��o
        {
            // exibe a mensagem gerada pela exce��o
            new TMessage('error', $e->getMessage());
            // desfaz todas altera��es no banco de dados
            TTransaction::rollback();
			$this->form->setData($cadastro);   // fill the form with the active record data
        }

    }
    
    
    /*
     * metodo onEdit()
     * Edita os dados de um registro
     */
    function onEdit( $param ) 
	{
		
		try 
		{
			
            if( isset( $param['key'] ) ) 
			{

                // get the parameter $key
                $key = $param['key'];

                TTransaction::open('pg_ceres');   // open a transaction with database 'samples'

                $object = new ConselhoRecord( $key );  // instantiates object City

                $this->form->setData( $object );   // fill the form with the active record data

                TTransaction::close();           // close the transaction
				
            }else 
			{
				
                $this->form->clear();
				
            }
			
        } catch (Exception $e) 
		{

			// in case of exception
            // shows the exception error message
            new TMessage('error', '<b>Error</b> ' . $e->getMessage());
			
            // undo all pending operations
            TTransaction::rollback();
			
        }
      
    }

}
?>