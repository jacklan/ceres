<?php
/*
 * classe HabilidadeForm
 * Cadastro de Habilidade: Contem o formularo
 */

class HabilidadeForm extends TPage
{
    private $form;     // formulario de cadastro
    
    /*
     * metodo construtor
     * Cria a pagina do formulario
     */
    public function __construct()
    {
        parent::__construct();
		
		// instancia um formulario
        $this->form = new TQuickForm;
        $this->form->class = 'form_Habilidade';
        $this->form->setFormTitle('<font color="red" size="3" face="Arial"><b>Formul&aacute;rio de Perfil do Servidor</b></font>');

        // cria os campos do formulario
        $codigo     = new THidden('id');
        $codigo->setEditable(false);
        $tipohabilidade_id =  new TCombo('tipohabilidade_id');
        $nome  = new TEntry('nome');

        $usuarioalteracao = new THidden('usuarioalteracao');
        $dataalteracao = new THidden('dataalteracao');
		
		// cria um rotulo para o titulo
        $titulo = new TLabel('<div style="position:floatval; width: 200px;"> <b>* Campo obrigatorio</b></div>');
        $titulo->setFontFace('Arial');
        $titulo->setFontColor('red');
        $titulo->setFontSize(10);

   //cria a colecao da tabela estrangeira habilidade_id
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');
        // instancia objeto da Classe Record
        $repository = new TRepository('TipoHabilidadeRecord');
        // carrega todos os objetos
        $collection = $repository->load(new TCriteria);
        //adiciona os objetos no combo
        foreach ($collection as $object) {
            $items[$object->id] = $object->nome;
        }

        // adiciona as opcoes na combo
        $tipohabilidade_id->addItems($items);

        // finaliza a transacao
        TTransaction::close();
		
		// define os campos
        $this->form->addQuickField(null, $codigo, 10);
		$this->form->addQuickField(null, $usuarioalteracao, 10);
        $this->form->addQuickField(null, $dataalteracao, 10);
		$this->form->addQuickField("Nome <font color=red><b>*</b></font>", $nome, 50);
		$this->form->addQuickField("Tipo Habilidade <font color=red><b>*</b></font>", $tipohabilidade_id, 50 );	
		$this->form->addQuickField(null, $titulo, 50);
                
        //Campos obrigatórios
        $nome->setProperty('required', 'required');              
        $tipohabilidade_id->setProperty('required', 'required');              
        // cria um botao de acao
        $this->form->addQuickAction('Salvar', new TAction(array($this, 'onSave')), 'ico_save.png');
        $this->form->addQuickAction('Voltar', new TAction(array('HabilidadeList', 'onReload')), 'ico_datagrid.gif');

        // adiciona a tabela a pagina
        parent::add($this->form);
    }
    
   
    /*
     * metodo onSave()
     * Executada quando o usuario clicar no botao salvar do formulario
     */
    function onSave()
    {
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');
        // obtem os dados no formulario em um objeto CarroRecord
        $cadastro = $this->form->getData('HabilidadeRecord');

        //antes de armazenar verifica se algum campo eh requerido e nao foi informado
        $dados = $cadastro->toArray();

        $msg = '';
        $icone = 'info';

        if (empty ($dados['nome'])){
           $msg .= 'O Nome deve ser informado.<br/>';
        }
		if (empty ($dados['tipohabilidade_id'])){
           $msg .= 'A Habilidade deve ser informado.<br/>';
        }

        try{

            if ($msg == ''){
              // armazena o objeto
              $cadastro->store();
              $msg = 'Dados armazenados com sucesso';

              // finaliza a transacao
              TTransaction::close();

            }else{
                $icone = 'error';
            }
            
            if ($icone == 'error'){
                // exibe mensagem de erro
                new TMessage($icone, $msg);
				$this->form->setData($cadastro);   // fill the form with the active record data
            }else{
                //chama o formulario com o grid
                new TMessage("info", "Registro salvo com sucesso!");
				TApplication::gotoPage('HabilidadeList','onReload'); // reload
            }
        }
        catch (Exception $e) // em caso de exce��o
        {
            // exibe a mensagem gerada pela exce��o
            new TMessage('error', $e->getMessage());
            // desfaz todas altera��es no banco de dados
            TTransaction::rollback();
			$this->form->setData($cadastro);   // fill the form with the active record data
        }

    }
    
    
    /*
     * metodo onEdit()
     * Edita os dados de um registro
     */
    function onEdit($param)
    {
		try {
            if (isset($param['key'])) {

                // get the parameter $key
                $key = $param['key'];

                TTransaction::open('pg_ceres');   // open a transaction with database 'samples'

                $object = new HabilidadeRecord($key);        // instantiates object City

                $this->form->setData($object);   // fill the form with the active record data

                TTransaction::close();           // close the transaction
            } else {
                $this->form->clear();
            }
        } catch (Exception $e) { // in case of exception
            // shows the exception error message
            new TMessage('error', '<b>Error</b> ' . $e->getMessage());
            // undo all pending operations
            TTransaction::rollback();
        }		
        
    }

}
?>