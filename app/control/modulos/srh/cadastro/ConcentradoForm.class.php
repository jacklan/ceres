<?php
/*
 * classe ConcentradoForm
 * Cadastro de Concentrado: Contem o formularo
 */
include_once 'app.library/funcdate.php';

class ConcentradoForm extends TPage
{
    private $form;     // formulario de cadastro
    
    /*
     * metodo construtor
     * Cria a pagina do formulario
     */
    public function __construct()
    {
        parent::__construct();

        // instancia um formulario
        $this->form = new TForm('form_Concentrado');
        
        // instancia uma tabela
        $panel = new TPanelForm(400, 500);


        // adiciona a tabela ao formulario
        $this->form->add($panel);
        
        // cria um r�tulo para o t�tulo
        $titulo = new TLabel('Cadastro de Concentrado');
        $titulo->setFontFace('Arial');
        $titulo->setFontColor('red');
        $titulo->setFontSize(18);

        // cria os campos do formulario
        $codigo     = new TEntry('id');
        $codigo->setEditable(false);
        $nome  = new TEntry('nome');
        $valorunitario = new TEntry('valorunitario');

        // define os tamanhos dos campos
        $codigo->setSize(40);
        $nome->setSize(300);
        $valorunitario->setSize(50);

        // adiciona o campo titulo
        $panel->put($titulo,$panel->getColuna(),$panel->getLinha());

        // adiciona o campo Codigo
        $panel->putCampo($codigo, 'Codigo', 0, 1);

        // adiciona o campo Nome
        $panel->putCampo($nome, 'Nome', 0, 1);

        // adiciona o campo Valor Unitario
        $panel->putCampo($valorunitario, 'Valor Unit&aacute;rio', 0, 1);

        // cria um botao de acao (salvar)
        $save_button=new TButton('save');
        $cancel_button=new TButton('cancel');
        // define a acao do botao
        $save_button->setAction(new TAction(array($this, 'onSave')), 'Salvar');
        // prepara o botao para chamar o form anterior
        //$lista = new ConcentradoList;
        $cancel_button->setAction(new TAction(array('ConcentradoList', '')), 'Cancelar');

        // adiciona a acao do formulario
        $panel->putCampo($save_button, null, -100, 1);
        $panel->putCampo($cancel_button, null, 0, 0);

        // define quais sao os campos do formulario
        $this->form->setFields(array($codigo, $nome, $save_button, $cancel_button));

        // adiciona a tabela a pagina
        parent::add($this->form);
    }
    
   
    /*
     * metodo onSave()
     * Executada quando o usuario clicar no botao salvar do formulario
     */
    function onSave()
    {
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');
        // obtem os dados no formulario em um objeto ConcentradoRecord
        $cadastro = $this->form->getData('ConcentradoRecord');

        //antes de armazenar verifica se algum campo eh requerido e nao foi informado
        $dados = $cadastro->toArray();

        $msg = '';
        $icone = 'info';

        if (empty ($dados['nome'])){
           $msg .= 'O Nome deve ser informado.';
        }

        try{

            if ($msg == ''){
              // armazena o objeto
              $cadastro->store();
              $msg = 'Dados armazenados com sucesso';

              // finaliza a transacao
              TTransaction::close();

            }else{
                $icone = 'error';
            }
            
            if ($icone == 'error'){
                // exibe mensagem de erro
                new TMessage($icone, $msg);
            }else{
                //chama o formulario com o grid
                // define duas acoes
                $action1 = new TAction(array('ConcentradoList', ''));
                // exibe um dialogo ao usuario
                new TConfirmation($msg, $action1);
            }
        }
        catch (Exception $e) // em caso de exce��o
        {
            // exibe a mensagem gerada pela exce��o
            new TMessage('error', $e->getMessage());
            // desfaz todas altera��es no banco de dados
            TTransaction::rollback();
        }

    }
    
    
    /*
     * metodo onEdit()
     * Edita os dados de um registro
     */
    function onEdit($param)
    {
        // obtem o parametro $key
        $key=$param['key'];
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');
        // instancia objeto da Classe Record
        $cadastro = new ConcentradoRecord($key);
        // lanca os dados no formulario
        $this->form->setData($cadastro);

        // finaliza a transacao
        TTransaction::close();
    }

}
?>