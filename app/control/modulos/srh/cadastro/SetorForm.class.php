<?php

/*
 * classe SetorForm
 * Cadastro de Setor: Contem o formularo
 */

//ini_set('display_errors', 1);
//ini_set('display_startup_erros', 1);
//error_reporting(E_ALL);

class SetorForm extends TPage {

    private $form;     // formulario de cadastro

    /*
     * metodo construtor
     * Cria a pagina do formulario
     */

    public function __construct() {
        parent::__construct();

        // instancia um formulario
        $this->form = new TQuickForm;
        $this->form->class = 'form_Setor';
        $this->form->setFormTitle('<font color="red" size="3" face="Arial"><b>Formul&aacute;rio de Setor</b></font>');

        // cria os campos do formulario
        $codigo = new THidden('id');
        $nome = new TEntry('nome');
        $situacao = new TCombo('situacao');
        $bhbasesetor = new TEntry('bhbasesetor');
        $bhbasedias = new TEntry('bhdiassetor');
        /* ------------------ MultiSearch ------------------ */

        /* ---------- cria um criterio de selecao ---------- */
        $criteria_servidor = new TCriteria;
        $criteria_servidor->add(new TFilter('situacao', '=', 'EM ATIVIDADE'), TExpression::OR_OPERATOR);
        $criteria_servidor->add(new TFilter('situacao', '=', 'A DISPOSICAO'), TExpression::OR_OPERATOR);

        $criteria3 = new TCriteria;
        if ($_SESSION['empresa_id'] == 1) {
            $criteria3->add(new TFilter('empresa_id', '=', $_SESSION['empresa_id']));
        }
        $criteria3->add($criteria_servidor);
        /* ---------- fim criacao criterio de selecao ---------- */

        /* --------- montar campo --------- */
        $servidor_id = new TDBMultiSearch('servidor_id', 'pg_ceres', 'vw_servidor_multisearchRecord', 'servidor_id', 'nome_matricula_servidor', 'nome_matricula_servidor', $criteria3);
        $servidor_id->style = "text-transform: uppercase;";
        $servidor_id->setProperty('placeholder', 'Nome ou Matricula ou CPF');
        $servidor_id->setMinLength(1);
        $servidor_id->setMaxSize(1);
        /*  --------- fim montar campo --------- */

        /* ------------------ MultiSearch Servidor ------------------ */
        $setorgrupo_id = new TCombo('setorgrupo_id');

        $usuarioalteracao = new THidden('usuarioalteracao');
        $dataalteracao = new THidden('dataalteracao');

        //carrega como situacao
        $items2 = array();
        $items2['ATIVO'] = 'ATIVO';
        $items2['INATIVO'] = 'INATIVO';
        $situacao->addItems($items2);

        $bhbasesetor->setProperty('placeholder', '20');
        $bhbasedias->setProperty('placeholder', '2');

        //cria a colecao da tabela estrangeira
        // inicia transacao com o banco 'pg_ceres'
        $items1 = array();
        TTransaction::open('pg_ceres');
        // instancia objeto da Classe Record
        $repository = new TRepository('SetorGrupoRecord');
        // carrega todos os objetos
        $criteria = new TCriteria;
        $criteria->setProperty('order', 'nome');
        $criteria->add(new TFilter('empresa_id', '=', $_SESSION['empresa_id']));

        // carrega os objetos de acordo com o criterio
        $collection = $repository->load($criteria);
        //adiciona os objetos no combo
        foreach ($collection as $object) {
            $items1[$object->id] = $object->nome;
        }

        // adiciona as opcoes na combo
        $setorgrupo_id->addItems($items1);

        // finaliza a transacao
        TTransaction::close();

        // cria um rotulo para o titulo
        $titulo = new TLabel('<div style="width: 200px;"> <b>* Campo obrigatorio</b></div>');
        $titulo->setFontFace('Arial');
        $titulo->setFontColor('red');
        $titulo->setFontSize(10);

        // define os campos
        $this->form->addQuickField(null, $codigo, 10);
        $this->form->addQuickField(null, $usuarioalteracao, 10);
        $this->form->addQuickField(null, $dataalteracao, 10);
        $this->form->addQuickField("Nome <font color=red><b>*</b></font>", $nome, 50);
        $this->form->addQuickField("Situação", $situacao, 50);
        $this->form->addQuickField("Servidor", $servidor_id, 450);
        $this->form->addQuickField("Setor Grupo", $setorgrupo_id, 50);
        $this->form->addQuickField("BH Base Horas", $bhbasesetor, 50);
        $this->form->addQuickField("BH Base Mês", $bhbasedias, 50);
        $this->form->addQuickField(null, $titulo, 50);

        // cria um botao de acao
        $this->form->addQuickAction('Salvar', new TAction(array($this, 'onSave')), 'ico_save.png')->class = 'btn btn-info';
        $this->form->addQuickAction('Voltar', new TAction(array('SetorList', 'onReload')), 'ico_datagrid.gif');

        // adiciona a tabela a pagina
        parent::add($this->form);
    }

    /*
     * metodo onSave()
     * Executada quando o usuario clicar no botao salvar do formulario
     */

    function onSave() {

        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');
        // obtem os dados no formulario em um objeto CarroRecord
        $object = $this->form->getData('SetorRecord');

        //lanca o default
        $object->servidor_id = key($object->servidor_id); // retorna o key do array
        $object->usuarioalteracao = $_SESSION['usuario'];
        $object->dataalteracao = date("d/m/Y H:i:s");
        $object->empresa_id = $_SESSION['empresa_id'];

        //antes de armazenar verifica se algum campo eh requerido e nao foi informado
        $dados = $object->toArray();

        $msg = '';
        $icone = 'info';

        if (empty($dados['nome'])) {
            $msg .= 'O Nome deve ser informado.';
        }

        try {

            if ($msg == '') {
                // armazena o objeto
                $object->store();
                $msg = 'Dados armazenados com sucesso';

                // finaliza a transacao
                TTransaction::close();
            } else {
                $icone = 'error';
            }

            if ($icone == 'error') {

                // exibe mensagem de erro
                new TMessage($icone, $msg);

                $this->form->setData($object);   // fill the form with the active record data
            } else {

                // exibe um dialogo ao usuario
                new TMessage("info", "Registro salvo com sucesso!");
                TApplication::gotoPage('SetorList', 'onReload'); // reload
            }
        } catch (Exception $e) { // em caso de exc
            // exibe a mensagem gerada pela exce��o
            new TMessage('error', $e->getMessage());
            // desfaz todas altera��es no banco de dados
            TTransaction::rollback();
            $this->form->setData($this->form->getData());   // fill the form with the active record data
        }
    }

    /*
     * metodo onEdit()
     * Edita os dados de um registro
     */

    function onEdit($param) {

        try {
            if (isset($param['key'])) {

                // get the parameter $key
                $key = $param['key'];

                TTransaction::open('pg_ceres');   // open a transaction with database 'samples'

                $object = new SetorRecord($key);        // instantiates object 

                $objServidor = new ServidorRecord($object->servidor_id);
                $object->servidor_id = [$objServidor->id => $objServidor->nome . " - " . $objServidor->matricula . " - " . $objServidor->cpf];

                $this->form->setData($object);   // fill the form with the active record data

                TTransaction::close();           // close the transaction
            } else {
                $this->form->clear();
            }
        } catch (Exception $e) { // in case of exception
            // shows the exception error message
            new TMessage('error', '<b>Error</b> ' . $e->getMessage());
            // undo all pending operations
            TTransaction::rollback();
        }
    }

}

?>