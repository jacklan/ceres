<?php

/*
 * classe GeraRelatorioSolicitacaoReport
 * Cadastro de GeraRelatorioSolicitacao: Contem a listagem e o formulario de busca
 */

include_once 'app/lib/funcdate.php';

use Adianti\Wrapper\TRPDFPadraoDesigner;


//ini_set('display_errors', 1);
//ini_set('display_startup_erros', 1);
//error_reporting(E_ALL);

class GeraRelatorioCertidaoTempoServicoReport extends TPage {

    private $form;
    
    public function __construct() {
        parent::__construct();

        // instancia um formulario
        $this->form = new TForm('form_busca_GeraRelatorioCertidaoTempoServico');
		
        // instancia uma tabela
        $panel = new TPanelForm(700, 100);
        $panel->setColuna(150);

        // adiciona a tabela ao formulario
        $this->form->add($panel);

        // cria um r�tulo para o t�tulo
        $titulo = new TLabel('Certid&atilde;o por Tempo de Servi&ccedil;o');
        $titulo->setFontFace('Arial');
        $titulo->setFontColor('red');
        $titulo->setFontSize(18);

        // adiciona o campo Titulo
        $panel->put($titulo, $panel->getColuna(), $panel->getLinha());

        // cria os campos do formulario
        $matricula = new TEntry('matricula');
        $matricula->setSize(40);
        
        // cria um botao de acao (cadastrar)
        $new_button = new TButton('gerarelatorio');

        //  $obj = new RelatorioAniversariantesPDF();
        // define a acao do botao cadastrar
        $new_button->setAction($action = new TAction(array($this, 'onGenerate')), 'Gerar Relat&oacute;rio');
        $new_button->setProperty('target', "_blank");

        $panel->setLinha(50);
        // adiciona o campo
        $panel->setColuna2(120);
        // adiciona campo opcao
        $panel->putCampo($matricula, 'Matricula:', 0, 0);

        //adiciona botao gerar grafico
        $panel->putCampo($new_button, null, 0, 1);
        // $panel->put($new_button, $panel->getColuna()+270,$panel->getLinha());
        // define quais sao os campos do formulario
        $this->form->setFields(array($matricula, $new_button));

        // monta a paina atraves de uma tabela
        $panel = new TPanelForm(700, 500);
        $panel->put($this->form, 0, 0);

        // adiciona a tabela a pagina
        parent::add($panel);
    }


    function onGenerate() {
    

        $nome_rel = 'CERTIDÃO DE TEMPO DE SERVIÇO';
        $nome_arq = "app/reports/" . $nome_rel . $_SESSION['servidor_id'] . ".pdf";

        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');
        $empresa = new EmpresaRecord($_SESSION['empresa_id']);
        TTransaction::close();

        $relat = new TRPDFPadraoDesigner("P", "A4", $empresa->id, $empresa->nome, $nome_rel);
        $relat->generate();

        // Detalhe ------------------------------------------------------------------------------------------------------------

        $matricula = $_REQUEST['matricula'];

        TTransaction::open('pg_ceres');
        $criteria = new TCriteria;
        // instancia um repositorio para Carros
        $repository = new TRepository('vw_servidor_certidaotemposervicoRecord');
        //filtra pelo campo selecionado pelo usuário
        $criteria->add(new TFilter('matricula', '=', $matricula));
        // carrega os objetos de acordo com o criterio
        $rows02 = $repository->load($criteria);
        // finaliza a transacao
        TTransaction::close();

        $relat->SetY(42);

        $i = 0;
        $faltas = 0;
        $suspencao = 0;
        $licenca = 0;
        $licencaPremio = 0;
        $averbacao = 0;

        if ($rows02) {
            // percorre os objetos retornados
            foreach ($rows02 as $row02) {

                // adiciona os dados do perfil do usuario no menu
                if ($row02->nomeservidor == '' || $row02->nomeservidor == null) {
                    $relat->SetFont('arial', '', 15);
                    $relat->SetX("15");
                    $relat->Cell(0, 5, utf8_decode("Nao existem dados cadastrados para os parametros desta consulta"), 0, 1, 'J');
                    $relat->Ln();
                }

                //define a fonte a ser usada
                $relat->SetFont('arial', '', 9);
                $relat->SetX("10");
                $relat->Cell(0, 5, utf8_decode('Nome Servidor: ' . $row02->nomeservidor), 0, 0, 'J');

                $relat->SetX("130");
                $relat->Cell(0, 5, utf8_decode('Matrícula: ' . $row02->matricula . ' Idade(Anos): ' . calcularIdade($row02->nascimento)), 0, 1, 'J');

                $relat->SetX("10");
                $relat->Cell(0, 5, utf8_decode("Cargo: " . $row02->cargonome . " Classe: " . $row02->classecargo . " Nivel: " . $row02->nivelsalarial . " Unidade Lotação: " . $row02->lotacao), 0, 1, 'J');
                $relat->SetX("10");

                if ($row02->dataaposentadoria <> null) {
                    $relat->Cell(0, 5, utf8_decode("Tempo de Serviço no Órgão:" . formatar_data($row02->admissao) . " a " . $row02->dataaposentadoria), 0, 1, 'J');
                } else {
                    $relat->Cell(0, 5, utf8_decode("Tempo de Serviço no Órgão:" . formatar_data($row02->admissao) . " a até atual momento"), 0, 1, 'J');
                }

                $relat->SetX("10");
                $relat->Cell(0, 5, utf8_decode('Ano'), 0, 0, 'J');

                $relat->SetX("20");
                $relat->Cell(0, 5, utf8_decode('Tempo'), 0, 0, 'J');

                $relat->SetX("80");
                $relat->Cell(0, 5, utf8_decode('Deduções'), 0, 0, 'J');

                $relat->SetX("150");
                $relat->Cell(0, 5, utf8_decode('Tempo'), 0, 1, 'J');

                $relat->SetX("20");
                $relat->Cell(0, 5, utf8_decode('Bruto'), 0, 0, 'J');

                $relat->SetX("40");
                $relat->Cell(0, 5, utf8_decode('Faltas'), 0, 0, 'J');

                $relat->SetX("60");
                $relat->Cell(0, 5, utf8_decode('Licenças'), 0, 0, 'J');

                $relat->SetX("80");
                $relat->Cell(0, 5, utf8_decode('Suspenção'), 0, 0, 'J');

                $relat->SetX("103");
                $relat->Cell(0, 5, utf8_decode('Outras'), 0, 0, 'J');

                $relat->SetX("120");
                $relat->Cell(0, 5, utf8_decode('Soma'), 0, 0, 'J');

                $relat->SetX("170");
                $relat->Cell(0, 5, utf8_decode('Líquido'), 0, 1, 'J');

                $inicio = $row02->ano_inicio;
                $fim = $row02->ano_fim;
                $somames = 0;
                $liquidomes = 0;
                $liquidosubtotal = 0;

                $relat->SetX("10");
                $relat->Cell(0, 5, utf8_decode($row02->ano_inicio), 0, 0, 'J');

                $relat->SetX("20");
                $relat->Cell(0, 5, utf8_decode($row02->diasinicio), 0, 0, 'J');

                //imprimir faltas
                TTransaction::open('pg_ceres');
                // cria um criterio de selecao, ordenado pelo id
                $criteria1 = new TCriteria;
                // instancia um repositorio para Carros
                $repository1 = new TRepository('vw_servidor_certidaotemposervico_faltasRecord');
                //filtra pelo campo selecionado pelo usuário
                $criteria1->add(new TFilter('servidor_id', '=', $servidor_id));
                $criteria1->add(new TFilter('ano', '=', $inicio));

                // carrega os objetos de acordo com o criterio
                $rows1 = $repository1->load($criteria1);
                if ($rows1) {
                    foreach ($rows1 as $row1) {
                        $relat->SetX("40");
                        $relat->Cell(0, 5, $row1->dias, 0, 0, 'J');
                        $faltas += $row1->dias;
                        $somames += $row1->dias;
                    }
                } else {
                    $relat->SetX("40");
                    $relat->Cell(0, 5, '-', 0, 0, 'J');
                }

                //imprimir licencas
                // cria um criterio de selecao, ordenado pelo id
                $criteria1 = new TCriteria;
                // instancia um repositorio para Carros
                $repository1 = new TRepository('vw_servidor_certidaotemposervico_licencasRecord');
                //filtra pelo campo selecionado pelo usuário
                $criteria1->add(new TFilter('servidor_id', '=', $servidor_id));
                $criteria1->add(new TFilter('ano', '=', $inicio));

                // carrega os objetos de acordo com o criterio
                $rows1 = $repository1->load($criteria1);
                if ($rows1) {
                    foreach ($rows1 as $row1) {
                        $relat->SetX("60");
                        $relat->Cell(0, 5, $row1->dias, 0, 0, 'J');
                        $licenca += $row1->dias;
                        $somames += $row1->dias;
                    }
                } else {
                    $relat->SetX("60");
                    $relat->Cell(0, 5, '-', 0, 0, 'J');
                }

                //imprimir suspensao
                // cria um criterio de selecao, ordenado pelo id
                $criteria1 = new TCriteria;
                // instancia um repositorio para Carros
                $repository1 = new TRepository('vw_servidor_certidaotemposervico_suspensaoRecord');
                //filtra pelo campo selecionado pelo usuário
                $criteria1->add(new TFilter('servidor_id', '=', $servidor_id));
                $criteria1->add(new TFilter('ano', '=', $inicio));

                // carrega os objetos de acordo com o criterio
                $rows1 = $repository1->load($criteria1);
                if ($rows1) {
                    foreach ($rows1 as $row1) {
                        $relat->SetX("80");
                        $relat->Cell(0, 5, $row1->dias, 0, 0, 'J');
                        $suspencao += $row1->dias;
                        $somames += $row1->dias;
                    }
                } else {
                    $relat->SetX("80");
                    $relat->Cell(0, 5, '-', 0, 0, 'J');
                }
                // finaliza a transacao
                TTransaction::close();

                $liquidomes = ($row02->diasinicio - $somames);
                $liquidosubtotal = $liquidomes;
                $relat->SetX("120");
                $relat->Cell(0, 5, $somames, 0, 0, 'J');

                $relat->SetX("170");
                $relat->Cell(0, 5, $liquidomes, 0, 1, 'J');


                $inicio++;
                $somadias = ($faltas + $licenca + $suspencao);
                $servidor_id = $row02->servidor_id;

                while ($inicio < $fim) {
                    $somames = 0;
                    $liquidomes = 0;
                    $diasano = 0;
                    //verificar se ano e bissexto
                    if (( (($inicio % 4) == 0 && ($inicio % 100) != 0) || ($inicio % 400) == 0)) {
                        $diasano = 366;
                    } else {
                        $diasano = 365;
                    }

                    $relat->SetX("10");
                    $relat->Cell(0, 5, utf8_decode($inicio), 0, 0, 'J');

                    $relat->SetX("20");
                    $relat->Cell(0, 5, utf8_decode($diasano), 0, 0, 'J');

                    //imprimir faltas
                    TTransaction::open('pg_ceres');
                    // cria um criterio de selecao, ordenado pelo id
                    $criteria1 = new TCriteria;
                    // instancia um repositorio para Carros
                    $repository1 = new TRepository('vw_servidor_certidaotemposervico_faltasRecord');
                    //filtra pelo campo selecionado pelo usuário
                    $criteria1->add(new TFilter('servidor_id', '=', $servidor_id));
                    $criteria1->add(new TFilter('ano', '=', $inicio));

                    // carrega os objetos de acordo com o criterio
                    $rows1 = $repository1->load($criteria1);
                    if ($rows1) {
                        foreach ($rows1 as $row1) {
                            $relat->SetX("40");
                            $relat->Cell(0, 5, $row1->dias, 0, 0, 'J');
                            $faltas += $row1->dias;
                            $somames += $row1->dias;
                        }
                    } else {
                        $relat->SetX("40");
                        $relat->Cell(0, 5, '-', 0, 0, 'J');
                    }

                    //imprimir licencas
                    // cria um criterio de selecao, ordenado pelo id
                    $criteria1 = new TCriteria;
                    // instancia um repositorio para Carros
                    $repository1 = new TRepository('vw_servidor_certidaotemposervico_licencasRecord');
                    //filtra pelo campo selecionado pelo usuário
                    $criteria1->add(new TFilter('servidor_id', '=', $servidor_id));
                    $criteria1->add(new TFilter('ano', '=', $inicio));

                    // carrega os objetos de acordo com o criterio
                    $rows1 = $repository1->load($criteria1);
                    if ($rows1) {
                        foreach ($rows1 as $row1) {
                            $relat->SetX("60");
                            $relat->Cell(0, 5, $row1->dias, 0, 0, 'J');
                            $licenca += $row1->dias;
                            $somames += $row1->dias;
                        }
                    } else {
                        $relat->SetX("60");
                        $relat->Cell(0, 5, '-', 0, 0, 'J');
                    }

                    //imprimir suspensao
                    // cria um criterio de selecao, ordenado pelo id
                    $criteria1 = new TCriteria;
                    // instancia um repositorio para Carros
                    $repository1 = new TRepository('vw_servidor_certidaotemposervico_suspensaoRecord');
                    //filtra pelo campo selecionado pelo usuário
                    $criteria1->add(new TFilter('servidor_id', '=', $servidor_id));
                    $criteria1->add(new TFilter('ano', '=', $inicio));

                    // carrega os objetos de acordo com o criterio
                    $rows1 = $repository1->load($criteria1);
                    if ($rows1) {
                        foreach ($rows1 as $row1) {
                            $relat->SetX("80");
                            $relat->Cell(0, 5, $row1->dias, 0, 0, 'J');
                            $suspencao += $row1->dias;
                            $somames += $row1->dias;
                        }
                    } else {
                        $relat->SetX("80");
                        $relat->Cell(0, 5, '-', 0, 0, 'J');
                    }
                    // finaliza a transacao
                    TTransaction::close();

                    $liquidomes = ($diasano - $somames);
                    $relat->SetX("120");
                    $relat->Cell(0, 5, $somames, 0, 0, 'J');

                    $relat->SetX("170");
                    $relat->Cell(0, 5, $liquidomes, 0, 1, 'J');


                    $inicio++;
                    $somadias = ($faltas + $licenca + $suspencao);
                    $liquidosubtotal += $liquidomes;
                }

                $relat->SetX("10");
                $relat->Cell(0, 5, utf8_decode($fim), 0, 0, 'J');

                $relat->SetX("20");
                $relat->Cell(0, 5, utf8_decode($row02->diasfim), 0, 0, 'J');
                //imprimir faltas
                TTransaction::open('pg_ceres');
                // cria um criterio de selecao, ordenado pelo id
                $criteria1 = new TCriteria;
                // instancia um repositorio para Carros
                $repository1 = new TRepository('vw_servidor_certidaotemposervico_faltasRecord');
                //filtra pelo campo selecionado pelo usuário
                $criteria1->add(new TFilter('servidor_id', '=', $servidor_id));
                $criteria1->add(new TFilter('ano', '=', $inicio));

                // carrega os objetos de acordo com o criterio
                $rows1 = $repository1->load($criteria1);
                if ($rows1) {
                    foreach ($rows1 as $row1) {
                        $relat->SetX("40");
                        $relat->Cell(0, 5, $row1->dias, 0, 0, 'J');
                        $faltas += $row1->dias;
                        $somames += $row1->dias;
                    }
                } else {
                    $relat->SetX("40");
                    $relat->Cell(0, 5, '-', 0, 0, 'J');
                }

                //imprimir licencas
                // cria um criterio de selecao, ordenado pelo id
                $criteria1 = new TCriteria;
                // instancia um repositorio para Carros
                $repository1 = new TRepository('vw_servidor_certidaotemposervico_licencasRecord');
                //filtra pelo campo selecionado pelo usuário
                $criteria1->add(new TFilter('servidor_id', '=', $servidor_id));
                $criteria1->add(new TFilter('ano', '=', $inicio));

                // carrega os objetos de acordo com o criterio
                $rows1 = $repository1->load($criteria1);
                if ($rows1) {
                    foreach ($rows1 as $row1) {
                        $relat->SetX("60");
                        $relat->Cell(0, 5, $row1->dias, 0, 0, 'J');
                        $licenca += $row1->dias;
                        $somames += $row1->dias;
                    }
                } else {
                    $relat->SetX("60");
                    $relat->Cell(0, 5, '-', 0, 0, 'J');
                }

                //imprimir suspensao
                // cria um criterio de selecao, ordenado pelo id
                $criteria1 = new TCriteria;
                // instancia um repositorio para Carros
                $repository1 = new TRepository('vw_servidor_certidaotemposervico_suspensaoRecord');
                //filtra pelo campo selecionado pelo usuário
                $criteria1->add(new TFilter('servidor_id', '=', $servidor_id));
                $criteria1->add(new TFilter('ano', '=', $inicio));

                // carrega os objetos de acordo com o criterio
                $rows1 = $repository1->load($criteria1);
                if ($rows1) {
                    foreach ($rows1 as $row1) {
                        $relat->SetX("80");
                        $relat->Cell(0, 5, $row1->dias, 0, 0, 'J');
                        $suspencao += $row1->dias;
                        $somames += $row1->dias;
                    }
                } else {
                    $relat->SetX("80");
                    $relat->Cell(0, 5, '-', 0, 0, 'J');
                }

                $liquidomes = ($row02->diasfim - $somames);
                $relat->SetX("120");
                $relat->Cell(0, 5, $somames, 0, 0, 'J');

                $relat->SetX("170");
                $relat->Cell(0, 5, $liquidomes, 0, 1, 'J');

                $somadias = ($faltas + $licenca + $suspencao);
                $liquidosubtotal += $liquidomes;

                $relat->SetFont('arial', 'B', 9);
                $relat->SetX("152");
                $relat->Cell(0, 5, "SubTotal: " . $liquidosubtotal . $t, 0, 1, 'J');

                $relat->Ln();
                //imprimir licença premio
                $criteria1 = new TCriteria;
                // instancia um repositorio para Carros
                $repository1 = new TRepository('vw_servidor_certidaotemposervico_licencas_premioRecord');
                //filtra pelo campo selecionado pelo usuário
                $criteria1->add(new TFilter('servidor_id', '=', $servidor_id));
                // carrega os objetos de acordo com o criterio
                $rows1 = $repository1->load($criteria1);
                $relat->SetX("10");
                $relat->Cell(0, 5, utf8_decode('Averbação de licença prêmio com fundamento no § 2º Art:102, da Lei Complementar 122/94.'), 0, 1, 'J');

                if ($rows1) {
                    $relat->SetX("10");
                    $relat->Cell(0, 5, utf8_decode("Licença(s) Prêmio(s) não Gozadas(s) - LC 122 de 30.06.94"), 0, 1, 'J');
                    foreach ($rows1 as $row1) {
                        $relat->SetX("10");
                        $relat->Cell(0, 5, utf8_decode("Data inicio: " . formatar_data($row1->datainicio) . " Data Fim: " . formatar_data($row1->datafim)), 0, 1, 'J');
                        $licencaPremio += $row1->dias;
                    }
                } else {
                    $relat->SetX("70");
                    $relat->Cell(0, 5, utf8_decode('Não consta nenhuma licença premio averbada'), 0, 1, 'J');
                }

                $total = $liquidosubtotal + $licencaPremio;
                //imprimir tempo de serviço averbação
                $criteria1 = new TCriteria;
                // instancia um repositorio para Carros
                $repository1 = new TRepository('vw_servidor_certidaotemposervico_averbacaoRecord');
                //filtra pelo campo selecionado pelo usuário
                $criteria1->add(new TFilter('servidor_id', '=', $servidor_id));
                // carrega os objetos de acordo com o criterio
                $rows1 = $repository1->load($criteria1);
                $relat->Ln();
                $relat->SetX("10");
                $relat->Cell(0, 5, utf8_decode("DISCRIMINAÇÃO DO TEMPO DE SERVIÇO AVERBADO"), 0, 1, 'J');
                $relat->SetX("10");
                $relat->Cell(0, 5, utf8_decode("Orgão/Entidade"), 0, 0, 'J');
                $relat->SetX("160");
                $relat->Cell(0, 5, utf8_decode("Anos / Dias"), 0, 1, 'J');

                if ($rows1) {
                    foreach ($rows1 as $row1) {
                        $relat->SetX("10");
                        $relat->Cell(0, 5, $row1->nomeorgao, 0, 0, 'J');
                        $relat->SetX("160");
                        $relat->Cell(0, 5, $row1->ano . " / " . $row1->dias, 0, 1, 'J');

                        $averbacao += $row1->dias;
                    }
                } else {
                    $relat->SetX("74");
                    $relat->Cell(0, 5, utf8_decode('Não consta nenhuma averbação de tempo de serviço'), 0, 1, 'J');
                }
            }

            $relat->Ln();
            $relat->SetX("10");
            //converter dias em os anos meses e dias
            $relat->Cell(0, 5, utf8_decode("Tempo Trabalho na EMATER/RN conta com: " . time1text($liquidosubtotal * 24 * 3600)), 0, 1, 'J');

            $relat->SetX("10");
            //converter dias em os anos meses e dias
            $relat->Cell(0, 5, utf8_decode("Averbação conta com: " . time1text($averbacao * 24 * 3600)), 0, 1, 'J');

            $total = $liquidosubtotal + $licencaPremio + $averbacao;
            $relat->SetFont('arial', 'B', 9);
            $relat->SetX("128");
            $relat->Cell(0, 5, "TEMPO TOTAL APURADO: " . $total . $t, 0, 1, 'J');
            $relat->Ln();

            $relat->SetX("10");
            $relat->Cell(0, 5, utf8_decode("CERTIFICO, face ao apurado, que o interessado conta nesta data, com o tempo total líquido de serviço de:"), 0, 1, 'J');
            $relat->SetX("10");
            $relat->Cell(0, 5, utf8_decode($total . " " . diasPorExtenso($total) . " ou seja, " . time1text($total * 24 * 3600)), 0, 1, 'J');
            $relat->getLinha();
            $relat->SetX("10");
            $relat->Cell(0, 5, utf8_decode("Observações: Conclusão da Certidão no Verso da Folha, devidamente assinada pelos responsáveis. "), 0, 1, 'J');
        }

        TTransaction::close();


        // Detalhe ------------------------------------------------------------------------------------------------------------

        $relat->save($nome_arq);

        if (!file_exists($nome_arq) OR is_writable($nome_arq)) {
            parent::openFile($nome_arq);
        } else {
            throw new Exception(_t('Permission denied') . ': ' . $nome_arq);
        }


    }

}

?>