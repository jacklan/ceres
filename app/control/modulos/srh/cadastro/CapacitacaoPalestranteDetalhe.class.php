<?php

//ini_set('display_errors', 1);
//ini_set('display_startup_erros', 1);
//error_reporting(E_ALL);

/*
 * classe CapacitacaoPalestranteDetalhe
 * Cadastro de CapacitacaoPalestrante: Contem a listagem e o formulario de busca
 */

use Adianti\Database\TFilter1;
use Adianti\Widget\Datagrid\TDatagridTables;

class CapacitacaoPalestranteDetalhe extends TPage {

    private $form;     // formulario de cadastro
    private $datagrid; // listagem

    /*
     * metodo construtor
     * Cria a pagina, o formulario e a listagem
     */

    public function __construct() {
        parent::__construct();

        // instancia um formulario
        $this->form = new TQuickForm;
        $this->form->class = 'form_CapacitacaoPalestranteDetalhe';
        $this->form->setFormTitle('<font color="red" size="3" face="Arial"><b>Detalhe de Palestrante</b></font>');

        // cria os campos do formulario
        $codigo = new THidden('id');
        $codigo->setEditable(False);
        $palestrante_id = new TCombo('palestrante_id');
        $usuarioalteracao = new THidden('usuarioalteracao');
        $usuarioalteracao->setEditable(false);
        $dataalteracao = new THidden('dataalteracao');
        $dataalteracao->setEditable(false);

        //coloca o valor do relacionamento
        $id = new THidden('capacitacao_id');
        $id->setValue(filter_input(INPUT_GET, 'fk'));


        //cria a colecao da tabela estrangeira habilidade_id
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');
		
        // instancia objeto da Classe Record
        $repository = new TRepository('PalestranteRecord');
		
        // carrega todos os objetos
		$criteria = new TCriteria;
		$criteria->setProperty('order', 'nome');
		
        $collection = $repository->load( $criteria );
		
        //adiciona os objetos no combo
        foreach ($collection as $object) {
            $items[$object->id] = $object->nome;
        }
        // adiciona as opcoes na combo
        $palestrante_id->addItems($items);
        // finaliza a transacao
        TTransaction::close();
	
        //cria a colecao da tabela estrangeira do Cabecalho
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');
        // instancia objeto da Classe Record
        $cadastro = new CapacitacaoRecord($_GET['fk']);
        // cria um criterio de selecao, ordenado pelo id do servidor
        //adiciona os objetos na tela com as informacoes do servidor
        if ($cadastro) {
            $nome = new TLabel($cadastro->nome);
        }
        // finaliza a transacao
        TTransaction::close();
		

        // define os campos
		$this->form->addQuickField('Capacitação', $nome, 400);
        $this->form->addQuickField('Palestrante <font color=red><b>*</b></font>', $palestrante_id, 60);
        $this->form->addQuickField(null, $dataalteracao, 10);
        $this->form->addQuickField(null, $usuarioalteracao, 10);
        $this->form->addQuickField(null, $id, 10);

        $palestrante_id->setProperty('placeholder', 'Endereço');
        $palestrante_id->setProperty('required', 'required');

		// define a acao do botao
        $action1 = new TAction(array($this, 'onSave'));
        $action1->setParameter( 'key', filter_input( INPUT_GET, 'key' ) );
        $action1->setParameter( 'fk', filter_input( INPUT_GET, 'fk' ) );
		
        // cria um botao de acao
        $this->form->addQuickAction('Salvar', $action1, 'ico_save.png');
        $this->form->addQuickAction('Voltar', new TAction(array('CapacitacaoList', 'onReload')), 'ico_datagrid.gif');

        // instancia objeto DataGrid
        $this->datagrid = new TDataGridTables;

        // instancia as colunas da DataGrid
        $dgpalestrante = new TDataGridColumn('nome_palestrante', 'Palestrante', 'left', 1200);

        //adiciona as colunas a DataGrid
		$this->datagrid->addColumn($dgpalestrante);

        $action2 = new TDataGridAction(array($this, 'onDelete'));
        $action2->setLabel('Deletar');
        $action2->setImage('ico_delete.png');
        $action2->setField('id');
        $action2->setFk('capacitacao_id');

        // adiciona as acoes a DataGrid
        
        $this->datagrid->addAction($action2);

        // cria o modelo da DataGrid, montando sua estrutura
        $this->datagrid->createModel();

        // monta a paina atraves de uma tabela
        $panel = new TPanelForm(700, 500);
        $panel->put($this->form, 0, 0);
        $panel->put($this->datagrid, 150, 150);


        // adiciona a tabela a pagina
        parent::add($panel);
    }

    /*
     * metodo onReload()
     * Carrega a DataGrid com os objetos do banco de dados
     */

    function onReload() {
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');

        // instancia um repositorio da Classe
        $repository = new TRepository('CapacitacaoPalestranteRecord');

        // cria um criterio de selecao
        $criteria = new TCriteria;

        //filtra pelo campo selecionado pelo usuário        
        $criteria->add(new TFilter('capacitacao_id', '=', filter_input(INPUT_GET, 'fk')));

        $criteria->setProperty('order', 'palestrante_id');

        // carrega os objetos de acordo com o criterio
        $cadastros = $repository->load($criteria);

        $this->datagrid->clear();
        if ($cadastros) {
            // percorre os objetos retornados
            foreach ($cadastros as $cadastro) {
                // adiciona o objeto na DataGrid
                $this->datagrid->addItem($cadastro);
            }
        }


        // finaliza a transacao
        TTransaction::close();
        $this->loaded = true;
    }

    /*
     * metodo onDelete()
     * Executada quando o usuario clicar no botao excluir da datagrid
     * Pergunta ao usuario se deseja realmente excluir um registro
     */

    function onDelete($param) {
        // obtem o parametro $key
        $key = $param['key'];

        // define duas acoes
        $action1 = new TAction(array($this, 'Delete'));
        

        // define os parametros de cada acao
        $action1->setParameter('key', $key);          

        //encaminha a chave estrangeira
		$action1->setParameter('fk', filter_input ( INPUT_GET, 'fk' ));
        
        // exibe um dialogo ao usuario
        new TQuestion('Deseja realmente excluir o registro ?', $action1);
    }

    /*
     * metodo Delete()
     * Exclui um registro
     */

    function Delete($param) {
        // obtem o parametro $key
        $key = $param['key'];
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');

        // instanicia objeto Record
        $cadastro = new CapacitacaoPalestranteRecord($key);

        try {
            // deleta objeto do banco de dados
            $cadastro->delete();

            // finaliza a transacao
            TTransaction::close();
        } catch (Exception $e) { // em caso de exce��o
            // exibe a mensagem gerada pela exce��o
            new TMessage('error', $e->getMessage());
            // desfaz todas altera��es no banco de dados
            TTransaction::rollback();
        }

        // re-carrega a datagrid
        $this->onReload();
        // exibe mensagem de sucesso
        new TMessage('info', "Registro Excluido com sucesso");
        //  msgAlert('CapacitacaoPalestranteDetalhe', 'onReload', 'key='.$key.'&fk='.$_GET['fk'], 'delete');
    }
	
	/*
     * metodo show()
     * Exibe a pagina
     */
    function show() {
        // carrega os dados no datagrid
        $this->onReload();
        //chama o metodo show da super classe
        parent::show();
    }
	
    /*
     * metodo onSave()
     * Executada quando o usuario clicar no botao salvar do formulario
     */
    function onSave() {


        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');

        // obtem os dados no formulario em um objeto Record da Classe
        $cadastro = $this->form->getData('CapacitacaoPalestranteRecord');

        //antes de armazenar verifica se algum campo eh requerido e nao foi informado
        $dados = $cadastro->toArray();

        $msg = '';
        $icone = 'info';


        if (empty($dados['palestrante_id'])) {
            $msg .= 'O Palestrante deve ser informado.';
        }

        try {

            if ($msg == '') {
                // armazena o objeto
                $cadastro->store();
                $msg = 'Dados armazenados com sucesso';

                // finaliza a transacao
                TTransaction::close();
            } else {
                $icone = 'error';
            }

            if ($icone == 'error') {
                // exibe mensagem de erro
                new TMessage($icone, $msg);
				$this->form->setData($cadastro);   // fill the form with the active record data
            } else {

                $param = array();
                $param ['fk'] = $dados['capacitacao_id'];

                // exibe um dialogo ao usuario
                new TMessage("info", "Registro salvo com sucesso!");
                TApplication::gotoPage('CapacitacaoPalestranteDetalhe', 'onReload', $param); // reload
            }
        } catch (Exception $e) { // em caso de exc
            // exibe a mensagem gerada pela exce��o
            new TMessage('error', $e->getMessage());
            // desfaz todas altera��es no banco de dados
            TTransaction::rollback();
        }
    }

    /*
     * metodo onEdit()
     * Edita os dados de um registro
     */

    function onEdit($param) {
        // obtem o parametro $key
        $key = $param['key'];
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');
        // instancia objeto da Classe Record
        $cadastro = new CapacitacaoPalestranteRecord($key);
        // lanca os dados no formulario
        $this->form->setData($cadastro);

        // finaliza a transacao
        TTransaction::close();
    }

}

?>