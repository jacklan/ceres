<?php

use Adianti\Widget\Datagrid\TDatagridTables;
include_once 'app/lib/funcdate.php';

class FeriasGozoDetalhe extends TPage {

    private $form;
    private $datagrid;


    public function __construct() {
        parent::__construct();

        $this->form = new TQuickForm;
        $this->form->class = 'form_Ferias';
        $this->form->setFormTitle('<font color="red" size="3" face="Arial"><b>Detalhe de Gozo das F&eacute;rias</b></font>');

        // cria um rotulo para o titulo
        $lbtitulo = new TLabel('<div style="width: 200px;"> <b>* Campo obrigatorio</b></div>');
        $lbtitulo->setFontFace('Arial');
        $lbtitulo->setFontColor('red');
        $lbtitulo->setFontSize(10);

        $codigo = new THidden('id');
        $codigo->setEditable(false);
        $datainicio = new TDate('iniciogozodetalhe');
        $datafim = new TDate('fimgozodetalhe');
        $motivoferias = new TEntry('motivoferias');
        $diasgozo = new TEntry('diasgozo');

        $diasgozo->setMask('00');


        $datainicio->setProperty('placeholder', '01/01/2015');
        $datafim->setProperty('placeholder', '10/01/2015');
        $diasgozo->setProperty('placeholder', '10');
        $motivoferias->setProperty('placeholder', 'Servidor precisou por motivo de doença');


        TTransaction::open('pg_ceres');

        $ferias = new FeriasRecord(filter_input(INPUT_GET, 'fk'));

        if ($ferias) {
            $inicioferias = new TLabel(TDate::date2br($ferias->iniciogozo));
            $fimferias = new TLabel(TDate::date2br($ferias->fimgozo));
            $exercicio = new TLabel($ferias->exercicio);
        }

        $servidor = new ServidorRecord($ferias->servidor_id);

        if ($servidor) {
            $nome = new TLabel($servidor->nome);
            $matricula = new TLabel($servidor->matricula);
        }

        TTransaction::close();


        $this->form->addQuickField(null, $codigo, 10);

        $this->form->addQuickField('Nome Servidor', $nome, 300);
        $this->form->addQuickField('Matr&iacute;cula', $matricula, 40);
        $this->form->addQuickField('Inicio Referente das Férias', $inicioferias, 300);
        $this->form->addQuickField('Fim Referente das Férias', $fimferias, 300);
        $this->form->addQuickField('Exercício Referente', $exercicio, 300);
        $this->form->addQuickField('Inicio Gozo <font color=red><b>*</b></font>', $datainicio, 20);
        $this->form->addQuickField('Fim Gozo <font color=red><b>*</b></font>', $datafim, 20);
        $this->form->addQuickField('Quantidade de Dias de Gozo <font color=red><b>*</b></font>', $diasgozo, 20);
        $this->form->addQuickField('Motivo das Férias', $motivoferias, 50);
        $this->form->addQuickField(null, $lbtitulo, 50);

        $action1 = new TAction(array($this, 'onSave'));
        $action1->setParameters(['fk' => filter_input ( INPUT_GET, 'fk' ), 'key' => filter_input ( INPUT_GET, 'key' )]);

        $this->form->addQuickAction('Salvar', $action1, 'ico_save.png')->class = 'btn btn-info btnleft';

        $action2 = new TAction(array("FeriasServidorDetalhe", 'onReload'));
        $action2->setParameters(['fk' => $ferias->servidor_id, 'key' => filter_input ( INPUT_GET, 'fk' )]);

        $this->form->addQuickAction('Voltar', $action2, 'ico_datagrid.gif');

        $this->datagrid = new TDatagridTables;

        $dgdatainicio = new TDataGridColumn('iniciogozodetalhe', 'Data In&iacute;cio', 'left', 600);
        $dgdatafim = new TDataGridColumn('fimgozodetalhe', 'Data Fim', 'left', 600);
        $diasgozo = new TDataGridColumn('diasgozo', 'Dias Gozo', 'left', 600);
        $motivoferias = new TDataGridColumn('motivoferias', 'Motivo Férias', 'left', 600);

        $this->datagrid->addColumn($dgdatainicio);
        $this->datagrid->addColumn($dgdatafim);
        $this->datagrid->addColumn($diasgozo);
        $this->datagrid->addColumn($motivoferias);

        $action1 = new TDataGridAction(array($this, 'onEdit'));
        $action1->setLabel('Editar');
        $action1->setImage('ico_edit.png');
        $action1->setField('id');
        $action1->setFk('ferias_id');

        $action2 = new TDataGridAction(array($this, 'onDelete'));
        $action2->setLabel('Deletar');
        $action2->setImage('ico_delete.png');
        $action2->setField('id');
        $action2->setFk('ferias_id');


        $this->datagrid->addAction($action1);
        $this->datagrid->addAction($action2);

        // cria o modelo da DataGrid, montando sua estrutura
        $this->datagrid->createModel();

        // monta a paina atraves de uma tabela
        $panel = new TPanelForm(700, 500);
        $panel->put($this->form, 0, 0);
        $panel->put($this->datagrid, 150, 240);

        parent::add($panel);
    }

    function onReload()
    {

        TTransaction::open('pg_ceres');

        $repository = new TRepository('FeriasGozoDetalheRecord');

        $criteria = new TCriteria;
        $criteria->setProperty('order', 'iniciogozodetalhe desc');

        $criteria->add(new TFilter('ferias_id', '=', filter_input(INPUT_GET, 'fk')));

        $cadastros = $repository->load($criteria);

        $this->datagrid->clear();

        if ($cadastros) {
            foreach ($cadastros as $cadastro) {

                $cadastro->iniciogozodetalhe = TDate::date2br($cadastro->iniciogozodetalhe);
                $cadastro->fimgozodetalhe = TDate::date2br($cadastro->fimgozodetalhe);

                $this->datagrid->addItem($cadastro);
            }
        }
        TTransaction::close();
        $this->loaded = true;
    }

    function onDelete($param) {

        $key = $param['key'];

        $action1 = new TAction(array($this, 'Delete'));
        $action2 = new TAction(array($this, 'onReload'));

        $action1->setParameter('key', $key);
        $action2->setParameter('key', $key);

        $action1->setParameter('fk', $_GET['fk']);
        $action2->setParameter('fk', $_GET['fk']);

        new TQuestion('Deseja realmente excluir o registro ?', $action1, $action2);
    }

    function Delete($param) {

        $key = $param['key'];

        TTransaction::open('pg_ceres');

        $cadastro = new FeriasGozoDetalheRecord($key);

        try {

            $cadastro->delete();


            TTransaction::close();
        } catch (Exception $e) {

            new TMessage('error', $e->getMessage());

            TTransaction::rollback();
        }

        $this->onReload();

        new TMessage('info', "Registro Exclu&iacute;do com sucesso");
    }

    /*
     * metodo show()
     * Exibe a pagina
     */

    function show() {

        $this->onReload();

        parent::show();
    }

    function onSave() {

        TTransaction::open('pg_ceres');

        $cadastro = $this->form->getData('FeriasGozoDetalheRecord');

        $cadastro->ferias_id = filter_input(INPUT_GET, 'fk');

        $dados = $cadastro->toArray();

        $msg = '';
        $icone = 'info';

        if (empty($dados['iniciogozodetalhe'])) {

            $msg .= 'A data de in&iacute;cio deve ser informada.<br/>';
        }

        if (empty($dados['fimgozodetalhe'])) {

            $msg .= 'A data de fim deve ser informada.<br/>';
        }

        if (empty($dados['diasgozo'])) {

            $msg .= 'A quantidade de dias deve ser informada.<br/>';
        }


        try {

            if ($msg == '') {

                $cadastro->store();

                $msg = 'Dados armazenados com sucesso';

                TTransaction::close();
            } else {
                $icone = 'error';
            }

            if ($icone == 'error') {

                new TMessage($icone, $msg);

            } else {

                $param = array();
                $param['key'] = filter_input ( INPUT_GET, 'key' );
                $param['fk'] = filter_input ( INPUT_GET, 'fk' );

                new TMessage("info", "Registro salvo com sucesso!");
                TApplication::gotoPage('FeriasGozoDetalhe', 'onReload', $param);
            }
        } catch (Exception $e) {
            new TMessage('error', $e->getMessage());

            TTransaction::rollback();
        }
    }

    function onEdit($param) {
        try {

            if (isset($param['key'])) {

                $key = $param['key'];

                TTransaction::open('pg_ceres');

                $object = new FeriasGozoDetalheRecord($key);

                $object->iniciogozodetalhe = TDate::date2br($object->iniciogozodetalhe);
                $object->fimgozodetalhe = TDate::date2br($object->fimgozodetalhe);

                $this->form->setData($object);

                TTransaction::close();
            } else {

                $this->form->clear();
            }
        } catch (Exception $e) {


            new TMessage('error', '<b>Error</b> ' . $e->getMessage());

            TTransaction::rollback();
        }
    }

}

?>