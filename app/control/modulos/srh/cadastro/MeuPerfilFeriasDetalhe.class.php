<?php

use Adianti\Widget\Datagrid\TDatagridTables;
include_once 'app/lib/funcdate.php';

class MeuPerfilFeriasDetalhe extends TPage
{
    private $form;
    private $datagrid;


    public function __construct() {
        parent::__construct();

        $this->form = new TQuickForm;
        $this->form->class = 'form_Ferias';
        $this->form->setFormTitle('<font color="red" size="3" face="Arial"><b>Detalhe de Gozo das F&eacute;rias</b></font>');

        // cria um rotulo para o titulo
        $lbtitulo = new TLabel('<div style="width: 200px;"> <b>* Campo obrigatorio</b></div>');
        $lbtitulo->setFontFace('Arial');
        $lbtitulo->setFontColor('red');
        $lbtitulo->setFontSize(10);

        $codigo = new THidden('id');
        $codigo->setEditable(false);
        $datainicio = new TDate('iniciogozodetalhe');
        $datafim = new TDate('fimgozodetalhe');
        $motivoferias = new TEntry('motivoferias');
        $diasgozo = new TEntry('diasgozo');

        $diasgozo->setMask('00');


        $datainicio->setProperty('placeholder', '01/01/2015');
        $datafim->setProperty('placeholder', '10/01/2015');
        $diasgozo->setProperty('placeholder', '10');
        $motivoferias->setProperty('placeholder', 'Servidor precisou por motivo de doença');


        TTransaction::open('pg_ceres');

        $cadastro = new ServidorRecord($_SESSION['servidor_id']);

        if ($cadastro) {
            $nome = new TLabel($cadastro->nome);
            $matricula = new TLabel($cadastro->matricula);
        }

        TTransaction::close();

        TTransaction::open('pg_ceres');

        $cadastro = new FeriasRecord(filter_input(INPUT_GET, 'key'));

        if ($cadastro) {
            $inicioferias = new TLabel(TDate::date2br($cadastro->iniciogozo));
            $fimferias = new TLabel(TDate::date2br($cadastro->fimgozo));
        }

        TTransaction::close();


        $this->form->addQuickField(null, $codigo, 10);

        $this->form->addQuickField('Nome Servidor', $nome, 300);
        $this->form->addQuickField('Matr&iacute;cula', $matricula, 40);
        $this->form->addQuickField('Inicio Referente das Férias', $inicioferias, 300);
        $this->form->addQuickField('Fim Referente das Férias', $fimferias, 300);

        $cancel_button = new TButton('cancel');


//        $action2 = new TAction(array($this, 'onCancel'));
//        $action2->setParameter('fk', $_GET['fk']);
//        $action2->setParameter('key', $_GET['key']);
//        $cancel_button->setAction(new TAction(array($this, 'onCancel')), 'Voltar');


        $this->datagrid = new TDatagridTables;

        $dgdatainicio = new TDataGridColumn('iniciogozodetalhe', 'Data In&iacute;cio', 'left', 600);
        $dgdatafim = new TDataGridColumn('fimgozodetalhe', 'Data Fim', 'left', 600);
        $diasgozo = new TDataGridColumn('diasgozo', 'Dias Gozo', 'left', 600);
        $motivoferias = new TDataGridColumn('motivoferias', 'Motivo Férias', 'left', 600);

        $action_voltar = new TAction(array('MeuPerfilFerias', 'onReload'));
        $action_voltar->setParameters(['fk' => filter_input ( INPUT_GET, 'fk' ), 'key' => filter_input ( INPUT_GET, 'fk' )]);
        $this->form->addQuickAction('Voltar', $action_voltar, 'ico_datagrid.gif');

        $this->datagrid->addColumn($dgdatainicio);
        $this->datagrid->addColumn($dgdatafim);
        $this->datagrid->addColumn($diasgozo);
        $this->datagrid->addColumn($motivoferias);


        $this->datagrid->createModel();

        $panel = new TPanelForm(700, 500);
        $panel->put($this->form, 0, 0);
        $panel->put($this->datagrid, 150, 240);

        parent::add($panel);
    }

    function onCancel($param){
        // define a acao de cancelamento

        $params = array();
        $params['fk'] = $param['fk'];
        $params['key'] = $param['key'];

        TApplication::gotoPage('MeuPerfilFerias','onReload', $params); // reload
    }

    function onReload()
    {

        TTransaction::open('pg_ceres');

        $repository = new TRepository('FeriasGozoDetalheRecord');

        $criteria = new TCriteria;
        $criteria->setProperty('order', 'iniciogozodetalhe desc');

        $criteria->add(new TFilter('ferias_id', '=', filter_input(INPUT_GET, 'key')));

        $cadastros = $repository->load($criteria);

        $this->datagrid->clear();

        if ($cadastros) {
            foreach ($cadastros as $cadastro) {

                $cadastro->iniciogozodetalhe = TDate::date2br($cadastro->iniciogozodetalhe);
                $cadastro->fimgozodetalhe = TDate::date2br($cadastro->fimgozodetalhe);

                $this->datagrid->addItem($cadastro);
            }
        }
        TTransaction::close();
        $this->loaded = true;
    }

    function onDelete($param) {

        $key = $param['key'];

        $action1 = new TAction(array($this, 'Delete'));
        $action2 = new TAction(array($this, 'onReload'));

        $action1->setParameter('key', $key);
        $action2->setParameter('key', $key);

        $action1->setParameter('fk', $_GET['fk']);
        $action2->setParameter('fk', $_GET['fk']);

        new TQuestion('Deseja realmente excluir o registro ?', $action1, $action2);
    }

    function Delete($param) {

        $key = $param['key'];

        TTransaction::open('pg_ceres');

        $cadastro = new FeriasGozoDetalheRecord($key);

        try {

            $cadastro->delete();


            TTransaction::close();
        } catch (Exception $e) {

            new TMessage('error', $e->getMessage());

            TTransaction::rollback();
        }

        $this->onReload();

        new TMessage('info', "Registro Exclu&iacute;do com sucesso");
    }

    /*
     * metodo show()
     * Exibe a pagina
     */

    function show() {

        $this->onReload();

        parent::show();
    }

    function onSave() {

        TTransaction::open('pg_ceres');

        $cadastro = $this->form->getData('FeriasGozoDetalheRecord');

        $cadastro->ferias_id = filter_input(INPUT_GET, 'fk');

        $dados = $cadastro->toArray();

        $msg = '';
        $icone = 'info';

        if (empty($dados['iniciogozodetalhe'])) {

            $msg .= 'A data de in&iacute;cio deve ser informada.<br/>';
        }

        if (empty($dados['fimgozodetalhe'])) {

            $msg .= 'A data de fim deve ser informada.<br/>';
        }

        if (empty($dados['diasgozo'])) {

            $msg .= 'A quantidade de dias deve ser informada.<br/>';
        }


        try {

            if ($msg == '') {

                $cadastro->store();

                $msg = 'Dados armazenados com sucesso';

                TTransaction::close();
            } else {
                $icone = 'error';
            }

            if ($icone == 'error') {

                new TMessage($icone, $msg);

            } else {

                $param = array();
                $param['fk'] = $dados['ferias_id'];

                new TMessage("info", "Registro salvo com sucesso!");
                TApplication::gotoPage('FeriasGozoDetalhe', 'onReload', $param);
            }
        } catch (Exception $e) {
            new TMessage('error', $e->getMessage());

            TTransaction::rollback();
        }
    }

    function onEdit($param) {
        try {

            if (isset($param['key'])) {

                $key = $param['key'];

                TTransaction::open('pg_ceres');

                $object = new FeriasGozoDetalheRecord($key);

                $object->iniciogozodetalhe = TDate::date2br($object->iniciogozodetalhe);
                $object->fimgozodetalhe = TDate::date2br($object->fimgozodetalhe);

                $this->form->setData($object);

                TTransaction::close();
            } else {

                $this->form->clear();
            }
        } catch (Exception $e) {


            new TMessage('error', '<b>Error</b> ' . $e->getMessage());

            TTransaction::rollback();
        }
    }
}