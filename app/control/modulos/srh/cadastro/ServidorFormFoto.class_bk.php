<?php

/*
 * classe ServidorFormFoto
 * Cadastro de Servidor: Contem o formularo da Foto
 */
ini_set('display_errors', 1);
ini_set('display_startup_erros', 1);
error_reporting(E_ALL);

//use Image\WideImage;
//use Image\WideImage\Exception\WideImage_Exception;

class ServidorFormFoto extends TPage {

    private $form;     // formulario de cadastro

    /*
     * metodo construtor
     * Cria a pagina do formulario
     */

    public function __construct() {
        parent::__construct();

        // instancia um formulario
        // creates the form
        $this->form = new TForm('form_Servidor');
        //$this->form = new TQuickForm;
        //   $this->form->setEnctype("multipart/form-data");
        $this->form->class = 'form_Servidor_foto';
        //$this->form->setEnctype("multipart/form-data");

        $table = new TTable;
        $table->width = '100%';
        $table_buttons = new TTable;

        // add the table inside the form
        $this->form->add($table);

        // creates a label with the title
        $titulo = new TLabel('Formulario Servidor');
        $titulo->setFontSize(18);
        $titulo->setFontFace('Arial');
        $titulo->setFontColor('red');

        // cria os campos do formulario
        $id = new THidden('servidor_id');
        $id->setValue(filter_input(INPUT_GET, 'fk'));
        $foto = new TFile('foto');
        $foto->setProperty("accept", "image/jpg");

        $usuarioalteracao = new THidden('usuarioalteracao');
        $dataalteracao = new THidden('dataalteracao');

        //cria a colecao da tabela estrangeira do Cabecalho
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');
        // instancia objeto da Classe Record
        $cadastro = new ServidorRecord(filter_input(INPUT_GET, 'fk'));
        // cria um criterio de selecao, ordenado pelo id do servidor
        //adiciona os objetos na tela com as informacoes do servidor
        if ($cadastro) {
            $nome = new TLabel($cadastro->nome);
            $matricula = new TLabel($cadastro->matricula);
        }
        // finaliza a transacao
        TTransaction::close();

        // define os tamanhos dos campos
        $foto->setSize(400);

        // add the form fields
        ################ Pagina 01 - Dados Pessoais ################
        $table->addRowSet('', $id);
        $table->addRowSet('', $usuarioalteracao);
        $table->addRowSet('', $dataalteracao);

        // adiciona campos
        $table->addRowSet('', $titulo);
        $table->addRowSet(new TLabel('Matricula:'), $matricula);
        $table->addRowSet("<b>Nome <font color=red>*</font></b>", $nome);
        $table->addRowSet(new TLabel('Foto'), $foto);

        //mostra a foto do servidor se o metodo eh onEdit
        if (filter_input(INPUT_GET, 'method') == 'onEdit') {
            $foto2 = new TImage('app/images/servidor/servidor_' . filter_input(INPUT_GET, 'fk') . '.jpg', 'foto', 150, 150);
            $foto2->style="width: 150px; height: 200px;";
            $table->addRowSet('', $foto2);
        }

        //Define o auto-sugerir
        $nome->setProperty('placeholder', 'Informe o Nome');

        // create an action button (save)
        $save_button = new TButton('save');

        // create an action servidor (save)
        $action = new TAction(array($this, 'onSave'));
        $action->setParameter('fk', '' . filter_input(INPUT_GET, 'fk') . '');
        $save_button->setAction($action, 'Save');

        $save_button->setImage('ico_save.png');

        // create an action button (new)
        $new_button = new TButton('list');
        $new_button->setAction(new TAction(array("ServidorList", 'onReload')), 'Voltar');
        $new_button->setImage('ico_datagrid.png');

        // add a row for the form action
        $row = $table_buttons->addRow();
        $row->addCell($save_button);
        $row->addCell($new_button);

        // add a row for the form action
        $row = $table->addRow();
        $row->class = 'tformaction';
        $cell = $row->addCell($table_buttons);
        $cell->colspan = 2;
        // define wich are the form fields
        $this->form->setFields(array($id, $foto, $save_button, $new_button));

        // wrap the page content using vertical box
        $vbox = new TVBox;
        $vbox->add($this->form);

        parent::add($vbox);
        
    }

    /*
     * metodo onSave()
     * Executada quando o usuario clicar no botao salvar do formulario
     */

    function onSave() {

        // pegar dados do form
        $object = $this->form->getData();

        $source_file = 'tmp/' . $object->foto;
        $target_file = 'app/images/servidor/servidor_' . $object->servidor_id . ".jpg";

        $extensao = strtolower(pathinfo(strtolower($object->foto), PATHINFO_EXTENSION));

        /* Exemplo de uso: 
          $Image = new ImageUpload();
          $Image->resize($object->foto, 150, 150);
          $Image->saveImage('xd.png');
          $source_file = $Image;
         */
        /*
        ########### redimencionar imagem ###########
        $img_origem = ImageCreateFromJpeg($object->foto);
        $largura = imagesx($img_origem);
        $altura = imagesy($img_origem);

        $nova_largura = 200;
        $nova_altura = $altura * $nova_largura / $largura;

        $img_destino = imagecreatetruecolor($nova_largura, $nova_altura);

        imagecopyresampled($img_destino, $img_origem, 0, 0, 0, 0, $nova_largura, $nova_altura, $largura, $altura);

        imageJPEG($img_destino, './' . $object->foto, 85);

         * 
         */
      //  $image = WideImage::load($object->foto)->resize(150, 150)->saveToFile('small.jpg');
        ########### redimencionar imagem ###########


//        if ($_FILES['foto']['size'] > 1024000) {
//            $msg = "O Arquvio deve no maximo 1Mb";
//            new TMessage("error", "Erro ao salvar a foto!" . $msg);
//        }
//        exit();
        //  $finfo = new finfo(FILEINFO_MIME_TYPE);
        //if the user uploaded a source file
//        if (file_exists($source_file) && $finfo->file($source_file) == 'image/png') {
        if (file_exists($source_file) && ($extensao == 'jpg' || $extensao == 'png' || $extensao == 'jpeg')) {
            try { 
                // move to the target directory
                // update the foto
                rename($source_file, $target_file);
             //   copy($image, $target_file);
                //$object->foto = 'images/' . $object->foto;
                $param = array();
                $param['fk'] = $object->servidor_id;
                TTransaction::open('pg_ceres');
                $obj = new ServidorRecord( $object->servidor_id );
                $obj->temfoto = '1';
                $obj->store();
                TTransaction::close(); 

                new TMessage("info", "Registro salvo com sucesso!");
                TApplication::gotoPage('ServidorFormFoto', 'onEdit', $param); // reload
            } catch (Exception $e) { // in case of exception
                new TMessage('error', '<b>Error</b> ' . $e->getMessage());
                TTransaction::rollback();
            }
        } else {
            new TMessage("error", "Erro ao salvar a foto! Apenas aquivo do tipo JPG");
        }

        /*
          // var_dump($this->form);
          //   $arqtmp = $_FILES["foto"]["tmp_name"];
          $servidor_id = $_REQUEST['servidor_id'];
          $foto = $_REQUEST['foto'];
          $msg = "";
          ############ Exemplo 01 ############
          // have attachments
          if ($foto) {
          $nome_foto = 'servidor_' . $servidor_id.".jpg";
          $target_folder = 'app/images/servidor/';
          $target_file = $target_folder . '' . $nome_foto;
          @mkdir($target_folder);
          copy($target_file,$target_folder);
          //rename('app/images/servidor/' . $nome_foto, $target_file);
          $msg = 'OK';
          }
         * 
         */

        // var_dump($_FILES['foto']['tmp_name']);
        //  var_dump($_FILES);
        ############ Exemplo 02 ############
        /* Defina aqui o tamanho m�ximo do arquivo em bytes: */
        /*
          if ($_FILES['foto']['size'] > 1024000) {
          $msg = "O Arquvio deve no maximo 1Mb";
          } else {
          /* Defina aqui o diret�rio destino do upload */
        /*
          if (!empty($_FILES['foto']['tmp_name']) and is_file($_FILES['foto']['tmp_name'])) {
          $caminho = 'app/images/servidor/servidor_' . $servidor_id . '.jpg';

          /* Defina aqui o tipo de arquivo suportado */
        /*
          if (eregi(".jpg$", $_FILES['foto']['name'])) {
          copy($_FILES['foto']['tmp_name'], $caminho);
          $msg = 'OK';
          } else {
          $msg = 'Apenas aquivo do tipo JPG';
          }
          } else {
          $msg = 'Caminho ou nome de arquivo Invalido!';
          }
          }
         * 
          ###############
          $target_dir = "app/images/servidor/";
          $target_file = $target_dir . basename($_FILES["foto"]["name"]);
          $uploadOk = 1;
          $imageFileType = pathinfo($target_file, PATHINFO_EXTENSION);
          // Check if image file is a actual image or fake image
          if (isset($servidor_id)) {
          $check = getimagesize($_FILES["foto"]["tmp_name"]);
          if ($check !== false) {
          echo "File is an image - " . $check["mime"] . ".";
          $uploadOk = 1;
          } else {
          echo "File is not an image.";
          $uploadOk = 0;
          }
          }
          // Check if file already exists
          if (file_exists($target_file)) {
          echo "Sorry, file already exists.";
          $uploadOk = 0;
          }
          // Check file size
          if ($_FILES["foto"]["size"] > 500000) {
          echo "Sorry, your file is too large.";
          $uploadOk = 0;
          }
          // Allow certain file formats
          if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif") {
          echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
          $uploadOk = 0;
          }
          // Check if $uploadOk is set to 0 by an error
          if ($uploadOk == 0) {
          echo "Sorry, your file was not uploaded.";
          // if everything is ok, try to upload file
          } else {
          if (move_uploaded_file($_FILES["foto"]["tmp_name"], $target_file)) {
          echo "The file " . basename($_FILES["foto"]["name"]) . " has been uploaded.";
          } else {
          echo "Sorry, there was an error uploading your file.";
          }
          }
         */
        ############
        //  $caminho = $_FILES["foto"]["tmp_name"];
        //$tipo = $_FILES["foto"]["type"];
        //$destino = 'app.img/servidor/servidor_'.$id.'.jpg';
        //move_uploaded_file($caminho, $destino);
        /*
          array(5) {
          ["name"]=&gt;
          string(16) "hidel_emater.jpg"
          ["type"]=&gt;
          string(10) "image/jpeg"
          ["tmp_name"]=&gt;
          string(25) "C:\php\upload\phpA2BA.tmp"
          ["error"]=&gt;
          int(0)
          ["size"]=&gt;
          int(141151)
          }
         *
         */

        /*
          $caminho = $_FILES["foto"]["tmp_name"];
          //var_dump($caminho);
          $conn = pg_connect("host='localhost' dbname='porteiranet' user='porteira' password='ematerrn' sslmode='disabled' ");
          //$conteudo=addslashes(fread(fopen($caminho, "r"), filesize($caminho)));
          $conteudo=fread(fopen($caminho, "r"), filesize($caminho));
          //$sql = "UPDATE servidor set foto = pg_escape_bytea('" . addslashes($conteudo) ."' where id = ".$id." ;";
          $sql = "UPDATE servidor set foto = pg_escape_bytea('" .$conteudo."') where id = ".$id." ;";
          var_dump($sql);
          $exec=pg_query ( $conn, $sql);
          pg_close($conn);
         */
        /*

          // --------- OPEN CONN ---
          $conn = pg_connect("host='localhost' dbname='porteiranet' user='porteira' password='ematerrn'");


          // --------- OPEN FILE ---
          $fp = fopen($arqtmp, "r");
          $buffer = fread($fp, filesize($arqtmp));
          fclose($fp);

          // --------- CREATE - UPDATE OID ---

          pg_exec($conn, "begin");

          $oid = pg_locreate($conn);
          $caminho = $_FILES["foto"]["tmp_name"];

          $conteudo=addslashes(fread(fopen($caminho, "r"), filesize($caminho)));
          $rs = pg_exec($conn,"UPDATE servidor foto = lo_import('" . addslashes($arq) ." where id = ".$id." ;");

          $msg = "UPDATE servidor foto = $handle where id = $id ;";
          $handle = pg_loopen ($conn, $oid, "w");
          pg_lowrite ($handle, $caminho);
          pg_loclose ($handle);
          $rs = pg_exec($conn,"UPDATE servidor foto = lo_import('" . addslashes($arq) ." where id = ".$id." ;");

          pg_exec($conn, "commit");

          pg_close();
         */
        /*
          // exibe um dialogo ao usuario
          //$msg = 'Dados armazenados com sucesso';
          if ($msg == 'OK') {
          new TMessage("info", "Registro salvo com sucesso!");
          TApplication::gotoPage('ServidorCapacitacaoDetalhe', 'onReload'); // reload
          } else {
          new TMessage("error", "Erro ao salvar a foto!");
          }
         * 
         */
    }

    /*
     * metodo onEdit()
     * Edita os dados de um registro
     */

    function onEdit($param) {
        try {
            if (isset($param['fk'])) {

                // get the parameter $key
                $key = $param['fk'];

                TTransaction::open('pg_ceres');   // open a transaction with database 'samples'

                $object = new ServidorRecord($key);        // instantiates object Servidor
                //$fotoimg  = new TImage('app.img/servidor/servidor_'.$key.'.jpg','foto',50,50);
                //$this->form->add($fotoimg);

                $this->form->setData($object);   // fill the form with the active record data

                TTransaction::close();           // close the transaction
            } else {
                $this->form->clear();
            }
        } catch (Exception $e) { // in case of exception
            // shows the exception error message
            new TMessage('error', '<b>Error</b> ' . $e->getMessage());
            // undo all pending operations
            TTransaction::rollback();
        }
    }

}

?>