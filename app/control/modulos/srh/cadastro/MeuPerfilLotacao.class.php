<?php

/*
 * classe ServidorLotacaoDetalhe
 * Cadastro de ServidorLotacao: Contem a listagem e o formulario de busca
 */

//ini_set('display_errors', 1);
//ini_set('display_startup_erros', 1);
//error_reporting(E_ALL);

use Adianti\Database\TFilter1;
use Adianti\Widget\Datagrid\TDatagridTables;

class MeuPerfilLotacao extends TPage {

    private $form;     // formulario de cadastro
    private $datagrid; // listagem

    /*
     * metodo construtor
     * Cria a pagina, o formulario e a listagem
     */

    public function __construct() {
        parent::__construct();

        // instancia um formulario
        $this->form = new TForm('form_ServidorLotacao');

        // instancia um Panel
        $panel = new TPanelForm(900, 100);

        // adiciona o panel ao formulario
        $this->form->add($panel);

        // cria um rotulo para o titulo
        $titulo = new TLabel('Meu Perfil - Lota&ccedil;&atilde;o');
        $titulo->setFontFace('Arial');
        $titulo->setFontColor('red');
        $titulo->setFontSize(18);

        // cria os campos do formulario
        $codigo = new TEntry('id');
        $codigo->setEditable(false);
        $datainicio = new TDate('datainicio');
        $datafim = new TDate('datafim');
        $documento = new TEntry('documento');
        $usuarioAlteracao = new TEntry('usuarioalteracao');
        $usuarioAlteracao->setEditable(false);
        $dataAlteracao = new TDate('dataalteracao');
        $dataAlteracao->setEditable(false);
        $unidadeoperativa_id = new TCombo('unidadeoperativa_id');
        $setor_id = new TCombo('setor_id');

        //cria a colecao da tabela estrangeira
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');
        // instancia objeto da Classe Record
        $repository = new TRepository('UnidadeOperativaRecord');
        // cria um criterio de selecao, ordenado pelo id
        $criteria = new TCriteria;
        $criteria->setProperty('order', 'nome');
        // carrega os objetos de acordo com o criterio
        $cadastros = $repository->load($criteria);
        //adiciona os objetos no combo
        foreach ($cadastros as $object) {
            $items1[$object->id] = $object->nome;
        }
        // adiciona as opcoes na combo
        $unidadeoperativa_id->addItems($items1);

        // finaliza a transacao
        TTransaction::close();

        //cria a colecao da tabela estrangeira
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');
        // instancia objeto da Classe Record
        $repository = new TRepository('SetorRecord');
        // cria um criterio de selecao, ordenado pelo id
        $criteria = new TCriteria;
        $criteria->setProperty('order', 'nome');
        // carrega os objetos de acordo com o criterio
        $cadastros = $repository->load($criteria);
        //adiciona os objetos no combo
        foreach ($cadastros as $object) {
            $items[$object->id] = $object->nome;
        }
        // adiciona as opcoes na combo
        $setor_id->addItems($items);

        // finaliza a transacao
        TTransaction::close();

        //cria a colecao da tabela estrangeira do Cabecalho
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');
        // instancia objeto da Classe Record
        $cadastro = new ServidorRecord($_GET['fk']);
        // cria um criterio de selecao, ordenado pelo id do servidor
        //adiciona os objetos na tela com as informacoes do servidor
        if ($cadastro) {
            $nome = new TLabel($cadastro->nome);
            $matricula = new TLabel($cadastro->matricula);
        }
        // finaliza a transacao
        TTransaction::close();


        //coloca o valor do relacionamento
        $id = new THidden('servidor_id');
        $id->setValue($_GET['fk']);

        // define os tamanhos dos campos

        $codigo->setSize(40);
        $unidadeoperativa_id->setSize(200);
        $setor_id->setSize(200);
        $documento->setSize(200);
        $datainicio->setSize(80);
        $datafim->setSize(80);


        // adiciona o campo titulo
        $panel->put($titulo, $panel->getColuna(), $panel->getLinha());


        // adiciona o campo do id da fk
        /* $panel->putCampo($id, null, 0, 0);

          // adiciona o campo Nome do Servidor
          $panel->putCampo($nome, 'Nome Servidor', 0, 1);

          // adiciona o campo Matricula do Servidor
          $panel->putCampo($matricula, 'Matr&iacute;cula', 0, 1);

          // adiciona o campo id da tabela
          $panel->putCampo($codigo, 'C&oacute;digo', 0, 1);

          // adiciona o campo de ServidorLotacao - fk
          $panel->putCampo($unidadeoperativa_id, 'Unidade Local' , 0 , 1);

          // adiciona o campo de ServidorLotacao - fk
          $panel->putCampo($setor_id, 'Setor' , 0 , 1);

          // adiciona o campo data inicio
          $panel->putCampo($datainicio, 'Data Inicio', 0, 1);

          $image = new TImage('app.images/ew_calendar.gif');
          $image->__set('alt', 'Selecione uma data');
          $image->__set('style', 'cursor:pointer;');
          $image->__set('id', 'cxdatainicio');
          $panel->putCampo($image, null, 105, 0);
          $script = new TScriptCalendar('scriptCalendar', 'datainicio', 'cxdatainicio');
          $panel->putCampo($script, null, 105, 0);

          // adiciona o campo data fim
          $panel->putCampo($datafim, 'Data Fim', 0, 1);


          $image = new TImage('app.images/ew_calendar.gif');
          $image->__set('alt', 'Selecione uma data');
          $image->__set('style', 'cursor:pointer;');
          $image->__set('id', 'cxdatafim');
          $panel->putCampo($image, null, 105, 0);
          $script = new TScriptCalendar('scriptCalendar', 'datafim', 'cxdatafim');
          $panel->putCampo($script, null, 105, 0);


          // adiciona o campo documento
          $panel->putCampo($documento, 'Documento', 0, 1);
         * */


        // cria um botao de acao (salvar)
        $cancel_button = new TButton('cancel');

        // prepara o botao para chamar o form anterior
        //$lista = new UsuarioServidorList;
        $action2 = new TAction(array($this, 'onCancel'));
        $action2->setParameter('fk', $_GET['fk']);
        $action2->setParameter('key', $_GET['key']);
        $cancel_button->setAction($action2, 'Voltar');

        // adiciona a acao do formulario
        $panel->putCampo($cancel_button, null, -100, 1);

        // define quais sao os campos do formulario
        $this->form->setFields(array($codigo, $nome, $matricula, $unidadeoperativa_id, $setor_id, $documento, $datainicio, $datafim, $cancel_button));

        // instancia objeto DataGrid
        $this->datagrid = new TDatagridTables;


        // instancia as colunas da DataGrid
        $dgunidade = new TDataGridColumn('nome_unidadeoperativa', 'Local', 'left', 500);
        $dgsetor = new TDataGridColumn('nome_setor', 'Setor', 'left', 500);
        $dgdocumento = new TDataGridColumn('documento', 'Documento', 'left', 200);
        $dgdatainicio = new TDataGridColumn('datainicio', 'Data In&iacute;cio', 'left', 80);
        $dgdatafim = new TDataGridColumn('datafim', 'Data Fim', 'left', 80);

        // adiciona as colunas a DataGrid
        //$this->datagrid->addColumn($dgnome);
        $this->datagrid->addColumn($dgunidade);
        $this->datagrid->addColumn($dgsetor);
        $this->datagrid->addColumn($dgdocumento);
        $this->datagrid->addColumn($dgdatainicio);
        $this->datagrid->addColumn($dgdatafim);

        // cria o modelo da DataGrid, montando sua estrutura
        $this->datagrid->createModel();

        // monta a paina atraves de uma tabela
        $panel = new TPanelForm(700, 500);
        $panel->put($this->form, 0, 0);
        $panel->put($this->datagrid, 150, 75);


        // adiciona a tabela a pagina
        parent::add($panel);
    }

    /*
     * metodo onCancel()
     * Executada quando o usuario clicar no botao cancelar do form
     * Exibe msg ao usuario informano que a operacao foi cancelada
     */

    function onCancel() {
        // define a acao de cancelamento

        $params = array();
        $params['fk'] = filter_input(INPUT_GET, 'fk');
        $params['key'] = filter_input(INPUT_GET, 'key');

        TApplication::gotoPage('MeuPerfilList', 'onReload', $params); // reload
    }

    /*
     * metodo onReload()
     * Carrega a DataGrid com os objetos do banco de dados
     */

    function onReload() {
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');

        // instancia um repositorio da Classe
        $repository = new TRepository('UnidadeServidorRecord');

        // cria um criterio de selecao
        $criteria = new TCriteria;

        //filtra pelo campo selecionado pelo usuario
        $criteria->add(new TFilter('servidor_id', '=', $_SESSION['servidor_id']));

        $criteria->setProperty('order', 'id');

        // carrega os objetos de acordo com o criterio
        $cadastros = $repository->load($criteria);

        $this->datagrid->clear();
        if ($cadastros) {
            // percorre os objetos retornados
            foreach ($cadastros as $cadastro) {

                $cadastro->datainicio = TDate::date2br($cadastro->datainicio);
                $cadastro->datafim = TDate::date2br($cadastro->datafim);
                // adiciona o objeto na DataGrid
                $this->datagrid->addItem($cadastro);
            }
        }


        // finaliza a transacao
        TTransaction::close();
        $this->loaded = true;
    }

    /*
     * metodo onDelete()
     * Executada quando o usuario clicar no botao excluir da datagrid
     * Pergunta ao usuario se deseja realmente excluir um registro
     */

    function onDelete($param) {
        // obtem o parametro $key
        $key = $param['key'];

        // define duas acoes
        $action1 = new TAction(array($this, 'Delete'));
        $action2 = new TAction(array($this, 'NaoDelete'));

        // define os parametros de cada acao
        $action1->setParameter('key', $key);
        $action2->setParameter('key', $key);

        //encaminha a chave estrangeira
        $action1->setParameter('fk', $_GET['fk']);
        $action2->setParameter('fk', $_GET['fk']);
        // exibe um dialogo ao usuario
        new TQuestion('Deseja realmente excluir o registro ?', $action1, $action2);
    }

    /*
     * metodo Delete()
     * Exclui um registro
     */

    function Delete($param) {
        // obtem o parametro $key
        $key = $param['key'];
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');

        // instanicia objeto Record
        $cadastro = new UnidadeServidorRecord($key);

        try {
            // deleta objeto do banco de dados
            $cadastro->delete();

            // finaliza a transacao
            TTransaction::close();
        } catch (Exception $e) { // em caso de exceção
            // exibe a mensagem gerada pela exceção
            new TMessage('error', $e->getMessage());
            // desfaz todas alterações no banco de dados
            TTransaction::rollback();
        }

        // re-carrega a datagrid
        $this->onReload();
        // exibe mensagem de sucesso
        new TMessage('info', "Registro Exclu&iacute;do com sucesso");
    }

    /*
     * metodo show()
     * Exibe a pagina
     */

    function show() {
        // carrega os dados no datagrid
        $this->onReload();
        //chama o metodo show da super classe
        parent::show();
    }

    /*
     * metodo onSave()
     * Executada quando o usuario clicar no botao salvar do formulario
     */

    function onSave() {
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');
        // obtem os dados no formulario em um objeto Record da Classe
        $cadastro = $this->form->getData('UnidadeServidorRecord');

        //antes de armazenar verifica se algum campo eh requerido e nao foi informado
        $dados = $cadastro->toArray();

        $msg = '';
        $icone = 'info';

        if (empty($dados['unidadeoperativa_id'])) {
            $msg .= 'A Unidade Operativa deve ser informada.';
        }

        if (empty($dados['setor_id'])) {
            $msg .= 'O Setor deve ser informado.';
        }

        if (empty($dados['datainicio'])) {
            $msg .= 'A data in&iacute;cio deve ser informada.';
        }



        try {

            if ($msg == '') {
                // armazena o objeto
                $cadastro->store();
                $msg = 'Dados armazenados com sucesso';

                // finaliza a transacao
                TTransaction::close();
            } else {
                $icone = 'error';
            }

            if ($icone == 'error') {
                // exibe mensagem de erro
                new TMessage($icone, $msg);
            } else {
                //chama o formulario com o grid
                // define duas acoes
                $action1 = new TAction(array('ServidorLotacaoDetalhe', 'onReload'));
                $action1->setParameter('fk', $dados['servidor_id']);
                // exibe um dialogo ao usuario
                new TConfirmation($msg, $action1);
                // re-carrega listagem
                //                $this->onReload();
            }
        } catch (Exception $e) { // em caso de exc
            // exibe a mensagem gerada pela exceção
            new TMessage('error', $e->getMessage());
            // desfaz todas alterações no banco de dados
            TTransaction::rollback();
        }
    }

    /*
     * metodo onEdit()
     * Edita os dados de um registro
     */

    function onEdit($param) {
        // obtem o parametro $key
        $key = $param['key'];
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');
        // instancia objeto da Classe Record
        $cadastro = new UnidadeServidorRecord($key);
        $cadastro->datainicio = TDate::date2br($cadastro->datainicio);
        $cadastro->datafim = TDate::date2br($cadastro->datafim);

        // lanca os dados no formulario
        $this->form->setData($cadastro);

        // finaliza a transacao
        TTransaction::close();
    }

}

?>