<?php
/*
 * classe tipodocumentoForm
 * Cadastro de tipodocumento: Contem o formularo
 */

class TipoLicencaForm extends TPage {
    private $form;     // formulario de cadastro

    /*
     * metodo construtor
     * Cria a pagina do formulario
     */
    public function __construct() {
        parent::__construct();
		
		// instancia um formulario
        $this->form = new TQuickForm;
        $this->form->class = 'form_tipolicenca';
        $this->form->setFormTitle('<font color="red" size="3" face="Arial"><b>Formul&aacute;rio de Tipos de Licen&ccedil;a</b></font>');

        // cria os campos do formulario
        $codigo     = new THidden('id');
        $codigo->setEditable(false);
        $nome  = new TEntry('nome');

        $usuarioalteracao = new THidden('usuarioalteracao');
        $dataalteracao = new THidden('dataalteracao');
		
		// cria um rotulo para o titulo
        $titulo = new TLabel('<div style="position:floatval; width: 200px;"> <b>* Campo obrigatorio</b></div>');
        $titulo->setFontFace('Arial');
        $titulo->setFontColor('red');
        $titulo->setFontSize(10);
		// cria um rotulo para o titulo
        $titulo = new TLabel('<div style="position:floatval; width: 200px;"> <b>* Campo obrigatorio</b></div>');
        $titulo->setFontFace('Arial');
        $titulo->setFontColor('red');
        $titulo->setFontSize(10);
		
       // define os campos
        $this->form->addQuickField(null, $codigo, 10);
		$this->form->addQuickField(null, $usuarioalteracao, 10);
        $this->form->addQuickField(null, $dataalteracao, 10);
		$this->form->addQuickField("Nome <font color=red><b>*</b></font>", $nome, 50);
		$this->form->addQuickField(null, $titulo, 50);
		
		//Campos obrigatórios
        $nome->setProperty('required', 'required');
		

       // cria um botao de acao
        $this->form->addQuickAction('Salvar', new TAction(array($this, 'onSave')), 'ico_save.png');
        $this->form->addQuickAction('Voltar', new TAction(array('TipoLicencaList', 'onReload')), 'ico_datagrid.gif');

        // adiciona a tabela a pagina
        parent::add($this->form);
    }


    /*
     * metodo onSave()
     * Executada quando o usuario clicar no botao salvar do formulario
     */
    function onSave() {
    // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');
        // obtem os dados no formulario em um objeto CarroRetipodocumentod
        $cadastro = $this->form->getData('TipoLicencaRecord');

        //antes de armazenar verifica se algum campo eh requerido e nao foi informado
        $dados = $cadastro->toArray();

        $msg = '';
        $icone = 'info';

        if (empty ($dados['nome'])) {
            $msg .= 'O Nome deve ser informado.';
        }

        try {

            if ($msg == '') {
            // armazena o objeto
                $cadastro->store();
                $msg = 'Dados armazenados com sucesso';

                // finaliza a transacao
                TTransaction::close();

            }else {
                $icone = 'error';
            }

            if ($icone == 'error') {
            // exibe mensagem de erro
                new TMessage($icone, $msg);
				$this->form->setData($cadastro);   // fill the form with the active record data
            }else {
            //chama o formulario com o grid
			
			
			new TMessage("info", "Registro salvo com sucesso!");
				TApplication::gotoPage('TipoLicencaList','onReload'); // reload
            // define duas acoes
                //$action1 = new TAction(array('TipoLicencaList', ''));
                // exibe um dialogo ao usuario
               // new TConfirmation($msg, $action1);
            }
        }
        catch (Exception $e) // em caso de exc
         {
        // exibe a mensagem gerada pela exce??o
            new TMessage('error', $e->getMessage());
            // desfaz todas altera??es no banco de dados
            TTransaction::rollback();
			$this->form->setData($cadastro);   // fill the form with the active record data
        }

    }

    /*
     * metodo onEdit()
     * Edita os dados de um registro
     */
    function onEdit($param) {
		
		
		try {
            if (isset($param['key'])) {

                // get the parameter $key
                $key = $param['key'];

                TTransaction::open('pg_ceres');   // open a transaction with database 'samples'

                $object = new TipoLicencaRecord($key);        // instantiates object City

                $this->form->setData($object);   // fill the form with the active record data

                TTransaction::close();           // close the transaction
            } else {
                $this->form->clear();
            }
        } catch (Exception $e) { // in case of exception
            // shows the exception error message
            new TMessage('error', '<b>Error</b> ' . $e->getMessage());
            // undo all pending operations
            TTransaction::rollback();
        }
    
    }

}
?>