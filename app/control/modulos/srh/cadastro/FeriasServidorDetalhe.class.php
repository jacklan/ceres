<?php

//ini_set('display_errors', 1);
//ini_set('display_startup_erros', 1);
//error_reporting(E_ALL);
/*
 * classe FaltaservidorDetalhe
 * Cadastro de Faltaservidor: Contem a listagem e o formulario de busca
 */
use Adianti\Widget\Datagrid\TDatagridTables;
include_once 'app/lib/funcdate.php';

class FeriasServidorDetalhe extends TPage {

    private $form;     // formulario de cadastro
    private $datagrid; // listagem

    /*
     * metodo construtor
     * Cria a pagina, o formulario e a listagem
     */

    public function __construct() {
        parent::__construct();

        // instancia um formulario
        $this->form = new TQuickForm;
        $this->form->class = 'form_Ferias';
        $this->form->setFormTitle('<font color="red" size="3" face="Arial"><b>Detalhe de F&eacute;rias do Servidor</b></font>');

        // cria um rotulo para o titulo
        $lbtitulo = new TLabel('<div style="position:floatval; width: 200px;"> <b>* Campo obrigatorio</b></div>');
        $lbtitulo->setFontFace('Arial');
        $lbtitulo->setFontColor('red');
        $lbtitulo->setFontSize(10);

        // cria os campos do formulario
        $codigo = new THidden('id');
        $codigo->setEditable(false);
        $datainicio = new TDate('iniciogozo');
        $datafim = new TDate('fimgozo');
        $exercicio = new TCombo('exercicio');
        $usuarioalteracao = new THidden('usuarioalteracao');
        $usuarioalteracao->setEditable(false);
        $dataalteracao = new THidden('dataalteracao');
        $dataalteracao->setEditable(false);

        //cria a colecao da tabela estrangeira do Cabecalho
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');
        // instancia objeto da Classe Record
        $cadastro = new ServidorRecord(filter_input(INPUT_GET, 'fk'));
        // cria um criterio de selecao, ordenado pelo id do servidor
        //adiciona os objetos na tela com as informacoes do servidor
        if ($cadastro) {
            $nome = new TLabel($cadastro->nome);
            $matricula = new TLabel($cadastro->matricula);
        }
        // finaliza a transacao
        TTransaction::close();

        //Cria um vetor com as opcoes da combo
        // adiciona as opcoes na combo situacao
        /*
        $items = array();
        $items['2016'] = '2016';
        $items['2015'] = '2015';
        $items['2014'] = '2014';
        $items['2013'] = '2013';
        $items['2012'] = '2012';
        $items['2011'] = '2011';
        $items['2010'] = '2010';
        $items['2009'] = '2009';
        $items['2008'] = '2008';
        $items['2007'] = '2007';
        $items['2006'] = '2006';
        $items['2005'] = '2005';
        $items['2004'] = '2004';
        $items['2003'] = '2003';
        $items['2002'] = '2002';
        $items['2001'] = '2001';
        $items['2000'] = '2000';
        $exercicio->addItems($items);
        $exercicio->setValue(date('Y'));
		*/
        $exercicio->addItems(retornaCinquentaAnos());
        //coloca o valor do relacionamento
        $id = new THidden('servidor_id');
        $id->setValue($_GET['fk']);

        // define os campos
        $this->form->addQuickField(null, $id, 1);
        $this->form->addQuickField(null, $codigo, 10);
        $this->form->addQuickField(null, $usuarioalteracao, 10);
        $this->form->addQuickField(null, $dataalteracao, 10);

        $this->form->addQuickField('Nome Servidor', $nome, 300);
        $this->form->addQuickField('Matr&iacute;cula', $matricula, 40);
        $this->form->addQuickField('Inicio Gozo <font color=red><b>*</b></font>', $datainicio, 20);
        $this->form->addQuickField('Fim Gozo <font color=red><b>*</b></font>', $datafim, 20);
        $this->form->addQuickField('Exercicio <font color=red><b>*</b></font>', $exercicio, 40);
        $this->form->addQuickField(null, $lbtitulo, 50);

        // cria um botao de acao
        $action1 = new TAction(array($this, 'onSave'));
        $action1->setParameter('fk', filter_input(INPUT_GET, 'fk'));

        $this->form->addQuickAction('Salvar', $action1, 'ico_save.png')->class = 'btn btn-info btnleft';

        // prepara o botao para chamar o form anterior
        $action2 = new TAction(array("ServidorForm", 'onEdit'));
        $action2->setParameter('fk', filter_input(INPUT_GET, 'fk'));

        $this->form->addQuickAction('Voltar', $action2, 'ico_datagrid.gif');

        $this->datagrid = new TDatagridTables;

        // instancia as colunas da DataGrid
        $dgdatainicio = new TDataGridColumn('iniciogozo', 'Data In&iacute;cio', 'left', 600);
        $dgdatafim = new TDataGridColumn('fimgozo', 'Data Fim', 'left', 600);
        $exercicio = new TDataGridColumn('exercicio', 'Exercicio', 'left', 600);

        // adiciona as colunas a DataGrid
        $this->datagrid->addColumn($dgdatainicio);
        $this->datagrid->addColumn($dgdatafim);
        $this->datagrid->addColumn($exercicio);

        // instancia duas acoes da DataGrid
        $action1 = new TDataGridAction(array($this, 'onEdit'));
        $action1->setLabel('Editar');
        $action1->setImage('ico_edit.png');
        $action1->setField('id');
        $action1->setFk('servidor_id');

        $action2 = new TDataGridAction(array($this, 'onDelete'));
        $action2->setLabel('Deletar');
        $action2->setImage('ico_delete.png');
        $action2->setField('id');
        $action2->setFk('servidor_id');

        $action3 = new TDataGridAction(array('FeriasGozoDetalhe', 'onReload'));
        $action3->setLabel('Detalhe das Férias');
        $action3->setImage('ico_ppce.png');
        $action3->setField('id');
        $action3->setFk('id');

        // adiciona as acoes a DataGrid
        $this->datagrid->addAction($action1);
        $this->datagrid->addAction($action2);
        $this->datagrid->addAction($action3);

        // cria o modelo da DataGrid, montando sua estrutura
        $this->datagrid->createModel();

        // monta a paina atraves de uma tabela
        $panel = new TPanelForm(700, 500);
        $panel->put($this->form, 0, 0);
        $panel->put($this->datagrid, 150, 240);

        // adiciona a tabela a pagina
        parent::add($panel);
    }

    /*
     * metodo onReload()
     * Carrega a DataGrid com os objetos do banco de dados
     */

    function onReload() {

        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');

        // instancia um repositorio da Classe
        $repository = new TRepository('FeriasRecord');

        // cria um criterio de selecao
        $criteria = new TCriteria;
        $criteria->setProperty('order', 'iniciogozo desc');

        //filtra pelo campo selecionado pelo usuário
        $criteria->add(new TFilter('servidor_id', '=', filter_input(INPUT_GET, 'fk')));


        // carrega os objetos de acordo com o criterio
        $cadastros = $repository->load($criteria);

        $this->datagrid->clear();
        if ($cadastros) {
            // percorre os objetos retornados
            foreach ($cadastros as $cadastro) {
                //converter datas do form para formato Brasileiro
                $cadastro->iniciogozo = TDate::date2br($cadastro->iniciogozo);
                $cadastro->fimgozo = TDate::date2br($cadastro->fimgozo);
                // adiciona o objeto na DataGrid
                $this->datagrid->addItem($cadastro);
            }
        }
        // finaliza a transacao
        TTransaction::close();
        $this->loaded = true;
    }

    /*
     * metodo onDelete()
     * Executada quando o usuario clicar no botao excluir da datagrid
     * Pergunta ao usuario se deseja realmente excluir um registro
     */

    function onDelete($param) {
        // obtem o parametro $key
        $key = $param['key'];

        // define duas acoes
        $action1 = new TAction(array($this, 'Delete'));
        $action2 = new TAction(array($this, 'onReload'));

        // define os parametros de cada acao
        $action1->setParameter('key', $key);
        $action2->setParameter('key', $key);

        //encaminha a chave estrangeira
        $action1->setParameter('fk', $_GET['fk']);
        $action2->setParameter('fk', $_GET['fk']);
        // exibe um dialogo ao usuario
        new TQuestion('Deseja realmente excluir o registro ?', $action1, $action2);
    }

    /*
     * metodo Delete()
     * Exclui um registro
     */

    function Delete($param) {
        // obtem o parametro $key
        $key = $param['key'];
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');

        // instanicia objeto Record
        $cadastro = new FeriasRecord($key);

        try {
            // deleta objeto do banco de dados
            $cadastro->delete();

            // finaliza a transacao
            TTransaction::close();
        } catch (Exception $e) { // em caso de exceção
            // exibe a mensagem gerada pela exceção
            new TMessage('error', $e->getMessage());
            // desfaz todas alterações no banco de dados
            TTransaction::rollback();
        }

        // re-carrega a datagrid
        $this->onReload();
        // exibe mensagem de sucesso
        new TMessage('info', "Registro Exclu&iacute;do com sucesso");
    }

    /*
     * metodo show()
     * Exibe a pagina
     */

    function show() {
        // carrega os dados no datagrid
        $this->onReload();
        //chama o metodo show da super classe
        parent::show();
    }

    /*
     * metodo onSave()
     * Executada quando o usuario clicar no botao salvar do formulario
     */

    function onSave() {
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');
        // obtem os dados no formulario em um objeto Record da Classe
        $cadastro = $this->form->getData('FeriasRecord');

        $cadastro->usuarioalteracao = $_SESSION['usuario'];
        $cadastro->dataalteracao = date("d/m/Y H:i:s");

        //antes de armazenar verifica se algum campo eh requerido e nao foi informado
        $dados = $cadastro->toArray();

        $msg = '';
        $icone = 'info';

        if (empty($dados['iniciogozo'])) {
            $msg .= 'A data de in&iacute;cio deve ser informada.<br/>';
        }

        if (empty($dados['fimgozo'])) {
            $msg .= 'A data de fim deve ser informada.<br/>';
        }

        if (empty($dados['exercicio'])) {
            $msg .= 'O exercicio deve ser informado.<br/>';
        }

        try {

            if ($msg == '') {
                // armazena o objeto
                $cadastro->store();
                $msg = 'Dados armazenados com sucesso';

                // finaliza a transacao
                TTransaction::close();
            } else {
                $icone = 'error';
            }

            if ($icone == 'error') {
                // exibe mensagem de erro
                new TMessage($icone, $msg);
            } else {

                // define array acoes
                $param = array();
                $param['fk'] = $dados['servidor_id'];

                // exibe um dialogo ao usuario
                new TMessage("info", "Registro salvo com sucesso!");
                TApplication::gotoPage('FeriasServidorDetalhe', 'onReload', $param); // reload				
            }
        } catch (Exception $e) { // em caso de exc
            // exibe a mensagem gerada pela exceção
            new TMessage('error', $e->getMessage());
            // desfaz todas alterações no banco de dados
            TTransaction::rollback();
        }
    }

    /*
     * metodo onEdit()
     * Edita os dados de um registro
     */

    function onEdit($param) {
        try {

            if (isset($param['key'])) {

                // get the parameter $key
                $key = $param['key'];

                TTransaction::open('pg_ceres');   // open a transaction with database 'samples'

                $object = new FeriasRecord($key);        // instantiates object City
                //converter datas do form para formato Brasileiro
                $object->iniciogozo = TDate::date2br($object->iniciogozo);
                $object->fimgozo = TDate::date2br($object->fimgozo);

                $this->form->setData($object);   // fill the form with the active record data

                TTransaction::close();           // close the transaction
            } else {

                $this->form->clear();
            }
        } catch (Exception $e) {

            // in case of exception
            // shows the exception error message
            new TMessage('error', '<b>Error</b> ' . $e->getMessage());

            // undo all pending operations
            TTransaction::rollback();
        }
    }

}

?>