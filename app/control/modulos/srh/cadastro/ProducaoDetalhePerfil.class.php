<?php

ini_set('display_errors', 1);
ini_set('display_startup_erros', 1);
error_reporting(E_ALL);

/*
 * classe ProducaoDetalhePerfil
 * Cadastro de Producao: Contem a listagem e o formulario de busca
 */

use Adianti\Widget\Datagrid\TDatagridTables;

class ProducaoDetalhePerfil extends TPage {

    private $form;     // formulario de cadastro
    private $datagrid; // listagem

    /*
     * metodo construtor
     * Cria a pagina, o formulario e a listagem
     */

    public function __construct() {
        parent::__construct();

        // instancia um formulario
        $this->form = new TQuickForm;
        $this->form->class = 'form_Producao';
        $this->form->setFormTitle('<font color="red" size="3" face="Arial"><b>Meu Perfil &raquo; Produ&ccedil;&atilde;o dos Servidores</b></font>');

        // cria os campos do formulario
        $codigo = new THidden('id');
        $codigo->setEditable(false);
        $titulo = new TEntry('titulo');
        $instituicao = new TEntry('instituicao');
        $dataproducao = new TDate('dataproducao');
        $tipomaterial_id = new TCombo('tipomaterial_id');
        $ano = new TCombo('ano');
        $descricao = new TText('descricao');
        $usuarioAlteracao = new TEntry('usuarioalteracao');
        $usuarioAlteracao->setEditable(false);
        $dataAlteracao = new TDate('dataalteracao');
        $dataAlteracao->setEditable(false);

        //campos hidden
        $usuarioalteracao = new THidden('usuarioalteracao');
        $dataalteracao = new THidden('dataalteracao');

        $statusalteracao = new THidden('statusalteracao');
        $datastatus = new THidden('datastatus');
        //lanca o default
        $statusalteracao->setValue($_SESSION['usuario']);
        $datastatus->setValue(date("d/m/Y H:i:s"));

        //cria a colecao da tabela estrangeira habilidade_id
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');
        // instancia objeto da Classe Record
        $repository = new TRepository('TipoMaterialRecord');
        $criteria = new TCriteria;
        $criteria->setProperty('order', 'nome');
        //filtra pelo campo selecionado pelo usuario
        //  $criteria->add(new TFilter($campo, $operador, $valor));
        // carrega os objetos de acordo com o criterio
        $collection = $repository->load($criteria);
        //adiciona os objetos no combo
        foreach ($collection as $object) {
            $items2[$object->id] = $object->nome;
        }

        // adiciona as opcoes na combo
        $tipomaterial_id->addItems($items2);

        // finaliza a transacao
        TTransaction::close();

        //cria a colecao da tabela estrangeira
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');
        // instancia objeto da Classe Record
        $repository = new TRepository('ProducaoRecord');
        // cria um criterio de selecao, ordenado pelo id
        $criteria = new TCriteria;
        $criteria->setProperty('order', 'servidor_id');
        // carrega os objetos de acordo com o criterio
        $cadastros = $repository->load($criteria);

        // finaliza a transacao
        TTransaction::close();

        //cria a colecao da tabela estrangeira do Cabecalho
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');
        // instancia objeto da Classe Record
        $cadastro = new ServidorRecord($_SESSION['servidor_id']);
        // cria um criterio de selecao, ordenado pelo id do servidor
        //adiciona os objetos na tela com as informacoes do servidor
        if ($cadastro) {
            $nome = new TLabel($cadastro->nome);
            $matricula = new TLabel($cadastro->matricula);
        }
        // finaliza a transacao
        TTransaction::close();

        //coloca o valor do relacionamento
        $id = new THidden('servidor_id');
        $id->setValue(filter_input(INPUT_GET, 'fk'));

        // define os campos
        $this->form->addQuickField(null, $codigo, 1);
        $this->form->addQuickField(null, $id, 1);
        $this->form->addQuickField('Titulo', $titulo, 50);
        $this->form->addQuickField('Instituicao', $instituicao, 40);
        $this->form->addQuickField('Data Producao', $dataproducao, 40);
        $this->form->addQuickField('Tipo Material', $tipomaterial_id, 40);
        $this->form->addQuickField('Ano', $ano, 40);

        $actionVoltar = new TAction(array('MeuPerfilList', 'onReload'));
        $actionVoltar->setParameter('fk', filter_input(INPUT_GET, 'fk'));
        $actionVoltar->setParameter('key', filter_input(INPUT_GET, 'key'));

        // cria um botao de acao
        $this->form->addQuickAction('Salvar', new TAction(array($this, 'onSave')), 'ico_save.png')->class = 'btn btn-info btnleft';
        $this->form->addQuickAction('Voltar', $actionVoltar, 'ico_datagrid.gif');

        //Campos obrigatórios
        $titulo->setProperty('required', 'required');
        $instituicao->setProperty('required', 'required');
        $descricao->setProperty('required', 'required');

        //Define o auto-sugerir
        $instituicao->setProperty('placeholder', 'Ex: UFRN...');
        $descricao->setProperty('placeholder', 'Escrever...');

        $items = array();
        $items['2016'] = '2016';
        $items['2015'] = '2015';
        $items['2014'] = '2014';
        $items['2013'] = '2013';
        $items['2012'] = '2012';
        $items['2011'] = '2011';
        $items['2010'] = '2010';
        $items['2009'] = '2009';
        $items['2008'] = '2008';
        $items['2007'] = '2007';
        $items['2006'] = '2006';
        $items['2005'] = '2005';
        $items['2004'] = '2004';
        $items['2003'] = '2003';
        $items['2002'] = '2002';
        $items['2002'] = '2001';
        $items['2000'] = '2000';
        $ano->addItems($items);
        $ano->setValue(date('Y'));

        // instancia objeto DataGrid
        $this->datagrid = new TDatagridTables;

        // instancia as colunas da DataGrid
        $dgtitulo = new TDataGridColumn('titulo', 'T&iacute;tulo', 'left', 200);
        $dginstituicao = new TDataGridColumn('instituicao', 'Institui&ccedil;&atilde;o', 'left', 1200);
        $dgdescricao = new TDataGridColumn('descricao', 'Descri&ccedil;&atilde;o', 'left', 400);
        $dgano = new TDataGridColumn('ano', 'Ano', 'left', 30);
        $dgstatus = new TDataGridColumn('status', 'Status', 'left', 180);

        // adiciona as colunas a DataGrid
        $this->datagrid->addColumn($dgtitulo);
        $this->datagrid->addColumn($dginstituicao);
        $this->datagrid->addColumn($dgdescricao);
        $this->datagrid->addColumn($dgano);
        $this->datagrid->addColumn($dgstatus);

        // instancia duas acoes da DataGrid
        $action1 = new TDataGridAction(array($this, 'onEdit'));
        $action1->setLabel('Editar');
        $action1->setImage('ico_edit.png');
        $action1->setField('id');
        $action1->setFk('servidor_id');

        $action2 = new TDataGridAction(array($this, 'onDelete'));
        $action2->setLabel('Deletar');
        $action2->setImage('ico_delete.png');
        $action2->setField('id');
        $action2->setFk('servidor_id');

        $action3 = new TDataGridAction(array('AreaProducaoDetalhePerfil', 'onReload'));
        $action3->setLabel('Pagina');
        $action3->setimage('ico_areaproducao.png');
        $action3->setField('id');
        $action3->setFk('servidor_id');
        $action3->setParameter('did', filter_input(INPUT_GET, 'fk'));

        // adiciona as acoes a DataGrid
        $this->datagrid->addAction($action1);
        $this->datagrid->addAction($action2);
        $this->datagrid->addAction($action3);

        // cria o modelo da DataGrid, montando sua estrutura
        $this->datagrid->createModel();

        // monta a paina atraves de uma tabela
        $panel = new TPanelForm(900, 500);
        $panel->put($this->form, 0, 0);
        $panel->put($this->datagrid, 30, 320);


        // adiciona a tabela a pagina
        parent::add($panel);
    }

    /*
     * metodo onReload()
     * Carrega a DataGrid com os objetos do banco de dados
     */

    function onReload() {
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');

        // instancia um repositorio da Classe
        $repository = new TRepository('ProducaoRecord');

        // cria um criterio de selecao
        $criteria = new TCriteria;
        //filtra pelo campo selecionado pelo usuário
        $criteria->add(new TFilter('servidor_id', '=', filter_input(INPUT_GET, 'fk')));

        // carrega os objetos de acordo com o criterio
        $cadastros = $repository->load($criteria);

        $this->datagrid->clear();
        if ($cadastros) {
            // percorre os objetos retornados
            foreach ($cadastros as $cadastro) {
                // adiciona o objeto na DataGrid
                $this->datagrid->addItem($cadastro);
            }
        }

        // finaliza a transacao
        TTransaction::close();
        $this->loaded = true;
    }

    /*
     * metodo onDelete()
     * Executada quando o usuario clicar no botao excluir da datagrid
     * Pergunta ao usuario se deseja realmente excluir um registro
     */

    function onDelete($param) {
        // obtem o parametro $key
        $key = $param['key'];

        // define duas acoes
        $action1 = new TAction(array($this, 'Delete'));
        $action2 = new TAction(array($this, 'onReload'));

        // define os parametros de cada acao
        $action1->setParameter('key', $key);
        $action2->setParameter('key', $key);

        //encaminha a chave estrangeira
        $action1->setParameter('fk', filter_input(INPUT_GET, 'fk'));
        $action1->setParameter('did', filter_input(INPUT_GET, 'did'));
        $action2->setParameter('fk', filter_input(INPUT_GET, 'fk'));
        $action2->setParameter('did', filter_input(INPUT_GET, 'did'));

        // exibe um dialogo ao usuario
        new TQuestion('Deseja realmente excluir o registro ?', $action1, $action2);
    }

    /*
     * metodo Delete()
     * Exclui um registro
     */

    function Delete($param) {
        try {
        // obtem o parametro $key
        $key = $param['key'];
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');

        // instanicia objeto Record
        $cadastro = new ProducaoRecord($key);

            // deleta objeto do banco de dados
            $cadastro->delete();

            // finaliza a transacao
            TTransaction::close();
        } catch (Exception $e) { // em caso de exceção
            // exibe a mensagem gerada pela exceção
            new TMessage('error', $e->getMessage());
            // desfaz todas alterações no banco de dados
            TTransaction::rollback();
        }

        // re-carrega a datagrid
        $this->onReload();
        // exibe mensagem de sucesso
        //new TMessage('info', "Registro Exclu&iacute;do com sucesso");
        // msgAlert("ProducaoDetalhePerfil", 'onReload', 'key=' . $key, 'delete');
    }

    /*
     * metodo show()
     * Exibe a pagina
     */

    function show() {
        // carrega os dados no datagrid
        $this->onReload();
        //chama o metodo show da super classe
        parent::show();
    }

    /*
     * metodo onSave()
     * Executada quando o usuario clicar no botao salvar do formulario
     */

    function onSave() {
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');
        // obtem os dados no formulario em um objeto Record da Classe
        $cadastro = $this->form->getData('ProducaoRecord');

        $cadastro->usuarioalteracao = $_SESSION['usuario'];
        $cadastro->dataalteracao = date("d/m/Y H:i:s");

        //antes de armazenar verifica se algum campo eh requerido e nao foi informado
        $dados = $cadastro->toArray();

        $msg = '';
        $icone = 'info';

        if (empty($dados['ano'])) {
            $msg .= 'O Ano deve ser informado';
        }
        if (empty($dados['tipomaterial_id'])) {
            $msg .= 'O tipo material deve ser informado';
        }

        try {

            if ($msg == '') {
                // armazena o objeto
                $cadastro->store();
                $msg = 'Dados armazenados com sucesso';

                // finaliza a transacao
                TTransaction::close();
            } else {
                $icone = 'error';
            }

            if ($icone == 'error') {
                // exibe mensagem de erro
                new TMessage($icone, $msg);
            } else {

                $param = array();
                $param ['fk'] = $dados['servidor_id'];
                //chama o formulario com o grid
                // exibe um dialogo ao usuario
                new TMessage("info", "Registro salvo com sucesso!");
                TApplication::gotoPage('ProducaoDetalhePerfil', 'onReload', $param); // reload
            }
        } catch (Exception $e) { // em caso de exc
            // exibe a mensagem gerada pela exceção
            new TMessage('error', $e->getMessage());
            // desfaz todas alterações no banco de dados
            TTransaction::rollback();
        }
    }

    /*
     * metodo onEdit()
     * Edita os dados de um registro
     */

    function onEdit($param) {

        try {
            if (isset($param['key'])) {

                // get the parameter $key
                $key = $param['key'];

                TTransaction::open('pg_ceres');   // open a transaction with database 'samples'

                $object = new ProducaoRecord($key);        // instantiates object City

                $this->form->setData($object);   // fill the form with the active record data

                TTransaction::close();           // close the transaction
            } else {
                $this->form->clear();
            }
        } catch (Exception $e) { // in case of exception
            // shows the exception error message
            new TMessage('error', '<b>Error</b> ' . $e->getMessage());
            // undo all pending operations
            TTransaction::rollback();
        }
    }

}
