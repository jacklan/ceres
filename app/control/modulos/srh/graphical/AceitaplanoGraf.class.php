<?php

/*
 * classe AceitaplanoGraf
 * Grafico de Cargos por Servidor: Contem o Grafico atraves da view vw_servidores_por_cargos
 */

//ini_set('display_errors', 1);
//ini_set('display_startup_erros', 1);
//error_reporting(E_ALL);

include_once 'app/lib/FusionCharts/FusionCharts.php';

class AceitaplanoGraf extends TPage {
    /*
     * metodo construtor
     * Cria a pagina e o Grafico
     */

    public function __construct() {
        parent::__construct();
    }

    /*
     * metodo onReload()
     * Carrega a DataGrid com os objetos do banco de dados
     */

    function onReload() {
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');

        // instancia um repositorio para Carros
        $repository = new TRepository('Vw_aceitaplanoRecord');

        // cria um criterio de selecao, ordenado pelo id
        $criteria = new TCriteria;
        $criteria->add(new TFilter('empresa_id', '=', $_SESSION['empresa_id']) );
        $criteria->setProperty('order', 'aceitaplano');
        // carrega os objetos de acordo com o criterio
        $cadastros = $repository->load($criteria);

        echo "<script LANGUAGE='Javascript' SRC='app/lib/FusionCharts/FusionCharts.js'></script>";

        $strXML = "<chart id='chart1' caption='Grafico de Servidores / Termo de Adesao' subCaption='Servidores Ativos e Aposentados' xAxisName='Aceite' yAxisName='Quantidade de Servidores'  pieSliceDepth='30' showBorder='1' formatNumberScale='0' numberSuffix='' animation='1' bgColor='99CCFF,FFFFFF' >";
        //  $strXML = "<chart id='chart1' caption='Grafico de Servidores / Termo de Adesao' xAxisName='REGIONAL' yAxisName='QUANTIDADE'  pieSliceDepth='10' showBorder='0' formatNumberScale='0' numberSuffix='' animation='1' exportEnabled='1' exportAtClient='1' exportHandler='fcExporter1' exportFileName='DashBoard Servidores Aposentadoria Regional' >";
        if ($cadastros) {
            foreach ($cadastros as $reg) {
                //antes de armazenar verifica se algum campo eh requerido e nao foi informado
                $dados = $reg->toArray();

                $strXML .= "<set label='" . $dados['aceitaplano'] . "' value='" . $dados['qtd'] . "'  link='n-index.php?class=RelatorioAceitaplanoPDF%26aceitaplano=" . $dados['aceitaplano'] . "' />";
            }
        }
        $strXML .= "</chart>";

        echo $strXML;

        echo "<div class='col-xs-12 col-sm-12 col-md-12 col-lg-12' style='margin-top:50px'>";
        echo renderChart("app/lib/FusionCharts/Column3D.swf", "", $strXML, "Chart1", "100%", 500, false, false);
        echo "</div>";

        // finaliza a transacao
        TTransaction::close();
        $this->loaded = true;
    }

    /*
     * metodo show()
     * Exibe a pagina
     */

    function show() {
        // se a listagem ainda nao foi carregada
        if (!$this->loaded) {
            $this->onReload();
        }
        parent::show();
    }

}

?>