<?php

/*
 * classe GeraGraficoQtdServidorList
 * Cadastro de GeraGraficoQtdServidorList: Contem a listagem e o formulario de busca
 */

class GeraGraficoQtdServidorList extends TPage {

    private $form;     // formulario de cadastro

    /*
     * metodo construtor
     * Cria a pagina, o formulario e a listagem
     */

    public function __construct() {
        parent::__construct();

        // instancia um formulario
        $this->form = new TForm('form_busca_GeraGraficoQtdServidor');

        // instancia uma tabela
        $panel = new TPanelForm(700, 100);
        $panel->setColuna(150);

        // adiciona a tabela ao formulario
        $this->form->add($panel);

        // cria um r�tulo para o t�tulo
        $titulo = new TLabel('Gr&iacute;fico Quantidade de Servidores por Regi&atilde;o');
        $titulo->setFontFace('Arial');
        $titulo->setFontColor('red');
        $titulo->setFontSize(12);

        // adiciona o campo Titulo
        $panel->put($titulo, $panel->getColuna(), $panel->getLinha());

        // cria os campos do formulario
        $opcao1 = new TCombo('grafico');
        $regional_id = new TCombo('regional_id');

        // cria vetor com as opcoes da combo grafico
        $items1 = array();
        $items1['PIZZA 3D'] = 'PIZZA 3D';
        $items1['COLUNA 3D'] = 'COLUNA 3D';


        //cria os campos com opcoes de grafico
        $opcao1->addItems($items1);
        //coloca o valor padrão
        $opcao1->setValue('COLUNA 3D');
        $opcao1->setSize(40);

        //cria a colecao da tabela estrangeira
        // inicia transacao com o banco 'pg_ceres'
        // cria vetor com as opcoes da combo mes
        $items2 = array();
        TTransaction::open('pg_ceres');
        // instancia objeto da Classe Record
        $repository = new TRepository('RegionalRecord');
        // cria um criterio de selecao, ordenado pelo id
        $criteria = new TCriteria;
        $criteria->setProperty('order', 'nome');
        // carrega os objetos de acordo com o criterio
        $cadastros = $repository->load($criteria);

        //adiciona os objetos no combo
        foreach ($cadastros as $object) {
            $items2[$object->id] = $object->nome;
        }

        // finaliza a transacao
        TTransaction::close();

        $items2[0] = 'TODAS';

        //cria os campos com opcoes de grafico
        $regional_id->addItems($items2);
        //coloca o valor padrão
        $regional_id->setValue('TODAS');
        $regional_id->setSize(40);

        // cria um botao de acao (cadastrar)
        $new_button = new TButton('gerarelatorio');

        // define a acao do botao cadastrar
        $new_button->setAction(new TAction(array('ServidorporRegionalGraf', 'onReload')), 'Gerar Grafico');

        $panel->setLinha(50);

        //adiciona campo op��o 1 - grafico
        $panel->putCampo($opcao1, 'Grafico', 0, 0);

        // adiciona campo mes
        $panel->putCampo($regional_id, 'Regional', 0, 1);

        //adiciona botao gerar grafico
        $panel->putCampo($new_button, null, 0, 1);
        // $panel->put($new_button, $panel->getColuna()+270,$panel->getLinha());
        // define quais sao os campos do formulario
        $this->form->setFields(array($opcao1, $regional_id, $new_button));

        // monta a paina atraves de uma tabela
        $panel = new TPanelForm(700, 500);
        $panel->put($this->form, 0, 0);

        // adiciona a tabela a pagina
        parent::add($panel);
    }

}

?>