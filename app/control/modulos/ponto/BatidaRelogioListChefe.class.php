<?php

use Adianti\Widget\Datagrid\TDatagridTables;
use Lib\Funcoes\Util;

include_once 'app/lib/funcdate.php';

class BatidaRelogioListChefe extends TPage
{
    private $form;
    private $datagrid;

    public function __construct()
    {
        parent::__construct();

        $this->form = new TQuickForm('batida_relogiolistchefe');
        $this->form->class = 'batida_relogiolistchefe';
        $this->form->setFormTitle('<b STYLE="color: red;"> Batidas Rel&oacute;gio Servidor</b>');

        $criteriaSetor = new TCriteria;
        $criteriaSetor->add(new TFilter('empresa_id', '=', $_SESSION['empresa_id']));

        $setor = new TDBCombo('setor_id', 'pg_ceres', 'SetorRecord', 'id', 'nome', 'nome', $criteriaSetor);
        $mes = new TCombo('mes');
        $ano = new TCombo('ano');


        $criteria_servidor = new TCriteria;
        $criteria_servidor->add(new TFilter('situacao', '=', 'EM ATIVIDADE'), TExpression::OR_OPERATOR);
        $criteria_servidor->add(new TFilter('situacao', '=', 'A DISPOSICAO'), TExpression::OR_OPERATOR);
        $criteria_servidor->add(new TFilter('situacao', '=', 'RESCINDIDO(A)'), TExpression::OR_OPERATOR);

        $criteriaS = new TCriteria;
        $criteriaS->add(new TFilter('empresa_id', '=', $_SESSION['empresa_id']));
        $criteriaS->add($criteria_servidor);


        $items_servidor = array('Nome', 'Matricula');
        $servidor_id = new TDBSeekButton('servidor_id', 'pg_ceres', $this->form->getName(), 'ServidorRecord', 'nome', 'servidor_id', 'nome', $items_servidor, $criteriaS);

        $servidor_nome = new TEntry('nome');
        $situacao = new TCombo('situacao');
//        $servidor_id->style = "text-transform: uppercase;";
//        $servidor_id->setProperty('placeholder', 'Nome ou Matricula ou CPF');
//        $servidor_id->setMinLength(1);
//        $servidor_id->setMaxSize(1);
        /* ------------------ MultiSearch Servidor ------------------ */
//        $servidor_id->style = "text-transform: uppercase;";
//        $servidor_id->setProperty('placeholder', 'Nome ou Matricula ou CPF');
//        $servidor_id->setMinLength(1);
//        $servidor_id->setMaxSize(1);
        /* ------------------ MultiSearch Servidor ------------------ */

        $items = [];
        $items['01'] = 'JANEIRO';
        $items['02'] = 'FEVEREIRO';
        $items['03'] = 'MARÇO';
        $items['04'] = 'ABRIL';
        $items['05'] = 'MAIO';
        $items['06'] = 'JUNHO';
        $items['07'] = 'JULHO';
        $items['08'] = 'AGOSTO';
        $items['09'] = 'SETEMBRO';
        $items['10'] = 'OUTUBRO';
        $items['11'] = 'NOVEMBRO';
        $items['12'] = 'DEZEMBRO';

        $mes->addItems($items);
        $mes->setValue(date('m'));

        $servidor_nome->setSize(200);

        TTransaction::open('pg_ceres');
        $repositoryS = new TRepository('vw_situacao_servidorRecord');
        $criteriaS = new TCriteria;
        $objectsS = $repositoryS->load($criteriaS);

        $itemsSituacao[0] = 'TODAS';
        foreach ($objectsS as $object) {
            $itemsSituacao[$object->situacao] = $object->situacao;
        }
        TTransaction::close();

        $situacao->addItems($itemsSituacao);
        $situacao->setValue('EM ATIVIDADE');
        $situacao->setSize(40);

        $ano->addItems(Util::retornaAnosemZero());
        $ano->setValue(date('Y'));

        $actionClearSetor = new TAction(array($this, 'onChangeActionSetorClear'));
        $servidor_nome->setExitAction($actionClearSetor);

        $actionClearServidor = new TAction(array($this, 'onChangeActionServidorClear'));
        $setor->setChangeAction($actionClearServidor);

        $mes->addValidation('Mês', new TRequiredValidator);
        $ano->addValidation('Ano', new TRequiredValidator);

        $this->form->addQuickFields('Servidor', array($servidor_id, $servidor_nome), [new \Adianti\Widget\Form\TLabel('Apenas o servidor')]);
        //$this->form->addQuickField('Servidor', $servidor_nome, 450);
        $this->form->addQuickField('Setor', $setor, 50);
        $this->form->addQuickField('Mes: <b style="color:red;">*</b>', $mes, 20);
        $this->form->addQuickField('Ano: <b style="color:red;">*</b>', $ano, 10);
        $this->form->addQuickField('Situação: ', $situacao, 30);
        $this->form->addQuickField("", new TLabel("<b>Não selecionar servidor, caso deseje buscar por todos.</b>"), 600);
        $this->form->addQuickField("", new TLabel("<b style='color: red;'>Campos Obrigatórios com *</b>"), 600);

        $this->form->addQuickAction('Buscar', new TAction([$this, 'onSearch']), 'ico_find.png');
        $this->form->addQuickAction('Gerar Relat&oacute;rio', new TAction(array($this, 'onGenerate')), 'fa:file-pdf-o red');
        $this->form->addQuickAction('Limpar Busca', new TAction(array($this, 'onClear')), 'fa:eraser red');

        $this->datagrid = new TDataGridTables;

        $dgservidor = new TDataGridColumn('nome_servidor', 'Servidor', 'left', 400);
        $dgmatricula = new TDataGridColumn('matricula', 'Matrícula', 'left', 280);
        $dgdatabatida = new TDataGridColumn('databatida', 'Data', 'left', 280);
        $dghorabatida = new TDataGridColumn('databatida', 'Hora', 'left', 280);
        $dgfuncao = new TDataGridColumn('funcao', 'Função', 'left', 280);
        $dgprocessado = new TDataGridColumn('processado', 'Processado', 'left', 280);

        $this->datagrid->addColumn($dgservidor);
        $this->datagrid->addColumn($dgmatricula);
        $this->datagrid->addColumn($dgdatabatida);
        $this->datagrid->addColumn($dghorabatida);
        $this->datagrid->addColumn($dgfuncao);
        $this->datagrid->addColumn($dgprocessado);

        $dgdatabatida->setTransformer('formatar_data');
        $dghorabatida->setTransformer('formatar_hora2');

        $this->datagrid->createModel();

        $panel = new TPanelForm(700, 500);
        $panel->put($this->form, 0, 0);
        $panel->put($this->datagrid, 150, 165);

        parent::add($panel);
    }

    function onGenerate()
    {

        try {

            $data = $this->form->getData();
            $this->form->validate();

            if (!empty($data->servidor_id)) {
                new RelatorioBatidaServidorChefePDF();

            } else {
                new RelatorioBatidaServidorLoteSetorChefePDF();

            }

        } catch (Exception $e) {
            new TMessage('error', $e->getMessage());
        }
    }

    function onReload()
    {
    }

    function onSearch()
    {
        try {

            $data = $this->form->getData();
            $this->form->setData($data);

            $this->form->validate();

            TTransaction::open('pg_ceres');

            $repository = new TRepository('Vw_Servidor_BatidaRecord');

            $criteria = new TCriteria;

            if ($data->setor_id != null) {
                $criteria->add(new TFilter('setor_id', '=', $data->setor_id));
            }

            if ($data->mes != null) {
                $criteria->add(new TFilter('mes', '=', $data->mes));
            }

            if ($data->ano != null) {
                $criteria->add(new TFilter('ano', '=', $data->ano));
            }

            if ($data->servidor_id != null) {
                $criteria->add(new TFilter('servidor_id', '=', $data->servidor_id));

            }

            $criteria->setProperty('order', 'nome_servidor DESC');
            $objects = $repository->load($criteria);

            $this->datagrid->clear();

            if ($objects) {
                foreach ($objects as $object) {

                    $this->datagrid->addItem($object);
                }
            }
            TTransaction::close();

        } catch (Exception $e) {
            new TMessage('error', $e->getMessage());
            TTransaction::rollback();
        }

    }


    public
    function onClear()
    {

        if (TSession::getValue('filter_Vw_Servidor_BatidaRecord')) {
            TSession::setValue('filter_Vw_Servidor_BatidaRecord', null);
        }

        $this->onReload();

    }

    public
    static function onChangeActionServidorClear($param)
    {

        TField::clearField('batida_relogiolistchefe', 'servidor_id');
        TField::clearField('batida_relogiolistchefe', 'nome');

    }

    public
    static function onChangeActionSetorClear($param)
    {

        TField::clearField('batida_relogiolistchefe', 'setor_id');

    }

    function show()
    {
        parent::show();
    }

}