<?php

//ini_set('display_errors', 1);
//ini_set('display_startup_erros', 1);
//error_reporting(E_ALL);

include_once 'app/lib/funcdate.php';

use Adianti\Widget\Datagrid\TDatagridTables;

class GeraServidoresSemBaterPonto extends TPage {

    private $form;
    private $datagrid;

    public function __construct() {
        parent::__construct();

        $this->form = new TQuickForm;
        $this->form->class = 'form_ServidoresSemBatida';
        $this->form->setFormTitle('<font color="red" size="3px" face="Arial"><b>Relat&oacute;rio de Servidores Por Mês Sem Bater Ponto</b></font>');
        
        $ano = new TCombo('ano');
        $mes = new TCombo('mes');
        $unidadeoperativa_id = new TCombo('unidadeoperativa_id');
        $setor_id = new TCombo('setor_id');
        
        $items= array();
        $items['janeiro'] = 'JANEIRO';
        $items['fevereiro'] = 'FEVEREIRO';
        $items['marco'] = 'MARCO';
        $items['abril'] = 'ABRIL';
        $items['maio'] = 'MAIO';
        $items['junho'] = 'JUNHO';
        $items['julho'] = 'JULHO';
        $items['agosto'] = 'AGOSTO';
        $items['setembro'] = 'SETEMBRO';
        $items['outubro'] = 'OUTUBRO';
        $items['novembro'] = 'NOVEMBRO';
        $items['dezembro'] = 'DEZEMBRO';

        $mes->addItems($items);

        $ano->addItems( retornaAnosemZero() );
        $ano->setValue( date('Y') );

        $items= array();
        TTransaction::open('pg_ceres');
        $repository = new TRepository('UnidadeOperativaRecord');

        $criteria = new TCriteria;
        $criteria->setProperty('order', 'nome');
        $criteria->add(new TFilter('empresa_id', '=', $_SESSION['empresa_id']));

        $collection = $repository->load($criteria);
        foreach ($collection as $object){
            $items[$object->id] = $object->nome;
        }

        $unidadeoperativa_id->addItems($items);
        $unidadeoperativa_id->setValue('158');
        $unidadeoperativa_id->setSize(40);

        TTransaction::close();

        $items= array();
        TTransaction::open('pg_ceres');
        $repository = new TRepository('SetorRecord');
        $criteria = new TCriteria;
        $criteria->setProperty('order', 'nome');
        $criteria->add(new TFilter('empresa_id', '=', $_SESSION['empresa_id']));

        $collection = $repository->load($criteria);
        $items['0'] = 'TODOS';
        foreach ($collection as $object){
            $items[$object->id] = $object->nome;
        }

        $setor_id->addItems($items);
        $setor_id->setValue('0');
        $setor_id->setSize(40);

        TTransaction::close();

        $this->form->addQuickField("Unidade Operativa", $unidadeoperativa_id, 30); 
        $this->form->addQuickField("Setor", $setor_id, 30);
        $this->form->addQuickField("Ano", $ano, 30 );                
        $this->form->addQuickField("Mês", $mes, 30 );        

        $action1 = new TAction(array( $this, 'MostrarRelatorio'));
        //$action2 = new TAction(array( $this, 'onGenerate'));

        $this->form->addQuickAction('Mostrar Relatorio', $action1, 'ico_save.png')->class = 'btn btn-info btnleft';

        //$this->form->addQuickAction('Gerar Relatorio', $action2, 'ico_save2.png')->class = 'btn btn-info btnleft';

        $this->datagrid = new TDataGridTables;

        $dgservidor   = new TDataGridColumn('nome_servidor',  'Servidor',  'left', 300);
        $dgunidade     = new TDataGridColumn('nome_unidadeoperativa',    'Unidade',    'left', 100);
        $dgsetor      = new TDataGridColumn('nome_setor',   'Setor',  'left',  100);
        $dgano      = new TDataGridColumn('ano',   'Ano',  'left',  70);
        
        $this->datagrid->addColumn($dgservidor);
        $this->datagrid->addColumn($dgunidade);
        $this->datagrid->addColumn($dgsetor);
        $this->datagrid->addColumn($dgano);        

        $this->datagrid->createModel();
        
        $panel = new TPanelForm(700, 500);
        $panel->put($this->form, 0, 0);
        $panel->put($this->datagrid, 150, 165);

        parent::add($panel);
        
    }

    function onReload(){
        $this->loaded = true;
    }

    function MostrarRelatorio(){

        $unidadeoperativa_id = $_REQUEST['unidadeoperativa_id'];
        $empresa_id = $_SESSION['empresa_id'];
        $setor_id = $_REQUEST['setor_id'];
        $ano = $_REQUEST['ano'];
        $mes = $_REQUEST['mes'];

        TTransaction::open('pg_ceres');

        $repository = new TRepository('vw_servidor_sembaterpontoRecord');

        $criteria = new TCriteria;

        $criteria->add(new TFilter('unidadeoperativa_id', '=', $unidadeoperativa_id));
        $criteria->add(new TFilter('empresa_id', '=', $empresa_id));

        if($setor_id != '0'){
            $criteria->add(new TFilter('setor_id', '=', $setor_id));
        }

        $criteria->add(new TFilter('ano', '=', $ano));

        if($mes == 'janeiro'){
            $criteria->add(new TFilter('janeiro', '=', 'N'));
        }
        if($mes == 'fevereiro'){
            $criteria->add(new TFilter('fevereiro', '=', 'N'));
        }
        if($mes == 'marco'){
            $criteria->add(new TFilter('marco', '=', 'N'));
        }
        if($mes == 'abril'){
            $criteria->add(new TFilter('abril', '=', 'N'));
        }
        if($mes == 'maio'){
            $criteria->add(new TFilter('maio', '=', 'N'));
        }
        if($mes == 'junho'){
            $criteria->add(new TFilter('junho', '=', 'N'));
        }
        if($mes == 'julho'){
            $criteria->add(new TFilter('julho', '=', 'N'));
        }
        if($mes == 'agosto'){
            $criteria->add(new TFilter('agosto', '=', 'N'));
        }
        if($mes == 'setembro'){
            $criteria->add(new TFilter('setembro', '=', 'N'));
        }
        if($mes == 'outubro'){
            $criteria->add(new TFilter('outubro', '=', 'N'));
        }
        if($mes == 'novembro'){
            $criteria->add(new TFilter('novembro', '=', 'N'));
        }
        if($mes == 'dezembro'){
            $criteria->add(new TFilter('dezembro', '=', 'N'));
        }

        $cadastros = $repository->load($criteria);
        $this->datagrid->clear();
        if ($cadastros){

            foreach ($cadastros as $cadastro){
                $this->datagrid->addItem($cadastro);
            }
        }

        TTransaction::close();
        $this->loaded = true;
    }

    function onGenerate(){
        try{

            $this->form->validate();
            $data = $this->form->getData();
            
            new RelatorioSemBatidasPontoPDF();
        }catch( Exception $e ){

            new TMessage( 'error', $e->getMessage() );
        }

    }
    /*      

    function show() {
        $this->onReload();
        parent::show();
    }
    */
    

}
?>