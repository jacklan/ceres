<?php

ini_set('display_errors', 1);
ini_set('display_startup_erros', 1);
error_reporting(E_ALL);

use Adianti\Widget\Datagrid\TDatagridTables;
use Lib\Funcoes\Util;
include_once 'app/lib/funcdate.php';

class TrocarEscalaDetalhe extends TPage
{
    public function __construct()
    {

        parent::__construct();


        $this->form = new TQuickForm('trocar_escala_detalhe');
        //$this->form->setFormTitle('<b STYLE="color: red;"> Batidas Rel&oacute;gio Servidor</b>');
        //$this->form = new BootstrapFormWrapper(new TQuickForm('trocar_escala_detalhe'));



        $criteria = new TCriteria;
        $criteria->add(new TFilter('situacao', '=', 'EM ATIVIDADE'), TExpression::OR_OPERATOR);

        $criteria2 = new TCriteria();
        $criteria2->add(new TFilter('empresa_id', '=',  $_SESSION['empresa_id']));

        $criteria3 = new TCriteria;

        $criteria3->add($criteria2);
        $criteria3->add($criteria);

        $criteria3->setProperty('order', 'nome');

        $items_servidor = array('Nome', 'Matricula');

        $servidor_id = new TDBSeekButton('servidor_id', 'pg_ceres', $this->form->getName(), 'ServidorRecord', 'nome', 'servidor_id', 'nome', $items_servidor, $criteria3);
        $servidor_nome = new TEntry('nome');
        $servidor_nome->setEditable(false);

        $servidor_id->setSize(100);
        $servidor_nome->setSize(380);


        $this->form->addQuickFields(new TLabel('Servidor: <font color=red><b>*</b></font>'),
            array($servidor_id, $servidor_nome));

        $action_buscar = new TAction(array($this, 'onSearch'));
        $action_buscar->setParameter('key', filter_input(INPUT_GET, 'key'));

        $this->form->addQuickAction('Buscar', $action_buscar, 'fa:search')->class = 'btn btn-sm btn-primary';
        $this->form->addQuickAction('Voltar', new TAction(array('TrocarEscalaList', 'onReload')), 'fa:arrow-left');

        $this->datagrid = new TDataGridTables;

        $dgdata = new TDataGridColumn('datainicio', 'Data', 'left', 1300);
        $dghorainicio = new TDataGridColumn('datainicio2', 'Hora Início', 'left', 1300);
        $dghorafim = new TDataGridColumn('datafim', 'Hora Fim', 'left', 1300);


        $this->datagrid->addColumn($dgdata);
        $this->datagrid->addColumn($dghorainicio);
        $this->datagrid->addColumn($dghorafim);

        $this->datagrid->disableDefaultClick();

        $actionEdit = new TDataGridAction(array('TrocarEscalaDetalheForm', 'onEdit'));
        $actionEdit->setLabel('Trocar Escala');
        $actionEdit->setImage( "fa:fw fa-exchange green fa-lg" );
        $actionEdit->setField('id');
        $actionEdit->setParameter('fk', filter_input(INPUT_GET, 'key'));

        $this->datagrid->addAction($actionEdit);

        $this->datagrid->createModel();

        $container = new TVBox();
        $container->style = "width: 100%";
        $container->add( TPanelGroup::pack( 'Trocar Escala', $this->form ) );
        $container->add( TPanelGroup::pack( NULL, $this->datagrid ) );

        parent::add( $container );

    }

    public function onSearch()
    {

        $data = $this->form->getData();

        try {

            if( !empty( $data->servidor_id ) && !empty( $data->nome ) ) {

                $filter = [];

                switch ( $data->servidor_id ) {

                    case "nome":
                        $filter[] = new TFilter( $data->servidor_id, "LIKE", "%" . $data->nome . "%" );
                        break;

                    default:
                        $filter[] = new TFilter( $data->servidor_id, "LIKE", $data->nome . "%" );
                        break;

                }

                TSession::setValue('ServidorEscalaRecord', $filter);

                $this->form->setData( $data );

                $this->onReload( $data );

            } else {

                TSession::setValue('ServidorEscalaRecord', '');

                $this->form->setData( $data );

                new TMessage( "error", "Selecione um servidor acima e após clique em buscar!" );

            }

        } catch ( Exception $ex ) {

            TTransaction::rollback();

            $this->form->setData( $data );

            new TMessage( "error",  $ex->getMessage() .'.' );

        }

    }

    public function onReload( $param = NULL )
    {

        try {

            TTransaction::open('pg_ceres');

            $repository = new TRepository('ServidorEscalaRecord');

            $criteria = new TCriteria();

            $criteria->add(new TFilter('servidor_id', '=', $param->servidor_id));

            $objects = $repository->load( $criteria );

            $this->datagrid->clear();

            if ( !empty( $objects ) ) {
                foreach ( $objects as $object ) {
                    $datainicioauxiliar = $object->datainicio;
                    $object->datainicio = TDate::date2br($object->datainicio);
                    $object->datainicio2 = substr($datainicioauxiliar,11,5);
                    $object->datafim = substr($object->datafim,11,5);
                    $this->datagrid->addItem( $object );
                }
            }

            $criteria->resetProperties();

            TTransaction::close();

        } catch ( Exception $ex ) {

            TTransaction::rollback();

            new TMessage( "error",  $ex->getMessage()  );

        }

    }

    public function onEdit()
    {

    }

    public function show()
    {
        parent::show();

    }
}