<?php

include_once 'app/lib/funcdate.php';

use Adianti\Widget\Datagrid\TDatagridTables;

class MarcacaoServidorList extends TPage
{

    private $form;
    private $datagrid;
    private $pageNavigation;

    public function __construct()
    {
        parent::__construct();


        $this->form = new TQuickForm('batidas_servidor');
        $this->form->setFormTitle('<font color="red" size="3" face="Arial"><b>Batidas Servidor</b></font>');

        TTransaction::open('pg_ceres');

        $codigo = new THidden('id');

        $servidor_id = new TDBMultiSearch('matricula', 'pg_ceres', 'vw_usuario', 'matricula', 'nome');
        $servidor_id->style = "text-transform: uppercase";
        $servidor_id->setProperty('placeholder', 'Digitar Maiuscúlo Nome Servidor ou Matricula');
        $servidor_id->setMinLength(3);
        $servidor_id->setMaxSize(1);
        $servidor_id->setSize(450, 50);

        $nome_servidor = new TEntry('nome_servidor');
        $nome_servidor->setEditable(False);

        $div_jornada = new TElement('div');
        $div_jornada->add($this->onDivJornadaServidor($_SESSION['servidor_id']));


        $this->form->addQuickField(null, $codigo, 10);
        $this->form->addQuickField('Pesquisa Servidor', $servidor_id, null);
        $this->form->addQuickField('Servidor', $nome_servidor, 50);

        $this->datagrid = new TDatagridTables;

        $dgdatamarcacao = new TDataGridColumn('databatida', 'Data Marca&ccedil;&atilde;o', 'left', 100);
        $dghora = new TDataGridColumn('databatida', 'Hora Marca&ccedil;&atilde;o', 'left', 50);
        $dgtipo = new TDataGridColumn('funcao', 'Tipo', 'left', 100);

        $this->datagrid->addColumn($dgdatamarcacao);
        $this->datagrid->addColumn($dghora);
        $this->datagrid->addColumn($dgtipo);

        $dgdatamarcacao->setTransformer('formatar_data');
        $dghora->setTransformer('formatar_hora2');

        $this->datagrid->createModel();

        $actionSave = new TAction(array($this, 'onReload'));
        $actionSave->setParameter('fk', filter_input(INPUT_GET, 'fk'));
        $actionSave->setParameter('did', filter_input(INPUT_GET, 'did'));
        $this->form->addQuickAction('Pesquisar', $actionSave, 'ico_lupa.png')->class = 'btn btn-info';

        $panel = new TPanelForm(700, 500);
        $panel->put($this->form, 0, 0);
        $panel->put($div_jornada, 0, 0);
        $panel->put($this->datagrid, 150, 330);


        parent::add($panel);
    }

    function onDivJornadaServidor($servidor_id)
    {

        $conn = TTransaction::get();

        $sth = $conn->prepare('select j.horajornada,
                                    j.entrada1,j.entrada1inicio,j.entrada1fim,
                                    j.entrada2,j.entrada2inicio,j.entrada2fim,
                                    j.entrada3,j.entrada3inicio,j.entrada3fim,
                                    j.saida1,j.saida1inicio,j.saida1fim,
                                    j.saida2,j.saida2inicio,j.saida2fim,
                                    j.saida3,j.saida3inicio,j.saida3fim
                                  from servidor s 
                                inner join jornada j on j.id = s.jornada_id WHERE s.id = ?');

        $sth->execute(array($servidor_id));
        $result = $sth->fetchAll();

        $div_detalhe = "<div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>
                        <div class='container'>
                          <div class='row'>";

        foreach ($result as $row) {
            if (empty($row['entrada2'])) {
                $div_detalhe .= "<div class='boxed col-lg-12 col-md-12 col-sm-12 col-xs-12'>Horário <font color='red' size='3px' face='Arial'><b>" . formatar_hora($row['entrada1']) . "</b></font> às <font color='red' size='3px' face='Arial'><b>" . formatar_hora($row['saida1']) . "</b></font></div>";
                $div_detalhe .= "<div class='boxed col-lg-12 col-md-12 col-sm-12 col-xs-12'>Entrada <font color='red' size='3px' face='Arial'><b>" . formatar_hora($row['entrada1inicio']) . "</b></font> até <font color='red' size='3px' face='Arial'><b>" . formatar_hora($row['entrada1fim']) . "</b></font></div>";
                $div_detalhe .= "<div class='boxed col-lg-12 col-md-12 col-sm-12 col-xs-12'>Saida <font color='red' size='3px' face='Arial'><b>" . formatar_hora($row['saida1inicio']) . "</b></font> até <font color='red' size='3px' face='Arial'><b>" . formatar_hora($row['saida1fim']) . "</b></font></div>";
            } else if (empty($row['entrada3'])) {
                $div_detalhe .= "<div class='boxed col-lg-6 col-md-6 col-sm-6 col-xs-12'>Turno 01 <font color='red' size='3px' face='Arial'><b>" . formatar_hora($row['entrada1']) . "</b></font> às <font color='red' size='3px' face='Arial'><b>" . formatar_hora($row['saida1']) . "</b></font></div><div class='boxed col-lg-6 col-md-6 col-sm-6 col-xs-12'> Turno 02 <font color='red' size='3px' face='Arial'><b>" . formatar_hora($row['entrada2']) . "</b></font> às <font color='red' size='3px' face='Arial'><b>" . formatar_hora($row['saida2']) . "</b></font></div>";
                $div_detalhe .= "<div class='boxed col-lg-6 col-md-6 col-sm-6 col-xs-12'>Entrada 01: <font color='red' size='3px' face='Arial'><b>" . formatar_hora($row['entrada1inicio']) . "</b></font> até <font color='red' size='3px' face='Arial'><b>" . formatar_hora($row['entrada1fim']) . "</b></font></div><div class='boxed col-lg-6 col-md-6 col-sm-6 col-xs-12'> Entrada 02: <font color='red' size='3px' face='Arial'><b>" . formatar_hora($row['entrada2inicio']) . "</b></font> até <font color='red' size='3px' face='Arial'><b>" . formatar_hora($row['entrada2fim']) . "</b></font></div>";
                $div_detalhe .= "<div class='boxed col-lg-6 col-md-6 col-sm-6 col-xs-12'>Saida 01: <font color='red' size='3px' face='Arial'><b>" . formatar_hora($row['saida1inicio']) . "</b></font> até <font color='red' size='3px' face='Arial'><b>" . formatar_hora($row['saida1fim']) . "</b></font></div><div class='boxed col-lg-6 col-md-6 col-sm-6 col-xs-12'> Saida 02: <font color='red' size='3px' face='Arial'><b>" . formatar_hora($row['saida2inicio']) . "</b></font> até <font color='red' size='3px' face='Arial'><b>" . formatar_hora($row['saida2fim']) . "</b></font></div>";
            } else if (!empty($row['entrada3'])) {
                $div_detalhe .= "<div class='boxed col-lg-4 col-md-4 col-sm-12 col-xs-12'>Turno 01 <font color='red' size='3px' face='Arial'><b>" . formatar_hora($row['entrada1']) . "</b></font> às <font color='red' size='3px' face='Arial'><b>" . formatar_hora($row['saida1']) . "</b></font></div><div class='boxed col-lg-4 col-md-4 col-sm-12 col-xs-12'> Turno 02 <font color='red' size='3px' face='Arial'><b>" . formatar_hora($row['entrada2']) . "</b></font> às <font color='red' size='3px' face='Arial'><b>" . formatar_hora($row['saida2']) . "</b></font></div><div class='boxed col-lg-4 col-md-4 col-sm-12 col-xs-12'> Turno 03 <font color='red' size='3px' face='Arial'><b>" . formatar_hora($row['entrada3']) . "</b></font> às <font color='red' size='3px' face='Arial'><b>" . formatar_hora($row['saida3']) . "</b></font></div>";
                $div_detalhe .= "<div class='boxed col-lg-4 col-md-4 col-sm-12 col-xs-12'>Entrada 01: <font color='red' size='3px' face='Arial'><b>" . formatar_hora($row['entrada1inicio']) . "</b></font> até <font color='red' size='3px' face='Arial'><b>" . formatar_hora($row['entrada1fim']) . "</b></font></div><div class='boxed col-lg-4 col-md-4 col-sm-12 col-xs-12'> Entrada 02: <font color='red' size='3px' face='Arial'><b>" . formatar_hora($row['entrada2inicio']) . "</b></font> até <font color='red' size='3px' face='Arial'><b>" . formatar_hora($row['entrada2fim']) . "</b></font></div><div class='boxed col-lg-4 col-md-4 col-sm-12 col-xs-12'> Entrada 03: <font color='red' size='3px' face='Arial'><b>" . formatar_hora($row['entrada3inicio']) . "</b></font> até <font color='red' size='3px' face='Arial'><b>" . formatar_hora($row['entrada3fim']) . "</b></font></div>";
                $div_detalhe .= "<div class='boxed col-lg-4 col-md-4 col-sm-12 col-xs-12'>Saida 01: <font color='red' size='3px' face='Arial'><b>" . formatar_hora($row['saida1inicio']) . "</b></font> até <font color='red' size='3px' face='Arial'><b>" . formatar_hora($row['saida1fim']) . "</b></font></div><div class='boxed col-lg-4 col-md-4 col-sm-12 col-xs-12'> Saida 02: <font color='red' size='3px' face='Arial'><b>" . formatar_hora($row['saida2inicio']) . "</b></font> até <font color='red' size='3px' face='Arial'><b>" . formatar_hora($row['saida2fim']) . "</b></font></div><div class='boxed col-lg-4 col-md-4 col-sm-12 col-xs-12'> Saida 03: <font color='red' size='3px' face='Arial'><b>" . formatar_hora($row['saida3inicio']) . "</b></font> até <font color='red' size='3px' face='Arial'><b>" . formatar_hora($row['saida3fim']) . "</b></font></div>";
            }
            $div_detalhe .= "                            
                      </div>
                    </div>
                  </div>";
            return $div_detalhe;
        }

        TTransaction::close();
    }


    function onReload($param = null)
    {
        if ($param['matricula']) {
            TTransaction::open('pg_ceres');

            $repository = new TRepository('BatidaRecord');
            $criteria = new TCriteria;

            $temp = explode("::", $param['matricula']);
            $criteria->add(new TFilter('matricula', '=', $temp[0]));

            $obj = new StdClass;
            $obj->nome_servidor = $temp[1];

            TForm::sendData('batidas_servidor', $obj);

            $criteria->setProperty('order', 'databatida DESC');

            $cadastros = $repository->load($criteria);

            $this->datagrid->clear();

            if ($cadastros) {
                foreach ($cadastros as $cadastro) {
                    $this->datagrid->addItem($cadastro);
                }
            }
            $criteria->resetProperties();

            TTransaction::close();


        }
        $this->loaded = true;
    }

    function show()
    {
        $this->onReload();
        parent::show();
    }

}
