<?php

//ini_set('display_errors', 1);
//ini_set('display_startup_erros', 1);
//error_reporting(E_ALL);


use Adianti\Database\TTransaction;
use Adianti\Database\TRepository;
use Adianti\Database\TCriteria;
use Adianti\Database\TFilter;
use Lib\Funcoes\Util;


class RelatorioResumoBatidaServidorEmpresaPDF extends FPDF 
{

    function Header() 
    {
//        $setor = $_REQUEST['setor_id'];
//
//        TTransaction::open('pg_ceres');
//
//
//        if ($setor != NULL){
//
//            $setor_record = new SetorRecord($setor);
//            $nome_setor = $setor_record->nome;
//
//        }
//
//        TTransaction::close();
    	
        $this->Image("app/images/logo_relatorio.jpg", 8, 11, 26, 18);

        $this->SetFont('Arial', 'B', 12);
        $this->SetX("25");
        $this->Cell(0, 5, "GOVERNO DO ESTADO DO RIO GRANDE DO NORTE", 0, 1, 'C');

        $this->SetFont('Arial', 'B', 10);
        $this->SetX("35");
        $this->Cell(0, 5, utf8_decode($_SESSION['empresa_nome']), 0, 1, 'J');

        $this->SetX("25");
        $this->Cell(0, 5, "RESUMO DE BATIDAS DOS SERVIDORES", 0, 1, 'C');

//        if ($setor != null){
//            $this->SetX("25");
//            $this->Cell(0, 5, utf8_decode("SETOR: ". $nome_setor), 0, 1, 'C');
//        }

        $this->Ln(10);
        
        $this->ColumnHeader();
        
    }

    function ColumnHeader()
    {

        $this->SetFont('Arial', 'B', 12);
        $this->SetFillColor(235, 235, 235);

        $this->SetX("10");
        $this->Cell(0, 5, utf8_decode("Nome"), 1, 0, 'L', 1);

        $this->SetX("100");
        $this->Cell(0, 5, utf8_decode("Matrícula"), 1, 0, 'L', 1);

        $this->SetX("125");
        $this->Cell(0, 5, utf8_decode("Trabalhado"), 1, 0, 'L', 1);

        $this->SetX("150");
        $this->Cell(0, 5, utf8_decode("Previsto"), 1, 0, 'L', 1);

        $this->SetX("175");
        $this->Cell(0, 5, utf8_decode("Saldo"), 1, 1, 'L', 1);

    }

    function ColumnDetail() 
    {

        TTransaction::open('pg_ceres');

        $repository = new TRepository('vw_ponto_resumo_mesRecord');
        
        $criteria = new TCriteria;
        
        $criteria->add(new TFilter('mes', '=', $_REQUEST['mes']));
        $criteria->add(new TFilter('ano', '=', $_REQUEST['ano']));
        $criteria->add(new TFilter('empresa_id', '=', $_SESSION['empresa_id']));
        $criteria->setProperty('order', 'nome');

        $rows = $repository->load($criteria);

        if( $rows ) 
        {

        	$ultimaMatricula = 0;
        	
            foreach( $rows as $row ) 
            {
            	
            	$servidor = new ServidorRecord( $row->servidor_id );
            	
            	if( $ultimaMatricula != $servidor->matricula )
            	{

                    $this->SetFont('Arial', '', 10);
                    $this->SetFillColor(255, 255, 255);


                    $this->SetX("10");
                    $this->Cell(0, 5, utf8_decode($servidor->nome), 1, 0, 'L', 1);

                    $this->SetX("100");
                    $this->Cell(0, 5, utf8_decode($servidor->matricula), 1, 0, 'L', 1);

                    $this->SetX("125");
                    $this->Cell(0, 5, utf8_decode($row->trabtotal), 1, 0, 'L', 1);

                    $this->SetX("150");
                    $this->Cell(0, 5, utf8_decode($row->prevtotal), 1, 0, 'L', 1);

                    $this->SetX("175");
                    $this->Cell(0, 5, utf8_decode($row->saldo), 1, 1, 'L', 1);
	            	 
	            	$ultimaMatricula = $servidor->matricula;
	            	
            	}
            	
            }

        }
        
        TTransaction::close();

    }

    function Footer() 
    {
    	
        $this->SetY(-15);
        $this->SetFont('Arial', 'I', 8);
        $data = date("d/m/Y H:i:s");
        $conteudo = "impresso em " . $data;
        $texto = $_SESSION['empresa_nome'];
        $this->Cell(0, 0, '', 1, 1, 'L');

        $this->Cell(0, 5, $texto, 0, 0, 'L');
        $this->Cell(0, 5, 'Pag. ' . $this->PageNo() . ' de ' . '{nb}' . ' - ' . $conteudo, 0, 0, 'R');
        $this->Ln();
        
    }

}

$pdf = new RelatorioResumoBatidaServidorEmpresaPDF("P", "mm", "A4");

$pdf->SetTitle("Relatorio de Resumo Batidas Servidores");

$pdf->SetSubject("Relatorio de Resumo Batidas Servidores");

$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Times', '', 12);
$pdf->ColumnDetail();
$file = "app/reports/RelatorioResumoBatidaServidorEmpresaPDF" . $_SESSION['servidor_id'] . ".pdf";

$pdf->Output($file);
$pdf->openFile($file);

?>