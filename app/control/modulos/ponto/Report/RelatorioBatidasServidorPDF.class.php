<?php

//ini_set('display_errors', 1);
//ini_set('display_startup_erros', 1);
//error_reporting(E_ALL);

use Adianti\Database\TTransaction;
use Adianti\Database\TRepository;
use Adianti\Database\TCriteria;
use Adianti\Database\TFilter;
use Lib\Funcoes\Util;

class RelatorioBatidasServidorPDF extends FPDF {

    function Header() {
        $this->Image("app/images/logo_relatorio.jpg", 8, 11, 26, 18);

        $this->SetFont('Arial', 'B', 12);
        $this->SetX("25");
        $this->Cell(0, 5, utf8_decode("GOVERNO DO ESTADO DO RIO GRANDE DO NORTE"), 0, 1, 'C');

        $this->SetFont('Arial', 'B', 10);
        $this->SetX("35");
        $this->Cell(0, 5, utf8_decode($_SESSION['empresa_nome']), 0, 1, 'J');

        $this->SetX("25");
        $this->Cell(0, 5, utf8_decode("SISTEMA DE PONTO"), 0, 1, 'C');


        $this->ColumnHeader();
    }

    function ColumnHeader() {
        
    }

    function ColumnDetail() {

        $this->SetX("20");

        TTransaction::open('pg_ceres');

        $repository = new TRepository('vw_marcacao_relogioRecord');

        $criteria = new TCriteria;

//        var_dump($_REQUEST);
//        exit;
        /*
          $criteria->add(new TFilter('servidor_id', '=', $_REQUEST['servidor_id']));
          $criteria->add(new TFilter('mes', '=', $_REQUEST['mes']));
          $criteria->add(new TFilter('ano', '=', $_REQUEST['ano']));
         */
        $criteria->add(new TFilter('matricula', '=', $_REQUEST['matricula']));
        $criteria->add(new TFilter('databatida', '=', TDate::date2br($_REQUEST['databatida'])));

        $rows = $repository->load($criteria);

        $previsao = 0;
        $previsaop = 0;
        $executado = 0;
        $executadop = 0;
        $parcial = 0;
        $horatotal = 0;
        $diahoje = date("d/m/Y");
        $matricula = '';
        $nome_servidor = '';
        $flag = true;
        if ($rows) {

            $i = 0;
            foreach ($rows as $row) {

                if ($matricula != $row->matricula) {


                    $matricula = $row->matricula;

                    $this->Ln();
                    $this->Ln();

                    $this->SetX("10");
                    $this->SetFont('arial', 'B', 10);
                    $this->Cell(0, 5, utf8_decode("Matricula: " . $row->matricula), 0, 0, 'L');

                    $this->SetX("43");
                    $this->SetFont('arial', 'B', 10);
                    $this->Cell(0, 5, utf8_decode("Nome: " . $row->nome_servidor . "    LOTAÇÃO: " . $row->lotacao), 0, 1, 'L');

                    $this->SetX("35");
                    $this->Cell(0, 5, utf8_decode("Turno 1"), 0, 0, 'L');

                    $this->SetX("82");
                    $this->Cell(0, 5, utf8_decode("Turno 2"), 0, 0, 'L');

                    $this->SetX("126");
                    $this->Cell(0, 5, utf8_decode("Turno 3"), 0, 1, 'L');

                    $this->SetFont('Arial', 'B', 10);

                    $this->SetX("10");
                    $this->Cell(0, 5, utf8_decode("Data"), 0, 0, 'L');

                    $this->SetX("33");
                    $this->Cell(0, 5, utf8_decode("Inicio/Fim"), 0, 0, 'L');

                    $this->SetX("55");
                    $this->Cell(0, 5, utf8_decode("Total"), 0, 0, 'L');

                    $this->SetX("80");
                    $this->Cell(0, 5, utf8_decode("Inicio/Fim"), 0, 'L');

                    $this->SetX("100");
                    $this->Cell(0, 5, utf8_decode("Total"), 0, 0, 'L');

                    $this->SetX("125");
                    $this->Cell(0, 5, utf8_decode("Inicio/Fim"), 0, 'L');

                    $this->SetX("145");
                    $this->Cell(0, 5, utf8_decode(" - Total"), 0, 0, 'L');

                    $this->SetX("165");
                    $this->Cell(0, 5, utf8_decode("Previsto dia"), 0, 0, 'L');

                    $this->SetX("186");
                    $this->Cell(0, 5, utf8_decode("Trabalhado"), 0, 1, 'L');
                    $this->Cell(0, 0, '', 1, 1, 'L');
                }

                $this->SetX("10");
                $this->Cell(0, 5, utf8_decode(TDate::date2br($row->databatida)), 0, 0, 'L');

                $this->SetX("33");
                if ($row->batida01) {
                    $this->Cell(0, 5, utf8_decode(Util::formatar_hora2($row->batida01) . "/"), 0, 0, 'L');
                } else {
                    $this->Cell(0, 5, utf8_decode($row->batida01), 0, 0, 'L');
                }

                $this->SetX("43");
                if ($row->batida02) {
                    $this->Cell(0, 5, utf8_decode(Util::formatar_hora2($row->batida02) . "  - "), 0, 0, 'L');
                } else {
                    $this->Cell(0, 5, utf8_decode($row->batida02), 0, 0, 'L');
                }

                $this->SetX("57");
                if ($row->hora01) {
                    $this->Cell(0, 5, utf8_decode(Util::formatar_hora($row->hora01)), 0, 0, 'L');
                } else {
                    $this->Cell(0, 5, utf8_decode($row->hora01), 0, 0, 'L');
                }

                $this->SetX("80");
                if ($row->batida03) {
                    $this->Cell(0, 5, utf8_decode(Util::formatar_hora2($row->batida03) . "/"), 0, 0, 'L');
                } else {
                    $this->Cell(0, 5, utf8_decode($row->batida03), 0, 0, 'L');
                }

                $this->SetX("90");
                if ($row->batida04) {
                    $this->Cell(0, 5, utf8_decode(Util::formatar_hora2($row->batida04) . "  - "), 0, 0, 'L');
                } else {
                    $this->Cell(0, 5, utf8_decode($row->batida04), 0, 0, 'L');
                }

                $this->SetX("105");
                if ($row->hora02) {
                    $this->Cell(0, 5, utf8_decode(Util::formatar_hora($row->hora02)), 0, 0, 'L');
                } else {
                    $this->Cell(0, 5, utf8_decode($row->hora02), 0, 0, 'L');
                }

                $this->SetX("125");
                if ($row->batida05) {
                    $this->Cell(0, 5, utf8_decode(Util::formatar_hora2($row->batida05) . "/"), 0, 0, 'L');
                } else {
                    $this->Cell(0, 5, utf8_decode($row->batida05), 0, 0, 'L');
                }

                $this->SetX("135");
                if ($row->batida06) {
                    $this->Cell(0, 5, utf8_decode(Util::formatar_hora2($row->batida06) . "  - "), 0, 0, 'L');
                } else {
                    $this->Cell(0, 5, utf8_decode($row->batida06), 0, 0, 'L');
                }

                $this->SetX("150");
                if ($row->hora03) {
                    $this->Cell(0, 5, utf8_decode(Util::formatar_hora($row->hora03)), 0, 0, 'L');
                } else {
                    $this->Cell(0, 5, utf8_decode($row->hora03), 0, 0, 'L');
                }

                $this->SetX("170");
                $this->Cell(0, 5, utf8_decode(Util::formatar_hora($row->hora_jornada)), 0, 0, 'L');


                if ($diahoje > $row->databatida) {
                    $previsaop = ($previsaop + $row->minutojornada);
                    $executadop = ($executadop + $row->minutototal);
                }

                $previsao = ($previsao + $row->minutojornada);
                $executado = ($executado + $row->minutototal);

                $horatotal = ($horatotal + $row->horatotal);

                $this->SetX("187");
                $this->Cell(0, 5, utf8_decode(Util::formatar_hora($row->horatotal)), 0, 1, 'L');
                $this->Cell(0, 0, '', 1, 1, 'L');

                $i = $i + 1;

                $this->Ln();
            }

//            $this->SetX("7");
//            $this->SetFont('Arial', 'B', 13);
//            $this->SetX("7");
//            $tmphora = ((int) ($previsao / 60)) . 'h' . ($previsao % 60);
//            $tmphora1 = ((int) ($executado / 60)) . 'h' . ($executado % 60);
//            $tmphorap = ((int) ($previsaop / 60)) . 'h' . ($previsaop % 60);
//            $tmphora1p = ((int) ($executadop / 60)) . 'h' . ($executadop % 60);
//            if ($executado > $previsao) {
//                $tmphora2 = '    Saldo:' . ((int) (($executado - $previsao) / 60)) . 'h' . (($executado - $previsao) % 60);
//            } else {
//                $tmphora2 = '    Débito:' . ((int) (($previsao - $executado) / 60)) . 'h' . (($previsao - $executado) % 60);
//            }
//            if ($executadop > $previsaop) {
//                $tmphora2p = '    Saldo:' . ((int) (($executadop - $previsaop) / 60)) . 'h' . (($executadop - $previsaop) % 60);
//            } else {
//                $tmphora2p = '    Débito:' . ((int) (($previsaop - $executadop) / 60)) . 'h' . (($previsaop - $executadop) % 60);
//            }
//            $this->Cell(0, 10, (utf8_decode("Dias úteis: ") . utf8_decode(substr($i, 0, 4)) . (utf8_decode("    Previsto: ") . utf8_decode($tmphora)) . (utf8_decode("    Trabalhado: ") . utf8_decode($tmphora1)) . utf8_decode($tmphora2)), 0, 1, 'L');
//            $this->Cell(0, 10, ((utf8_decode("Parcial:   Previsto: ") . utf8_decode($diahoje) . " / " . utf8_decode($tmphorap)) . (utf8_decode("    Trabalhado: ") . utf8_decode($tmphora1p)) . utf8_decode($tmphora2p)), 0, 1, 'L');
        }

        TTransaction::close();

        $this->Cell(0, 0, '', 0, 1, 'L');
        $this->Cell(0, 0, '', 0, 1, 'L');
        $this->Cell(0, 0, '', 0, 1, 'L');
    }

    function Footer() {
        $this->SetY(-15);
        $this->SetFont('Arial', 'I', 8);
        $data = date("d/m/Y H:i:s");
        $conteudo = "impresso em " . $data;
        $texto = $_SESSION['empresa_nome'];
        $this->Cell(0, 0, '', 1, 1, 'L');

        $this->Cell(0, 5, $texto, 0, 0, 'L');
        $this->Cell(0, 5, 'Pag. ' . $this->PageNo() . ' de ' . '{nb}' . ' - ' . $conteudo, 0, 0, 'R');
        $this->Ln();
    }

}

$pdf = new RelatorioBatidasServidorPDF("P", "mm", "A4");

$pdf->SetTitle("Relatorio de Indicadores");

$pdf->SetSubject("Relatorio de Indicadores");

$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Times', '', 12);
$pdf->ColumnDetail();
$file = "app/reports/RelatorioBatidasServidorPDF" . $_SESSION['servidor_id'] . ".pdf";

$pdf->Output($file);
$pdf->openFile($file);
