<?php

//ini_set('display_errors', 1);
//ini_set('display_startup_erros', 1);
//error_reporting(E_ALL);

class MarcacaoFormSaida extends TPage{

    private $form;     
    
    public function __construct(){
        parent::__construct();

        $this->form = new TQuickForm;
        $this->form->class = 'form_ponto_marcacaosaida';
        $this->form->setFormTitle('<font color="red" size="3" face="Arial"><b>Marcação de Saida
    </b></font>');

        $id     = new THidden('id');
        $servidor_id = new THidden('matricula');
        $servidor_id->setValue($_SESSION["usuario"]);
        $tipo = new THidden('funcao');
        $tipo->setValue('SAIDA');

        $datamarcacao = new TDate('databatida');
        $datamarcacao->setEditable(false);
        $datamarcacao->setValue(date('d/m/y H:i\h'));

        TTransaction::open('pg_ceres');

        $cadastro = new ServidorRecord($_SESSION['servidor_id']);

        if($cadastro){
            $nome_servidor = new TLabel($cadastro->nome);
        }
        TTransaction::close();

        $datamarcacao->setSize(50);

        $campo = new TLabel('<div style="position:floatval; width: 200px;"> <b>* Campos obrigat&oacute;rios</b></div>');
        $campo->setFontFace('Arial');
        $campo->setFontColor('red');
        $campo->setFontSize(10);

        $this->form->addQuickField(null, $id, 10);
        $this->form->addQuickField(null, $tipo, 10);
        $this->form->addQuickField(null, $servidor_id, 10);
        $this->form->addQuickField('Servidor <font color=red><b>*</b></font>', $nome_servidor, 300);
        $this->form->addQuickField('Data Marcação <font color=red><b>*</b></font>', $datamarcacao, 20);
        
        $this->form->addQuickField(null, $campo, 50);
        
        $this->form->addQuickAction('Salvar', new TAction(array($this, 'onSave')), 'ico_save.png')->class = 'btn btn-info';
        $this->form->addQuickAction('Voltar', new TAction(array('MarcacaoList', 'onReload')), 'ico_datagrid.gif');

        parent::add($this->form);
    }
    function onSave() {

        TTransaction::open('pg_ceres');

        $cadastro = $this->form->getData('BatidaRecord');

        $dados = $cadastro->toArray();

        $msg = '';
        $icone = 'info';
        
        if (empty($dados['matricula'])) {
            $msg .= 'O Nome deve ser informado.</br>';
        }
        if (empty($dados['databatida'])) {
            $msg .= 'A Data deve ser informado.</br>';
        }

        try {

            $this->form->validate();
            if ($msg == '') {

                $cadastro->store();
                $msg = 'Dados armazenados com sucesso';
                TTransaction::close();
            } else {
                $icone = 'error';
            }

            if ($icone == 'error') {

                new TMessage($icone, $msg);
            } else {
                new TMessage("info", "Registro salvo com sucesso!");
                TApplication::gotoPage('MarcacaoList','onReload');
            }
        } catch (Exception $e) {
            new TMessage('error', $e->getMessage());
            TTransaction::rollback();
        }
    }

    function onEdit($param) {

        try {
            if (isset($param['key'])) {

                $key = $param['key'];

                TTransaction::open('pg_ceres');   
                $object = new BatidaRecord($key); 

                $this->form->setData($object);  

                TTransaction::close();          
            } 
        } catch (Exception $e) { 
            new TMessage('error', '<b>Error</b> ' . $e->getMessage());

            TTransaction::rollback();
        }

    }


}
?>