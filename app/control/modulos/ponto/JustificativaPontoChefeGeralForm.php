<?php

include_once 'app/lib/funcdate.php';

use Adianti\Widget\Datagrid\TDatagridTables;

class JustificativaPontoChefeGeralForm extends TPage
{

    private $form;
    private $datagrid;

    public function __construct()
    {
        parent::__construct();

        $this->form = new BootstrapFormWrapper(new TQuickForm, 'form-vertical');

        //  $this->form = new TQuickForm;
        // $this->form->class = 'form_ponto_escala';
        $this->form->setFormTitle('<font color="red" size="3" face="Arial"><b>Processamento de Justificativa</b></font>');

        $codigo = new THidden('id');

        $tipojustificativa_id = new TCombo('tipojustificativaponto_id');
        $tipojustificativa_id->setEditable(false);
        $justificativa = new TText('justificativa');
        $justificativa->setEditable(false);
        $databatida = new TDate('databatida');
        $dataautorizacao = new TDate('dataautorizacao');
        $dataautorizacao->setEditable(false);
        $dataautorizacao->setValue(date('d/m/Y'));
        $situacaojustificativa = new TCombo('situacaojustificativa');
        $chefe_id = new THidden('chefe_id');

        TTransaction::open('pg_ceres');
        $cadastro = new ServidorRecord($_GET['fk']);
        if ($cadastro) {
            $nome = new TLabel($cadastro->nome);
            $matricula = new TLabel($cadastro->matricula);
        }
        TTransaction::close();

        $id = new THidden('servidor_id');
        $id->setValue($_GET['fk']);

        $cehfe_id_compareceu = new THidden('cehfe_id_compareceu');

        $campo = new TLabel('<div style="position: relative; width: 200px;"><b>* Campo obrigatorio</b></div>');
        $campo->setFontFace('Arial');
        $campo->setFontColor('red');
        $campo->setFontSize(10);

        $items = array();
        TTransaction::open('pg_ceres');

        $repository = new TRepository('TipoJustificativaPontoRecord');
        $criteria = new TCriteria;
        $criteria->setProperty('order', 'nome');

        $collection = $repository->load($criteria);

        foreach ($collection as $object) {
            $items[$object->id] = $object->nome;
        }

        $tipojustificativa_id->addItems($items);

        TTransaction::close();

        $key = filter_input(INPUT_GET, 'key');

        TTransaction::open('pg_ceres');
        $objMR = new Marcacao_RelogioRecord($key);

        if ($objMR) {
            $objMR->databatida = TDate::date2br($objMR->databatida);
            $databatida = new TLabel($objMR->databatida);
            $servidor_id = $objMR->servidor_id;
        }
        TTransaction::close();

        $div_jornada = new TElement('div');
        $div_jornada->add($this->onDivJornadaServidor($servidor_id));

        $items = array();
        $items['DEFERIDO'] = 'DEFERIDO';
        $items['ENVIAR PARA RH'] = 'ENVIAR PARA RH';
        $items['INDEFERIDO'] = 'INDEFERIDO';
        $situacaojustificativa->addItems($items);

        $this->form->setFieldsByRow(1); // seta uma linha por vez no formulario

        $this->form->addQuickField('Nome Servidor', $nome, 300);
        $this->form->addQuickField('Matr&iacute;cula', $matricula, 40);

        $this->form->addQuickField(null, $codigo, 10);
        $this->form->addQuickField(null, $chefe_id, 10);
        $this->form->addQuickField('Data da Batida', $databatida, 20);
        $this->form->addQuickField('Tipo da Justificativa', $tipojustificativa_id, 30);
        $this->form->addQuickField('Justificativa', $justificativa, 50);
        $this->form->addQuickField('Data Autorização', $dataautorizacao, 30);
        $this->form->addQuickField('Situação da Justificativa <font color=red><b>*</b></font>', $situacaojustificativa, 20);

        $this->form->addQuickField(null, $campo, 50);

        $actionSave = new TAction(array($this, 'onSave'));

        $actionSave->setParameter("key", filter_input(INPUT_GET, "key"));
        $actionSave->setParameter("fk", filter_input(INPUT_GET, "fk"));

        $btnSave = $this->form->addQuickAction('Salvar', $actionSave, 'ico_save.png');
        $btnSave->{'style'} = 'float: left; width: 50px !important';
        $btnVoltar = $this->form->addQuickAction('Voltar', new TAction(array($this, 'onCancel')), 'ico_back.gif');
        $btnVoltar->{'style'} = 'float: left; width: 50px !important';

        $this->datagrid = new TDatagridTables;

        $dgbatida01 = new TDataGridColumn('batida01', 'Batida 01', 'left', 50);
        $dgbatida02 = new TDataGridColumn('batida02', 'Batida 02', 'left', 50);
        $dgbatida03 = new TDataGridColumn('batida03', 'Batida 03', 'left', 50);
        $dgbatida04 = new TDataGridColumn('batida04', 'Batida 04', 'left', 50);
        $dgbatida05 = new TDataGridColumn('batida05', 'Batida 05', 'left', 50);
        $dgbatida06 = new TDataGridColumn('batida06', 'Batida 06', 'left', 50);

        $this->datagrid->addColumn($dgbatida01);
        $this->datagrid->addColumn($dgbatida02);
        $this->datagrid->addColumn($dgbatida03);
        $this->datagrid->addColumn($dgbatida04);
        $this->datagrid->addColumn($dgbatida05);
        $this->datagrid->addColumn($dgbatida06);

        $this->datagrid->createModel();

        $panel = new \Adianti\Widget\Container\TPanelGroup('<font color="red" size="3px" face="Arial"><b>Abono de Justificativa</b></font>');
        $panel->add($this->form);
        $panel->add($div_jornada);

        // wrap the page content using vertical box
        $vbox = new TVBox;
        $vbox->add($panel);
        $vbox->add($this->datagrid);

        parent::add($vbox);
    }

    function onSave()
    {
        try {
            TTransaction::open('pg_ceres');

            $msg = '';
            $icone = 'info';

            $object = $this->form->getData('Marcacao_RelogioRecord');
            $object->chefe_id = $_SESSION['servidor_id'];

            if ($this->form->getFieldData('situacaojustificativa') == '') {
                $msg .= 'O campo Situacao Justificativa deve ser preenchido.';
            }

            if ($msg == '') {
                $object->store();

                $msg = 'Dados armazenados com sucesso';

            } else {

                $icone = 'error';
            }
            if ($icone == 'error') {
                $this->form->setData($object);
                new TMessage($icone, $msg);
            } else {
                new TMessage("info", "Registro salvo com sucesso!");
                TApplication::gotoPage('JustificativaPontoChefeGeral', 'onReload');

            }
        } catch (Exception $e) {
            new TMessage('error', $e->getMessage());
            TTransaction::rollback();
        }

        TTransaction::close();
    }

    function onCancel()
    {
        TApplication::gotoPage('JustificativaPontoChefeGeral', 'onReload');
    }

    function onEdit($param)
    {
        $key = $param['key'];
        TTransaction::open('pg_ceres');
        $object = new Marcacao_RelogioRecord($key);

        $this->form->setData($object);

        $this->onReload($key);

        TTransaction::close();
    }

    function onReload($param = NULL)
    {
        try {
            TTransaction::open('pg_ceres');

            $repository = new TRepository('Marcacao_RelogioRecord');
            $criteria = new TCriteria;
            $criteria->add(new TFilter('id', '=', $param));

            $objects = $repository->load($criteria);

            $this->datagrid->clear();

            if ($objects) {
                foreach ($objects as $objMR) {
                    $objMR->batida01 = substr($objMR->batida01, 10, 9);
                    $objMR->batida02 = substr($objMR->batida02, 10, 9);
                    $objMR->batida03 = substr($objMR->batida03, 10, 9);
                    $objMR->batida04 = substr($objMR->batida04, 10, 9);
                    $objMR->batida05 = substr($objMR->batida05, 10, 9);
                    $objMR->batida06 = substr($objMR->batida06, 10, 9);
                    $this->datagrid->addItem($objMR);
                }
            }
            TTransaction::close();
            $this->loaded = true;
        } catch (Exception $e) {

        }
    }

    static function onDivJornadaServidor($servidor_id)
    {
        TTransaction::open('pg_ceres');
        $conn = TTransaction::get(); // obtém a conexão

        $sth = $conn->prepare('select j.horajornada,
                                    j.entrada1,j.entrada1inicio,j.entrada1fim,
                                    j.entrada2,j.entrada2inicio,j.entrada2fim,
                                    j.entrada3,j.entrada3inicio,j.entrada3fim,
                                    j.saida1,j.saida1inicio,j.saida1fim,
                                    j.saida2,j.saida2inicio,j.saida2fim,
                                    j.saida3,j.saida3inicio,j.saida3fim
                                  from servidor s 
                                inner join jornada j on j.id = s.jornada_id WHERE s.id = ?');

        $sth->execute([$servidor_id]);
        $result = $sth->fetchAll();

        TTransaction::close();
        $div_detalhe = "<div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>
                        <div class='container'>
                          <div class='row'>
                          <h4 style=\"padding: 20px; color:  blue; font-size: 24px; font-weight: bold;\">Jornada</h4>";
        // exibe os resultados
        foreach ($result as $row) {
            if (empty($row['entrada2'])) {
                $div_detalhe .= "<div class='boxed col-lg-12 col-md-12 col-sm-12 col-xs-12'>Horário <font color='red' size='3px' face='Arial'><b>" . formatar_hora($row['entrada1']) . "</b></font> às <font color='red' size='3px' face='Arial'><b>" . formatar_hora($row['saida1']) . "</b></font></div>";
                $div_detalhe .= "<div class='boxed col-lg-12 col-md-12 col-sm-12 col-xs-12'>Entrada <font color='red' size='3px' face='Arial'><b>" . formatar_hora($row['entrada1inicio']) . "</b></font> até <font color='red' size='3px' face='Arial'><b>" . formatar_hora($row['entrada1fim']) . "</b></font></div>";
                $div_detalhe .= "<div class='boxed col-lg-12 col-md-12 col-sm-12 col-xs-12'>Saida <font color='red' size='3px' face='Arial'><b>" . formatar_hora($row['saida1inicio']) . "</b></font> até <font color='red' size='3px' face='Arial'><b>" . formatar_hora($row['saida1fim']) . "</b></font></div>";
            } else if (empty($row['entrada3'])) {
                $div_detalhe .= "<div class='boxed col-lg-6 col-md-6 col-sm-6 col-xs-12'>Turno 01 <font color='red' size='3px' face='Arial'><b>" . formatar_hora($row['entrada1']) . "</b></font> às <font color='red' size='3px' face='Arial'><b>" . formatar_hora($row['saida1']) . "</b></font></div><div class='boxed col-lg-6 col-md-6 col-sm-6 col-xs-12'> Turno 02 <font color='red' size='3px' face='Arial'><b>" . formatar_hora($row['entrada2']) . "</b></font> às <font color='red' size='3px' face='Arial'><b>" . formatar_hora($row['saida2']) . "</b></font></div>";
                $div_detalhe .= "<div class='boxed col-lg-6 col-md-6 col-sm-6 col-xs-12'>Entrada 01: <font color='red' size='3px' face='Arial'><b>" . formatar_hora($row['entrada1inicio']) . "</b></font> até <font color='red' size='3px' face='Arial'><b>" . formatar_hora($row['entrada1fim']) . "</b></font></div><div class='boxed col-lg-6 col-md-6 col-sm-6 col-xs-12'> Entrada 02: <font color='red' size='3px' face='Arial'><b>" . formatar_hora($row['entrada2inicio']) . "</b></font> até <font color='red' size='3px' face='Arial'><b>" . formatar_hora($row['entrada2fim']) . "</b></font></div>";
                $div_detalhe .= "<div class='boxed col-lg-6 col-md-6 col-sm-6 col-xs-12'>Saida 01: <font color='red' size='3px' face='Arial'><b>" . formatar_hora($row['saida1inicio']) . "</b></font> até <font color='red' size='3px' face='Arial'><b>" . formatar_hora($row['saida1fim']) . "</b></font></div><div class='boxed col-lg-6 col-md-6 col-sm-6 col-xs-12'> Saida 02: <font color='red' size='3px' face='Arial'><b>" . formatar_hora($row['saida2inicio']) . "</b></font> até <font color='red' size='3px' face='Arial'><b>" . formatar_hora($row['saida2fim']) . "</b></font></div>";
            } else if (!empty($row['entrada3'])) {
                $div_detalhe .= "<div class='boxed col-lg-4 col-md-4 col-sm-12 col-xs-12'>Turno 01 <font color='red' size='3px' face='Arial'><b>" . formatar_hora($row['entrada1']) . "</b></font> às <font color='red' size='3px' face='Arial'><b>" . formatar_hora($row['saida1']) . "</b></font></div><div class='boxed col-lg-4 col-md-4 col-sm-12 col-xs-12'> Turno 02 <font color='red' size='3px' face='Arial'><b>" . formatar_hora($row['entrada2']) . "</b></font> às <font color='red' size='3px' face='Arial'><b>" . formatar_hora($row['saida2']) . "</b></font></div><div class='boxed col-lg-4 col-md-4 col-sm-12 col-xs-12'> Turno 03 <font color='red' size='3px' face='Arial'><b>" . formatar_hora($row['entrada3']) . "</b></font> às <font color='red' size='3px' face='Arial'><b>" . formatar_hora($row['saida3']) . "</b></font></div>";
                $div_detalhe .= "<div class='boxed col-lg-4 col-md-4 col-sm-12 col-xs-12'>Entrada 01: <font color='red' size='3px' face='Arial'><b>" . formatar_hora($row['entrada1inicio']) . "</b></font> até <font color='red' size='3px' face='Arial'><b>" . formatar_hora($row['entrada1fim']) . "</b></font></div><div class='boxed col-lg-4 col-md-4 col-sm-12 col-xs-12'> Entrada 02: <font color='red' size='3px' face='Arial'><b>" . formatar_hora($row['entrada2inicio']) . "</b></font> até <font color='red' size='3px' face='Arial'><b>" . formatar_hora($row['entrada2fim']) . "</b></font></div><div class='boxed col-lg-4 col-md-4 col-sm-12 col-xs-12'> Entrada 03: <font color='red' size='3px' face='Arial'><b>" . formatar_hora($row['entrada3inicio']) . "</b></font> até <font color='red' size='3px' face='Arial'><b>" . formatar_hora($row['entrada3fim']) . "</b></font></div>";
                $div_detalhe .= "<div class='boxed col-lg-4 col-md-4 col-sm-12 col-xs-12'>Saida 01: <font color='red' size='3px' face='Arial'><b>" . formatar_hora($row['saida1inicio']) . "</b></font> até <font color='red' size='3px' face='Arial'><b>" . formatar_hora($row['saida1fim']) . "</b></font></div><div class='boxed col-lg-4 col-md-4 col-sm-12 col-xs-12'> Saida 02: <font color='red' size='3px' face='Arial'><b>" . formatar_hora($row['saida2inicio']) . "</b></font> até <font color='red' size='3px' face='Arial'><b>" . formatar_hora($row['saida2fim']) . "</b></font></div><div class='boxed col-lg-4 col-md-4 col-sm-12 col-xs-12'> Saida 03: <font color='red' size='3px' face='Arial'><b>" . formatar_hora($row['saida3inicio']) . "</b></font> até <font color='red' size='3px' face='Arial'><b>" . formatar_hora($row['saida3fim']) . "</b></font></div>";
            }
            $div_detalhe .= "                            
                      </div>
                    </div>
                  </div>";
            return $div_detalhe;
        }
    }

    function show()
    {
        $this->onReload();
        parent::show();
    }

}