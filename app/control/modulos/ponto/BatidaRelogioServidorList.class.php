<?php

include_once 'app/lib/funcdate.php';

use Adianti\Database\TFilter1;
use Adianti\Widget\Datagrid\TDatagridTables;

class BatidaRelogioServidorList extends TPage {

    private $form;
    private $datagrid;

    public function __construct() {
        parent::__construct();

        $this->form = new TQuickForm;
        $this->form->class = 'form_busca_Batida_Relogio';
        $this->form->setFormTitle('<font color="red" size="3px" face="Arial"><b>Cadastro de Batidas Rel&oacute;gio</b></font>');

        $codigo = new THidden('id');
        $servidor_id = new TCombo('servidor_id');
        $mes = new TCombo('mes');
        $ano = new TCombo('ano');

        $dataalteracao = new THidden('dataalteracao');
        $usuarioalteracao = new THidden('usuarioalteracao');

        $items = array();
        TTransaction::open('pg_ceres');
        $repository = new TRepository('ServidorRecord');

        $criteria = new TCriteria;
        $criteria->setProperty('order', 'nome');
        $cadastros = $repository->load($criteria);

        foreach ($cadastros as $object) {
            $items[$object->id] = $object->nome;
        }

        $servidor_id->addItems($items);
        TTransaction::close();

        $items = array();
        $items['01'] = 'JANEIRO';
        $items['02'] = 'FEVEREIRO';
        $items['03'] = 'MARÇO';
        $items['04'] = 'ABRIL';
        $items['05'] = 'MAIO';
        $items['06'] = 'JUNHO';
        $items['07'] = 'JULHO';
        $items['08'] = 'AGOSTO';
        $items['09'] = 'SETEMBRO';
        $items['10'] = 'OUTUBRO';
        $items['11'] = 'NOVEMBRO';
        $items['12'] = 'DEZEMBRO';

        $mes->addItems($items);
        $mes->setValue(date('m'));

        $ano->addItems(retornaAnosemZero());
        $ano->setValue(date('Y'));


        // cria um rotulo para o aviso
        $aviso = new TLabel('<div style="position:floatval; width: 200px;"> <b>* Campo obrigat&oacute;rio</b></div>');
        $aviso->setFontFace('Arial');
        $aviso->setFontColor('red');
        $aviso->setFontSize(10);


        $this->form->addQuickField(null, $codigo, 10);
        $this->form->addQuickField(null, $usuarioalteracao, 10);
        $this->form->addQuickField(null, $dataalteracao, 10);


        $this->form->addQuickField("Servidor: ", $servidor_id, 30);    
        $this->form->addQuickField('Mes: ', $mes, 15);
        $this->form->addQuickField('Ano: ', $ano, 10);
        $this->form->addQuickField(null, $aviso, 50);

        $action1 = new TAction(array($this, 'onReload'));
        
        
        /*
        $action1->setParameter('key', filter_input(INPUT_GET, 'key'));
        $action1->setParameter('fk', filter_input(INPUT_GET, 'fk'));
        */

        $this->form->addQuickAction('Buscar', $action1, 'ico_find.png');
        $this->form->addQuickAction('Voltar', new TAction(array('ProgramaTematicoPexList', 'onSearch')), 'ico_datagrid.gif');
        $this->form->addQuickAction('Gerar Relat&oacute;rio', new TAction(array($this, 'onGenerate')), '');
        
        $this->datagrid = new TDataGridTables;

        $dgcodigo   = new TDataGridColumn('id',  'C&oacute;digo',  'center', 60);
        $dgservidor   = new TDataGridColumn('nome',  'Servidor',  'left', 260);
        $dgmatricula     = new TDataGridColumn('matricula',    'Matricula',    'left', 150);
        $dgdatabatida   = new TDataGridColumn('databatida',   'Data',  'left',  80);
        $dghorabatida   = new TDataGridColumn('databatida',   'Hora',  'left',  80);
        $dgfuncao = new TDataGridColumn('cargo_nome', 'Fun&ccedil;&atilde;o', 'left', 100);
        $dgprocessado = new TDataGridColumn('processado', 'Processado', 'left', 200);

        $this->datagrid->addColumn($dgservidor);
        $this->datagrid->addColumn($dgmatricula);
        $this->datagrid->addColumn($dgdatabatida);
        $this->datagrid->addColumn($dghorabatida);
        $this->datagrid->addColumn($dgfuncao);
        $this->datagrid->addColumn($dgprocessado);

        $dgdatabatida->setTransformer('formatar_data');
        $dghorabatida->setTransformer('formatar_hora2');

        $this->datagrid->createModel();

        $panel = new TPanelForm(700, 500);
        $panel->put($this->form, 0, 0);
        $panel->put($this->datagrid, 150, 165);

        parent::add($panel);
    }

    function onReload() {

        if($_REQUEST['servidor_id']){

            TTransaction::open('pg_ceres');
            $repository = new TRepository('vw_marcacao_relogioRecord');

            $criteria = new TCriteria;

            $criteria->add(new TFilter('servidor_id', '=', $_REQUEST['servidor_id']));
            $criteria->add(new TFilter('mes', '=', $_REQUEST['mes']));
            $criteria->add(new TFilter('ano', '=', $_REQUEST['ano']));

            $criteria->setProperty('order', 'databatida DESC');

            $cadastros = $repository->load($criteria);

            $this->datagrid->clear();

            if ($cadastros) {
                foreach ($cadastros as $cadastro) {
                    $this->datagrid->addItem($cadastro);
                }
            }
            TTransaction::close();
        }

        $this->loaded = true;
    }

    function onDelete($param){
        $key=$param['key'];

        $action1 = new TAction(array($this, 'Delete'));
        $action2 = new TAction(array($this, 'NaoDelete'));

        $action1->setParameter('key', $key);
        $action2->setParameter('key', $key);

        new TQuestion('Deseja realmente excluir o registro ?', $action1, $action2);
    }

    function Delete($param){
        $key=$param['key'];
        TTransaction::open('pg_ceres');

        $cadastro = new TurmaRecord($key);

        try{
            $cadastro->delete();

            TTransaction::close();
        }
        catch (Exception $e){
            new TMessage('error', $e->getMessage());
            TTransaction::rollback();
        }

        $this->onReload();
        new TMessage('info', "Registro Excluido com sucesso");
    }

    function onSearch() {

       if($_REQUEST['servidor_id']){

        TTransaction::open('pg_ceres');
        $repository = new TRepository('vw_marcacao_relogioRecord');

        $criteria = new TCriteria;

        $criteria->add(new TFilter('servidor_id', '=', $_REQUEST['servidor_id']));
        $criteria->add(new TFilter('mes', '=', $_REQUEST['mes']));
        $criteria->add(new TFilter('ano', '=', $_REQUEST['ano']));

        $criteria->setProperty('order', 'databatida DESC');

        $cadastros = $repository->load($criteria);

        $this->datagrid->clear();

        if ($cadastros) {
            foreach ($cadastros as $cadastro) {
                $this->datagrid->addItem($cadastro);
            }
        }
        TTransaction::close();
    }

    $this->loaded = true;
}

    function onGenerate(){

        try{
            $this->form->validate();
            
            $data = $this->form->getData();
            
            new RelatorioBatidasServidorPDF();
            
        }catch( Exception $e ){
            new TMessage( 'error', $e->getMessage() );
            
        }


    }
    /*

    function onEdit($param) {
        $key = $param['key'];
        TTransaction::open('pg_ceres');
        $cadastro = new PexObjetivoRecord($key);
        $this->form->setData($cadastro);
        TTransaction::close();
    }
    */

}