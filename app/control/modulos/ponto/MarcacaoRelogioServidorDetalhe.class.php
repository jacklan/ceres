<?php

//ini_set('display_errors', 1);
//ini_set('display_startup_erros', 1);
//error_reporting(E_ALL);
//Ericleison 


include_once 'app/lib/funcdate.php';

use Adianti\Widget\Datagrid\TDatagridTables;

class MarcacaoRelogioServidorDetalhe extends TPage
{

    private $form;     // formulario de cadastro
    private $datagrid; // listagem

    public function __construct()
    {
        parent::__construct();

        $this->form = new TQuickForm;
        $this->form->class = 'form_Marcacao_Relogio_Servidor';
        $this->form->setFormTitle('<font color="red" size="3px" face="Arial"><b>Relatório de Marca&ccedil;&atilde;o Relogio do Servidor</b></font>');

        $codigo = new THidden('id');
        $mes = new TCombo('mes');
        $ano = new TCombo('ano');
        $relatorio = new TCombo('relatorio');
        
        $itemsRelatorio  = array();
        $itemsRelatorio['1'] = 'Marcação Batida';
        $itemsRelatorio['2'] = 'Marcação Batidas Apuradas';

        $items = array();
        $items['1'] = 'JANEIRO';
        $items['2'] = 'FEVEREIRO';
        $items['3'] = 'MAR&Ccedil;O';
        $items['4'] = 'ABRIL';
        $items['5'] = 'MAIO';
        $items['6'] = 'JUNHO';
        $items['7'] = 'JULHO';
        $items['8'] = 'AGOSTO';
        $items['9'] = 'SETEMBRO';
        $items['10'] = 'OUTUBRO';
        $items['11'] = 'NOVEMBRO';
        $items['12'] = 'DEZEMBRO';

        $relatorio->addItems($itemsRelatorio);
        $relatorio->setValue(2);
        
        $mes->addItems($items);
        $mes->setValue(date('m'));

        $ano->addItems(retornaAnosemZero());
        $ano->setValue(date('Y'));
        TTransaction::open('pg_ceres');
        $servidor_id = new THidden('servidor_id');
        $servidor_id->setValue($_SESSION['servidor_id']);
        $nomeservidor = new ServidorRecord($_SESSION['servidor_id']);
        TTransaction::close();

        $this->form->addQuickField(null, $codigo, 10);
        $this->form->addQuickField(null, $servidor_id, 10);
        $this->form->addQuickField('Servidor: ', new \Adianti\Widget\Form\TLabel($nomeservidor->nome), 300);
        $this->form->addQuickField('Mes: ', $mes, 20);
        $this->form->addQuickField('Ano: ', $ano, 10);
        $this->form->addQuickField('Escolher Relatório: ', $relatorio, 35);

        $action1 = new TAction(array($this, 'onReload'));

        $this->form->addQuickAction('Buscar', $action1, 'fa:search');
        $this->form->addQuickAction('Gerar Relat&oacute;rio', new TAction(array($this, 'onGenerate')), 'fa:file-pdf-o red');

        $this->datagrid = new TDataGridTables;

        $dgdatabatida = new TDataGridColumn('databatida', 'Data Batida', 'left', 100);
        $dgprevisto = new TDataGridColumn('hora_jornada', 'Previsto', 'left', 80);
        $dgtrabalhado = new TDataGridColumn('horatotal', 'Trabalhado', 'left', 80);
//        $dgdatabatidalink = new TDataGridColumn('databatidalink', 'Data Batida', 'left', 200);
        $dglink = new TDataGridColumn('link', 'Link', 'left', 100);
        $dgbatida01 = new TDataGridColumn('batida01', 'Batida 01', 'left', 80);
        $dgbatida02 = new TDataGridColumn('batida02', 'Batida 02', 'left', 80);
        $dghoras01 = new TDataGridColumn('hora01', 'Calc Horas 01', 'left', 80);
        $dgbatida03 = new TDataGridColumn('batida03', 'Batida 03', 'left', 80);
        $dgbatida04 = new TDataGridColumn('batida04', 'Batida 04', 'left', 80);
        $dghoras02 = new TDataGridColumn('hora02', 'Calc Horas 02', 'left', 80);
        $dgbatida05 = new TDataGridColumn('batida05', 'Batida 05', 'left', 80);
        $dgbatida06 = new TDataGridColumn('batida06', 'Batida 06', 'left', 80);
        $dghoras03 = new TDataGridColumn('hora03', 'Calc Horas 03', 'left', 80);

        $dgmes = new TDataGridColumn('mes', 'Mes', 'left', 80);
        $dgano = new TDataGridColumn('ano', 'Ano', 'left', 80);

        $this->datagrid->addColumn($dgdatabatida);
        $this->datagrid->addColumn($dgprevisto);
        $this->datagrid->addColumn($dgtrabalhado);
        $this->datagrid->addColumn($dglink);
        $this->datagrid->addColumn($dgbatida01);
        $this->datagrid->addColumn($dgbatida02);
        $this->datagrid->addColumn($dghoras01);
        $this->datagrid->addColumn($dgbatida03);
        $this->datagrid->addColumn($dgbatida04);
        $this->datagrid->addColumn($dghoras02);
        $this->datagrid->addColumn($dgbatida05);
        $this->datagrid->addColumn($dgbatida06);
        $this->datagrid->addColumn($dghoras03);
        $this->datagrid->addColumn($dgmes);
        $this->datagrid->addColumn($dgano);

        $edit = new TDataGridAction(array($this, 'onReload'));
        $edit->setLabel('Editar');
        $edit->setImage('sem_icone_nessa.png');
        $edit->setField('databatida');

        $this->datagrid->addAction($edit);

        $dgdatabatida->setTransformer('formatar_data');
        $dgbatida01->setTransformer('formatar_hora2');
        $dgbatida02->setTransformer('formatar_hora2');
        $dgbatida03->setTransformer('formatar_hora2');
        $dgbatida04->setTransformer('formatar_hora2');
        $dgbatida05->setTransformer('formatar_hora2');
        $dgbatida06->setTransformer('formatar_hora2');
        $dgprevisto->setTransformer('formatar_hora');
        $dghoras01->setTransformer('formatar_hora');
        $dghoras02->setTransformer('formatar_hora');
        $dghoras03->setTransformer('formatar_hora');
        $dgtrabalhado->setTransformer('formatar_hora');

        $this->datagrid->createModel();

        $vbox = new TVBox;
        $vbox->add($this->form);
        $vbox->add($this->datagrid);
        $vbox->style = 'width:100%;';

        parent::add($vbox);
    }

    function onReload()
    {
        $data = $this->form->getData();

        if (!empty($data->ano)) {
            TTransaction::open('pg_ceres');
            $repository = new TRepository('vw_marcacao_relogioRecord');

            $criteria = new TCriteria;

            $criteria->add(new TFilter('servidor_id', '=', $_SESSION['servidor_id']));
            $criteria->add(new TFilter('ano', '=', $data->ano));

            $criteria->setProperty('order', 'databatida DESC');

            $objects = $repository->load($criteria);

            $this->datagrid->clear();

            if ($objects) {
                foreach ($objects as $object) {
                    $this->datagrid->addItem($object);
                }
            }
            TTransaction::close();

            $this->loaded = true;
        }
    }

    function onGenerate()
    {
        try {

            $this->form->validate();
            $dadosForm = $this->form->getData();
           
            if($dadosForm->relatorio == 2){

                new RelatorioBatidasPontoServidorPDF();

            }else if($dadosForm->relatorio == 1){

                new RelatorioBatidaServidorChefePDF();

            }else{
                new TMessage('error','Escolha o tipo de Relatório');
            }

        } catch (Exception $e) {
            new TMessage('error', $e->getMessage());

        }
    }

}