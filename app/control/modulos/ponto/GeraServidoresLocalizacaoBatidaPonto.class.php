<?php

//ini_set('display_errors', 1);
//ini_set('display_startup_erros', 1);
//error_reporting(E_ALL);

/*
 * Classe GeraServidoresLocalizacaoBatidaPonto
 * Gera Relatorio Localizacao das Batidas Servidor
 * Autor: Jackson Meires
 * Data:  24/04/2017
 */

include_once 'app/lib/funcdate.php';

class GeraServidoresLocalizacaoBatidaPonto extends TPage {

    private $form;

    public function __construct() {
        parent::__construct();

        $this->form = new TQuickForm;
        $this->form->class = 'form';
        $this->form->setFormTitle('<font color="red" size="3px" face="Arial"><b>Relat&oacute;rio Localiza&ccedil;&atilde;o Batidas Ponto Servidore</b></font>');

        $ano = new TCombo('ano');
        $mes = new TCombo('mes');
        $matricula = new TEntry('matricula');
        $matricula->setProperty('placeholder', '1750100');
        
        $items = [];
        $items['0'] = 'TODOS';
        $items['1'] = 'JANEIRO';
        $items['2'] = 'FEVEREIRO';
        $items['3'] = 'MARCO';
        $items['4'] = 'ABRIL';
        $items['5'] = 'MAIO';
        $items['6'] = 'JUNHO';
        $items['7'] = 'JULHO';
        $items['8'] = 'AGOSTO';
        $items['9'] = 'SETEMBRO';
        $items['10'] = 'OUTUBRO';
        $items['11'] = 'NOVEMBRO';
        $items['12'] = 'DEZEMBRO';

        $mes->addItems($items);
        $mes->setValue(date('m'));

        $ano->addItems(retornaAnosemZero());
        $ano->setValue(date('Y'));

        $this->form->addQuickField("Ano", $ano, 30);
        $this->form->addQuickField("Mês", $mes, 30);
        
        $lb = new \Adianti\Widget\Form\TLabel('Opcional, se deixar em branco traz todos');
        $lb->setFontColor('red');
        
        $this->form->addQuickField("Matricula", $matricula, 30);
        $this->form->addQuickField("", $lb, 300);

        $action1 = new TAction(array($this, 'onGenerate'));

        $this->form->addQuickAction('Mostrar Relatorio', $action1, 'fa:file-pdf-o')->class = 'btn btn-info btnleft';

        $panel = new TPanelForm(700, 500);
        $panel->put($this->form, 0, 0);

        parent::add($panel);
    }

    function onGenerate() {
        try {
            
            $this->form->setData($this->form->getData());
            $this->form->validate();

            new RelatorioServidoresLocalizacaoBatidasPontoPDF();
        } catch (Exception $e) {

            new TMessage('error', $e->getMessage());
        }
    }

}
