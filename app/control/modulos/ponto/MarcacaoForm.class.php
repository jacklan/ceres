<?php
include_once 'app/lib/funcdate.php';

class MarcacaoForm extends TPage{
    private $form;
    public function __construct(){
        parent::__construct();

        $this->form = new TForm('form_marcacao');
        $panel = new TPanelForm(400, 500);
        $this->form->add($panel);
        
        $titulo = new TLabel('Cadastro de marcacao');
        $titulo->setFontFace('Arial');
        $titulo->setFontColor('red');
        $titulo->setFontSize(16);

        $codigo     = new TEntry('id');
        $codigo->setEditable(false);
        $servidor_id = new THidden('servidor_id');
        $servidor_id->setValue($_SESSION["servidor_id"]);

        $datamarcacao = new TDate('datamarcacao');
        $datamarcacao -> setEditable(false);
        $macequipamento = new THidden('macequipamento');

        //$macequipamento ->setEditable(false);
        //$items1 = array();
        //$items1[date('d/m/Y h:i')]= date('d/m/Y h:i');
        //$datamarcacao->addItems($items1);
        //$datamarcacao = date('d/m/y h:i\h' );
        //$datamarcacao->addItems($items1);
        //$items2 = array();
        //$items2['ENTRADA1'] = 'ENTRADA1';
        //$items2['SAIDA1'] = 'SAIDA1';
        //$items2['ENTRADA2'] = 'ENTRADA2';
        //$items2['SAIDA2'] = 'SAIDA2';
        //$items2['ENTRADA3'] = 'ENTRADA3';
        //$items2['SAIDA3'] = 'SAIDA3';
        //$sentido->addItems($items2);

        $datamarcacao->setSize(200);

        $panel->put($titulo,$panel->getColuna(),$panel->getLinha());
        $panel->setLinha($panel->getLinha()+30);
        $panel->put('Servidor:'.$_SESSION["nome"],$panel->getColuna(),$panel->getLinha());
        $panel->putCampo($servidor_id, null, 0, 0);
        $panel->putCampo($datamarcacao, 'Data marcacao', 20, 1);
        $panel->putCampo($macequipamento,null , 20, 0);

        $save_button=new TButton('save');
        $cancel_button=new TButton('cancel');
        $save_button->setAction(new TAction(array($this, 'onSave')), 'Salvar');
        $cancel_button->setAction(new TAction(array('MarcacaoList', 'onReload')), 'Cancelar');

        $panel->putCampo($save_button, null, -100, 1);
        $panel->putCampo($cancel_button, null, 0, 0);

        $this->form->setFields(array($codigo, $datamarcacao, $servidor_id,  $macequipamento,$save_button, $cancel_button));

        parent::add($this->form);
    }
    

    function onSave(){
        TTransaction::open('pg_ceres');
        
        $cadastro = $this->form->getData('MarcacaoRecord');

        $dados = $cadastro->toArray();

        $msg = '';
        $icone = 'info';

        if (empty ($dados['datamarcacao'])){
            $msg .= 'A Data do inicio das marcacao deve ser informado.';
        }
        if (empty ($dados['servidor_id'])){
            $msg .= 'O Servidor deve ser informado.';
        }
        if(empty ($dados['macequipamento'])){
            $msg .= 'O mac deve ser informado';
        }
        try{
            if ($msg == ''){
                $cadastro->store();
                $msg = 'Dados armazenados com sucesso';
                TTransaction::close();
            }else{
                $icone = 'error';
            }
            if ($icone == 'error' ){

                $this->form->setData($this->form->getData('marcacaoRecord'));
                new TMessage($icone, $msg);

            }else{
                $action1 = new TAction(array('MarcacaoList', 'onReload'));
                new TConfirmation($msg, $action1);
            }
        }catch (Exception $e){

            new TMessage('error', $e->getMessage());
            TTransaction::rollback();
        }

    }
    
    function onEdit($param){
        $key=$param['key'];
        TTransaction::open('pg_ceres');

        $cadastro = new marcacaoRecord($key);

        $cadastro ->datamarcacao = date('d/m/Y H:i');
        $cadastro->macequipamento = pegaMac();
        $cadastro->servidor_id = $_SESSION["servidor_id"];

        $this->form->setData($cadastro);
        
        TTransaction::close();
    }

}
?>