<?php

//ini_set('display_errors', 1);
//ini_set('display_startup_erros', 1);
//error_reporting(E_ALL);
/*
 * classe MarcacaoList
 * Bater Ponto Servidor
 * Autor:Jackson Meires
 * Data: 07/03/2018
 */
include_once 'app/lib/funcdate.php';

use Adianti\Widget\Datagrid\TDatagridTables;

class MarcacaoList extends TPage
{

    private $form;
    private $datagrid;
    private $pageNavigation;

    public function __construct()
    {
        parent::__construct();

        //$this->form = new \Adianti\Widget\Wrapper\TQuickForm();
        $this->form = new BootstrapFormWrapper(new TQuickForm, 'form-vertical'); // HERE CHANGE CLASS
        $title = '<b style="color: red !important;">Bater Ponto</b>';

        $codigo = new THidden('id');
        $servidor_id = new THidden('matricula');
        $servidor_id->setValue($_SESSION["usuario"]);
        $tipo = new THidden('funcao');
        $numeroip = new THidden('numeroip');
        $numeromac = new THidden('numeromac');
        $nomesistemaoperacional = new THidden('nomesistemaoperacional');

        $datamarcacao = new TDate('databatida');
        $datamarcacao->setEditable(false);
        $datamarcacao->setValue(date('d/m/y H:i\h'));

        $latitude = new THidden('latitude');
        $latitude->id = 'latitude';
        $longitude = new THidden('longitude');
        $longitude->id = 'longitude';

        TTransaction::open('pg_ceres');

        $cadastro = new ServidorRecord($_SESSION['servidor_id']);

        if ($cadastro) {
            $nome_servidor = new TLabel($cadastro->nome);
        }
        ############### chamar funcao com o label da jornada do servidor ###############
        $div_jornada = new TElement('div');
        $div_jornada->add($this->onDivJornadaServidor($_SESSION['servidor_id']));
        ################################################################################

        $this->form->setFieldsByRow(1); // seta uma linha por vez no formulario

        $this->form->addQuickField(null, $codigo, 10);
        $this->form->addQuickField(null, $servidor_id, 10);
        $this->form->addQuickField(null, $tipo, 10);
        $this->form->addQuickField(null, $latitude, 10);
        $this->form->addQuickField(null, $longitude, 10);
        $this->form->addQuickField(null, $numeroip, 10);
        $this->form->addQuickField(null, $numeromac, 10);
        $this->form->addQuickField(null, $nomesistemaoperacional, 10);
        $this->form->addQuickField('Servidor', $nome_servidor, 300);
        $this->form->addQuickField('Data Marcação', $datamarcacao, 45);



        // creates the script element
        $script = new TElement('script');
        $script->type = 'text/javascript';
        $script->add("
            $( document ).ready(function() {
              if (navigator.geolocation) {
                        navigator.geolocation.getCurrentPosition(showPosition);
                    } else {
                        alert('Geolocaliza&ccedil;&atilde;o n&atilde;o &eacute; suportada por este navegador<br>Por favor atualize seu navegador ou tente com outro navegador.');
                    }
                function showPosition(position) {

                    $('#latitude').val( position.coords.latitude );
                    $('#longitude').val( position.coords.longitude );
                }
            }); ");

        $actionEntrada = new TAction(array('MarcacaoList', 'onEntrada'));
        $actionSaida = new TAction(array('MarcacaoList', 'onSaida'));
        $actionJustificar = new TAction(array('JustificativaPontoList', 'onReload'));
        $actionRelatorio = new TAction(array('MarcacaoRelogioServidorDetalhe', 'onReload'));

        $btnEntrada = $this->form->addQuickAction('Entrar', $actionEntrada, 'checkin.png');
        $btnEntrada->{'style'} = 'float: left; width: 90px !important';

        $btnSaida = $this->form->addQuickAction('Sair', $actionSaida, 'checkout.png');
        $btnSaida->{'style'} = 'float: left; width: 90px !important';

        $btnJustificativa = $this->form->addQuickAction('Justificar', $actionJustificar, 'ico_laudo.png');
        $btnJustificativa->{'style'} = 'float: left; width: 110px !important ';

        $btnRelatorio = $this->form->addQuickAction('Relatório', $actionRelatorio, 'ico_fichacatalografica.png');
        $btnRelatorio->{'style'} = 'float: left; width: 105px !important ';

        //$this->datagrid = new TDataGridTables;
        $this->datagrid = new \Adianti\Wrapper\BootstrapDatagridWrapper(new TQuickGrid);

        $dgdatamarcacao = new TDataGridColumn('databatida', 'Data Marca&ccedil;&atilde;o', 'left', 100);
        $dghora = new TDataGridColumn('databatida', 'Hora Marca&ccedil;&atilde;o', 'left', 50);
        $dgtipo = new TDataGridColumn('funcao', 'Tipo', 'left', 100);

        $this->datagrid->addColumn($dgdatamarcacao);
        $this->datagrid->addColumn($dghora);
        $this->datagrid->addColumn($dgtipo);

        $dgdatamarcacao->setTransformer('formatar_data');
        $dghora->setTransformer('formatar_hora2');

        $action1 = new TDataGridAction(array($this, 'onReload'));
        $action1->setLabel('Alterar');
        $action1->setImage('ico_edite.png');
        $action1->setField('id');
        $action1->setFk('id');

        $this->datagrid->addAction($action1);

        $this->datagrid->createModel();

        // creates the page navigation
        $this->pageNavigation = new TPageNavigation;
        $this->pageNavigation->setAction(new TAction(array($this, 'onReload')));
        $this->pageNavigation->setWidth($this->datagrid->getWidth());

        $panel = new \Adianti\Widget\Container\TPanelGroup($title);
        $panel->add($this->form);
        $panel->add($div_jornada);
        $panel->add($script);

        $panel2 = new \Adianti\Widget\Container\TPanelGroup('<b style="color: red !important;">Batidas do Mês Atual</b>');
        $panel2->add($this->datagrid);
        $panel2->add($this->pageNavigation);

        // wrap the page content using vertical box
        $vbox = new TVBox;
        $vbox->add($panel);
        $vbox->add($panel2);

        parent::add($vbox);
    }

    function onDivJornadaServidor($servidor_id)
    {

        $conn = TTransaction::get(); // obtém a conexão

        $sth = $conn->prepare('select j.horajornada,
                                    j.entrada1,j.entrada1inicio,j.entrada1fim,
                                    j.entrada2,j.entrada2inicio,j.entrada2fim,
                                    j.entrada3,j.entrada3inicio,j.entrada3fim,
                                    j.saida1,j.saida1inicio,j.saida1fim,
                                    j.saida2,j.saida2inicio,j.saida2fim,
                                    j.saida3,j.saida3inicio,j.saida3fim
                                  from servidor s 
                                inner join jornada j on j.id = s.jornada_id WHERE s.id = ?');

        $sth->execute(array($servidor_id));
        $result = $sth->fetchAll();

        $div_detalhe = "<div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>
                        <div class='container'>
                          <div class='row'>";
        // exibe os resultados
        foreach ($result as $row) {
            if (empty($row['entrada2'])) {
                $div_detalhe .= "<div class='boxed col-lg-12 col-md-12 col-sm-12 col-xs-12'>Horário <font color='red' size='3px' face='Arial'><b>" . formatar_hora($row['entrada1']) . "</b></font> às <font color='red' size='3px' face='Arial'><b>" . formatar_hora($row['saida1']) . "</b></font></div>";
                $div_detalhe .= "<div class='boxed col-lg-12 col-md-12 col-sm-12 col-xs-12'>Entrada <font color='red' size='3px' face='Arial'><b>" . formatar_hora($row['entrada1inicio']) . "</b></font> até <font color='red' size='3px' face='Arial'><b>" . formatar_hora($row['entrada1fim']) . "</b></font></div>";
                $div_detalhe .= "<div class='boxed col-lg-12 col-md-12 col-sm-12 col-xs-12'>Saida <font color='red' size='3px' face='Arial'><b>" . formatar_hora($row['saida1inicio']) . "</b></font> até <font color='red' size='3px' face='Arial'><b>" . formatar_hora($row['saida1fim']) . "</b></font></div>";
            } else if (empty($row['entrada3'])) {
                $div_detalhe .= "<div class='boxed col-lg-6 col-md-6 col-sm-6 col-xs-12'>Turno 01 <font color='red' size='3px' face='Arial'><b>" . formatar_hora($row['entrada1']) . "</b></font> às <font color='red' size='3px' face='Arial'><b>" . formatar_hora($row['saida1']) . "</b></font></div><div class='boxed col-lg-6 col-md-6 col-sm-6 col-xs-12'> Turno 02 <font color='red' size='3px' face='Arial'><b>" . formatar_hora($row['entrada2']) . "</b></font> às <font color='red' size='3px' face='Arial'><b>" . formatar_hora($row['saida2']) . "</b></font></div>";
                $div_detalhe .= "<div class='boxed col-lg-6 col-md-6 col-sm-6 col-xs-12'>Entrada 01: <font color='red' size='3px' face='Arial'><b>" . formatar_hora($row['entrada1inicio']) . "</b></font> até <font color='red' size='3px' face='Arial'><b>" . formatar_hora($row['entrada1fim']) . "</b></font></div><div class='boxed col-lg-6 col-md-6 col-sm-6 col-xs-12'> Entrada 02: <font color='red' size='3px' face='Arial'><b>" . formatar_hora($row['entrada2inicio']) . "</b></font> até <font color='red' size='3px' face='Arial'><b>" . formatar_hora($row['entrada2fim']) . "</b></font></div>";
                $div_detalhe .= "<div class='boxed col-lg-6 col-md-6 col-sm-6 col-xs-12'>Saida 01: <font color='red' size='3px' face='Arial'><b>" . formatar_hora($row['saida1inicio']) . "</b></font> até <font color='red' size='3px' face='Arial'><b>" . formatar_hora($row['saida1fim']) . "</b></font></div><div class='boxed col-lg-6 col-md-6 col-sm-6 col-xs-12'> Saida 02: <font color='red' size='3px' face='Arial'><b>" . formatar_hora($row['saida2inicio']) . "</b></font> até <font color='red' size='3px' face='Arial'><b>" . formatar_hora($row['saida2fim']) . "</b></font></div>";
            } else if (!empty($row['entrada3'])) {
                $div_detalhe .= "<div class='boxed col-lg-4 col-md-4 col-sm-12 col-xs-12'>Turno 01 <font color='red' size='3px' face='Arial'><b>" . formatar_hora($row['entrada1']) . "</b></font> às <font color='red' size='3px' face='Arial'><b>" . formatar_hora($row['saida1']) . "</b></font></div><div class='boxed col-lg-4 col-md-4 col-sm-12 col-xs-12'> Turno 02 <font color='red' size='3px' face='Arial'><b>" . formatar_hora($row['entrada2']) . "</b></font> às <font color='red' size='3px' face='Arial'><b>" . formatar_hora($row['saida2']) . "</b></font></div><div class='boxed col-lg-4 col-md-4 col-sm-12 col-xs-12'> Turno 03 <font color='red' size='3px' face='Arial'><b>" . formatar_hora($row['entrada3']) . "</b></font> às <font color='red' size='3px' face='Arial'><b>" . formatar_hora($row['saida3']) . "</b></font></div>";
                $div_detalhe .= "<div class='boxed col-lg-4 col-md-4 col-sm-12 col-xs-12'>Entrada 01: <font color='red' size='3px' face='Arial'><b>" . formatar_hora($row['entrada1inicio']) . "</b></font> até <font color='red' size='3px' face='Arial'><b>" . formatar_hora($row['entrada1fim']) . "</b></font></div><div class='boxed col-lg-4 col-md-4 col-sm-12 col-xs-12'> Entrada 02: <font color='red' size='3px' face='Arial'><b>" . formatar_hora($row['entrada2inicio']) . "</b></font> até <font color='red' size='3px' face='Arial'><b>" . formatar_hora($row['entrada2fim']) . "</b></font></div><div class='boxed col-lg-4 col-md-4 col-sm-12 col-xs-12'> Entrada 03: <font color='red' size='3px' face='Arial'><b>" . formatar_hora($row['entrada3inicio']) . "</b></font> até <font color='red' size='3px' face='Arial'><b>" . formatar_hora($row['entrada3fim']) . "</b></font></div>";
                $div_detalhe .= "<div class='boxed col-lg-4 col-md-4 col-sm-12 col-xs-12'>Saida 01: <font color='red' size='3px' face='Arial'><b>" . formatar_hora($row['saida1inicio']) . "</b></font> até <font color='red' size='3px' face='Arial'><b>" . formatar_hora($row['saida1fim']) . "</b></font></div><div class='boxed col-lg-4 col-md-4 col-sm-12 col-xs-12'> Saida 02: <font color='red' size='3px' face='Arial'><b>" . formatar_hora($row['saida2inicio']) . "</b></font> até <font color='red' size='3px' face='Arial'><b>" . formatar_hora($row['saida2fim']) . "</b></font></div><div class='boxed col-lg-4 col-md-4 col-sm-12 col-xs-12'> Saida 03: <font color='red' size='3px' face='Arial'><b>" . formatar_hora($row['saida3inicio']) . "</b></font> até <font color='red' size='3px' face='Arial'><b>" . formatar_hora($row['saida3fim']) . "</b></font></div>";
            }
            $div_detalhe .= "                            
                      </div>
                    </div>
                  </div>";
            return $div_detalhe;
        }

        TTransaction::close();
    }

    function onEntrada($param)
    {

        // obtem o parametro $key
        // define duas acoes
        $action1 = new TAction(array($this, 'BaterEntrada'));

        // define os parametros de cada acao
        $action1->setParameter('matricula', $param['matricula']);
        $action1->setParameter('databatida', $param['databatida']);
        $action1->setParameter('latitude', $param['latitude']);
        $action1->setParameter('longitude', $param['longitude']);

        // exibe um dialogo ao usuario
        new TQuestion('Confirma a batida de <font color=green><b>ENTRADA</b></font> agora?', $action1);
    }

    function BaterEntrada($param)
    {

        TTransaction::open('pg_ceres');

        $object = $this->form->getData('BatidaRecord');
        $object->funcao = 'ENTRADA';
        $object->matricula = $param['matricula'];
        //$object->databatida = $param['databatida'];
        $object->latitude = $param['latitude'];
        $object->longitude = $param['longitude'];
        $object->databatida = date('d/m/y H:i\h');
        $object->numeroip = getUserIP();
        $object->numeromac = getMac();
        $object->nomesistemaoperacional = getOS();

        $msg = '';
        $icone = 'info';

        try {

            $this->form->validate();
            if ($msg == '') {

                $object->store();
                $msg = 'Dados armazenados com sucesso';

                TTransaction::close();
            } else {
                $icone = 'error';
            }

            if ($icone == 'error') {
                new TMessage($icone, $msg);
            } else {
                new TMessage("info", "Ponto batido com sucesso!");
                TApplication::gotoPage('MarcacaoList', 'onReload');
            }
        } catch (Exception $e) {

            new TMessage('error', $e->getMessage());
            TTransaction::rollback();
        }
    }

    function onSaida($param)
    {

        // define duas acoes
        $action1 = new TAction(array($this, 'BaterSaida'));

        $action1->setParameter('matricula', $param['matricula']);
        $action1->setParameter('databatida', $param['databatida']);
        $action1->setParameter('latitude', $param['latitude']);
        $action1->setParameter('longitude', $param['longitude']);

        // exibe um dialogo ao usuario
        new TQuestion('Confirma a batida de <font color=red><b>SA&Iacute;DA</b></font> agora?', $action1);
    }

    function BaterSaida($param)
    {

        TTransaction::open('pg_ceres');

        $object = $this->form->getData('BatidaRecord');
        $object->funcao = 'SAIDA';
        $object->matricula = $param['matricula'];
        //$object->databatida = $param['databatida'];
        $object->latitude = $param['latitude'];
        $object->longitude = $param['longitude'];
        $object->databatida = date('d/m/y H:i\h');
        $object->numeroip = getUserIP();
        $object->numeromac = getMac();
        $object->nomesistemaoperacional = getOS();
        $dados = $object->toArray();

        $msg = '';
        $icone = 'info';

        if (empty($dados['matricula'])) {
            $msg .= 'O Nome deve ser informado.</br>';
        }
        if (empty($dados['databatida'])) {
            $msg .= 'A Data deve ser informado.</br>';
        }

        try {

            $this->form->validate();
            if ($msg == '') {

                $object->store();
                $msg = 'Dados armazenados com sucesso';
                TTransaction::close();
            } else {
                $icone = 'error';
            }

            if ($icone == 'error') {

                new TMessage($icone, $msg);
            } else {
                new TMessage("info", "Ponto batido com sucesso!");
                TApplication::gotoPage('MarcacaoList', 'onReload');
            }
        } catch (Exception $e) {
            new TMessage('error', $e->getMessage());
            TTransaction::rollback();
        }
    }

    function onReload($param = NULL)
    {

        TTransaction::open('pg_ceres');

        $repository = new TRepository('BatidaRecord');
        $criteria = new TCriteria;

        ############### Filtra apenas pelo mes atual ###############
        $criteria->add(new TFilter('date_trunc(\'month\', databatida)', '=', 'NOESC:date_trunc(\'month\', CURRENT_TIMESTAMP)'));
        $criteria->add(new TFilter('date_trunc(\'YEAR\', databatida)', '=', 'NOESC:date_trunc(\'YEAR\', CURRENT_TIMESTAMP)'));

        $limit = 6;
        $criteria->setProperties($param); // order, offset
        $criteria->setProperty('limit', $limit);

        $criteria->add(new TFilter('matricula', '=', $_SESSION["usuario"]));

        $criteria->setProperty('order', 'databatida DESC');

        $objects = $repository->load($criteria);

        $this->datagrid->clear();

        if ($objects) {
            foreach ($objects as $object) {
                $this->datagrid->addItem($object);
            }
        }
        // reset the criteria for record count
        $criteria->resetProperties();
        $count = $repository->count($criteria);

        $this->pageNavigation->setCount($count); // count of records
        $this->pageNavigation->setProperties($param); // order, page
        $this->pageNavigation->setLimit($limit); // limit
        TTransaction::close();
        $this->loaded = true;
    }

    function show()
    {
        $this->onReload();
        parent::show();
    }

}
