<?php

/*
* classe MarcacaoMacForm
* Cadastro de Marcação Mac: Contem o formularo
* Autor:Lucas Vicente
* Data: 01/09/2016
 */

class MarcacaoMacForm extends TPage {

    private $form;     // formulario de cadastro

    /*
     * metodo construtor
     * Cria a pagina do formulario
     */

    public function __construct() {
        parent::__construct();
        
        // instancia um formulario
        $this->form = new TQuickForm;
        $this->form->class = 'form_ponto_marcacaomac';
        $this->form->setFormTitle('<font color="red" size="3" face="Arial"><b>Formul&aacute;rio de Marcação MAC
            </b></font>');
        
        // cria os campos do formulario
        $codigo = new THidden('id');
        $codigo->setEditable(false);
        $numeromac = new TEntry('numeromac');
        $unidadeoperativa_id = new TCombo('unidadeoperativa_id');
        $situacao = new TCombo('situacao');
        $marcacaomac_id = new TCombo('marcacaomac_id');

        $usuarioalteracao = new THidden('usuarioalteracao');
        $dataalteracao = new THidden('dataalteracao');

        TTransaction::open('pg_ceres');
        $repository = new TRepository('UnidadeOperativaRecord');
        $criteria = new TCriteria;
        $criteria->setProperty('order', 'nome');
        $collection = $repository->load($criteria);
        foreach ($collection as $object){
            $items[$object->id] = $object->nome;
        }
        $unidadeoperativa_id->addItems($items);
        TTransaction::close();

        $items2 = array();
        $items2['ATIVO'] = 'ATIVO';
        $items2['INATIVO'] = 'INATIVO';
        $situacao->addItems($items2);
        $situacao->setValue('ATIVO');

        $numeromac->setProperty('placeholder', 'Ex.:XX-XX-XX-XX-XX-XX');
        
        // cria um rotulo para o titulo
        $campo = new TLabel('<div style="position:floatval; width: 200px;"> <b>* Campos obrigat&oacute;rios</b></div>');
        $campo->setFontFace('Arial');
        $campo->setFontColor('red');
        $campo->setFontSize(10);

        
        
        // define os campos
        $this->form->addQuickField(null, $codigo, 10);
        $this->form->addQuickField(null, $usuarioalteracao, 10);
        $this->form->addQuickField(null, $dataalteracao, 10);
        $this->form->addQuickField('N&uacute;mero MAC <font color=red><b>*</b></font>', $numeromac, 50);
        $this->form->addQuickField('Unidade Operativa <font color=red><b>*</b></font>', $unidadeoperativa_id, 50);
        $this->form->addQuickField('Situação <font color=red><b>*</b></font>', $situacao, 50);
        
        $this->form->addQuickField(null, $campo, 50);
        
        // cria um botao de acao
        $this->form->addQuickAction('Salvar', new TAction(array($this, 'onSave')), 'ico_save.png')->class = 'btn btn-info';
        $this->form->addQuickAction('Voltar', new TAction(array('MarcacaoMacList', 'onReload')), 'ico_datagrid.gif');

        // adiciona a tabela a pagina
        parent::add($this->form);
    }

    /*
     * metodo onSave()
     * Executada quando o usuario clicar no botao salvar do formulario
     */

    function onSave() {
        
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');
        // obtem os dados no formulario em um objeto CarroRecord

        $cadastro = $this->form->getData('MarcacaoMacRecord');
        //lanca o default
        $cadastro->usuarioalteracao = $_SESSION['usuario'];
        $cadastro->dataalteracao = date("d/m/Y H:i:s");
        $cadastro->empresa_id = $_SESSION['empresa_id'];

        //antes de armazenar verifica se algum campo eh requerido e nao foi informado
        $dados = $cadastro->toArray();

        $msg = '';
        $icone = 'info';

        if (empty($dados['numeromac'])) {
            $msg .= 'O Numero MAC deve ser informado.</br>';
        }
        if (empty($dados['unidadeoperativa_id'])) {
            $msg .= 'A Unidade Operativa deve ser informada.</br>';
        }
        if (empty($dados['situacao'])) {
            $msg .= 'A Situação deve ser informada.</br>';
        }

        try {
        $this->form->validate();
            if ($msg == '') {
                // armazena o objeto
                $cadastro->store();
                $msg = 'Dados armazenados com sucesso';

                // finaliza a transacao
                TTransaction::close();
            } else {
                $icone = 'error';
            }

            if ($icone == 'error') {
                // exibe mensagem de erro
                new TMessage($icone, $msg);
            } else {                
             new TMessage("info", "Registro salvo com sucesso!");
                TApplication::gotoPage('MarcacaoMacList','onReload'); // reload
                
            }
        } catch (Exception $e) { // em caso de exce��o
            // exibe a mensagem gerada pela exce��o
            new TMessage('error', $e->getMessage());
            // desfaz todas altera��es no banco de dados
            TTransaction::rollback();
        }
    }

    /*
     * metodo onEdit()
     * Edita os dados de um registro
     */

    function onEdit($param) {
        
        try {
            if (isset($param['key'])) {

                // get the parameter $key
                $key = $param['key'];

                TTransaction::open('pg_ceres');   // open a transaction with database 'samples'

                $object = new MarcacaoMacRecord($key); // instantiates object City

                $this->form->setData($object);   // fill the form with the active record data

                TTransaction::close();           // close the transaction
            } 
        } catch (Exception $e) { // in case of exception
            // shows the exception error message
            new TMessage('error', '<b>Error</b> ' . $e->getMessage());
            // undo all pending operations
            TTransaction::rollback();
        }
        
    }

}

?>