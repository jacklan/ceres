<?php

include_once 'app/lib/funcdate.php';

use Adianti\Database\TFilter1;
use Adianti\Widget\Datagrid\TDatagridTables;
//teste

class ConfirmarPontoChefeList extends TPage {

    private $form;
    private $datagrid;

    public function __construct() {
        parent::__construct();

        $this->form = new TQuickForm;
        $this->form->class = 'form_ponto_marcacao2';
        $this->form->setFormTitle('<font color="red" size="3px" face="Arial"><b>Servidor Presente ou ausente</b></font>');

        $this->datagrid = new TDataGridTables;

        $dgnome     = new TDataGridColumn('servidor_nome',    'Servidor',    'left', 60);
        $dgdatamarcacao     = new TDataGridColumn('db_data',    'Data Marca&ccedil;&atilde;o',    'left', 60);
        $dghora    = new TDataGridColumn('db_hora',    'Hora Marca&ccedil;&atilde;o',    'left', 60);
        $dgtipo      = new TDataGridColumn('funcao',    'Tipo',    'left', 60);
        $dgprocessado      = new TDataGridColumn('situacaochefe',    'Situação',    'left', 600);

        $this->datagrid->addColumn($dgnome);
        $this->datagrid->addColumn($dgdatamarcacao);
        $this->datagrid->addColumn($dghora);
        $this->datagrid->addColumn($dgtipo);
        $this->datagrid->addColumn($dgprocessado);

        $action2 = new TDataGridAction(array($this, 'onAusentar'));
        $action2->setLabel('Funcionario Ausente');
        $action2->setImage('ico_cancelar.png');
        $action2->setField('batida_id');
        
        $action2->setDisplayCondition( array($this, 'displayColumn') );
        
        $this->datagrid->addAction($action2);

        $this->datagrid->createModel();

        $panel = new TPanelForm(700, 500);
        $panel->put($this->form, 0, 0);
        $panel->put($this->datagrid, 150, 165);

        parent::add($panel);
    }

    public function displayColumn( $object ){
        if ($object->batida_id != '0' ){
            return TRUE;
        }
        return FALSE;
    }

    function onReload() {

        $servidorTemp = $_SESSION['servidor_id'];
        $setorTemp = $_SESSION['setor_id'];

        TTransaction::open('pg_ceres');

        $repository = new TRepository('vw_validacao_batidaRecord');
        $criteria = new TCriteria;

        $criteria->add(new TFilter('chefe_id', '=', $servidorTemp));
        $criteria->add(new TFilter('setor_id', '=', $setorTemp));

        $cadastros = $repository->load($criteria);

        $this->datagrid->clear();
        
        if ($cadastros) {
            foreach ($cadastros as $cadastro) {

                $this->datagrid->addItem($cadastro);
            }
        }

        TTransaction::close();
        $this->loaded = true;
    }
    
    function show() {
        $this->onReload();
        parent::show();
    }

    public function onAusentar($param){
        try {
            if (isset($param['key'])) {

                $key = $param['key'];
                TTransaction::open('pg_ceres');   

                $object = new BatidaRecord($key);     
                $object->situacaochefe = "AUSENTE";
                $object->store();
                new TMessage('info', "Servidor confirmado como ausente!");

                TTransaction::close();       

                $this->onReload();

            } else {
            }
        } catch (Exception $e) { 
            new TMessage('error', '<b>Error</b> ' . $e->getMessage());
            TTransaction::rollback();
        }
    }


}