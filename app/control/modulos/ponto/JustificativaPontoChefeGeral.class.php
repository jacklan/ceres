<?php

include_once 'app/lib/funcdate.php';

use Adianti\Database\TFilter1;
use Adianti\Widget\Datagrid\TDatagridTables;

class JustificativaPontoChefeGeral extends TPage
{
    private $form;
    private $datagrid;

    public function __construct() {
        parent::__construct();

        $this->form = new TForm('form_JustificativaPontoChefeGeral');

        $panel = new TPanelForm(1000, 200);
        $this->form->add($panel);

        $titulo = new TLabel('Justificativas à Serem Abonadas');
        $titulo->setFontFace('Arial');
        $titulo->setFontColor('red');
        $titulo->setFontSize(12);

        $panel->put($titulo, $panel->getColuna(), $panel->getLinha());

        $this->datagrid = new TDataGridTables;

        $dgservidor = new TDataGridColumn('nome_servidor', 'Servidor', 'left', 400);
        $dgdatabatida = new TDataGridColumn('databatida', 'Data', 'left', 150);
        $dghorajornada = new TDataGridColumn('hora_jornada', 'Jornada', 'left', 150);
        $dghoratotal = new TDataGridColumn('horatotal', 'Trabalhado', 'left', 150);
        $dgsituacao = new TDataGridColumn('situacaojustificativa', 'Situa&ccedil;&atilde;o', 'left', 150);
        $dgtipojustificativa = new TDataGridColumn('nometipojustificativa', 'Tipo Justificativa', 'left', 300);
        $dgpdf = new TDataGridColumn('justificativapdf', 'Justificativa PDF', 'left', 500);

        $dgdatabatida->setTransformer('formatar_data');

        $this->datagrid->addColumn($dgservidor);
        $this->datagrid->addColumn($dgdatabatida);
        $this->datagrid->addColumn($dghorajornada);
        $this->datagrid->addColumn($dghoratotal);
        $this->datagrid->addColumn($dgsituacao);
        $this->datagrid->addColumn($dgtipojustificativa);
        $this->datagrid->addColumn($dgpdf);

        $action1 = new TDataGridAction(array('JustificativaPontoChefeGeralForm', 'onEdit'));
        $action1->setLabel('Alterar');
        $action1->setImage('ico_edit.png');
        $action1->setField('id');
        $action1->setFk('servidor_id');

        $this->datagrid->addAction($action1);

        $this->datagrid->createModel();

        $panel = new TPanelForm(1000, 500);
        $panel->put($this->form, 0, 0);
        $panel->put($this->datagrid, 0, 50);

        parent::add($panel);
    }

    function onReload() {
        TTransaction::open('pg_ceres');

        $repository = new TRepository('vw_justificativa_acompanhamentoRecord');

        $criteria = new TCriteria;

        //$criteria->add(new TFilter('chefe_id', '=', $_SESSION['servidor_id']));
        $criteria->add(new TFilter('situacaojustificativa', '=', 'AGUARDANDO'));

        $criteria->setProperty('order', 'databatida desc');
        $objects = $repository->load($criteria);

        $this->datagrid->clear();
        if ($objects) {
            foreach ($objects as $object) {
                $object->nome_servidor = substr($object->nome_servidor, 0, 25)."...";
                $this->datagrid->addItem($object);
            }
        }

        TTransaction::close();
        $this->loaded = true;
    }

    function show() {
        $this->onReload();
        parent::show();
    }
}