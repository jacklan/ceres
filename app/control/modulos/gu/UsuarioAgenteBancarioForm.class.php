<?php

ini_set('display_errors', 1);
ini_set('display_startup_erros', 1);
error_reporting(E_ALL);

/**
 * UsuarioAgenteBancarioForm
 *
 * @version    2.0
 * @package    ceres
 * @subpackage app/control/gu
 * @data       05/05/2017     
 * @author     Jackson Meires
 * @copyright  Copyright (c) 2006-2014 Adianti Solutions Ltd. (http://www.adianti.com.br)
 * @license    http://www.adianti.com.br/framework-license
 */
class UsuarioAgenteBancarioForm extends TPage {

    private $form;     // formulario de cadastro

    /*
     * metodo construtor
     * Cria a pagina do formulario
     */

    public function __construct() {
        parent::__construct();

        // instancia um formulario
        $this->form = new \Adianti\Widget\Wrapper\TQuickForm('form_usuario_agente_bancario');
        $this->form->setFormTitle('<font color="red" size="3" face="Arial"><b>Formul&aacute;rio do Usuario Agente Bancario</b></font>');

        // cria os campos do formulario
        $codigo = new THidden('id');
        $agentebancario_id = new \Adianti\Widget\Wrapper\TDBCombo('agentebancario_id', 'pg_ceres', 'AgenteBancarioRecord', 'id', 'nome', 'nome');
        $login = new \Adianti\Widget\Form\TEntry('login');
        $login->setEditable(FALSE);
        $senha = new TPassword('senha');
        $expira = new TCombo('expira');
        $dataexpira = new TDate('dataexpira');
        $ativo = new TCombo('ativo');

        $usuarioalteracao = new THidden('usuarioalteracao');
        $dataalteracao = new THidden('dataalteracao');

        $actionUsuario = new TAction(array($this, 'onChangeActionLoginCPF'));
        $agentebancario_id->setChangeAction($actionUsuario);

        // cria um rotulo para o titulo
        $titulo = new TLabel('<div style="position:floatval; width: 200px;"> <b>* Campo obrigatorio</b></div>');
        $titulo->setFontFace('Arial');
        $titulo->setFontColor('red');
        $titulo->setFontSize(10);

        // cria um vetor com as opcoes da combo
        $items = array();
        $items['S'] = 'S';
        $items['N'] = 'N';

        // adiciona as opcoes na combo
        $expira->addItems($items);
        //coloca o valor padrao no combo
        $expira->setValue('N');

        $ativo->addItems($items);
        //coloca o valor padrao no combo     
        $ativo->setValue('S');

        // define os campos
        $this->form->addQuickField(null, $usuarioalteracao, 10);
        $this->form->addQuickField(null, $dataalteracao, 10);
        $this->form->addQuickField(null, $codigo, 50);
        $this->form->addQuickField('Agente <font color=red><b>*</b></font>', $agentebancario_id, 40);
        $this->form->addQuickField('Login <font color=red><b>*</b></font>', $login, 40);
        $this->form->addQuickField('Senha <font color=red><b>*</b></font>', $senha, 40);
        $this->form->addQuickField('Expira', $expira, 40);
        $this->form->addQuickField('Ativo <font color=red><b>*</b></font>', $ativo, 40);
        $this->form->addQuickField('Data Expira', $dataexpira, 40);
        $this->form->addQuickField(null, $titulo, 50);

        //campos obrigarorios
        $agentebancario_id->addValidation('Agente Bancario', new TRequiredValidator); // required field
        $login->addValidation('Login', new TRequiredValidator); // required field
        $senha->addValidation('Senha', new TRequiredValidator); // required field
        $ativo->addValidation('Ativo', new TRequiredValidator); // required field

        TTransaction::open('pg_ceres');
        if (!empty($_GET['method'])) {
            $action = new \Adianti\Control\TAction(array('UsuarioAgenteBancarioList', 'onSearch'));
            $action->setParameter('nome_agentebancario', (new UsuarioRecord(filter_input(INPUT_GET, 'key')))->login);
        } else {
            $action = new \Adianti\Control\TAction(array('UsuarioAgenteBancarioList', 'onLoad'));
        }
        TTransaction::close(); // finaliza a transacao
        // cria um botao de acao
        $this->form->addQuickAction('Salvar', new TAction(array($this, 'onSave')), 'ico_save.png')->class = 'btn btn-info';
        $this->form->addQuickAction('Voltar', $action, 'ico_datagrid.gif');

        // adiciona o form na pagina
        parent::add($this->form);
    }

    public static function onChangeActionLoginCPF($param) {

        if ($param['agentebancario_id'] && $param['agentebancario_id'] != 0) {
            try {

                TTransaction::open('pg_ceres');

                $repository = new TRepository('AgenteBancarioRecord');

                $criteria = new TCriteria;

                $criteria->add(new TFilter('id', '=', $param['agentebancario_id']));

                $objects = $repository->load($criteria);

                if ($objects) {
                    $objUsuario = new stdClass();
                    foreach ($objects as $object) {
                        $objUsuario->login = $object->cpf;
                    }
                    Adianti\Widget\Form\TForm::sendData('form_usuario_agente_bancario', $objUsuario);
                } else {
                    new TMessage('INFO', 'Nenhum agente encontrado.');
                }
                TTransaction::close();
            } catch (Exception $e) {
                new TMessage('INFO', $e->getMessage());
            }
        }
    }

    /*
     * metodo onSave()
     * Executada quando o usuario clicar no botao salvar do formulario
     */

    function onSave() {
        try {
            // inicia transacao com o banco 'pg_ceres'
            TTransaction::open('pg_ceres');
            // obtem os dados no formulario em um objeto Record da Classe
            $object = $this->form->getData('UsuarioRecord');

            //lanca o default
            $object->usuarioalteracao = $_SESSION['usuario'];
            $object->dataalteracao = date("d/m/Y H:i:s");

            // form validation
            $this->form->validate();

            // armazena o objeto
            $object->store();

            // exibe um dialogo ao usuario
            new TMessage("info", "Registro salvo com sucesso!");

            $param = [];
            $param['nome_agentebancario'] = $object->login;

            TApplication::gotoPage('UsuarioAgenteBancarioList', 'onSearch', $param); // reload
            // finaliza a transacao
            TTransaction::close();
        } catch (Exception $e) { // em caso de exce��o
            // exibe a mensagem gerada pela exce��o
            new TMessage('error', $e->getMessage());
            // desfaz todas altera��es no banco de dados
            TTransaction::rollback();
            $this->form->setData($this->form->getData());
        }
    }

    /*
     * metodo onEdit()
     * Edita os dados de um registro
     */

    function onEdit($param) {

        try {
            if (isset($param['key'])) {

                // get the parameter $key
                $key = $param['key'];

                TTransaction::open('pg_ceres');   // open a transaction with database 'samples'

                $object = new UsuarioRecord($key);        // instantiates object City

                $object->dataexpira = TDate::date2br($object->dataexpira);
                $this->form->setData($object);   // fill the form with the active record data

                TTransaction::close();           // close the transaction
            } else {
                //$this->form->clear();
            }
        } catch (Exception $e) { // in case of exception
            // shows the exception error message
            new TMessage('error', '<b>Error</b> ' . $e->getMessage());
            // undo all pending operations
            TTransaction::rollback();
        }
    }

}
