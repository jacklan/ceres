<?php

/**
 * AgenteBancarioForm
 *
 * @version    2.0
 * @package    ceres
 * @subpackage app/control/gu
 * @data       05/05/2017     
 * @author     Jackson Meires
 * @copyright  Copyright (c) 2006-2014 Adianti Solutions Ltd. (http://www.adianti.com.br)
 * @license    http://www.adianti.com.br/framework-license
 */
include 'app/lib/funcdate.php';

class AgenteBancarioForm extends TPage {

    private $form;     // formulario de cadastro

    /*
     * metodo construtor
     * Cria a pagina do formulario
     */

    public function __construct() {
        parent::__construct();

        // instancia um formulario
        $this->form = new \Adianti\Widget\Wrapper\TQuickForm;
        $this->form->setFormTitle('<font color="red" size="3" face="Arial"><b>Formul&aacute;rio do Agentes Bancarios</b></font>');

        // cria os campos do formulario
        $codigo = new THidden('id');
        $nome = new TEntry('nome');
        $cpf = new \Adianti\Widget\Form\TEntry('cpf');
        $banco_id = new \Adianti\Widget\Wrapper\TDBCombo('banco_id', 'pg_ceres', 'BancoRecord', 'id', 'nome', 'nome');
        $situacao = new TCombo('situacao');

        $usuarioalteracao = new THidden('usuarioalteracao');
        $dataalteracao = new THidden('dataalteracao');

        // cria um vetor com as opcoes da combo
        $items2 = array();
        $items2['ATIVO'] = 'ATIVO';
        $items2['INATIVO'] = 'INATIVO';

        // adiciona as opcoes na combo
        $situacao->addItems($items2);
        //coloca o valor padrao no combo
        $situacao->setValue('ATIVO');

        // cria um rotulo para o titulo
        $obs = new TLabel('<div style="position:floatval; width: 200px;"> <b>* Campo obrigatorio</b></div>');
        $obs->setFontFace('Arial');
        $obs->setFontColor('red');
        $obs->setFontSize(10);

        // define os campos
        $this->form->addQuickField(null, $usuarioalteracao, 10);
        $this->form->addQuickField(null, $dataalteracao, 10);
        $this->form->addQuickField(null, $codigo, 50);
        $this->form->addQuickField('Nome <font color=red><b>*</b></font>', $nome, 40);
        $this->form->addQuickField('CPF <font color=red><b>*</b></font>', $cpf, 40);
        $this->form->addQuickField('Banco <font color=red><b>*</b></font>', $banco_id, 40);
        $this->form->addQuickField('Situação <font color=red><b>*</b></font>', $situacao, 40);
        $this->form->addQuickField(null, $obs, 50);

        //campos obrigarorios
        $nome->addValidation('Nome', new TRequiredValidator); // required field
        $cpf->addValidation('Login', new Adianti\Validator\TCPFValidator()); // required field
        $banco_id->addValidation('Banco', new TRequiredValidator); // required field
        $situacao->addValidation('Situação', new TRequiredValidator); // required field
        // cria um botao de acao
        $this->form->addQuickAction('Salvar', new TAction(array($this, 'onSave')), 'ico_save.png')->class = 'btn btn-info';
        $this->form->addQuickAction('Voltar', new \Adianti\Control\TAction(array('AgenteBancarioList', 'onReload')), 'ico_datagrid.gif');

        // adiciona o form na pagina
        parent::add($this->form);
    }

    /*
     * metodo onSave()
     * Executada quando o usuario clicar no botao salvar do formulario
     */

    function onSave() {
        try {
            // inicia transacao com o banco 'pg_ceres'
            TTransaction::open('pg_ceres');
            // obtem os dados no formulario em um objeto Record da Classe
            $object = $this->form->getData('AgenteBancarioRecord');

            $this->form->validate(); // form validation

            $object->store(); // armazena o objeto
            // exibe um dialogo ao usuario
            new Adianti\Widget\Dialog\TMessage('info', "Registro Salvo com Sucesso!");

            TApplication::gotoPage('AgenteBancarioList', 'onLoad'); // reload

            TTransaction::close();
        } catch (Exception $e) { // em caso de exce��o
            // exibe a mensagem gerada pela exce��o
            new TMessage('error', $e->getMessage());
            // desfaz todas altera��es no banco de dados
            TTransaction::rollback();
            $this->form->setData($this->form->getData());
        }
    }

    /*
     * metodo onEdit()
     * Edita os dados de um registro
     */

    function onEdit($param) {
        try {
            if (isset($param['key'])) {

                // get the parameter $key
                $key = $param['key'];

                TTransaction::open('pg_ceres');   // open a transaction with database 'samples'

                $object = new AgenteBancarioRecord($key);        // instantiates object City

                $this->form->setData($object);   // fill the form with the active record data

                TTransaction::close();           // close the transaction
            } else {
                //$this->form->clear();
            }
        } catch (Exception $e) { // in case of exception
            // shows the exception error message
            new TMessage('error', '<b>Error</b> ' . $e->getMessage());
            // undo all pending operations
            TTransaction::rollback();
        }
    }

}
