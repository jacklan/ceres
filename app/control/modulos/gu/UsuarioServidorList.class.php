<?php
ini_set('display_errors', 1);
ini_set('display_startup_erros', 1);
error_reporting(E_ALL);
/*
 * classe UsuarioServidorList
 * Cadastro de UsuarioServidor: Contem a listagem e o formulario de busca
 */

use Adianti\Database\TFilter1;
use Adianti\Widget\Datagrid\TDatagridTables;

class UsuarioServidorList extends TPage {

    private $form;     // formulario de cadastro
    private $datagrid; // listagem

    /*
     * metodo construtor
     * Cria a pagina, o formulario e a listagem
     */

    public function __construct() {
        parent::__construct();

        // instancia um formulario
        $this->form = new TForm('form_busca_UsuarioServidor');

        // instancia um Panel
        $panel = new TPanelForm(900, 100);

        // adiciona o panel ao formulario
        $this->form->add($panel);

        // cria um rótulo para o título
        $titulo = new TLabel('Listagem de Usu&aacute;rios Servidores');
        $titulo->setFontFace('Arial');
        $titulo->setFontColor('red');
        $titulo->setFontSize(12);

        // adiciona o campo Titulo
        $panel->put($titulo, $panel->getColuna(), $panel->getLinha());

        $nome = new TEntry('nome');
        $nome->setSize(40);
        $nome->setProperty('placeholder', 'Nome ou Matricula');

        // cria um botao de acao (buscar)
        $find_button = new TButton('busca');
        // cria um botao de acao (cadastrar)
        $new_button = new TButton('novo');
        
        //campos obrigarorios
       // $nome->addValidation('Nome ou Login', new TRequiredValidator); // required field

        // define a acao do botao buscar
        $find_button->setAction(new TAction(array($this, 'onSearch')), 'Buscar');

        // define a acao do botao cadastrar
        $new_button->setAction(new TAction(array('UsuarioServidorForm', 'onEdit')), 'Novo');

        // adiciona o campo
        $panel->put(new TLabel('Informe o valor da busca:'), $panel->getColuna(), $panel->getLinha());
        $panel->put($nome, $panel->getColuna(), $panel->getLinha());
        $panel->put($find_button, $panel->getColuna(), $panel->getLinha());
        $panel->put($new_button, $panel->getColuna(), $panel->getLinha());

        // define quais sao os campos do formulario
        $this->form->setFields(array($nome, $new_button, $find_button));

        // instancia objeto DataGrid
        $this->datagrid = new TDatagridTables;

        // instancia as colunas da DataGrid

        $dgnome = new TDataGridColumn('nome', 'Nome', 'left', 1200);
        $dgregional = new TDataGridColumn('nome_regional', 'Regional', 'left', 200);
        $dgmunicipio = new TDataGridColumn('nome_municipio', 'Municipio', 'left', 100);
        $dglogin = new TDataGridColumn('login', 'Login', 'left', 100);

        // adiciona as colunas a DataGrid
        $this->datagrid->addColumn($dgnome);
        $this->datagrid->addColumn($dgregional);
        $this->datagrid->addColumn($dgmunicipio);
        $this->datagrid->addColumn($dglogin);

        // instancia duas acoes da DataGrid
        $action1 = new TDataGridAction(array('UsuarioServidorForm', 'onEdit'));
        $action1->setLabel('Editar');
        $action1->setImage('ico_edit.png');
        $action1->setField('usuario_id');

        $action2 = new TDataGridAction(array($this, 'onDelete'));
        $action2->setLabel('Deletar');
        $action2->setImage('ico_delete.png');
        $action2->setField('usuario_id');

        $action3 = new TDataGridAction(array('UsuarioPerfilDetalhe', 'onReload'));
        $action3->setLabel('Detalhe do Perfil');
        $action3->setImage('ico_clientes.png');
        $action3->setField('usuario_id');
        $action3->setFk('usuario_id');

        // adiciona as acoes a DataGrid
        $this->datagrid->addAction($action1);
        $this->datagrid->addAction($action2);
        $this->datagrid->addAction($action3);

        // cria o modelo da DataGrid, montando sua estrutura
        $this->datagrid->createModel();

        // monta a paina atraves de uma tabela
        $panel = new TPanelForm(700, 500);
        $panel->put($this->form, 0, 0);
        $panel->put($this->datagrid, 150, 100);


        // adiciona a tabela a pagina
        parent::add($panel);
    }

    /*
     * metodo onReload()
     * Carrega a DataGrid com os objetos do banco de dados
     */

    function onSearch($param) {
            // inicia transacao com o banco 'pg_ceres'
            TTransaction::open('pg_ceres');
            // form validation
            $this->form->validate();
            
            // instancia um repositorio da Classe
            $repository = new TRepository('vw_usuarioRecord');
            $dataForm = $this->form->getData();

            //obtem os dados do formulario de busca
            $dados = ($dataForm->nome != null ? $dataForm->nome : $param['nome']);
            // cria um criterio de selecao, ordenado pelo id
            $criteria = new TCriteria;
            $criteria->setProperty('order', 'nome');
            //filtra pelo campo selecionado pelo usuario
            $criteria->add(new TFilter('tipo', '!=', 'LATICINIO'));
            $criteria->add(new TFilter('empresa_id', '=', $_SESSION['empresa_id']));
            //verifica se o usuario preencheu o formulario
            if ($dados) {
                if (is_numeric($dados)) {
                    $criteria->add(new TFilter('matricula', '=', $dados));
                } else {
                    //filtra pelo campo selecionado pelo campo ignore case
                    $criteria->add(new TFilter1('special_like(' . 'nome' . ",'" . $dados . "')"));
                }
            }

            // carrega os objetos de acordo com o criterio
            $object = $repository->load($criteria);

            $this->datagrid->clear();
            if ($object) {
                // percorre os objetos retornados
                foreach ($object as $cadastro) {
                    // adiciona o objeto na DataGrid
                    $this->datagrid->addItem($cadastro);
                }
            }
            // finaliza a transacao
            TTransaction::close();
            $this->loaded = true;
       
    }

    /*
     * metodo onDelete()
     * Executada quando o usuario clicar no botao excluir da datagrid
     * Pergunta ao usuario se deseja realmente excluir um registro
     */

    function onDelete($param) {
        // obtem o parametro $key
        $key = $param['key'];

        // define duas acoes
        $action1 = new TAction(array($this, 'Delete'));

        // define os parametros de cada acao
        $action1->setParameter('key', $key);

        // exibe um dialogo ao usuario
        new TQuestion('Deseja realmente excluir o registro ?', $action1);
    }

    /*
     * metodo Delete()
     * Exclui um registro
     */

    function Delete($param) {
        // obtem o parametro $key
        $key = $param['key'];
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');

        // instanicia objeto Record
        $cadastro = new UsuarioRecord($key);

        try {
            // deleta objeto do banco de dados
            $cadastro->delete();

            // exibe um dialogo ao usuario
            new TMessage("info", "Registro deletado com sucesso!");

            // finaliza a transacao
            TTransaction::close();
        } catch (Exception $e) { // em caso de exce��o
            // exibe a mensagem gerada pela exce��o
            new TMessage('error', $e->getMessage());
            // desfaz todas altera��es no banco de dados
            TTransaction::rollback();
        }

        // re-carrega a datagrid
        $this->onLoad();
    }

    /*
     * metodo onLoad()
     * Carrega a DataGrid com os objetos do banco de dados
     */

    function onLoad() {
        
    }

}
