<?php

//ini_set('display_errors', 1);
//ini_set('display_startup_erros', 1);
//error_reporting(E_ALL);


/**
 * ModuloForm
 *
 * @version    2.0
 * @package    ceres
 * @subpackage app/control/gu
 * @author     Jackson Meires
 * @data       31/05/2017
 * @copyright  Copyright (c) 2006-2014 Adianti Solutions Ltd. (http://www.adianti.com.br)
 * @license    http://www.adianti.com.br/framework-license
 */
include_once 'app/lib/funcdate.php';

class ModuloForm extends TPage {

    private $form;     // formulario de cadastro

    /*
     * metodo construtor
     * Cria a pagina do formulario
     */

    public function __construct() {
        parent::__construct();

        // instancia um formulario
        $this->form = new TQuickForm;
        $this->form->class = 'form_Modulo';
        $this->form->setFormTitle('<font color="red" size="3" face="Arial"><b>Formul&aacute;rio de M&oacute;dulo</b></font>');

        // cria os campos do formulario
        $codigo = new THidden('id');
        $nome = new TEntry('nome');
        $versao = new Adianti\Widget\Form\TCombo('versao');

        $usuarioalteracao = new THidden('usuarioalteracao');
        $dataalteracao = new THidden('dataalteracao');

        $items[1] = 1;
        $items[2] = 2;
        $versao->addItems($items);
        $versao->setValue(2);

        // cria um rotulo para o titulo
        $obs = new TLabel('<div style="position:floatval; width: 200px;"> <b>* Campo obrigatorio</b></div>');
        $obs->setFontFace('Arial');
        $obs->setFontColor('red');
        $obs->setFontSize(10);

        $nome->setProperty('style', 'text-transform: uppercase');

        //campos obrigarorios
        $nome->addValidation('Nome', new TRequiredValidator); // required field
        $versao->addValidation('Versão do Sistema', new TRequiredValidator); // required field
        // define os campos
        $this->form->addQuickField(null, $codigo, 10);
        $this->form->addQuickField('Nome <font color=red><b>*</b></font>', $nome, 50);
        $this->form->addQuickField('Versão <font color=red><b>*</b></font>', $versao, 10);
        $this->form->addQuickField(null, $usuarioalteracao, 10);
        $this->form->addQuickField(null, $dataalteracao, 10);
        $this->form->addQuickField(null, $obs, 50);

        $nome->setProperty('placeholder', 'Preencha o nome');

        // cria um botao de acao
        $this->form->addQuickAction('Salvar', new TAction(array($this, 'onSave')), 'ico_save.png')->class = 'btn btn-info';
        $this->form->addQuickAction('Voltar', new TAction(array('ModuloList', 'onReload')), 'ico_datagrid.gif');

        // adiciona a tabela a pagina
        parent::add($this->form);
    }

    /*
     * metodo onSave()
     * Executada quando o usuario clicar no botao salvar do formulario
     */

    function onSave() {
        try {
            // inicia transacao com o banco 'pg_ceres'
            TTransaction::open('pg_ceres');
            // obtem os dados no formulario em um objeto CarroRecord
            $object = $this->form->getData('ModuloRecord');

            //lanca o default
            $object->usuarioalteracao = $_SESSION['usuario'];
            $object->dataalteracao = date("d/m/Y H:i:s");

            // form validation
            $this->form->validate();

            $object->store(); // armazena o objeto

            $this->form->setData($object);   // fill the form with the active record data
            // exibe mensagem de erro
            new TMessage("info", "Registro salvo com sucesso!");
            TApplication::gotoPage('ModuloList', 'onReload'); // reload

            TTransaction::close();           // close the transaction
        } catch (Exception $e) { // em caso de exce��o
            // exibe a mensagem gerada pela exce��o
            new TMessage('error', $e->getMessage());
            $this->form->setData($this->form->getData());   // fill the form with the active record data
            // desfaz todas altera��es no banco de dados
            TTransaction::rollback();
        }
    }

    /*
     * metodo onEdit()
     * Edita os dados de um registro
     */

    function onEdit($param) {

        try {
            if (isset($param['key'])) {

                // get the parameter $key
                $key = $param['key'];

                TTransaction::open('pg_ceres');   // open a transaction with database 'samples'

                $object = new ModuloRecord($key);        // instantiates object City

                $this->form->setData($object);   // fill the form with the active record data

                TTransaction::close();           // close the transaction
            } else {
                $this->form->clear();
            }
        } catch (Exception $e) { // in case of exception
            // shows the exception error message
            new TMessage('error', '<b>Error</b> ' . $e->getMessage());
            // undo all pending operations
            TTransaction::rollback();
        }
    }

}
