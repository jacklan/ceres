<?php

//ini_set('display_errors', 1);
//ini_set('display_startup_erros', 1);
//error_reporting(E_ALL);

/*
 * classe PerfilPaginaDetalhe
 * Cadastro de PerfilPagina: Contem a listagem e o formulario de busca
 */

 use Adianti\Database\TFilter1;
use Adianti\Widget\Datagrid\TDatagridTables;

class PerfilUsuariosCadastradosDetalhe extends TPage
{
    private $form;     // formulario de cadastro
    private $datagrid; // listagem

    /*
     * metodo construtor
     * Cria a pagina, o formulario e a listagem
     */
    public function __construct()
    {
        parent::__construct();

        $this->form = new TQuickForm;
        $this->form->class = 'form_Perfil';
        $this->form->setFormTitle('<font color="red" size="3" face="Arial"><b>Usu&aacute;rios com Permiss&atilde;o</b></font>');
        
        // cria os campos do formulario
        $usuarioalteracao = new THidden('usuarioalteracao');
        $dataalteracao = new THidden('dataalteracao');

        //cria a colecao da tabela estrangeira do Cabecalho
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');
        // instancia objeto da Classe Record
        $cadastro = new PerfilRecord($_GET['fk']);
        // cria um criterio de selecao, ordenado pelo id do perfil
        //adiciona os objetos na tela com as informacoes do perfil
        if ($cadastro){
        	$nomeperfil = new TLabel($cadastro->nome);
        	$nomemodulo = new TLabel($cadastro->nome_modulo);
        }
        // finaliza a transacao
        TTransaction::close();
        
        //coloca o valor do relacionamento
        $perfil_id = new THidden('perfil_id');
        $perfil_id->setValue($_GET['fk']);
        
        // define os tamanhos dos campos
        $this->form->addQuickField(null, $usuarioalteracao, 10);
        $this->form->addQuickField(null, $dataalteracao, 10);
        $this->form->addQuickField("Perfil:", $nomeperfil, 700);
        $this->form->addQuickField("M&oacute;dulo:", $nomemodulo, 700);
        
        
	    $this->form->addQuickAction('Voltar', new TAction(array('PerfilList', 'onReload')), 'ico_datagrid.gif');
     
        // instancia objeto DataGrid
        $this->datagrid = new TDatagridTables;
     
        // instancia as colunas da DataGrid
        $dgnome     = new TDataGridColumn('nome_servidor',    'Nome',    'left',800);
        $dsituacao     = new TDataGridColumn('situacao_atividade',    'Situacao',    'left', 200);
        $dgempresa     = new TDataGridColumn('nome_empresa',    'Empresa',    'left', 800);

        
        // adiciona as colunas a DataGrid
        $this->datagrid->addColumn($dgnome);
        $this->datagrid->addColumn($dgempresa);
        $this->datagrid->addColumn($dsituacao);
        
        
        $action2 = new TDataGridAction(array($this, 'onDelete'));
        $action2->setLabel('Deletar');
        $action2->setImage('ico_delete.png');
        $action2->setField('usuarioperfil_id');
        $action2->setFk('perfil_id');
        
        // adiciona as acoes a DataGrid
        //$this->datagrid->addAction($action1);
        $this->datagrid->addAction($action2);
        
        // cria o modelo da DataGrid, montando sua estrutura
        $this->datagrid->createModel();

        // monta a paina atraves de uma tabela
        $panel = new TPanelForm(700, 500);
        $panel->put($this->form, 0, 0);
        $panel->put($this->datagrid, 150, 170);

        // adiciona a tabela a pagina
        parent::add($panel);
    }
    
    /*
     * metodo onReload()
     * Carrega a DataGrid com os objetos do banco de dados
     */
    function onReload()
    {
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');

        // instancia um repositorio da Classe
        $repository = new TRepository('vw_perfilusuariocadastradoRecord');

        // cria um criterio de selecao
        $criteria = new TCriteria;
		
        //filtra pelo campo selecionado pelo usuario
        $criteria->add(new TFilter('perfil_id', '=', $_GET['fk']));
		

        // carrega os objetos de acordo com o criterio
        $cadastros = $repository->load($criteria);

        $this->datagrid->clear();
        if ($cadastros)
        {
            // percorre os objetos retornados
            foreach ($cadastros as $cadastro)
            {
                // adiciona o objeto na DataGrid
                $this->datagrid->addItem($cadastro);
            }
        }

        // finaliza a transacao
        TTransaction::close();
        $this->loaded = true;
    }
    
    
    /*
     * metodo onDelete()
     * Executada quando o usuario clicar no botao excluir da datagrid
     * Pergunta ao usuario se deseja realmente excluir um registro
     */
    function onDelete($param)
    {
        // obtem o parametro $key
        $key=$param['key'];
        
        // define duas acoes
        $action1 = new TAction(array($this, 'Delete'));
        
        // define os parametros de cada acao
        $action1->setParameter('key', $key);

        //encaminha a chave estrangeira
        $action1->setParameter('fk', $_GET['fk']);
		
        // exibe um dialogo ao usuario
        new TQuestion('Deseja realmente excluir o registro ?', $action1);
		
    }
    
    /*
     * metodo Delete()
     * Exclui um registro
     */
    function Delete($param)
    {
        // obtem o parametro $key
        $key=$param['key'];
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');

        // instanicia objeto Record
        $cadastro = new UsuarioPerfilRecord($key);

        try{
            // deleta objeto do banco de dados
            $cadastro->delete();
			
			// exibe um dialogo ao usuario
            new TMessage("info", "Registro deletado com sucesso!");

            // finaliza a transacao
            TTransaction::close();
        }
        catch (Exception $e) // em caso de exce��o
        {
            // exibe a mensagem gerada pela exce��o
            new TMessage('error', $e->getMessage());
            // desfaz todas altera��es no banco de dados
            TTransaction::rollback();
        }

        // re-carrega a datagrid
        $this->onReload();
		
    }
    
    /*
     * metodo show()
     * Exibe a pagina
     */
    function show()
    {
		
        // carrega os dados no datagrid
        $this->onReload();
        //chama o metodo show da super classe
        parent::show();

    }

}
?>