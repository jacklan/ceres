<?php

/*
 * classe PaginaDependenciaDetalhe
 * Cadastro de PaginaDependencia: Contem a listagem e o formulario de busca
 */
use Adianti\Database\TFilter1;
use Adianti\Widget\Datagrid\TDatagridTables;

ini_set('display_errors', 1);
ini_set('display_startup_erros', 1);
error_reporting(E_ALL);

class PaginaDependenciaDetalhe extends TPage
{
    private $form;     // formulario de cadastro
    private $datagrid; // listagem

    /*
     * metodo construtor
     * Cria a pagina, o formulario e a listagem
     */
    public function __construct()
    {
        parent::__construct();

		// instancia um formulario
		$this->form = new TQuickForm;
        $this->form->class = 'form_PaginaDependencia';
        $this->form->setFormTitle('<font color="red" size="3" face="Arial"><b>Detalhe Pagina Dependencia</b></font>');
		
        // cria os campos do formulario
        $codigo     = new THidden('id');
        $codigo->setEditable(false);
        $arquivo = new TEntry('arquivo');

        //cria a colecao da tabela estrangeira do Cabecalho
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');
        // instancia objeto da Classe Record
        $cadastro = new PaginaRecord( filter_input(INPUT_GET, 'fk') );
        // cria um criterio de selecao, ordenado pelo id do perfil
        //adiciona os objetos na tela com as informacoes do perfil
        if ($cadastro){
            $nome_pagina = new TLabel($cadastro->nome);
            $nome_arquivo = new TLabel($cadastro->arquivo);
        }
        // finaliza a transacao
        TTransaction::close();

        //coloca o valor do relacionamento
        $pagina_id = new THidden('pagina_id');
        $pagina_id->setValue( filter_input(INPUT_GET, 'fk') );

        // define os campos
		$this->form->addQuickField(null, $pagina_id, 1);
		$this->form->addQuickField('Codigo', $codigo, 10);
		$this->form->addQuickField('Arquivo <font color=red><b>*</b></font>', $arquivo, 40);
		
		 // create an action (Experiencia)
        $action = new TAction(array( $this, 'onSave'));
        $action->setParameter('fk', '' . filter_input(INPUT_GET, 'fk') . '');
        $action->setParameter('key', '' . filter_input(INPUT_GET, 'key') . '');
		
		// cria um botao de acao
		$this->form->addQuickAction('Salvar', $action, 'ico_save.png');
		$this->form->addQuickAction('Voltar', new TAction(array('PaginaList', 'onReload')), 'ico_datagrid.gif');
        
        // instancia objeto DataGrid
        $this->datagrid = new TDatagridTables;
     
        // instancia as colunas da DataGrid
        $dgcodigo   = new TDataGridColumn('id',  'C&oacute;digo',  'center', 70);
        $dgarquivo     = new TDataGridColumn('arquivo',    'Arquivo',    'left', 1200);

        // adiciona as colunas a DataGrid
        //$this->datagrid->addColumn($dgcodigo);
        $this->datagrid->addColumn($dgarquivo);
        
        // instancia duas acoes da DataGrid
        $action1 = new TDataGridAction(array($this, 'onEdit'));
        $action1->setLabel('Editar');
        $action1->setImage('ico_edit.png');
        $action1->setField('id');
        $action1->setFk('pagina_id');
        
        $action2 = new TDataGridAction(array($this, 'onDelete'));
        $action2->setLabel('Deletar');
        $action2->setImage('ico_delete.png');
        $action2->setField('id');
        $action2->setFk('pagina_id');
        
        // adiciona as acoes a DataGrid
        $this->datagrid->addAction($action1);
        $this->datagrid->addAction($action2);
        
        // cria o modelo da DataGrid, montando sua estrutura
        $this->datagrid->createModel();

        // monta a paina atraves de uma tabela
        $panel = new TPanelForm(700, 500);
        $panel->put($this->form, 0, 0);
        $panel->put($this->datagrid, 150, 170);


        // adiciona a tabela a pagina
        parent::add($panel);
    }
    
    /*
     * metodo onReload()
     * Carrega a DataGrid com os objetos do banco de dados
     */
    function onReload()
    {
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');

        // instancia um repositorio da Classe
        $repository = new TRepository('PaginaDependenciaRecord');

        // cria um criterio de selecao
        $criteria = new TCriteria;

        //filtra pelo campo selecionado pelo usuario
        $criteria->add( new TFilter( 'pagina_id', '=', filter_input(INPUT_GET, 'fk') ) );

        // carrega os objetos de acordo com o criterio
        $cadastros = $repository->load($criteria);

        $this->datagrid->clear();
        if ($cadastros)
        {
            // percorre os objetos retornados
            foreach ($cadastros as $cadastro)
            {
                // adiciona o objeto na DataGrid
                $this->datagrid->addItem($cadastro);
            }
        }


        // finaliza a transacao
        TTransaction::close();
        $this->loaded = true;
    }
    
    /*
     * metodo onDelete()
     * Executada quando o usuario clicar no botao excluir da datagrid
     * Pergunta ao usuario se deseja realmente excluir um registro
     */
    function onDelete($param)
    {
        // obtem o parametro $key
        $key=$param['key'];
        
        // define duas acoes
        $action1 = new TAction(array($this, 'Delete'));
        
        
        // define os parametros de cada acao
        $action1->setParameter('key', $key);
       
        //encaminha a chave estrangeira
        $action1->setParameter('fk', $_GET['fk']);
        // exibe um dialogo ao usuario
		
        new TQuestion('Deseja realmente excluir o registro ?', $action1 );
    }
    
    /*
     * metodo Delete()
     * Exclui um registro
     */
    function Delete($param)
    {
        // obtem o parametro $key
        $key=$param['key'];
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');

        // instanicia objeto Record
        $cadastro = new PaginaDependenciaRecord($key);

        try{
            // deleta objeto do banco de dados
            $cadastro->delete();
			new TMessage("info", "Registro deletado com sucesso!");
            // finaliza a transacao
            TTransaction::close();
        }
        catch (Exception $e) // em caso de exce��o
        {
            // exibe a mensagem gerada pela exce��o
            new TMessage('error', $e->getMessage());
            // desfaz todas altera��es no banco de dados
            TTransaction::rollback();
        }

        // re-carrega a datagrid
        $this->onReload();
        // exibe mensagem de sucesso
        //new TMessage('info', "Registro Excluido com sucesso");
    }
    
    /*
     * metodo show()
     * Exibe a pagina
     */
    function show()
    {
        // carrega os dados no datagrid
        $this->onReload();
        //chama o metodo show da super classe
        parent::show();

    }


    /*
     * metodo onSave()
     * Executada quando o usuario clicar no botao salvar do formulario
     */
    function onSave()
    {
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');
        // obtem os dados no formulario em um objeto Record da Classe
        $cadastro = $this->form->getData('PaginaDependenciaRecord');
		
        //antes de armazenar verifica se algum campo eh requerido e nao foi informado
        $dados = $cadastro->toArray();
	
        $msg = '';
        $icone = 'info';

        if (empty ($dados['pagina_id'])){
           $msg .= 'O Pagina deve ser informada.<br />';
        }

        if (empty ($dados['arquivo'])){
           $msg .= 'O Arquivo Dependente deve ser informado.<br />';
        }

        try{

            if ($msg == ''){
              // armazena o objeto
              $cadastro->store();
              $msg = 'Dados armazenados com sucesso';

              // finaliza a transacao
              TTransaction::close();

            }else{
                $icone = 'error';
            }

            if ($icone == 'error'){
				
                // exibe mensagem de erro
                new TMessage($icone, $msg);
				
				$this->form->setData($cadastro); 
				$this->onReload();
				
            }else{
				
				$param = array();
				$param ['fk'] = $dados['pagina_id'];
				
                //chama o formulario com o grid
                // exibe um dialogo ao usuario
                new TMessage("info", "Registro salvo com sucesso!");
				TApplication::gotoPage('PaginaDependenciaDetalhe','onReload', $param); // reload
            
			}
        }
        catch (Exception $e) // em caso de exce��o
        {
            // exibe a mensagem gerada pela exce��o
            new TMessage('error', $e->getMessage());
			
            // desfaz todas altera��es no banco de dados
            TTransaction::rollback();
			
			$this->form->setData($cadastro);   // fill the form with the active record data
			$this->onReload();
			
        }

    }


    /*
     * metodo onEdit()
     * Edita os dados de um registro
     */
    function onEdit($param)
    {
		try {
            if (isset($param['key'])) {
				
                // get the parameter $key
                $key = $param['key'];

                TTransaction::open('pg_ceres');   // open a transaction with database 'samples'
				
                $object = new PaginaDependenciaRecord($key);        // instantiates object City
				
                $this->form->setData($object);   // fill the form with the active record data
				
                TTransaction::close();           // close the transaction
				
            } else {
                $this->form->clear();
            }
        } catch (Exception $e) { // in case of exception
            // shows the exception error message
            new TMessage('error', '<b>Error</b> ' . $e->getMessage());
            // undo all pending operations
            TTransaction::rollback();
        }	
        
    }

     


}
?>