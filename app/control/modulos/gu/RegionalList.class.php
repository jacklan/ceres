<?php
/*
 * classe RegionalList
 * Cadastro de Regional: Contem a listagem e o formulario de busca
 */

use Adianti\Database\TFilter1;
use Adianti\Widget\Datagrid\TDatagridTables;

class RegionalList extends TPage
{
    private $form;     // formulario de cadastro
    private $datagrid; // listagem

    /*
     * metodo construtor
     * Cria a pagina, o formulario e a listagem
     */
    public function __construct()
    {
        parent::__construct();

        $this->form = new TForm('form_busca_Regional');

        $panel = new TPanelForm(900, 100);

        $this->form->add($panel);

        $titulo = new TLabel('Listagem de Regional');
        $titulo->setFontFace('Arial');
        $titulo->setFontColor('red');
        $titulo->setFontSize(12);

        $panel->put($titulo, $panel->getColuna(), $panel->getLinha());

        $opcao = new TCombo('opcao');

        $items = array();
        $items['nome'] = 'Nome';

        $opcao->addItems($items);
        $opcao->setValue('nome');
        $opcao->setSize(40);

        $nome = new TEntry('nome');
        $nome->setSize(40);

        $find_button = new TButton('busca');
        $new_button = new TButton('novo');

        $find_button->setAction(new TAction(array($this, 'onSearch')), 'Buscar');

        $new_button->setAction(new TAction(array("RegionalForm", 'onEdit')), 'Novo');

        $panel->putCampo(null, 'Selecione o campo:', 0, 0);
        $panel->put($opcao, $panel->getColuna(), $panel->getLinha());
        $panel->put(new TLabel('Informe o valor da busca:'), $panel->getColuna(), $panel->getLinha());
        $panel->put($nome, $panel->getColuna(), $panel->getLinha());
        $panel->put($find_button, $panel->getColuna(), $panel->getLinha());
        $panel->put($new_button, $panel->getColuna(), $panel->getLinha());

        $this->form->setFields(array($nome, $find_button, $new_button));

        $this->datagrid = new TDatagridTables;

        $dgnome = new TDataGridColumn('nome', 'Nome', 'left', 1200);
        $dgnome_chefe = new TDataGridColumn('nome_chefe', 'Chefe', 'left', 100);
        $dgnome_tutor = new TDataGridColumn('nome_tutor', 'Tutor', 'left', 100);
        $dgcor = new TDataGridColumn('cor', 'Cor Mapa', 'left', 100);

        $this->datagrid->addColumn($dgnome);
        $this->datagrid->addColumn($dgnome_chefe);
        $this->datagrid->addColumn($dgnome_tutor);
        $this->datagrid->addColumn($dgcor);

        $action1 = new TDataGridAction(array('RegionalForm', 'onEdit'));
        $action1->setLabel('Editar');
        $action1->setImage('ico_edit.png');
        $action1->setField('id');

        $action2 = new TDataGridAction(array($this, 'onDelete'));
        $action2->setLabel('Deletar');
        $action2->setImage('ico_delete.png');
        $action2->setField('id');

        $action3 = new TDataGridAction(array('GerenteRegionalDetalhe', 'onReload'));
        $action3->setLabel('Gerente regional');
        $action3->setImage('ico_atividades.png');
        $action3->setField('id');
        $action3->setFk('id');

        $this->datagrid->addAction($action1);
        $this->datagrid->addAction($action2);
        $this->datagrid->addAction($action3);

        $this->datagrid->createModel();

        $panel = new TPanelForm(700, 500);
        $panel->put($this->form, 0, 0);
        $panel->put($this->datagrid, 150, 100);


        // adiciona a tabela a pagina
        parent::add($panel);
    }

    /*
     * metodo onReload()
     * Carrega a DataGrid com os objetos do banco de dados
     */
    function onReload()
    {
        TTransaction::open('pg_ceres');

        $repository = new TRepository('RegionalRecord');

        $criteria = new TCriteria;

        $criteria->add(new TFilter('empresa_id', '=', $_SESSION['empresa_id']));

        $criteria->setProperty('order', 'nome');

        $cadastros = $repository->load($criteria);

        $this->datagrid->clear();
        if ($cadastros) {
            foreach ($cadastros as $cadastro) {
                $this->datagrid->addItem($cadastro);
            }
        }

        TTransaction::close();
        $this->loaded = true;
    }

    /*
 * metodo onSearch()
 * Carrega a DataGrid com os objetos do banco de dados
 */
    function onSearch()
    {
        TTransaction::open('pg_ceres');

        $repository = new TRepository('RegionalRecord');

        $criteria = new TCriteria;
        $criteria->setProperty('order', 'nome');

        $campo = $this->form->getFieldData('opcao');
        $dados = $this->form->getFieldData('nome');

        if (($dados) && ($_GET['method'] != '')) {
            if (is_numeric($dados)) {
                $operador = '=';
                $valor = strtoupper($dados);
            } else {
                $operador = 'like';
                $valor = strtoupper("%{$dados}%");
            }
            $criteria->add(new TFilter($campo, $operador, $valor));
        }

        $objects = $repository->load($criteria);

        $this->datagrid->clear();
        if ($objects) {
            foreach ($objects as $object) {
                $this->datagrid->addItem($object);
            }
        }


        TTransaction::close();
        $this->loaded = true;
    }

    /*
     * metodo onDelete()
     * Executada quando o usuario clicar no botao excluir da datagrid
     * Pergunta ao usuario se deseja realmente excluir um registro
     */
    function onDelete($param)
    {
        $key = $param['key'];

        $action1 = new TAction(array($this, 'Delete'));

        $action1->setParameter('key', $key);

        new TQuestion('Deseja realmente excluir o registro ?', $action1);
    }

    /*
     * metodo Delete()
     * Exclui um registro
     */
    function Delete($param)
    {
        try {
            $key = $param['key'];
            TTransaction::open('pg_ceres');

            $object = new RegionalRecord($key);

            $object->delete();
            new TMessage("info", "Registro deletado com sucesso!");

            TTransaction::close();
        } catch (Exception $e) // em caso de exce��o
        {
            new TMessage('error', $e->getMessage());
            TTransaction::rollback();
        }

        $this->onReload();

    }

    /*
     * metodo show()
     * Exibe a pagina
     */
    function show()
    {
        $this->onReload();
        parent::show();

    }
}
