<?php

ini_set('display_errors', 1);
ini_set('display_startup_erros', 1);
error_reporting(E_ALL);

/**
 * AgenteBancarioList
 *
 * @version    2.0
 * @package    ceres
 * @subpackage app/control/gu
 * @data       05/05/2017     
 * @author     Jackson Meires
 * @copyright  Copyright (c) 2006-2014 Adianti Solutions Ltd. (http://www.adianti.com.br)
 * @license    http://www.adianti.com.br/framework-license
 */
use Adianti\Database\TFilter1;
use Adianti\Widget\Datagrid\TDatagridTables;

class AgenteBancarioList extends TPage {

    private $form;     // formulario de cadastro
    private $datagrid; // listagem

    /*
     * metodo construtor
     * Cria a pagina, o formulario e a listagem
     */

    public function __construct() {
        parent::__construct();

        // instancia um formulario
        $this->form = new TForm('form');

        // instancia um Panel
        $panel = new TPanelForm(900, 100);

        // adiciona o panel ao formulario
        $this->form->add($panel);

        // cria um rótulo para o título
        $titulo = new TLabel('Listagem de Agentes Bancarios');
        $titulo->setFontFace('Arial');
        $titulo->setFontColor('red');
        $titulo->setFontSize(12);

        // adiciona o campo Titulo
        $panel->put($titulo, $panel->getColuna(), $panel->getLinha());

        $nome = new TEntry('nome');
        $nome->setSize(40);
        $nome->setProperty('placeholder', 'Nome ou CPF');

        // cria um botao de acao (buscar)
        $find_button = new TButton('busca');
        // cria um botao de acao (cadastrar)
        $new_button = new TButton('novo');

        // define a acao do botao buscar
        $find_button->setAction(new TAction(array($this, 'onSearch')), 'Buscar');

        // define a acao do botao cadastrar
        $new_button->setAction(new TAction(array('AgenteBancarioForm', 'onEdit')), 'Novo');

        // adiciona o campo
        $panel->put(new TLabel('Informe o valor da busca:'), $panel->getColuna(), $panel->getLinha());
        $panel->put($nome, $panel->getColuna(), $panel->getLinha());
        $panel->put($find_button, $panel->getColuna(), $panel->getLinha());
        $panel->put($new_button, $panel->getColuna(), $panel->getLinha());

        // define quais sao os campos do formulario
        $this->form->setFields(array($nome, $new_button, $find_button));

        // instancia objeto DataGrid
        $this->datagrid = new TDatagridTables;

        // instancia as colunas da DataGrid
        $dgnome = new TDataGridColumn('nome', 'Nome', 'left', 800);
        $dgcpf = new TDataGridColumn('cpf', 'CPF', 'left', 200);
        $dgsituacao = new TDataGridColumn('situacao', 'Situação', 'left', 100);
        $dgnome_banco = new TDataGridColumn('nome_banco', 'Banco', 'left', 100);

        // adiciona as colunas a DataGrid
        $this->datagrid->addColumn($dgnome);
        $this->datagrid->addColumn($dgcpf);
        $this->datagrid->addColumn($dgsituacao);
        $this->datagrid->addColumn($dgnome_banco);

        // instancia duas acoes da DataGrid
        $action1 = new TDataGridAction(array('AgenteBancarioForm', 'onEdit'));
        $action1->setLabel('Editar');
        $action1->setImage('ico_edit.png');
        $action1->setField('id');

        $action2 = new TDataGridAction(array($this, 'onDelete'));
        $action2->setLabel('Deletar');
        $action2->setImage('ico_delete.png');
        $action2->setField('id');

        // adiciona as acoes a DataGrid
        $this->datagrid->addAction($action1);
        $this->datagrid->addAction($action2);

        // cria o modelo da DataGrid, montando sua estrutura
        $this->datagrid->createModel();

        // monta a paina atraves de uma tabela
        $panel = new TPanelForm(700, 500);
        $panel->put($this->form, 0, 0);
        $panel->put($this->datagrid, 150, 100);

        // adiciona a tabela a pagina
        parent::add($panel);
    }

    /*
     * metodo onReload()
     * Carrega a DataGrid com os objetos do banco de dados
     */

    function onSearch($param) {
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');

        // instancia um repositorio da Classe
        $repository = new TRepository('AgenteBancarioRecord');
        //obtem os dados do formulario de busca
        $dados = ($this->form->getFieldData('nome') != null ? $this->form->getFieldData('nome') : $param['nome']);

        // cria um criterio de selecao, ordenado pelo id
        $criteria = new \Adianti\Database\TCriteria();
        //filtra pelo campo selecionado pelo usuario
        //  $criteria->add(new TFilter('agentebancario_id', 'is not',null));

        $criteria->setProperty('order', 'nome');
        //verifica se o usuario preencheu o formulario
        if ($dados) {
            if (is_numeric($dados)) {
                $criteria->add(new TFilter('cpf', '=', $dados));
            } else {
                //filtra pelo campo selecionado pelo campo ignore case
                $criteria->add(new TFilter1('special_like(' . 'nome' . ",'" . $dados . "')"));
            }
        }

        // carrega os objetos de acordo com o criterio
        $cadastros = $repository->load($criteria);

        $this->datagrid->clear();
        if ($cadastros) {
            // percorre os objetos retornados
            foreach ($cadastros as $cadastro) {
                // adiciona o objeto na DataGrid
                $this->datagrid->addItem($cadastro);
            }
        }
        // finaliza a transacao
        TTransaction::close();
        $this->loaded = true;
    }

    /*
     * metodo onDelete()
     * Executada quando o usuario clicar no botao excluir da datagrid
     * Pergunta ao usuario se deseja realmente excluir um registro
     */

    function onDelete($param) {
        // obtem o parametro $key
        $key = $param['key'];

        // define duas acoes
        $action1 = new TAction(array($this, 'Delete'));

        // define os parametros de cada acao
        $action1->setParameter('key', $key);

        // exibe um dialogo ao usuario
        new TQuestion('Deseja realmente excluir o registro ?', $action1);
    }

    /*
     * metodo Delete()
     * Exclui um registro
     */

    function Delete($param) {
        try {
            // obtem o parametro $key
            $key = $param['key'];
            // inicia transacao com o banco 'pg_ceres'
            TTransaction::open('pg_ceres');

            // instanicia objeto Record
            $object = new AgenteBancarioRecord($key);

            // deleta objeto do banco de dados
            $object->delete();

            // exibe um dialogo ao usuario
         //   new TMessage("info", "Registro deletado com sucesso!");

            // finaliza a transacao
            TTransaction::close();
        } catch (Exception $e) { // em caso de exce��o
            // exibe a mensagem gerada pela exce��o
            new TMessage('error', $e->getMessage());
            // desfaz todas altera��es no banco de dados
            TTransaction::rollback();
        }

        // re-carrega a datagrid
        $this->onReload();
    }

    /*
     * metodo onLoad()
     * Carrega a DataGrid com os objetos do banco de dados
     */

    function onLoad() {
        
    }
    /*
     * metodo onReload()
     * Carrega a DataGrid com os objetos do banco de dados
     */

    function onReload() {
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');

        // instancia um repositorio da Classe
        $repository = new TRepository('AgenteBancarioRecord');

        // cria um criterio de selecao, ordenado pelo id
        $criteria = new \Adianti\Database\TCriteria();
        //filtra pelo campo selecionado pelo usuario
        //$criteria->add(new TFilter('agentebancario_id', 'is not', null));
        $criteria->setProperty('order', 'nome');
        // carrega os objetos de acordo com o criterio
        $objects = $repository->load($criteria);

        $this->datagrid->clear();
        if ($objects) {
            // percorre os objetos retornados
            foreach ($objects as $object) {
                // adiciona o objeto na DataGrid
                $this->datagrid->addItem($object);
            }
        }
        // finaliza a transacao
        TTransaction::close();
        $this->loaded = true;
    }

    function show() {
        $this->onReload();
        parent::show();
    }

}
