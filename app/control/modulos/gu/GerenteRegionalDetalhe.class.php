<?php
/*
 * classe Gerenteregional
 * Cadastro de ServidorUnidade: Contem a listagem e o formulario de busca
 */

use Adianti\Database\TFilter1;
use Adianti\Widget\Datagrid\TDatagridTables;

class GerenteRegionalDetalhe extends TPage
{
    private $form;     // formulario de cadastro
    private $datagrid; // listagem

    /*
     * metodo construtor
     * Cria a pagina, o formulario e a listagem
     */
    public function __construct()
    {
        parent::__construct();

        // instancia um formulario
        $this->form = new TQuickForm;
        $this->form->setFormTitle('<font color="red" size="3" face="Arial"><b>Formul&aacute;rio Gerente Regional</b></font>');

        $codigo = new THidden('id');
        $datainicio = new TDate('datainicio');
        $datafim = new TDate('datafim');
        $regional_id = new THidden('regional_id');
        $regional_id->setValue($_GET['fk']);

        // cria um rotulo para o titulo
        $obs = new TLabel('<div style="width: 200px;"> <b>* Campo obrigatorio</b></div>');
        $obs->setFontFace('Arial');
        $obs->setFontColor('red');
        $obs->setFontSize(10);

        /* ---------- cria um criterio de selecao ---------- */
        $criteria_servidor = new TCriteria;
        $criteria_servidor->add(new TFilter('situacao', '=', 'EM ATIVIDADE'), TExpression::OR_OPERATOR);
        $criteria_servidor->add(new TFilter('situacao', '=', 'A DISPOSICAO'), TExpression::OR_OPERATOR);

        $criteria3 = new TCriteria;
        if ($_SESSION['empresa_id'] == 1) {
            $criteria3->add(new TFilter('empresa_id', '=', $_SESSION['empresa_id']));
        }
        $criteria3->add($criteria_servidor);
        /* ---------- fim criacao criterio de selecao ---------- */

        /* --------- montar campo --------- */
        $servidor_id = new TDBMultiSearch('servidor_id', 'pg_ceres', 'vw_servidor_multisearchRecord', 'servidor_id', 'nome_matricula_servidor', 'nome_matricula_servidor', $criteria3);
        $servidor_id->style = "text-transform: uppercase;";
        $servidor_id->setProperty('placeholder', 'Nome ou Matricula ou CPF');
        $servidor_id->setMinLength(1);
        $servidor_id->setMaxSize(1);
        /*  --------- fim montar campo --------- */

        /* ------------------ MultiSearch Servidor ------------------ */

        try {

            if (!empty($_GET['fk'])) {
                TTransaction::open('pg_ceres');

                $object = new RegionalRecord($_GET['fk']);
                $nome_regional = new TLabel($object->nome);
                $this->form->addQuickField('Reginal', $nome_regional, 350);
                TTransaction::close();
            }
        } catch (Exception $e) {

        }

        $servidor_id->addValidation('Servidor', new TRequiredValidator);
        $datainicio->addValidation('Data Início', new TRequiredValidator);

        $datainicio->setProperty('placeholder', '01/01/2000');
        $datafim->setProperty('placeholder', '01/01/2000');

        $this->form->addQuickField('', $codigo, 0);

        $this->form->addQuickField('Servidor <font color=red><b>*</b></font>', $servidor_id, 450);
        $this->form->addQuickField('Data Início <font color=red><b>*</b></font>', $datainicio, 40);
        $this->form->addQuickField('Data Fim', $datafim, 40);
        $this->form->addQuickField("<font color=red><b>OBS: </b></font>", new TLabel("<font color=red style='font-size: 16px;'><b>Pesquisar nome do servidor em caixa alta, \"CAPS LOCK\" Habilitado</b></font>"), 600);

        $this->form->addQuickField(null, $obs, 50);

        $action = new TAction(array($this, 'onSave'));
        $action->setParameter('regional_id', $_GET['fk']);

        $this->form->addQuickAction('Salvar', $action, 'ico_save.png')->class = 'btn btn-info';
        $this->form->addQuickAction('Voltar', new TAction(array('RegionalList', 'onReload')), 'ico_datagrid.gif');

        $this->datagrid = new TDataGridTables;

        $dgnomeservidor = new TDataGridColumn('nome_servidor', 'Servidor', 'left', 1200);
        $dgdatainicior = new TDataGridColumn('datainicio', 'Início', 'left', 1200);
        $dgdatafim = new TDataGridColumn('datafim', 'Fim', 'left', 1200);

        $this->datagrid->addColumn($dgnomeservidor);
        $this->datagrid->addColumn($dgdatainicior);
        $this->datagrid->addColumn($dgdatafim);

        $action1 = new TDataGridAction(array($this, 'onEdit'));
        $action1->setLabel('Editar');
        $action1->setImage('ico_edit.png');
        $action1->setField('id');
        $action1->setFk('regional_id');

        $action2 = new TDataGridAction(array($this, 'onDelete'));
        $action2->setLabel('Deletar');
        $action2->setImage('ico_delete.png');
        $action2->setField('id');
        $action2->setFk('regional_id');

        $this->datagrid->addAction($action1);
        $this->datagrid->addAction($action2);

        $this->datagrid->createModel();

        $panel = new TPanelForm(700, 500);
        $panel->put($this->form, 0, 0);
        $panel->put($this->datagrid, 150, 200);

        parent::add($panel);
    }

    /*
     * metodo onSave()
     * Executada quando o usuario clicar no botao salvar do formulario
     */
    function onSave($param)
    {

        try {
            TTransaction::open('pg_ceres');
            $object = $this->form->getData('GerenteRegionalRecord');

            $this->form->validate();

            $object->regional_id = $param['regional_id'];
            $object->servidor_id = key($object->servidor_id); // retorna o key do array
            $object->usuarioalteracao = $_SESSION['usuario'];
            $object->dataalteracao = date("d/m/Y H:i:s");

            $object->store();

            $param = [];
            $param ['fk'] = $object->regional_id;

            new TMessage("info", "Registro salvo com sucesso!");
            TApplication::gotoPage('GerenteRegionalDetalhe', 'onReload', $param); // reload

            TTransaction::close();
        } catch (Exception $e) {
            new TMessage('error', $e->getMessage());
            TTransaction::rollback();
            $this->form->setData($this->form->getData());   // fill the form with the active record data
        }

    }

    /*
     * metodo onReload()
     * Carrega a DataGrid com os objetos do banco de dados
     */
    function onReload($param)
    {
        TTransaction::open('pg_ceres');

        $repository = new TRepository('GerenteRegionalRecord');

        $criteria = new TCriteria;

        $criteria->add(new TFilter('regional_id', '=', !empty($_GET['fk']) ? $_GET['fk'] : $param['fk']));

        $objects = $repository->load($criteria);

        $this->datagrid->clear();
        if ($objects) {
            foreach ($objects as $object) {

                $object->datainicio = \Adianti\Widget\Form\TDate::date2br($object->datainicio);
                $object->datafim = \Adianti\Widget\Form\TDate::date2br($object->datafim);
                $this->datagrid->addItem($object);
            }
        }
        TTransaction::close();
        $this->loaded = true;
    }


    /*
     * metodo onDelete()
     * Executada quando o usuario clicar no botao excluir da datagrid
     * Pergunta ao usuario se deseja realmente excluir um registro
     */
    function onDelete($param)
    {
        $key = $param['key'];

        $action1 = new TAction(array($this, 'Delete'));

        $action1->setParameter('key', $key);
        $action1->setParameter('fk', $_GET['fk']);

        new TQuestion('Deseja realmente excluir o registro ?', $action1);
    }

    /*
     * metodo Delete()
     * Exclui um registro
     */
    function Delete($param)
    {
        try {
            $key = $param['key'];
            TTransaction::open('pg_ceres');

            $object = new GerenteRegionalRecord($key);

            $object->delete();
            new TMessage("info", "Registro deletado com sucesso!");

            TTransaction::close();
        } catch (Exception $e) {
            new TMessage('error', $e->getMessage());
            TTransaction::rollback();
        }

        $this->onReload($_GET['fk']);

    }

    /*
     * metodo show()
     * Exibe a pagina
     */
    function show()
    {
        $this->onReload($_GET['fk']);
        parent::show();

    }


    /*
     * metodo onEdit()
     * Edita os dados de um registro
     */
    function onEdit($param)
    {

        try {
            if (isset($param['key'])) {

                // get the parameter $key
                $key = $param['key'];

                TTransaction::open('pg_ceres');   // open a transaction with database 'samples'

                $object = new GerenteRegionalRecord($key);        // instantiates object City

                $objServidor = new ServidorRecord($object->servidor_id);
                $object->servidor_id = [$objServidor->id => $objServidor->nome . " - " . $objServidor->matricula . " - " . $objServidor->cpf];

                $object->datainicio = \Adianti\Widget\Form\TDate::date2br($object->datainicio);
                $object->datafim = \Adianti\Widget\Form\TDate::date2br($object->datafim);


                $this->form->setData($object);   // fill the form with the active record data

                TTransaction::close();           // close the transaction

            }
        } catch (Exception $e) { // in case of exception
            new TMessage('error', '<b>Error</b> ' . $e->getMessage());
            TTransaction::rollback();
        }

    }


}