<?php
/*
 * classe RegionalForm
 * Cadastro de Regional: Contem o formularo
 * Autor: Jackson Meires
 * Data: 14/12/2016
 */

use Adianti\Database\TFilter1;

//ini_set('display_errors', 1);
//ini_set('display_startup_erros', 1);
//error_reporting(E_ALL);

class RegionalForm extends TPage
{
    private $form;     // formulario de cadastro

    /*
     * metodo construtor
     * Cria a pagina do formulario
     */
    public function __construct()
    {
        parent::__construct();

        $this->form = new TQuickForm;
        $this->form->class = 'form_empresa';
        $this->form->setFormTitle('<font color="red" size="3" face="Arial"><b>Formulário de Regional</b></font>');

        $codigo = new THidden('id');
        $nome = new TEntry('nome');
        $empresa_id = new TCombo('empresa_id');
        $cormapa = new TColor('cormapa');
        /* ---------- cria um criterio de selecao ---------- */
        $criteria_servidor = new TCriteria;
        $criteria_servidor->add(new TFilter('situacao', '=', 'EM ATIVIDADE'), TExpression::OR_OPERATOR);
        $criteria_servidor->add(new TFilter('situacao', '=', 'A DISPOSICAO'), TExpression::OR_OPERATOR);

        $criteria3 = new TCriteria;
        if ($_SESSION['empresa_id'] == 1) {
        //    $criteria3->add(new TFilter('empresa_id', '=', $_SESSION['empresa_id']));
        }
        $criteria3->add($criteria_servidor);
        /* ---------- fim criacao criterio de selecao ---------- */

        /* --------- montar campo --------- */
        $chefe_id = new TDBMultiSearch('chefe_id', 'pg_ceres', 'vw_servidor_multisearchRecord', 'servidor_id', 'nome_matricula_servidor', 'nome_matricula_servidor', $criteria3);
        $chefe_id->style = "text-transform: uppercase;";
        $chefe_id->setProperty('placeholder', 'Nome ou Matricula ou CPF');
        $chefe_id->setMinLength(1);
        $chefe_id->setMaxSize(1);

        $tutor_id = new TDBMultiSearch('tutor_id', 'pg_ceres', 'vw_servidor_multisearchRecord', 'servidor_id', 'nome_matricula_servidor', 'nome_matricula_servidor', $criteria3);
        $tutor_id->style = "text-transform: uppercase;";
        $tutor_id->setProperty('placeholder', 'Nome ou Matricula ou CPF');
        $tutor_id->setMinLength(1);
        $tutor_id->setMaxSize(1);
        /*  --------- fim montar campo --------- */

        /* ------------------ MultiSearch Servidor ------------------ */

        $usuarioalteracao = new THidden('usuarioalteracao');
        $dataalteracao = new THidden('dataalteracao');

        // cria um rotulo para o titulo
        $obs = new TLabel('<div style="width: 200px;"> <b>* Campo obrigatorio</b></div>');
        $obs->setFontFace('Arial');
        $obs->setFontColor('red');
        $obs->setFontSize(10);

        //cria a colecao da tabela estrangeira
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');
        // instancia objeto da Classe Record
        $repository = new TRepository('EmpresaRecord');
        // carrega todos os objetos
        $collection = $repository->load(new TCriteria);
        //adiciona os objetos no combo
        foreach ($collection as $object) {
            $items[$object->id] = $object->sigla;
        }

        // adiciona as opcoes na combo
        $empresa_id->addItems($items);
        //coloca o valor padrao no combo
        $empresa_id->setValue('EMATER');

        // finaliza a transacao         
        TTransaction::close();

        $nome->addValidation('Nome', new TRequiredValidator);
        $empresa_id->addValidation('Empresa', new TRequiredValidator);
        $chefe_id->addValidation('Chefe', new TRequiredValidator);
        $tutor_id->addValidation('Tutor', new TRequiredValidator);

        $this->form->addQuickField(null, $usuarioalteracao, 1);
        $this->form->addQuickField(null, $dataalteracao, 1);
        $this->form->addQuickField(null, $codigo, 10);
        $this->form->addQuickField('Nome <font color=red><b>*</b></font>', $nome, 40);
        $this->form->addQuickField('Empresa <font color=red><b>*</b></font>', $empresa_id, 20);
        $this->form->addQuickField('Cor mapa <font color=red><b>*</b></font>', $cormapa, 60);
        $this->form->addQuickField('Chefe <font color=red><b>*</b></font>', $chefe_id, 450);
        $this->form->addQuickField('Tutor <font color=red><b>*</b></font>', $tutor_id, 450);
        $this->form->addQuickField("<font color=red><b>OBS: </b></font>", new TLabel("<font color=red style='font-size: 16px;'><b>Pesquisar nome do CHEFE ou TUTOR em caixa alta, \"CAPS LOCK\" Habilitado</b></font>"), 600);

        $this->form->addQuickField(null, $obs, 50);


        $nome->setProperty('placeholder', 'Preencha o nome');
        $cormapa->setProperty('placeholder', 'Ex.: #FF0011');

        $this->form->addQuickAction('Salvar', new TAction(array($this, 'onSave')), 'ico_save.png')->class = 'btn btn-info';
        $this->form->addQuickAction('Voltar', new TAction(array('RegionalList', 'onReload')), 'ico_datagrid.gif');

        parent::add($this->form);
    }


    /*
     * metodo onSave()
     * Executada quando o usuario clicar no botao salvar do formulario
     */
    function onSave()
    {
        try {

            TTransaction::open('pg_ceres');
            $object = $this->form->getData('RegionalRecord');

            $object->chefe_id = key($object->chefe_id); // retorna o key do array
            $object->tutor_id = key($object->tutor_id); // retorna o key do array
            $object->usuarioalteracao = $_SESSION['usuario'];
            $object->dataalteracao = date("d/m/Y H:i:s");

            $this->form->validate();

            $object->store();

            new TMessage("info", "Registro salvo com sucesso!");
            TApplication::gotoPage('RegionalList', 'onReload'); // reload
            TTransaction::close();
        } catch (Exception $e) {
            new TMessage('error', $e->getMessage());
            TTransaction::rollback();
            $this->form->setData($this->form->getData());   // fill the form with the active record data
        }
    }

    /*
     * metodo onEdit()
     * Edita os dados de um registro
     */
    function onEdit($param)
    {
        try {
            if (isset($param['key'])) {

                // get the parameter $key
                $key = $param['key'];

                TTransaction::open('pg_ceres');   // open a transaction with database 'samples'

                $object = new RegionalRecord($key);        // instantiates object Regional

                $objChefe = new ServidorRecord($object->chefe_id);
                $object->chefe_id = [$objChefe->id => $objChefe->nome . " - " . $objChefe->matricula . " - " . $objChefe->cpf];

                $objTutor = new ServidorRecord($object->tutor_id);
                $object->tutor_id = [$objTutor->id => $objTutor->nome . " - " . $objTutor->matricula . " - " . $objTutor->cpf];

                $this->form->setData($object);   // fill the form with the active record data

                TTransaction::close();           // close the transaction
            }

        } catch (Exception $e) { // in case of exception
            // shows the exception error message
            new TMessage('error', '<b>Error</b> ' . $e->getMessage());
            // undo all pending operations
            TTransaction::rollback();
        }

    }

}