<?php

ini_set('display_errors', 1);
ini_set('display_startup_erros', 1);
error_reporting(E_ALL);

/**
 * UsuarioAgenteBancarioPerfilDetalhe
 *
 * @version    2.0
 * @package    ceres
 * @subpackage app/control/gu
 * @data       05/05/2017     
 * @author     Jackson Meires
 * @copyright  Copyright (c) 2006-2014 Adianti Solutions Ltd. (http://www.adianti.com.br)
 * @license    http://www.adianti.com.br/framework-license
 */
include_once 'app/lib/funcdate.php';

use Adianti\Widget\Datagrid\TDatagridTables;

class UsuarioAgenteBancarioPerfilDetalhe extends TPage {

    private $form;     // formulario de cadastro
    private $datagrid; // listagem

    /*
     * metodo construtor
     * Cria a pagina, o formulario e a listagem
     */

    public function __construct() {
        parent::__construct();

        // create the form using TQuickForm class
        $this->form = new TQuickForm;
        $this->form->setFormTitle('<font color="red" size="3" face="Arial"><b>Detalhe dos Perfis de Usuarios Agentes</b></font>');
        // cria os campos do formulario
        $codigo = new THidden('id');
        $usuario_id = new THidden('usuario_id');
        $usuario_id->setValue(filter_input(INPUT_GET, 'fk'));
        $perfil_id = new TSelect('perfil_id');

        $usuarioalteracao = new THidden('usuarioalteracao');
        $dataalteracao = new THidden('dataalteracao');

        //cria a colecao da tabela estrangeira
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');
        // instancia objeto da Classe Record
        $repository = new TRepository('PerfilRecord');
        // cria um criterio de selecao, ordenado pelo id
        $criteria = new TCriteria;
        $criteria->setProperty('order', 'nome');
        // carrega os objetos de acordo com o criterio
        $cadastros = $repository->load($criteria);

        //adiciona os objetos no combo
        foreach ($cadastros as $object) {
            $items[$object->id] = $object->nome . ' / ' . $object->nome_modulo;
        }

        // adiciona as opcoes na combo
        $perfil_id->addItems($items);

        // finaliza a transacao
        TTransaction::close();

        //cria a colecao da tabela estrangeira do Cabecalho
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');
        // instancia objeto da Classe Record
        $objUsuario = new vw_usuario_agentebancarioRecord(filter_input(INPUT_GET, 'fk'));
        // cria um criterio de selecao, ordenado pelo id do usuario
        //adiciona os objetos na tela com as informacoes do usuario
        if ($objUsuario) {
            $nome = new TLabel($objUsuario->nome_agentebancario);
            $login = new TLabel($objUsuario->login);
        }
        // finaliza a transacao
        TTransaction::close();

        // add the fields inside the form
        $this->form->addQuickField(null, $codigo, 10);
        $this->form->addQuickField(null, $usuario_id, 10);
        $this->form->addQuickField(null, $usuarioalteracao, 10);
        $this->form->addQuickField(null, $dataalteracao, 10);
        $this->form->addQuickField("Usuario", $nome, 400);
        $this->form->addQuickField("Login", $login, 200);
        $this->form->addQuickField('Perfil', $perfil_id, 40);

        $perfil_id->setSize(450, 100);

        // define duas acoes
        $actionSave = new TAction(array($this, 'onSave'));
        $actionSave->setParameter('fk', filter_input(INPUT_GET, 'fk'));

        // define the form action 
        $this->form->addQuickAction('Salvar', $actionSave, 'ico_save.png')->class = 'btn btn-info';
        $this->form->addQuickAction('Voltar', new TAction(array('UsuarioAgenteBancarioList', 'onReload')), 'ico_datagrid.gif');

        // instancia objeto DataGrid
        $this->datagrid = new TDatagridTables;

        // instancia as colunas da DataGrid
        $dgnome_perfil = new TDataGridColumn('nome_perfil', 'Perfil', 'left', 1200);
        $dgnome_perfil_modulo = new TDataGridColumn('nome_perfil_modulo', 'Módulo', 'left', 1200);

        // adiciona as colunas a DataGrid
        $this->datagrid->addColumn($dgnome_perfil);
        $this->datagrid->addColumn($dgnome_perfil_modulo);

        // instancia duas acoes da DataGrid
        $action1 = new TDataGridAction(array($this, 'onEdit'));
        $action1->setLabel('Editar');
        $action1->setImage('ico_edit.png');
        $action1->setField('id');
        $action1->setFk('usuario_id');

        $action2 = new TDataGridAction(array($this, 'onDelete'));
        $action2->setLabel('Deletar');
        $action2->setImage('ico_delete.png');
        $action2->setField('id');
        $action2->setFk('usuario_id');

        // adiciona as acoes a DataGrid
        $this->datagrid->addAction($action1);
        $this->datagrid->addAction($action2);

        // cria o modelo da DataGrid, montando sua estrutura
        $this->datagrid->createModel();

        // monta a paina atraves de uma tabela
        $panel = new TPanelForm(700, 500);
        $panel->put($this->form, 0, 0);
        $panel->put($this->datagrid, 150, 170);


        // adiciona a tabela a pagina
        parent::add($panel);
    }

    /*
     * metodo onReload()
     * Carrega a DataGrid com os objetos do banco de dados
     */

    function onReload() {
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');

        // instancia um repositorio da Classe
        $repository = new TRepository('UsuarioPerfilRecord');

        // cria um criterio de selecao
        $criteria = new TCriteria;

        //filtra pelo campo selecionado pelo usuario
        $criteria->add(new TFilter('usuario_id', '=', filter_input(INPUT_GET, 'fk')));

        // carrega os objetos de acordo com o criterio
        $objects = $repository->load($criteria);

        $this->datagrid->clear();
        if ($objects) {
            // percorre os objetos retornados
            foreach ($objects as $object) {
                // adiciona o objeto na DataGrid
                $this->datagrid->addItem($object);
            }
        }
        // finaliza a transacao
        TTransaction::close();
        $this->loaded = true;
    }

    /*
     * metodo onDelete()
     * Executada quando o usuario clicar no botao excluir da datagrid
     * Pergunta ao usuario se deseja realmente excluir um registro
     */

    function onDelete($param) {
        // obtem o parametro $key
        $key = $param['key'];

        // define duas acoes
        $action1 = new TAction(array($this, 'Delete'));
        $action2 = new TAction(array($this, 'onReload'));

        // define os parametros de cada acao
        $action1->setParameter('key', $key);
        $action2->setParameter('key', $key);

        //encaminha a chave estrangeira
        $action1->setParameter('fk', filter_input(INPUT_GET, 'fk'));
        $action2->setParameter('fk', filter_input(INPUT_GET, 'fk'));
        // exibe um dialogo ao usuario
        new TQuestion('Deseja realmente excluir o registro ?', $action1, $action2);
    }

    /*
     * metodo Delete()
     * Exclui um registro
     */

    function Delete($param) {
        // obtem o parametro $key
        $key = $param['key'];
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');

        // instanicia objeto Record
        $cadastro = new UsuarioPerfilRecord($key);

        try {
            // deleta objeto do banco de dados
            $cadastro->delete();

            // finaliza a transacao
            TTransaction::close();
        } catch (Exception $e) { // em caso de exceção
            // exibe a mensagem gerada pela exceção
            new TMessage('error', $e->getMessage());
            // desfaz todas alterações no banco de dados
            TTransaction::rollback();
        }

        // re-carrega a datagrid
        $this->onReload();
        // exibe mensagem de sucesso
        //new TMessage('info', "Registro Excluido com sucesso");
    }

    /*
     * metodo show()
     * Exibe a pagina
     */

    function show() {
        // carrega os dados no datagrid
        $this->onReload();
        //chama o metodo show da super classe
        parent::show();
    }

    /*
     * metodo onSave()
     * Executada quando o usuario clicar no botao salvar do formulario
     */

    function onSave($param) {
        try {
            // inicia transacao com o banco 'pg_ceres'
            TTransaction::open('pg_ceres');
            // obtem os dados no formulario em um objeto Record da Classe
            $object = $this->form->getData('UsuarioPerfilRecord');

            //lanca o default
            $object->usuarioalteracao = $_SESSION['usuario'];
            $object->dataalteracao = date("d/m/Y H:i:s");

            // form validation
            $this->form->validate();

            foreach ($object->perfil_id as $item) {

                $objUP = new UsuarioPerfilRecord(!empty($object->id) ? $object->id : null);
                $objUP->perfil_id = $item;
                $objUP->usuario_id = $param['fk'];
                $objUP->usuarioalteracao = $_SESSION['usuario'];
                $objUP->dataalteracao = date("d/m/Y H:i:s");

                $objUP->store(); // save the object
            }

            // exibe um dialogo ao usuario
            $param = [];
            $param['fk'] = $object->usuario_id;

            TApplication::gotoPage('UsuarioAgenteBancarioPerfilDetalhe', 'onReload', $param); // reload
            // finaliza a transacao
            TTransaction::close();
        } catch (Exception $e) { // em caso de exceção
            // exibe a mensagem gerada pela exceção
            new TMessage('error', $e->getMessage());
            // desfaz todas alterações no banco de dados
            TTransaction::rollback();
        }
    }

    /*
     * metodo onEdit()
     * Edita os dados de um registro
     */

    function onEdit($param) {
        try {
            if (isset($param['key'])) {

                // get the parameter $key
                $key = $param['key'];

                TTransaction::open('pg_ceres');   // open a transaction with database 'samples'

                $object = new UsuarioPerfilRecord($key);        // instantiates object City

                $this->form->setData($object);   // fill the form with the active record data

                TTransaction::close();           // close the transaction
            } else {
                $this->form->clear();
            }
        } catch (Exception $e) { // in case of exception
            // shows the exception error message
            new TMessage('error', '<b>Error</b> ' . $e->getMessage());
            // undo all pending operations
            TTransaction::rollback();
        }
    }

}